import logging

from examples import Tension_Escande_navier_D2Q9_Ansumali
from examples import wave_in_beam_vec_Ansumali
from examples import dynamic_crack_mixed_D2Q9_Ansumali
from examples import dynamic_TRT_vectorised_Ansumali


if __name__ == "__main__":
    logging.basicConfig(
        filename="test.log",
        format="%(levelname)s -- %(filename)s (%(lineno)s) -- %(message)s",
        filemode="w",
        level=logging.WARNING,
    )

    Tension_Escande_navier_D2Q9_Ansumali()
    wave_in_beam_vec_Ansumali()
    dynamic_TRT_vectorised_Ansumali()
    dynamic_crack_mixed_D2Q9_Ansumali()
    print("moment chain tests completed")

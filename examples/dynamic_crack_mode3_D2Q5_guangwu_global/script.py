import logging
from pathlib import Path

from SolidLBM.mesher import Mesh
from SolidLBM.solver import Computation

# FIXME: problem with relative import when running script directly
try:
    from .post_eval import post_process_data
except ImportError:
    from post_eval import post_process_data


name = "strip"
working_dir = Path(__file__).parent.absolute()


def create_lattice(h: float = 2**-4):
    seed_point = (1 + h / 2.0, h / 2.0, 0.0)

    mesh = Mesh(
        name=name,
        working_directory=working_dir,
        cell_size=h,
        seed_point=seed_point,
    )

    mesh.create_mesh_neighbor_points(verbose=False)
    mesh.print_to_file()
    mesh.tmp_print_initial_conditions_to_file()
    mesh.compute_cell_volumes_areas_boundary_names_at_boundary_points()
    mesh.print_alt_bc_to_file()
    mesh.plot_mesh(dpi=300)


def run_test(*, run_mesher=True):
    print("Dynamic Crack (mode III) - Guangwu - D2Q5")

    if run_mesher:
        create_lattice()

    comp = Computation(working_dir, name=name)
    comp.execute_command_sequence()

    # check SIF as test for crack propagation
    median = post_process_data(name, path=working_dir)
    median_ref = 0.0208332
    e_rel = 1 - median / median_ref

    print(f"  median of K: {median}")
    print(f"  comparison:  {median_ref}")
    if abs(e_rel) < 1e-3:
        print("  >> TEST PASSED <<")
    else:
        print(f"  << TEST FAILED >>    rel. error: |{e_rel:.2e}| > 1.00e-3")

    # check displacement at AuxPoint
    aux_1, aux_2 = [pt for pt in comp.Lattice.AuxPoints if pt.ID in [2050, 2051]]

    w = aux_1.w[0]
    w_ref = 0.006945570191
    e_rel = 1 - w / w_ref

    print("")
    print(f"  w at AuxPoint {aux_1.ID}: {w:+1.12f}")
    print(f"  expected at 2050:   ±{w_ref:1.12f}")

    if abs(e_rel) < 1e-3:
        print("  >> TEST PASSED <<")
    else:
        print("  << TEST FAILED >>")


if __name__ == "__main__":
    logging.basicConfig(
        filename=working_dir / f"{name}.log",
        format="%(levelname)s -- %(filename)s (%(lineno)s) -- %(message)s",
        filemode="w",
        level=logging.INFO,
    )

    run_test(run_mesher=False)

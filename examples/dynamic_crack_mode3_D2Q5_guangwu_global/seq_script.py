import logging

from SolidLBM.util.geometry.Point import Point
from SolidLBM.mesher.mesh.Mesh import Mesh
from SolidLBM.solver.solution.Computation import Computation

from post_eval import *

name = 'strip'
h = 2**-4

seed_point = Point(1 + h/2.0, h/2.0, 0.0)

logging.basicConfig(
        filename = name + '.log',
        encoding = 'utf-8',
        format = '%(levelname)s -- %(filename)s (%(lineno)s) -- %(message)s',
        filemode = 'w',
        level = logging.INFO,
    )

mesh = Mesh(
    name=name,
    working_directory='./',
    cell_size=h,
    seed_point=seed_point,
)

mesh.create_mesh_neighbor_points(verbose=True)
mesh.print_to_file()
mesh.tmp_print_initial_conditions_to_file()
mesh.compute_cell_volumes_areas_boundary_names_at_boundary_points()
mesh.print_alt_bc_to_file()
mesh.plot_mesh(dpi=300)

comp = Computation(working_directory='./', name=name)
# comp.execute_command_sequence()
latt = comp.Lattice

''' COMMAND SEQUENCE '''        
comp.command_init()
while comp.current_time <= comp.Parameters['max_time']:
#    comp.command_vtk()
    comp.command_output_crack()
    for idx in range(12):
        comp.command_equi()
        comp.command_colli()
        comp.command_stre()
        comp.command_update_bc()
        comp.command_boun()
        comp.command_update_distribution_functions()
        comp.command_integrate()
        comp.command_dyn_crack()
        comp.command_time()
print('')

# check SIF as test for crack propagation
""" === evaluation of data === """
median = post_process_data(name)

print("  comp:   0.0208332")
print(f"  median: {median}")
if (abs(median - 0.0208332) <= 1.0e-8):
    print("  >> TEST PASSED <<")
else:
    print("  << TEST FAILED >>")

# check displacement at AuxPoint
aux_1, aux_2 = [pt for pt in comp.Lattice.AuxPoints if pt.ID in [2050, 2051]]
print('')
print(f'  w at AuxPoint {aux_1.ID}: {aux_1.w[0]:+1.12f}')
print('  expected at 2050:   ±0.006945570191')

if abs(0.006945570190) - abs(aux_1.w[0]) <= 1.0e-10 and abs(aux_1.w[0] + aux_2.w[0]) <= 1.0e-10:
    print("  >> TEST PASSED <<")
else:
    print("  << TEST FAILED >>")

import logging
from pathlib import Path

import meshio
from scipy.interpolate import griddata
import numpy as np

import SolidLBM.mesher as mesher
import SolidLBM.parsolver as solver


name = "beam"
working_dir = Path(__file__).parent.absolute()


def create_lattice(h: float = 2**-5, name: str = name) -> None:
    seed_point = (h / 2, h / 2, 0.0)

    mesh4 = mesher.Mesh(
        name=name,
        working_directory=Path(__file__).parent.absolute(),
        cell_size=h,
        seed_point=seed_point,
        mesh_type="D2Q9",
    )

    mesh4.create_mesh_neighbor_points(verbose=False)
    mesh4.compute_cell_volumes_areas_boundary_names_at_boundary_points()
    mesh4.print_to_file()
    mesh4.print_alt_bc_to_file()


def run_test(name: str = name, *, run_mesher=True) -> None:
    global error

    def _get_error(disp_lb, disp_ref) -> float:
        """Compute the relative error"""
        # compute the L2-derivation
        diff = np.abs(disp_lb - disp_ref)
        diff_magnitude = np.einsum("ij,ij", diff, diff)

        # normalise for relative L2-error
        norm = np.einsum("ij,ij", disp_ref, disp_ref)
        err = np.sqrt(diff_magnitude) / norm

        return err

    print("moment chain - vectorised (CPU) - TRT", end="")

    if run_mesher:
        create_lattice()

    comp = solver.Computation(
        working_dir=working_dir,
        name=name,
        model="Ansumali_TRT",
        verbose=False,
    )

    comp.setup_output(["w"], 0.25)

    tmax = comp.Lattice.par["max_time"]
    dt = comp.Lattice.par["dth"]
    print(f" - τ+: {comp.Lattice.par['tau+']:.4f}, τ-: {comp.Lattice.par['tau-']:.4f}")

    fe_ref = get_fe_ref(name)
    k = len(fe_ref)

    idx = 1
    error = []
    while comp.time_condition():
        comp.collision_and_streaming()
        comp.boundary_handling()
        comp.distribution_update()
        comp.integration()
        comp.output_at_interval()

        # get error at intervals
        if comp.current_time >= (tmax - 0.5 * dt) * idx / k:
            disp_lb = comp.Lattice.Aw
            err = _get_error(disp_lb, fe_ref[idx - 1])
            error.append(err)
            idx += 1

        comp.time_update()

    print("  Compare mean error to FE:")
    comparison = np.allclose(np.mean(error), 0.37035, rtol=1e-3)
    if comparison:
        print("  >> TEST PASSED <<")
    else:
        print("  << TEST FAILED >>")

    ### Test vtu output
    print("  Compare cumulated error to FE:")
    comparison = sum(error) < 5.6
    if comparison:
        print("  >> TEST PASSED <<")
    else:
        print("  << TEST FAILED >>")


def get_fe_ref(name: str = name) -> list:
    """Get the reference FE data"""
    nodes = np.loadtxt(working_dir / f"{name}.msh", usecols=(1, 2))
    fe_ref = [interpolate_ref(nodes, name, k) for k in range(1, 16)]
    return fe_ref


def interpolate_ref(nodes, model: str, k: int):
    """Get the interpolation of FE data at lattice nodes"""

    def _get_ref_data(path: Path, k: int, name: str = name):
        """Load the FE data from disk"""

        assert path.is_dir(), f"Path {path} is not a directory."

        reader = meshio.xdmf.TimeSeriesReader(path / f"{name}_fe.xdmf")
        pts, _ = reader.read_points_cells()
        data = reader.read_data(k)

        return pts, data[1]["u"]

    # load FE data
    pts_fe, disp_fe = _get_ref_data(working_dir / "fe_ref", k, model)

    # interpolate FE data at lattice nodes
    disp_ref_x = griddata(pts_fe[:, :2], disp_fe[:, 0], nodes[:, :2], method="linear")
    disp_ref_y = griddata(pts_fe[:, :2], disp_fe[:, 1], nodes[:, :2], method="linear")
    disp_ref = np.stack((disp_ref_x, disp_ref_y), axis=1)
    return disp_ref


if __name__ == "__main__":
    logging.basicConfig(
        filename=working_dir / f"{name}.log",
        format="%(levelname)s -- %(filename)s (%(lineno)s) -- %(message)s",
        filemode="w",
        level=logging.DEBUG,
    )

    run_test(run_mesher=True)

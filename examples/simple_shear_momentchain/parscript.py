import SolidLBM.parsolver as solver


working_dir = "./"

print("initialising computation ...")
comp = solver.Computation(
    working_dir,
    "block",
    "Ansumali_SRT",
    # 'Ansumali_TRT',
)

comp.setup_output(
    quantities=["w", "j", "rho", "sigma"],
    time_interval=0.1,
    output_dir="output_par/",
)

print("running computation ...", end="")

while comp.time_condition():
    comp.collision_and_streaming()
    comp.boundary_handling()
    comp.distribution_update()
    comp.integration()
    comp.output_at_interval()
    comp.time_update()

print("\n... finished.")

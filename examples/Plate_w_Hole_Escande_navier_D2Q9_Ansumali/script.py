import SolidLBM.util.geometry.Point as Point_Module
import SolidLBM.mesher.mesh.Mesh as Mesh_Module
import SolidLBM.solver.solution.Computation as Computation_Module
import SolidLBM.mesher.mesh.InitialCondition as Condition_Module

#h = 0.25
h = 0.0125 # 0.5
seed_point = Point_Module.Point(-0.25-h/2, h/2, 0.0)
#seed_point = Point_Module.Point(0.0, 0.0, 0.0)

mesh4 = Mesh_Module.Mesh(name='block', working_directory='./', cell_size=h, seed_point=seed_point,mesh_type = "D2Q9")
mesh4.create_mesh_neighbor_points()
mesh4.compute_cell_volumes_areas_boundary_names_at_boundary_points()
mesh4.plot_mesh()
mesh4.print_to_file()
mesh4.print_alt_bc_to_file()
mesh4.tmp_print_initial_conditions_to_file()


computation4 = Computation_Module.Computation(working_directory='./', name='block')
computation4.execute_command_sequence()

#print("Navier Equation - D2Q9 - Ansumali - Block Tension - Not Corrected")
#print("Displacement at top right corner = " + str(computation4.Lattice.Points[396].w[1]))
#print("Expected displacement: 8.677190622581437e-05")
#if (abs(computation4.Lattice.Points[396].w[1] - 8.677190622581437e-05) <= 1.0e-16):
#    print("TEST PASSED")
#else:
#    print("TEST FAILED")

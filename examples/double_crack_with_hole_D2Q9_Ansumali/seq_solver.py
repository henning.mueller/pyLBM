import SolidLBM.solver.solution.Computation as Computation_Module


n = 512
m = 1

name='doublecrack'
comp = Computation_Module.Computation(
    working_directory = './',
    name = name,
    verbose = True,
)

latt = comp.Lattice

i = 0
comp.command_init()
# """
while comp.current_time <= comp.Parameters['max_time']:
    print(f"    time {comp.current_time: >4f} / {comp.Parameters['max_time']: >4f},                iteration {i: >3d}", end='\r')

    comp.command_equi()
    comp.command_colli()
    comp.command_stre()
    comp.command_update_bc()
    comp.command_boun()
    comp.command_update_distribution_functions()
    comp.command_integrate()
    comp.command_dyn_crack()
    comp.command_time()
    if i % m == 0:
        comp.command_vtk()
        comp.command_output_crack()

    i += 1
    
print('\n... done')
# """

# import crackeval.combi.crackpath as cpath
# import crackeval.utils as cutils

# cfig = cpath.plot()
# cutils.save_plot(cfig, 'cpath')


# import crackeval.combi.jvec as jvec

# jfig = jvec.plot_g(steps=True, tip=(1, 2))
# cutils.save_plot(jfig, 'g-v-a')

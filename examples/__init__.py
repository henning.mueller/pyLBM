from .circle_wave_D2Q5_guangwu.script import run_test as circle_wave_D2Q5_guangwu
from .circle_wave_D2Q9_chopard.script import run_test as circle_wave_D2Q9_chopard
from .block_tension_navier_D2Q5_not_corrected.script import run_test as block_tension_navier_D2Q5_not_corrected
from .circle_tension_navier_D2Q5_corrected.script import run_test as circle_tension_navier_D2Q5_corrected
from .block_shear_navier_D2Q5_corrected.script import run_test as block_shear_navier_D2Q5_corrected
from .Tension_Escande_navier_D2Q9_Ansumali.script import run_test as Tension_Escande_navier_D2Q9_Ansumali
from .wave_in_beam_vec_Ansumali.script import run_test as wave_in_beam_vec_Ansumali
from .stationary_crack_D2Q5_guangwu_local.script import run_test as stationary_crack_D2Q5_guangwu_local
from .dynamic_crack_mode3_D2Q5_guangwu_global.script import run_test as dynamic_crack_mode3_D2Q5_guangwu_global
from .dynamic_crack_mixed_D2Q9_Ansumali.script import run_test as dynamic_crack_mixed_D2Q9_Ansumali
from .dynamic_TRT_vectorised_Ansumali.script import run_test as dynamic_TRT_vectorised_Ansumali


__all__ = [
    circle_wave_D2Q5_guangwu,
    circle_wave_D2Q9_chopard,
    block_tension_navier_D2Q5_not_corrected,
    circle_tension_navier_D2Q5_corrected,
    block_shear_navier_D2Q5_corrected,
    Tension_Escande_navier_D2Q9_Ansumali,
    wave_in_beam_vec_Ansumali,
    stationary_crack_D2Q5_guangwu_local,
    dynamic_crack_mode3_D2Q5_guangwu_global,
    dynamic_crack_mixed_D2Q9_Ansumali,
    dynamic_TRT_vectorised_Ansumali,
]

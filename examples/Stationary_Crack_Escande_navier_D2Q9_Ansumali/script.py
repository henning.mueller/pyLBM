from SolidLBM.mesher import Mesh
from SolidLBM.solver import Computation

h = 0.05
seed_point = (1-h/2, 1-h/2, 0.0)

mesh4 = Mesh(
    name="block",
    working_directory="./",
    cell_size=h,
    seed_point=seed_point,
    mesh_type="D2Q9",
)
mesh4.create_mesh_neighbor_points(verbose=True)
mesh4.compute_cell_volumes_areas_boundary_names_at_boundary_points()
mesh4.plot_mesh()
mesh4.print_to_file()
mesh4.print_alt_bc_to_file()
mesh4.tmp_print_initial_conditions_to_file()

computation4 = Computation(
    working_directory="./",
    name="block",
    verbose=True,
)
computation4.setup_output(quantities=["w", "j", "sigma"])
computation4.execute_command_sequence()

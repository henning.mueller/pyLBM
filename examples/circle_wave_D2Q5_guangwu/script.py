import logging

from SolidLBM.mesher import Mesh
from SolidLBM.solver import Computation
from pathlib import Path


name = 'disc_with_hole'
working_dir = Path(__file__).parent.absolute()


def run_test():
    h = 0.025
    seed_point = (0.25, 0.0,0.0)

    print("Guangwu Wave Equation - D2Q5 - Mesoscopic Boundary condition")
    mesh2 = Mesh(
            name=name,
            working_directory=working_dir,
            cell_size=h,
            seed_point=seed_point,
            mesh_type='D2Q5'
        )

    mesh2.create_mesh_neighbor_points()
    mesh2.compute_cell_volumes_areas_boundary_names_at_boundary_points()
    mesh2.print_alt_bc_to_file()
    mesh2.plot_mesh()
    mesh2.print_to_file()

    computation2 = Computation(working_directory=working_dir, name=name)
    computation2.execute_command_sequence()

    w = computation2.Lattice.Points[1590].w[0]
    w_ref = 0.0007515263813366964
    e_rel = 1 - w / w_ref

    print(f"  Displacement at top left corner: {w}")
    print(f"  Expected displacement:           {w_ref}")
    if abs(e_rel) < 1e-3:
        print("  >> TEST PASSED <<")
    else:
        print(f"  << TEST FAILED >>    rel. error: |{e_rel:.2e}| > 1.00e-3")


if __name__ == "__main__":
    logging.basicConfig(
        filename = working_dir / f"{name}.log",
        format = '%(levelname)s -- %(filename)s (%(lineno)s) -- %(message)s',
        filemode = 'w',
        level = logging.INFO,
    )
    run_test()

"""reads data of crack tip from HDF5 file 
"""

import pathlib
from collections.abc import Iterable

import matplotlib as mpl
import matplotlib.pyplot as plt

import tables as tb
import numpy as np


Figure = plt.Figure
Array = np.ndarray


def post_process_data(name: str,
                      l: float = 1.0, w0: float = 0.1, tf: float = 5) -> float:
    """combined post-processing for the example:
        - plot data,
        - compute the theoretical value,
        - compute the median of the numerical data,
        - save the plot.
    """
    fig_sif = plot_sif(name)
    _, fig_sif = add_mandal_orig(fig_sif, name, w0*2, l)
    median, fig_sif = add_stat_median(fig_sif, name, tf=tf)
    save_plot(fig_sif, './sif_results', dpi=300)
    return median


def _find_file(name: str = '', suffix: str = 'h5') -> pathlib.Path:
    filename = name + '*.' + suffix
    filepath = next(pathlib.Path('.').rglob(filename))
    return filepath


def _read_data_h5(
        name: str = '',
        path: [pathlib.Path, str] = None,
        tip: int = 1,
    ) -> dict[Array]:
    """read data from HDF5 file, written with pytables
    load data of crack tip in table from group and arrange as dict
    """

    if not path:
        path: pathlib.Path = _find_file(name=name, suffix='h5')

    with tb.open_file(path, 'r') as df:
        crack_tb = df.root.cracks
        t = crack_tb.col('time')
        a = crack_tb.col('length')

        tip: str = 'tip_' + str(tip)        
        tip_grp = df.root.__getattr__(tip)
        tip_tb = tip_grp.crack_tip
        k = tip_tb.col('sif')[()]
        v = tip_tb.col('speed')[()]
        pos = tip_tb.col('position')[()]

    data = {
        'k': k,
        'v': v,
        'position': pos,
        't': t,
        'a': a,
    }
    return data


def _read_parameters_h5(
        name: str = '',
        path: [pathlib.Path, str] = None,
        idx: int = 1,
        ) -> dict:
    """load parameters from attributes in HDF5 file"""

    if not path:
        path: pathlib.Path = _find_file(name=name, suffix='h5')

    par_keys = ('c_d', 'c_s', 'v_max', 'criterion', 'critical_value', 'd_min', 'delta_h', 'delta_t', 'domain_shape')
    params = dict()
    with tb.open_file(path, 'r') as df:
        tip: str = 'tip_' + str(idx)
        tip_grp = df.root.__getattr__(tip)
        for k in par_keys:
            if k in df.root.cracks.attrs:
                params[k] = df.root.cracks.attrs.__getattr__(k)
    params['d_max'] = max(params.get('domain_shape', (np.nan, np.nan)))
    return params


def _split_time(t_in, t_len) -> tuple[float, int]:
    """get time interval of length t_len from the end"""
    t0 = t_in[-1] - t_len
    n = np.where(t_in > t0)
    t = t_in[n]
    return t, n


def _line_plot(fig: plt.Figure, y: float) -> Figure:
    """plot a straight line parallel to the x-axis"""
    ax = fig.axes[0]
    x = ax.get_xlim()
    ax.plot(
        x,
        [y, y],
        linestyle='--',
        c='gray',
    )
    return fig


def plot_sif(
        name: str = '', 
        norm: float = 1.0,
        tip: [int, tuple[int]] = 1, 
        mode: [int, tuple[int]] = 3,
    ) -> Figure:
    """plot stress intensity factor (SIF) and crack length over time
    may also apply filter to SIF for smoothing
    parameters: name <- string, for file names
                plot_a <- boolean, plot length over time or not
                tip <- index of crack tip
                mode <- 1, 2, 3 for crack modes I, II, III
    """

    if not hasattr(tip, '__len__'):
        tip = tip,
    if not hasattr(mode, '__len__'):
        mode = mode,
    
    sif = [None] * len(tip)
    for idx, itip in enumerate(tip):
        crack_data = _read_data_h5(name, tip=itip)
        sif[idx] = crack_data['k']
    t = crack_data['t']
    
    fig, ax = plt.subplots()
    colors = iter(('tab:blue', 'tab:green', 'tab:olive'))

    for idx, itip in enumerate(tip):
        for j in mode:                                  # mode: 1, 2 or 3
            k_tip = 'K' + str(j) + '_' + str(itip)
            k = sif[idx][...,j-1] / norm                # index j must start at 0
            color = next(colors)
            
            ax.plot(
                t, k, 
                '+',
                markersize=4,
                c=color,
                label=k_tip,
            )

    ax.set_xlabel(r'$t \, \frac{L}{c_s}$')
    if norm == 1.0:
        ax.set_ylabel('SIF K')
    else:
        ax.set_ylabel('K / K_c')
    fig.set_tight_layout(True)
    return fig


def save_plot(fig: plt.Figure, path: str, dpi: int = 300, *, tex=False):
    """Saves a plot to disk
    The default is a .png file.
    If tex is set to True a .pgf is generated. This requires XeLatex to be installed.
    """
    if not tex:
        path += '.png'
    else:               # save as .pgf for LaTeX documents
        mpl.use("pgf")
        mpl.rcParams.update({
            # 'pgf.texsystem': 'pdflatex',  # not working with pdflatex due to bug in matplotlib
            'pgf.texsystem': 'xelatex',
            'font.family': 'serif',
            'font.size': 10,
            'text.usetex': True,
            'pgf.rcfonts': False,
            "pgf.preamble": "\n".join([
                r'\usepackage{amssymb,amsmath,amsfonts}',
                r'\usepackage{mathtools}',
                r'\usepackage{nicefrac}',
            ]),
        })
        path += '.pgf'
    fig.savefig(path, dpi=dpi)


def sif_mandal_orig(w_0: float, v_crk: float, c_s: float,
                    height: float, mu: float = 1.0) -> float:
    """compute the analytical value for a dynamical crack in a long, narrow strip
    (Mandal 2003)
    """
    beta = np.sqrt(1 - (v_crk / c_s) ** 2)      # is v_crk relative or absolute?
    k = mu * w_0 * np.sqrt(2 * beta / (height * (2 * height * beta + 1)))

    # print("mandal original:", k)
    return k


def add_stat_median(fig: plt.Figure, name: str,
                     color: str = 'tab:olive', tf: float = 15) -> tuple[float, Figure]:
    """statistical evaluation of SIF: median and 25% percentile"""

    crack_data = _read_data_h5(name, tip=1)
    sif = crack_data['k'][...,2]
    time = crack_data['t']

    t, n = _split_time(time, tf)
    k_data = sif[n]

    k_m = np.median(k_data)
    per25, per75 = np.percentile(k_data, [25, 75])

    ax = fig.axes[0]
    ax.plot(t, k_m * np.ones_like(t), linestyle='-', c=color)
    ax.plot(t, per75 * np.ones_like(t), linestyle=':', c=color)
    ax.plot(t, per25 * np.ones_like(t), linestyle=':', c=color)

    # print("median:", k_m, "25%:", k_m - per25, "75%:", per75 - k_m)
    return k_m, fig


def add_mandal_orig(
        fig: plt.Figure,
        name: str,
        w_0: float,
        height: float,
        mu: float = 1.0,
    ) -> tuple[float, Figure]:
    """add line with analytical value to figure - original problem
    (Mandal 2020)
    """
    params = _read_parameters_h5(name=name)
    sif = sif_mandal_orig(w_0, params['v_max'], params['c_s'], height, mu=mu)
    _line_plot(fig, sif)
    return sif, fig

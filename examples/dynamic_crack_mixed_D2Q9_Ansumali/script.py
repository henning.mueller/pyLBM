from pathlib import Path
import logging

from SolidLBM.mesher import Mesh
from SolidLBM.solver import Computation


name = "strip"
working_dir = Path(__file__).parent.absolute()


def create_lattice(h: float = 2**-5, name: str = name) -> None:
    seed_point = (h / 2, h / 2, 0.0)

    mesh = Mesh(
        name=name,
        working_directory=working_dir,
        cell_size=h,
        seed_point=seed_point,
        mesh_type="D2Q9",
    )

    mesh.create_mesh_neighbor_points(verbose=False)
    mesh.print_to_file()
    mesh.compute_cell_volumes_areas_boundary_names_at_boundary_points()
    mesh.print_alt_bc_to_file()


def run_test(*, run_mesher=True, print_progress=False) -> None:
    print("moment chain - point-wise - dynamic crack mixed mode")

    if run_mesher:
        create_lattice()

    comp = Computation(working_dir, name=name, verbose=print_progress)
    comp.command_init()
    comp.setup_output(quantities=["w", "j", "sigma"])

    tmax = comp.Parameters["max_time"]
    i = 0
    while comp.current_time <= tmax:
        # if print_progress:
        #     print(
        #         f"time {comp.current_time: >4f} / {tmax: >4f} -",
        #         f"iteration {i: >3d}",
        #         end="\r",
        #     )

        comp.command_equi()
        comp.command_colli()
        comp.command_stre()
        comp.command_update_bc()
        comp.command_boun()
        comp.command_update_distribution_functions()
        comp.command_integrate()
        comp.command_dyn_crack()
        comp.command_time()
        comp.write_output()
        i += 1

    xr, yr = 0.136079, -0.197229
    tol = 1e-6

    (ct,) = comp.Crackset.cracktips
    xc, yc, _ = ct.crack_tip.coords()

    print(f"  expected crack tip position: ({xr}, {yr})")
    print(f"  actual position:             ({xc:.6f}, {yc:.6f})")
    if abs(xr - xc) < tol and abs(yr - yc) < tol:
        print("  >> TEST PASSED <<")
    else:
        print("  << TEST FAILED >>")


if __name__ == "__main__":
    logging.basicConfig(
        filename=working_dir / f"{name}.log",
        format="%(levelname)s -- %(filename)s (%(lineno)s) -- %(message)s",
        filemode="w",
        level=logging.WARNING,
    )

    run_test(run_mesher=False, print_progress=True)

from SolidLBM.mesher import Mesh


working_dir = './'

# spacing = 0.0500
# spacing = 0.0250
spacing = 2**-6

seed_point = (spacing/2, spacing/2, 0.0)

print("creating lattice...")
mesh = Mesh(
    name='block',
    working_directory=working_dir,
    cell_size=spacing,
    seed_point=seed_point,
    mesh_type = "D2Q9"
)

mesh.create_mesh_neighbor_points(verbose=True)
mesh.compute_cell_volumes_areas_boundary_names_at_boundary_points()
# mesh4.plot_mesh()
mesh.print_to_file()
mesh.print_alt_bc_to_file()
mesh.tmp_print_initial_conditions_to_file()

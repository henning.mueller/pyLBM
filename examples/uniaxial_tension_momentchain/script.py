from SolidLBM.solver.solution.Computation import Computation


working_dir = "./"

print("initialising computation ...")
comp = Computation(
    name="block",
    working_directory=working_dir,
    output_path="output_ser/",
    verbose=True,
)

comp.command_init()
comp.setup_output(
    quantities=["w", "j", "rho", "sigma"],
    time_interval=0.1,
)

print("running computation ...", end="")
params = comp.Parameters
tmax = params["max_time"]

while comp.current_time <= tmax:
    comp.command_equi()
    comp.command_colli()
    comp.command_stre()
    comp.command_update_bc()
    comp.command_boun()
    comp.command_update_distribution_functions()
    comp.command_integrate()
    comp.command_time()
    comp.output_at_interval()

print("\n... finished.")

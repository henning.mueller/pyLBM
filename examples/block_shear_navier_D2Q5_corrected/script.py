import logging

from SolidLBM.mesher import Mesh
from SolidLBM.solver import Computation
from pathlib import Path
def run_test():
    name = 'block'
    h = 0.05
    seed_point = (-0.25-h/2, 0.0, 0.0)

    logging.basicConfig(
            filename = name+'.log',
            format = '%(levelname)s -- %(filename)s (%(lineno)s) -- %(message)s',
            filemode = 'w',
            level = logging.INFO,
        )

    print("Navier Equation - D2Q5 - Chopard - Block Shear - Corrected")

    mesh5 = Mesh(
            name=name,
            working_directory=Path(__file__).parent.absolute(),
            cell_size=h,
            seed_point=seed_point
        )

    mesh5.create_mesh_neighbor_points()
    mesh5.compute_cell_volumes_areas_boundary_names_at_boundary_points()
    mesh5.plot_mesh()
    mesh5.print_to_file()
    mesh5.print_alt_bc_to_file()
    mesh5.tmp_print_initial_conditions_to_file()

    computation5 = Computation(working_directory=Path(__file__).parent.absolute(), name='block')
    computation5.execute_command_sequence()


    w = computation5.Lattice.Points[396].w[0]
    w_ref = 9.996944503540827e-06
    e_rel = 1 - w / w_ref

    print(f"  Displacement at top right corner: {w}")
    print(f"  Expected displacement:            {w_ref}")
    if abs(e_rel) < 1e-3:
        print("  >> TEST PASSED <<")
    else:
        print(f"  << TEST FAILED >>    rel. error: |{e_rel:.2e}| > 1.00e-3")

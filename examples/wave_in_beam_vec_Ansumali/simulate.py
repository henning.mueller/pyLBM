import logging
from pathlib import Path

from SolidLBM.mesher import Mesh
from SolidLBM.parsolver import Computation as Computation_Par


name = "beam"
working_dir = Path(__file__).parent.absolute()


logging.basicConfig(
    filename=working_dir / f"{name}.log",
    format="%(levelname)s -- %(filename)s (%(lineno)s) -- %(message)s",
    filemode="w",
    level=logging.WARNING,
)

h: float = 2**-6

mesh = Mesh(
    name=name,
    working_directory=working_dir,
    cell_size=h,
    seed_point=(h / 2, h / 2, 0.0),
    mesh_type="D2Q9",
)

mesh.create_mesh_neighbor_points(verbose=False)
mesh.compute_cell_volumes_areas_boundary_names_at_boundary_points()
mesh.print_to_file()
mesh.print_alt_bc_to_file()


oi = 0.05  # output interval
outdir = "vtu"

comp = Computation_Par(
    working_dir=working_dir,
    name=name,
    model="Ansumali_SRT",
    verbose=False,
)

comp.setup_output(["w", "rho", "j", "sigma", "sigma_vm"], oi, output_dir=outdir)

while comp.time_condition():
    comp.collision_and_streaming()  # equilibrium, collide, stream
    comp.boundary_handling()  # boundary values, boundary conditions
    comp.distribution_update()  # update distribution post streaming
    comp.integration()  # compute displacemend and ouput quantities
    comp.output_at_interval()  # write vtk at certain times
    comp.time_update()  # progress computation time

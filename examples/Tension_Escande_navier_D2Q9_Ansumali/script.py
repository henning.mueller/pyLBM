from pathlib import Path
import logging

from SolidLBM.mesher import Mesh
from SolidLBM.solver import Computation


name = 'block'
working_dir = Path(__file__).parent.absolute()


def create_lattice(h: float = 0.05, name: str = name) -> None:
    seed_point = (h/2, h/2, 0.0)

    mesh = Mesh(
        name=name,
        working_directory=working_dir,
        cell_size=h,
        seed_point=seed_point,
        mesh_type = "D2Q9"
    )

    mesh.create_mesh_neighbor_points()
    mesh.compute_cell_volumes_areas_boundary_names_at_boundary_points()
    mesh.print_to_file()
    mesh.print_alt_bc_to_file()
    mesh.tmp_print_initial_conditions_to_file()


def run_test(*, run_mesher=True, print_progress=False):
    print("moment chain - point-wise - Block Tension")
    
    if run_mesher:
        create_lattice()

    comp = Computation(working_dir, name=name, verbose=print_progress)
    comp.command_init()
    comp.setup_output(quantities=["w", "j", "sigma"])

    tmax = comp.Parameters["max_time"]
    while comp.current_time <= tmax:
        if print_progress:
            print(
                f"time {comp.current_time: >4f} / {tmax: >4f}",
                end="\r",
            )
        comp.command_equi()
        comp.command_colli()
        comp.command_stre()
        comp.command_update_bc()
        comp.command_boun()
        comp.command_update_distribution_functions()
        comp.command_integrate()
        comp.write_output()
        comp.command_time()

    w = comp.Lattice.Points[1599].w[1]
    w_ref = -0.003184
    e_rel = 1 - w / w_ref

    print(f"  Displacement in y at bottom left corner: {w}")
    print(f"  Expected displacement:                   {w_ref}")
    if abs(e_rel) < 1e-3:
        print("  >> TEST PASSED <<")
    else:
        print(f"  << TEST FAILED >>    rel. error: |{e_rel:.2e}| > 1.00e-3")


if __name__ == "__main__":
    logging.basicConfig(
        filename=working_dir / f"{name}.log",
        format="%(levelname)s -- %(filename)s (%(lineno)s) -- %(message)s",
        filemode="w",
        level=logging.WARNING,
    )

    run_test(run_mesher=False, print_progress=True)

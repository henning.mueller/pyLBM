import numpy as np
import matplotlib.pyplot as plt
from time import time

import SolidLBM.util.geometry.Point as Point_Module
import SolidLBM.mesher.mesh.Mesh as Mesh_Module

from SolidLBM.solver.solution.Computation_Par import Computation_Par as Computation

h = 0.05
seed_point = Point_Module.Point(-0.0-h/2, 0.0*h/2, 0.0)

if True:
    mesh4 = Mesh_Module.Mesh(name='block', working_directory='./sf_notch_crack/', cell_size=h, seed_point=seed_point,mesh_type = "D2Q9")
    mesh4.create_mesh_neighbor_points()
    mesh4.compute_cell_volumes_areas_boundary_names_at_boundary_points()
    mesh4.print_to_file()
    mesh4.print_alt_bc_to_file()
    # mesh4.tmp_print_initial_conditions_to_file()

################################################################################
# Lattice = ParLatticeModule.Lattice_Par_CPU_Ansumali('./sf_notch_crack/', 'block')
# Lattice.initialise_distribution_functions_and_fields()
# Lattice.init_boundary_conditions()
computation = Computation('./sf_notch_crack/', 'block', verbose=True)
lattice = computation.Lattice

# plot
if False:
    ax,fig = plt.subplots()
    for BC in computation.boundary_conditions:
        pid = BC.point_ids
        if len(pid) > 0:
            plt.plot(lattice.Acoord[pid,0], lattice.Acoord[pid,1],'.', label = BC.ID)
    plt.legend()
    plt.show()

#find index of closest point
x1 = 0.005
y1 = 0.41

p1 = np.argmin(np.sqrt((lattice.Acoord[:,0] - x1) ** 2 + (lattice.Acoord[:,1] - y1) ** 2))

with open('log', 'w+') as f:
    f.write('')

#create the vtk object for an output
# Lattice.create_vtk_mesh()
computation.setup_output()

# t_step = 0
while computation.time_condition():
    # t_step += 1

#for t_step in range(70):
    # print("Current time step: {}, time: {}".format(t_step, Lattice.current_time))

    #EQUILIBRIUM
    # Lattice.compute_eq_functions()
    computation.equilibrium()

    #COLLISION
    # Lattice.collide()
    computation.collision()

    #STREAM
    # Lattice.stream()
    computation.streaming()

    #UPDATE_BOUNDARY_CONDITION
    # Lattice.apply_boundary_conditions()
    computation.boundary_handling()

    #UPDATE_DISTRIBUTION_FUNCTIONS
    # Lattice.update_distribution_functions()
    computation.distribution_update()

    #INTEGRATE
    # Lattice.integrate()
    computation.integration()

    #TIME
    # Lattice.update_time()
    computation.time_update()

    with open('log', 'a') as f:
        f.write("{},{},{}\n".format(computation.current_time, lattice.Aw[p1,0], lattice.Aw[p1,1]))


#0.003540984156445512 = Lattice.Aw[p1,0]

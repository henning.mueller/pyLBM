import sys,os
import matplotlib.pyplot as plt
import argparse

ap = argparse.ArgumentParser()
ap.add_argument("-f", "--filename", required=False, type=str, default='.',
                help="path to file")

args = vars(ap.parse_args())
filename = "log"

with open(filename,'r') as f: lines = [l.split(',') for l in f]

x_val= [float(line[0]) for line in lines]
y_val= [float(line[1]) for line in lines]


ax,fig = plt.subplots()
plt.xlabel('x axis')
plt.ylabel('y axis')
plt.plot(x_val,y_val,'-')
plt.show()



import logging
import numpy as np

from time import time

#import dev version of pyLBM -> tailor to your own system
# sys.path.append("/Users/fsteinme/Desktop/pyLBM/")


import SolidLBM.util.geometry.Point as Point_Module
import SolidLBM.mesher.mesh.Mesh as Mesh_Module

h = 0.1
seed_point = Point_Module.Point(-0.25-h/2, h/2, 0.0)

mesh4 = Mesh_Module.Mesh(name='block', working_directory='./sf_block_tension/', cell_size=h, seed_point=seed_point,mesh_type = "D2Q9")
mesh4.create_mesh_neighbor_points()
mesh4.compute_cell_volumes_areas_boundary_names_at_boundary_points()
mesh4.print_to_file()
mesh4.print_alt_bc_to_file()
# mesh4.tmp_print_initial_conditions_to_file()

################################################################################
from SolidLBM.solver.solution.Computation_Par import Computation_Par as Computation


# Lattice = ParLatticeModule.Lattice_Par_CPU_Ansumali('./sf_block_tension/', 'block')
# Lattice.initialise_distribution_functions_and_fields()
# Lattice.init_boundary_conditions()
computation = Computation('./sf_block_tension/', 'block', verbose=True)
lattice = computation.Lattice


#create the vtk object for an output
# lattice.create_vtk_mesh()
computation.setup_output()

#find index of closest point
x1 = 0
x2 = 0.5
x3 = 1.0
y1 = 1.0

p1 = np.argmin(np.sqrt((lattice.Acoord[:,0] - x1) ** 2 + (lattice.Acoord[:,1] - y1) ** 2))
p2 = np.argmin(np.sqrt((lattice.Acoord[:,0] - x2) ** 2 + (lattice.Acoord[:,1] - y1) ** 2))
p3 = np.argmin(np.sqrt((lattice.Acoord[:,0] - x3) ** 2 + (lattice.Acoord[:,1] - y1) ** 2))

with open('log', 'w+') as f:
    f.write('')

# t_step = 0
# while lattice.current_time < 4.0:
    # t_step += 1
#for t_step in range(70):
    # print("Current time step: {}, time: {}".format(t_step, lattice.current_time))

while computation.time_condition():
    #EQUILIBRIUM
    # Lattice.compute_eq_functions()
    computation.equilibrium()

    #COLLISION
    # Lattice.collide()
    computation.collision()

    #STREAM
    # Lattice.stream()
    computation.streaming()

    #UPDATE_BOUNDARY_CONDITION
    # Lattice.apply_boundary_conditions()
    computation.boundary_handling()

    #UPDATE_DISTRIBUTION_FUNCTIONS
    # Lattice.update_distribution_functions()
    computation.distribution_update()

    #INTEGRATE
    # Lattice.integrate()
    computation.integration()

    #TIME
    # Lattice.update_time()
    computation.time_update()

    with open('log', 'a') as f:
        f.write("{},{},{},{}\n".format(computation.current_time,lattice.Aw[p1,1], lattice.Aw[p2,1], lattice.Aw[p3,1],))

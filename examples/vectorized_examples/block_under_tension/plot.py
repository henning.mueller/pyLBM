import sys,os
import matplotlib.pyplot as plt
import argparse

filename = "log"

with open(filename,'r') as f: lines = [l.split(',') for l in f]

x_val= [float(line[0]) for line in lines]
y_val= [float(line[1]) for line in lines]
z_val= [float(line[2]) for line in lines]
zz_val=[float(line[3]) for line in lines]

ax,fig = plt.subplots()
plt.xlabel('x axis')
plt.ylabel('y axis')
plt.plot(x_val,y_val,'-')
plt.plot(x_val,z_val,'-')
plt.plot(x_val,zz_val,'-')
plt.show()



import logging

from SolidLBM.mesher import Mesh
from SolidLBM.solver import Computation
from pathlib import Path


working_dir = Path(__file__).parent.absolute()
name = 'disc_with_crack'


def create_lattice(h: float = 1.0 / 21.0):
    seed_point = (h / 2, h / 2, 0.0)
    
    mesh = Mesh(
        name=name,
        working_directory=working_dir,
        cell_size=h,
        seed_point=seed_point,
    )

    mesh.create_mesh_neighbor_points()
    mesh.plot_mesh()
    mesh.print_to_file()
    mesh.tmp_print_initial_conditions_to_file()
    mesh.compute_cell_volumes_areas_boundary_names_at_boundary_points()
    mesh.print_alt_bc_to_file()

def run_test(*, run_mesher=True):
    print("Guangwu Stationary Crack - D2Q5 - Mesoscopic Boundary condition")

    vtk_file_path = working_dir / 'vtk' / f"{name}_0001.vtk"
    out_file_path = working_dir /'output' / f"{name}_w.outloc"
    try:
        Path.vtk_file_path.unlink()
        logging.debug('VTK file removed')
        Path.out_file_path.unlink()
        logging.debug('OutLoc file removed')
    except Exception as e:
        logging.info(f"Deleting files not successful: {e}")

    if run_mesher:
        create_lattice()
    
    computation = Computation(working_directory=working_dir, name=name)
    computation.execute_command_sequence()

    outloc_exists = out_file_path.is_file()
    vtk1_exists = vtk_file_path.is_file()
    vtk_files = list((working_dir / 'vtk').iterdir())
    file_count = len(vtk_files)
    print(f"  output files: *.outloc: {outloc_exists}, *_0001.vtk: {vtk1_exists}, {file_count} vtk files")

    if outloc_exists and vtk1_exists and file_count == 4:
        print('  >> TEST PASSED <<')
    else:
        print("  << TEST FAILED >>")


if __name__ == "__main__":
    logging.basicConfig(
            filename = name+'.log',
            format = '%(levelname)s -- %(filename)s (%(lineno)s) -- %(message)s',
            filemode = 'w',
            level = logging.INFO,
        )
    
    run_test(run_mesher=False)

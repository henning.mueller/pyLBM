import logging

import SolidLBM.mesher as mesher


h = 2**-5
name = 'beam'

seed_point = mesher.Point(h/2, h/2, 0.0)

logging.basicConfig(
        filename = name + '.log',
        encoding = 'utf-8',
        format = '%(levelname)s -- %(filename)s (%(lineno)s) -- %(message)s',
        filemode = 'w',
        level = logging.WARNING,
    )

mesh4 = mesher.Mesh(
    name = name,
    working_directory = './',
    cell_size = h,
    seed_point = seed_point,
    mesh_type = "D2Q9",
)

mesh4.create_mesh_neighbor_points(verbose=True)
mesh4.compute_cell_volumes_areas_boundary_names_at_boundary_points()
mesh4.print_to_file()
mesh4.print_alt_bc_to_file()
#mesh4.tmp_print_initial_conditions_to_file()
#mesh4.plot_mesh()

import logging
from pathlib import Path

import meshio
from scipy.interpolate import griddata
import numpy as np

import SolidLBM.mesher as mesher
import SolidLBM.solver as solver


name = "beam"
working_dir = Path(__file__).parent.absolute()


def get_fe_ref(name: str = name) -> list:
    """Get the reference FE data"""
    nodes = np.loadtxt(working_dir / f"{name}.msh", usecols=(1, 2))
    fe_ref = [interpolate_ref(nodes, name, k) for k in range(1, 16)]
    return fe_ref


def interpolate_ref(nodes, model: str, k: int):
    """Get the interpolation of FE data at lattice nodes"""
    # global t_fe

    def _get_ref_data(path: Path, k: int, name: str = name):
        """Load the FE data from disk"""

        assert path.is_dir(), f"Path {path} is not a directory."

        reader = meshio.xdmf.TimeSeriesReader(path / f"{name}_fe.xdmf")
        pts, _ = reader.read_points_cells()
        data = reader.read_data(k)

        return pts, data[1]["u"]

    # load FE data
    pts_fe, disp_fe = _get_ref_data(working_dir / "fe_ref", k, model)

    # interpolate FE data at lattice nodes
    disp_ref_x = griddata(pts_fe[:, :2], disp_fe[:, 0], nodes[:, :2], method="linear")
    disp_ref_y = griddata(pts_fe[:, :2], disp_fe[:, 1], nodes[:, :2], method="linear")
    disp_ref = np.stack((disp_ref_x, disp_ref_y), axis=1)
    return disp_ref


def create_lattice(h: float = 2**-5, name: str = name) -> None:
    seed_point = (h / 2, h / 2, 0.0)

    mesh = mesher.Mesh(
        name=name,
        working_directory=Path(__file__).parent.absolute(),
        cell_size=h,
        seed_point=seed_point,
        mesh_type="D2Q9",
    )

    mesh.create_mesh_neighbor_points(verbose=True)
    mesh.print_to_file()
    mesh.compute_cell_volumes_areas_boundary_names_at_boundary_points()
    mesh.print_alt_bc_to_file()
    mesh.tmp_print_initial_conditions_to_file()


def run_test(name: str = name, *, run_mesher=True) -> None:
    global error
    global comp

    def _get_error(disp_lb, disp_ref) -> float:
        """Compute the relative error"""
        # compute the L2-derivation
        diff = np.abs(disp_lb - disp_ref)
        diff_magnitude = np.einsum("ij,ij", diff, diff)

        # normalise for relative L2-error
        norm = np.einsum("ij,ij", disp_ref, disp_ref)
        err = np.sqrt(diff_magnitude) / norm

        return err

    print("Ansumali - serial Points - TRT")

    if run_mesher:
        create_lattice()

    comp = solver.Computation(
        working_directory=working_dir,
        name=name,
        verbose=True,
    )
    comp.command_init()

    params = comp.Parameters
    tmax = params["max_time"]
    dt = params["dth"]
    print(f"τ+: {params['tau+']}, τ-: {params['tau-']}")

    fe_ref = get_fe_ref(name)
    k = len(fe_ref)

    idx = 1
    error = []
    while comp.current_time <= tmax:
        print(
            f"time {comp.current_time: >4f} / {tmax: >4f}",
            end="\r",
        )
        comp.command_equi()
        comp.command_colli()
        comp.command_stre()
        comp.command_update_bc()
        comp.command_boun()
        comp.command_update_distribution_functions()
        comp.command_integrate()

        # get error at intervals
        if comp.current_time >= (tmax - 0.5 * dt) * idx / k:
            comp.command_vtk()
            disp_lb = np.array([pt.w for pt in comp.Lattice.Points])

            err = _get_error(disp_lb, fe_ref[idx - 1])
            print(f"\n{err}")

            error.append(err)
            idx += 1

        comp.command_time()

    # print(f"\nerror: {sum(error):5.4e}")

    # print("  Compare mean error to FE:")
    # comparison = np.allclose(np.mean(error), 0.37035, rtol=1e-3)
    # if comparison:
    #     print("  >> TEST PASSED <<")
    # else:
    #     print("  << TEST FAILED >>")

    # ### Test vtu output
    # print("  Compare cumulated error to FE:")
    # comparison = sum(error) < 5.6
    # if comparison:
    #     print("  >> TEST PASSED <<")
    # else:
    #     print("  << TEST FAILED >>")


if __name__ == "__main__":
    logging.basicConfig(
        filename=working_dir / f"{name}.log",
        format="%(levelname)s -- %(filename)s (%(lineno)s) -- %(message)s",
        filemode="w",
        level=logging.DEBUG,
    )

    # run_test(run_mesher=False)
    run_test(run_mesher=True)
    print("... done")

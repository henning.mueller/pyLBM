import numerics
from domain import Domain
from material import Material
import functions


name = 'beam_fe'

# material & dynamic constants
material = Material(name, use_weak_form=True)

# set MPI environment
mpienv = numerics.MPIenv()

# generate domain
domain = Domain(name)
domain.create_beam(mpienv.comm)

# define functions for boundary and time integration
boun_func = functions.Sine(name)
newmark = functions.NewmarkIntegration(name)

# create solver instance
solver = numerics.Solver(name, mpienv, newmark, boun_func, domain)

# write some parameters
numerics.report(mpienv, material, solver, newmark)

solver.time_loop(material)

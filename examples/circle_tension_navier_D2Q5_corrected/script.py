import logging

from SolidLBM.mesher import Mesh
from SolidLBM.solver import Computation
from pathlib import Path
def run_test():
    name = 'block'
    h = 0.05
    seed_point = (-0.25-h/2, h/2, 0.0)

    logging.basicConfig(
            filename = name+'.log',
            format = '%(levelname)s -- %(filename)s (%(lineno)s) -- %(message)s',
            filemode = 'w',
            level = logging.INFO,
        )

    print("Navier Equation - D2Q5 - Chopard - Circle Tension - Corrected")

    mesh3 = Mesh(
            name=name,
            working_directory=Path(__file__).parent.absolute(),
            cell_size=h,
            seed_point=seed_point
        )

    mesh3.create_mesh_neighbor_points()
    mesh3.compute_cell_volumes_areas_boundary_names_at_boundary_points()
    mesh3.plot_mesh()
    mesh3.print_to_file()
    mesh3.print_alt_bc_to_file()
    mesh3.tmp_print_initial_conditions_to_file()

    computation3 = Computation(working_directory=Path(__file__).parent.absolute(), name='block')
    computation3.execute_command_sequence()


    w = computation3.Lattice.Points[5].w[1]
    w_ref = 1.8333327511007263e-07
    e_rel = 1 - w / w_ref

    print(f"  Displacement at hole:  {w}")
    print(f"  Expected displacement: {w_ref}")
    if abs(e_rel) < 1e-3:
        print("  >> TEST PASSED <<")
    else:
        print(f"  << TEST FAILED >>    rel. error: |{e_rel:.2e}| > 1.00e-3")

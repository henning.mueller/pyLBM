import logging

from SolidLBM.mesher import Mesh
from SolidLBM.solver import Computation
from pathlib import Path

def run_test():
    name = 'disc_with_hole'
    h = 0.025
    seed_point = (0.25, 0.0,0.0)

    logging.basicConfig(
            filename = name+'.log',
            format = '%(levelname)s -- %(filename)s (%(lineno)s) -- %(message)s',
            filemode = 'w',
            level = logging.INFO,
        )

    print("Chopard Wave Equation - D2Q9 - Macroscopic Boundary condition")
    mesh1 = Mesh(
            name=name,
            working_directory=Path(__file__).parent.absolute(),
            cell_size=h,
            seed_point=seed_point,
            mesh_type='D2Q9',
        )

    mesh1.create_mesh_neighbor_points()
    mesh1.plot_mesh()
    mesh1.print_to_file()

    computation1 = Computation(working_directory=Path(__file__).parent.absolute(), name='disc_with_hole')
    computation1.execute_command_sequence()


    w = computation1.Lattice.Points[1590].w[0]
    w_ref = 0.0002380891024338674
    e_rel = 1 - w / w_ref

    print(f"  Displacement at top left corner: {w}")
    print(f"  Expected displacement:           {w_ref}")
    if abs(e_rel) < 1e-3:
        print("  >> TEST PASSED <<")
    else:
        print(f"  << TEST FAILED >>    rel. error: |{e_rel:.2e}| > 1.00e-3")

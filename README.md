[![latest release][release-badge]][perma-release]
[![License: MIT][mit-shield]][mit]
[![License: CC BY 4.0][cc-by-shield]][cc-by]


# pyLBM

**Lattice Boltzmann methods for elastodynamics written in Python**
---
![wave travelling in beam][headerpic]

This is a research project from the [Institute for Mechanics](https://www.mechanik.tu-darmstadt.de) at TU Darmstadt.
It started at the [Chair for Applied Mechanics](https://mv.rptu.de/fgs/ltm/) at RPTU Kasierslautern-Landau (formerly TU Kaiserslautern).

### Overview

_pyLBM_ builds around the Python-package _SolidLBM_.
It acts as a refrence implementation of the methods and concepts developed within the project.
This is accompanied by publications in scientific journals that present the algorithms and the methodology.

###### Examples

Input files are included for some simulations.
These examples showcase some of the abilities of the method, but also act as tests and generally give an idea on how to use the software.

### Structure and features of the project

The package is written in Python with an object-oriented paradigm.

- The subpackage `mesher` creates the lattice structure with the lattice scheme (connectivity) and saves it to disk.

- `solver` and `parsolver` are used to run simulations.
`solver` uses a node-wise implementation, executed in serial, whereas `parsolver`'s data structures are based on *Numpy*-Arrays for vectorisation.
*Cupy* provides capabilities to execute the algorithms in parallel on GPGPU [^1].

[^1]: The Cupy-based implementation might not work in v3.0. 
This will be fixed in a future update.

- `util` provides classes and methods needed for both the `mesher` and the `solver`, such as geometry objects needed for the lattice structure.

Not all features are present in both types of solvers. The vectorised one offers higher performance, but often reduced features.

![deformed square with hole][feature1pic]{width=30%}
![deformed square with slits][feature2pic]{width=35%}

###### non-exhaustive list of features

- linear-elastodynamics with Dirichlet- and Neumann-type boundary conditions
- algorithms for the solution of balance laws (*moment-chain*) as current state-of-the-art
- algorithms for the solution of the Navier-Lamé-equation based on wave equations (modeled after G. Yan & B. Chopard)[^2]
- dynamic crack propagation
    * based on configurational forces for mixed mode I & II (*moment-chain*)
    * from the stress intensity factor for mode III (wave based *Guangwu*)

[^2]: *Chopard*- and *Guangwu*-models are treated as legacy methods from version 3.0 onwards.

The _master_ branch contains the most stable code.
The examples should work with the master branch.
More features, especially experimental ones still under development, can be found in different branches.

![crack mode I][crack1pic]{width=32%}
![crack mode II][crack2pic]{width=33%}

### Install package

The dependencies of the package can be installed with [Poetry](https://python-poetry.org/).
    
    poetry install

Some additional dependencies for development can be installed as extras

    poetry install --all-extras

These are needed for the testing examples and generally for the development of the project.

Further, the package has to be put on the pythonpath, so it can be imported.

### Run simulations

The package `SolidLBM`, or rather the respective subpackages and modules, can be imported in a script.
From `solver` and `parsolver` the `Computation`-class can be imported, which provides the interface for running simulations.

The specific simulation model is defined via input files.

The directory *examples* contains several scripts.
The files can be used as templates for the definition of new simulations.
Further models are published alongside publications.

Also provided are scripts to run basic tests of the code, working as integration tests for now.

![crack mode III][footerpic]

### Documentation & related works

The software is very much a work-in-progress. A documentation is still lacking. Consult the [publications][pubs] or the source code for insights.

---

This software is licensed under the [MIT license][mit]

[![CC BY 4.0][cc-by-image]][cc-by]
All related work is licensed under the
[Creative Commons Attribution 4.0 International License][cc-by],
unless otherwise stated.


[cc-by]: http://creativecommons.org/licenses/by/4.0/
[cc-by-image]: https://licensebuttons.net/l/by/4.0/80x15.png
[cc-by-shield]: https://img.shields.io/badge/License-CC%20BY%204.0-lightgrey.svg

[mit]: https://opensource.org/licenses/MIT
[mit-shield]: https://img.shields.io/badge/License-MIT-yellow.svg

[perma-release]: https://git.rwth-aachen.de/SolidLBM/pyLBM/-/releases/permalink/latest
<!-- does not find release in SolidLBM namespace -->
[release-badge]: https://git.rwth-aachen.de/henning.mueller/pyLBM/-/badges/release.svg

[headerpic]: doc/pic/beam.gif
[footerpic]: doc/pic/strip_crack_opening.png
[feature1pic]: doc/pic/lochscheibe.png
[feature2pic]: doc/pic/block_shear.png
[crack1pic]: doc/pic/mode1_dynamic_crack_waves.png
[crack2pic]: doc/pic/mode2_K-field.png

<!-- raw file does not work with relative path, needs hard link -->
[pubs]: https://git.rwth-aachen.de/SolidLBM/pyLBM/-/raw/master/doc/PUBLICATIONS.pdf

import logging
import sys
import numpy as np

import pytest

import matplotlib.pyplot as plt

from time import time

import SolidLBM.util.geometry.Point as Point_Module
import SolidLBM.mesher.mesh.Mesh as Mesh_Module
import SolidLBM.mesher.mesh.InitialCondition as Condition_Module
import SolidLBM.solver.solution.Computation as Computation_Module

import SolidLBM.solver.solution.Lattice_Par_CPU_Ansumali as ParLatticeModule


def run_simulation():
    h = 0.05
    seed_point = Point_Module.Point(-0.0-h/2, 0.0*h/2, 0.0)

    mesh4 = Mesh_Module.Mesh(name='block', working_directory='./sf_notch_crack/', cell_size=h, seed_point=seed_point,mesh_type = "D2Q9")
    mesh4.create_mesh_neighbor_points()
    mesh4.compute_cell_volumes_areas_boundary_names_at_boundary_points()
    mesh4.print_to_file()
    mesh4.print_alt_bc_to_file()
    mesh4.tmp_print_initial_conditions_to_file()

    ################################################################################
    Lattice = ParLatticeModule.Lattice_Par_CPU_Ansumali('./sf_notch_crack/', 'block')
    Lattice.initialise_distribution_functions_and_fields()
    Lattice.init_boundary_conditions()

    #find index of closest point
    x1 = 0.005
    y1 = 0.41

    p1 = np.argmin(np.sqrt((Lattice.Acoord[:,0] - x1) ** 2 + (Lattice.Acoord[:,1] - y1) ** 2))

    #create the vtk object for an output
    Lattice.create_vtk_mesh()

    t_step = 0
    while Lattice.current_time < 2.0:
        t_step += 1

        ##EQUILIBRIUM
        Lattice.compute_eq_functions()

        #COLLISION
        Lattice.collide()

        #STREAM
        Lattice.stream()

        #UPDATE_BOUNDARY_CONDITION
        Lattice.apply_boundary_conditions()

        #UPDATE_DISTRIBUTION_FUNCTIONS
        Lattice.update_distribution_functions()

        #INTEGRATE
        Lattice.integrate()

        #TIME
        Lattice.update_time()

    return Lattice.Aw[p1,0]

def test_displacement():
    u0 = run_simulation()
    assert round(u0,8) == round(0.003540984156445512,8)

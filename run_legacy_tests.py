import logging

from examples import circle_wave_D2Q5_guangwu
from examples import circle_wave_D2Q9_chopard
from examples import block_tension_navier_D2Q5_not_corrected
from examples import circle_tension_navier_D2Q5_corrected
from examples import block_shear_navier_D2Q5_corrected
from examples import stationary_crack_D2Q5_guangwu_local
from examples import dynamic_crack_mode3_D2Q5_guangwu_global

from SolidLBM.solver.solution.LatticePointD2_abstract import LatticePointD2_abstract


LatticePointD2_abstract.FD_ACC_DEFAULT = 2


if __name__ == "__main__":
    logging.basicConfig(
        filename="legacy_test.log",
        format="%(levelname)s -- %(filename)s (%(lineno)s) -- %(message)s",
        filemode="w",
        level=logging.WARNING,
    )

    circle_wave_D2Q5_guangwu()
    circle_wave_D2Q9_chopard()
    block_tension_navier_D2Q5_not_corrected()
    circle_tension_navier_D2Q5_corrected()
    block_shear_navier_D2Q5_corrected()
    stationary_crack_D2Q5_guangwu_local()
    dynamic_crack_mode3_D2Q5_guangwu_global()
    print("legacy tests completed")

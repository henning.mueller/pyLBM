FROM python:3.10.14-slim-bookworm
# FROM python:3.12.4-slim-bookworm

ENV POETRY_VERSION=1.8.2

RUN pip install -U pip setuptools && \
    pip install poetry==${POETRY_VERSION}

WORKDIR /usr/src/app

COPY pyproject.toml .

RUN poetry config virtualenvs.create false && poetry install --no-root --all-extras

COPY run_moment_chain_tests.py .
COPY /SolidLBM/ ./SolidLBM
COPY /examples/ ./examples


CMD ["python", "run_moment_chain_tests.py"]


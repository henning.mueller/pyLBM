import os
import logging
from abc import ABC, abstractmethod
from typing import TypeVar


vtkUnstructuredGrid = TypeVar("vtkUnstructuredGrid")


class Lattice_Par_GPU_abstract:
    """
    Abstract Lattice class based on numpy arrays. Provides basic functions for
    Lattice manipulation.
    TODO: explain parameters
    TODO: logging
    TODO: make a check that all needed files are there
    TODO: Create a way to switch between numpy and cupy
    """

    def __init__(self, parameter_file=""):
        self.par = dict()
        self._read_parameter_file(parameter_file)

        self.output_dict = dict()

    def _read_parameter_file(self, parameter_file):
        string_arguments = {"point_type", "aux_point_switch"}
        with open(parameter_file, "r") as current_file:
            for my_line in current_file:
                my_strings = my_line.split()
                if my_strings:
                    key, value = my_strings[0], my_strings[2]
                else:
                    continue
                if key not in string_arguments:
                    self.par[key] = float(value)
                else:
                    self.par[key] = value
        logging.info(f'Reading parameters from "{parameter_file}"')

    @abstractmethod
    def compute_eq_functions(self):
        pass

    @abstractmethod
    def collide(self):
        pass

    @abstractmethod
    def stream(self):
        pass

    @abstractmethod
    def apply_boundary_conditions(self):
        pass

    def update_distribution_functions(self):
        self.Af = self.Af_tmp.copy()

    @abstractmethod
    def integrate(self):
        pass

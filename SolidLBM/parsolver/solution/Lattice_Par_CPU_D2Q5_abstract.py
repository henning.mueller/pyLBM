import numpy as np

import SolidLBM.parsolver.solution.Lattice_Par_CPU_abstract as Lattice_Par_CPU_abstract_Module
import logging
import os


class Lattice_Par_CPU_D2Q5_abstract(Lattice_Par_CPU_abstract_Module.Lattice_Par_CPU_abstract):
    """Specified Lattice Class for 2D Simulations

    in_file_path: path to the .msh file
    TODO: to be defined
    """

    def __init__(self, working_dir, name):
        """TODO: to be defined."""
        super(Lattice_Par_CPU_D2Q5_abstract, self).__init__(working_dir, name)
        self._read_lattice_geo_from_file()
        self.num_points = self.Ageo.shape[0]
        self._init_Af()
        self._init_Aw()

    def _read_lattice_geo_from_file(self):
        """
        Reads geometry of Lattice from a specified file and stores it in Ageo.
        """

        mesh_file = os.path.join(self.working_dir, self.name + ".msh")
        with open(mesh_file, "r") as current_file:
            geo_list = []
            coord_list = []
            for line in current_file:  # create point objects
                if line.startswith("#"):
                    if "CellSize" in line:
                        self.cell_size = float(line.split()[3]) / self.par["L"]  # relate to total domain size
                        print("Cell Size: {}".format(self.cell_size))
                    continue
                s = line.split()
                coord_list.append(
                    [
                        float(s[1]) / self.par["L"],
                        float(s[2]) / self.par["L"],
                        float(s[3]) / self.par["L"],
                    ]
                )
                nb_list = [int(s[4]), int(s[5]), int(s[6]), int(s[7])]
                if -1 in nb_list:
                    geo_list.append([1, int(s[4]), int(s[5]), int(s[6]), int(s[7])])
                else:
                    geo_list.append([0, int(s[4]), int(s[5]), int(s[6]), int(s[7])])
            self.Ageo = np.array(geo_list)
            self.Acoord = np.array(coord_list)
        logging.info('Reading mesh geometry and connective data from "{}"'.format(mesh_file))

    def _init_Af(self):
        self.Af = np.zeros((self.num_points, 5))

    def _init_Aw(self):
        self.Aw = np.zeros((self.num_points,))

    # LBM functions start here

    def compute_eq_functions(self):
        self.Af_eq = self.Af.copy()
        Aw_dot = self.Af.sum(axis=1)
        self.Af_eq[:, 0] = Aw_dot - (self.par["D"] * self.par["la"] * self.Aw) / self.par["ch"] ** 2
        self.Af_eq[:, 1] = self.Af_eq[:, 2] = self.Af_eq[:, 3] = self.Af_eq[:, 4] = (
            self.par["la"] * self.par["D"] * self.Aw
        ) / (self.par["ch"] ** 2 * self.par["b"])

    def collide(self):
        self.Af = self.Af - 1.0 / self.par["tauh"] * (self.Af - self.Af_eq)

    def stream(self):
        """ "
        TODO: This is only valid for D2Q5, either create another abstract Lattice class or do a more general implementatio
        """
        self.Af_tmp = self.Af.copy()  # temporary copy to do streaming and later apply boundary conditions
        self.Af_tmp[:, 1:5] = -1  # set missing distribution functions to -1; row 0 -> 0 velo stays the same
        for i in range(1, 5):
            np.put(
                self.Af_tmp[:, i],
                self.Ageo[:, i][self.Ageo[:, i] >= 0],
                self.Af[:, i][self.Ageo[:, i] >= 0],
            )

    def apply_boundary_conditions(self):
        """This has to be done in the specific implementation, general implementation of boundary conditions is difficult"""
        pass

    def update_time(self):
        """TODO: check on where to store the time? maybe in parameters? or create computation class"""
        self.current_time += self.par["dth"] * self.par["L"] / self.par["cs"]

    def update_distribution_functions(self):
        self.Af = self.Af_tmp.copy()

    def integrate(self):
        self.Aw += self.Af.sum(axis=1) * self.par["dth"]
        self.output_dict["w"] = self.Aw

from time import ctime
from time import time
from types import coroutine
import numpy as np
import cupy as cp
import math
import sys
import os
from numpy.core import numeric
from numpy.core.fromnumeric import ptp

from numpy.testing._private.utils import jiffies, nulp_diff

import SolidLBM.parsolver.solution.Lattice_Par_GPU_D2Q9_abstract as Lattice_Par_GPU_D2Q9_abstract_Module


class Lattice_Par_GPU_Ansumali(Lattice_Par_GPU_D2Q9_abstract_Module.Lattice_Par_GPU_D2Q9_abstract):
    """Docstring for Lattice.
    TODO: to be defined
    """

    def __init__(self, working_dir, name):
        """TODO: to be defined."""
        super(Lattice_Par_GPU_Ansumali, self).__init__(working_dir, name)

        self._compute_dependent_parameters()
        self.rho = cp.array([self.par["rho0h"]] * self.num_points)

        self._compute_lattice_velocity()
        self._compute_c_tensors()
        self.boundary_conditions = []
        self.corner_points = []
        self.corner_distris = []

        print("Initialization working")

    def _compute_dependent_parameters(self):
        """TODO: This Needs to be implemented"""
        self.par["cs"] = cp.sqrt(self.par["mue"] / self.par["rho0"])
        self.par["cd"] = cp.sqrt((self.par["lambda"] + 2.0 * self.par["mue"]) / self.par["rho0"])
        self.par["h"] = self.cell_size
        self.par["lambdah"] = self.par["lambda"] / self.par["mue"]
        self.par["mueh"] = self.par["mue"] / self.par["mue"]
        self.par["hh"] = self.par["h"] / self.par["L"]
        self.par["csh"] = self.par["cs"] / self.par["cs"]
        self.par["cdh"] = self.par["cd"] / self.par["cs"]

        self.par["dth"] = 1.0 / math.sqrt(3.0) * self.par["hh"] / self.par["csh"]
        self.par["tauh"] = 0.55 * self.par["dth"]
        self.par["ch"] = self.par["hh"] / self.par["dth"]

        self.par["rho0h"] = self.par["rho0"] / self.par["rho0"]

    def _compute_lattice_velocity(self):
        self.c = self.lattice_directions * self.par["ch"]

    def _compute_c_tensors(self):
        cc = self.par["ch"]
        self.c_tensors = cp.array(
            [
                [
                    [0.0, 0.0],  # c0c0
                    [0.0, 0.0],
                ],
                [
                    [cc.get() ** 2, 0.0],  # c1c1
                    [0.0, 0.0],
                ],
                [
                    [0.0, 0.0],  # c2c2
                    [0.0, cc.get() ** 2],
                ],
                [
                    [cc.get() ** 2, 0.0],  # c3c3
                    [0.0, 0.0],
                ],
                [
                    [0.0, 0.0],  # c4c4
                    [0.0, cc.get() ** 2],
                ],
                [
                    [cc.get() ** 2, cc.get() ** 2],  # c5c5
                    [cc.get() ** 2, cc.get() ** 2],
                ],
                [
                    [cc.get() ** 2, -(cc.get() ** 2)],  # c6c6
                    [-(cc.get() ** 2), cc.get() ** 2],
                ],
                [
                    [cc.get() ** 2, cc.get() ** 2],  # c7c7
                    [cc.get() ** 2, cc.get() ** 2],
                ],
                [
                    [cc.get() ** 2, -(cc.get() ** 2)],  # c8c8
                    [-(cc.get() ** 2), cc.get() ** 2],
                ],
            ]
        )

    def initialise_distribution_functions_and_fields(self):
        rho0 = self.par["rho0h"]
        b = self.par["csh"]
        j = rho0 * self.Awdot

        j0TimesCi = cp.vstack([j[:, 0]] * 9).T * self.c[:, 0] + cp.vstack([j[:, 1]] * 9).T * self.c[:, 1]
        OneTimesCiCi = self.c_tensors[:, 0, 0] + self.c_tensors[:, 1, 1]
        OneTimesOne = 2.0

        self.Af = self.weights * (
            rho0 + j0TimesCi / b**2 + 1.0 / (2.0 * b**4) * (-rho0 * b**2 * OneTimesCiCi + rho0 * b**4 * OneTimesOne)
        )

    def _compute_SS(self):
        # ==========================calculate dx/dy to compute gradient==============================
        # only first order at boundary, save the array eventually
        # double cell size at inner nodes
        dx = cp.ones((self.num_points, 2)) * (1.0 / (self.cell_size * 2))
        dy = cp.ones((self.num_points, 2)) * (1.0 / (self.cell_size * 2))

        # single cell size at boundary nodes (first order)
        dx[:, 1] = dx[:, 1] * -1
        dy[:, 1] = dy[:, 1] * -1

        # set to one cell size at boundarys
        dx[self.Ageo[:, 1] == -1] = dx[self.Ageo[:, 1] == -1] * 2
        dx[self.Ageo[:, 3] == -1] = dx[self.Ageo[:, 3] == -1] * 2

        dy[self.Ageo[:, 2] == -1] = dy[self.Ageo[:, 2] == -1] * 2
        dy[self.Ageo[:, 4] == -1] = dy[self.Ageo[:, 4] == -1] * 2

        # ==========================calculate rhodx and rhody ==============================

        # calculate indices
        def get_index(arr):
            condlist = [arr != -1, arr == -1]
            choicelist = [arr, cp.arange(self.num_points)]
            return cp.select(condlist, choicelist)

        sumf = self.Af.sum(axis=1)

        Arho = cp.zeros((self.num_points, 2))
        Arho[:, 0] = sumf[get_index(self.Ageo[:, 1])] * dx[:, 0] + sumf[get_index(self.Ageo[:, 3])] * dx[:, 1]
        Arho[:, 1] = sumf[get_index(self.Ageo[:, 2])] * dy[:, 0] + sumf[get_index(self.Ageo[:, 4])] * dy[:, 1]

        ASS = cp.zeros((self.num_points, 2))
        ASS[:, 0] = (self.par["mueh"] - self.par["lambdah"]) / self.par["rho0h"] * Arho[:, 0]
        ASS[:, 1] = (self.par["mueh"] - self.par["lambdah"]) / self.par["rho0h"] * Arho[:, 1]

        # Add BulkForce -> Ask what is meant by that (Source Ident?) TODO
        return ASS

    def compute_eq_functions(self):
        ASS = self._compute_SS()

        # ==========================compute moments ==============================
        # compute first moments
        Afirst_moment_x = cp.sum(self.Af * self.c[:, 0], axis=1)
        Afirst_moment_y = cp.sum(self.Af * self.c[:, 1], axis=1)

        # compute j  TODO: do completly vectorized
        Ajx = Afirst_moment_x + self.par["dth"] / 2.0 * ASS[:, 0]
        Ajy = Afirst_moment_y + self.par["dth"] / 2.0 * ASS[:, 1]

        b = self.par["csh"]

        # compute second moment -> change to einsum notation
        P = cp.zeros((self.num_points, 2, 2))
        P[:, 0, 0] = cp.sum(self.Af * self.c_tensors[:, 0, 0], axis=1)
        P[:, 0, 1] = cp.sum(self.Af * self.c_tensors[:, 0, 1], axis=1)
        P[:, 1, 0] = cp.sum(self.Af * self.c_tensors[:, 1, 0], axis=1)
        P[:, 1, 1] = cp.sum(self.Af * self.c_tensors[:, 1, 1], axis=1)

        # ==========================compute eq distribution ==============================
        jTimesCi = Ajx[:, None] * self.c[:, 0] + Ajy[:, None] * self.c[:, 1]
        PTimesCiCi = (
            P[:, 0, 0].reshape(P[:, 0, 0].shape[0], 1)
            @ self.c_tensors[:, 0, 0].reshape(1, self.c_tensors[:, 0, 0].shape[0])
            + P[:, 0, 1].reshape(P[:, 0, 1].shape[0], 1)
            @ self.c_tensors[:, 1, 0].reshape(1, self.c_tensors[:, 1, 0].shape[0])
            + P[:, 1, 0].reshape(P[:, 1, 0].shape[0], 1)
            @ self.c_tensors[:, 0, 1].reshape(1, self.c_tensors[:, 0, 1].shape[0])
            + P[:, 1, 1].reshape(P[:, 1, 1].shape[0], 1)
            @ self.c_tensors[:, 1, 1].reshape(1, self.c_tensors[:, 1, 1].shape[0])
        )

        OneTimesCiCi = self.c_tensors[:, 0, 0] + self.c_tensors[:, 1, 1]
        PTimesOne = cp.vstack([P[:, 0, 0] + P[:, 1, 1]] * 9).T
        OneTimesOne = 2.0

        self.Af_eq = self.weights * (
            self.rho[:, None]
            + jTimesCi / (b**2)
            + 1.0
            / (2.0 * b**4)
            * (
                PTimesCiCi
                - b**2 * PTimesOne
                - self.rho[:, None] * b**2 * OneTimesCiCi
                + self.rho[:, None] * b**4 * OneTimesOne
            )
        )

    def collide(self):
        ASS = self._compute_SS()
        b = self.par["csh"]
        CiTimesSS = ASS[:, 0].reshape(ASS.shape[0], 1) @ self.c[:, 0].reshape(1, self.c.shape[0]) + ASS[:, 1].reshape(
            ASS.shape[0], 1
        ) @ self.c[:, 1].reshape(1, self.c.shape[0])
        Si = self.weights / b**2 * CiTimesSS
        self.Af = (
            self.Af
            - self.par["dth"] / self.par["tauh"] * (self.Af - self.Af_eq)
            + self.par["dth"] * (1.0 - self.par["dth"] / (2.0 * self.par["tauh"])) * Si
        )

    def stream(self):
        self.Af_tmp = self.Af.copy()
        self.Af_tmp[:, 1:9] = -1  # set missing distribution functions ton -1
        for i in range(1, 9):
            cp.put(
                self.Af_tmp[:, i],
                self.Ageo[:, i][self.Ageo[:, i] >= 0],
                self.Af[:, i][self.Ageo[:, i] >= 0],
            )

    def _add_points_to_bc(self):
        """Reads in the .msh file and adds all point id's to their respective boundary. In addition the boundary point
        normals are fetched from the .msh file and added to the Boundary Condition as well"""

        mesh_file = os.path.join(self.working_dir, self.name + ".msh")
        with open(mesh_file, "r") as current_file:
            for line in current_file:  # create point objects
                if line.startswith("#"):
                    continue
                s = line.split()
                bc_ids = None
                point_id = int(s[0])
                number_of_boundary_distributions = cp.count_nonzero(self.Ageo[point_id] == -1).item()
                if number_of_boundary_distributions == 0:
                    continue  # continue if not a boundary point

                # get id's of the boundary condition each distribution belongs to
                boundary_ids = [int(s[i]) for i in range(12, 12 + number_of_boundary_distributions)]
                # get id's of the missing distribution functions via lookup in Ageo
                # reverse the directions since the missing ones after streaming are the oppsoite ones
                distribution_id_list = list(self.opposite_directions[(self.Ageo[point_id] == -1).nonzero()[0]])

                number_of_boundary_conditions = len(np.unique(boundary_ids, axis=0))

                if number_of_boundary_conditions == 1:
                    normals = [
                        float(s[12 + 4 * s.count("-1")]),
                        float(s[12 + 4 * s.count("-1") + 1]),
                    ]
                    bc_ids = [boundary_ids[0]]

                elif number_of_boundary_conditions == 2:
                    bc_ids = sorted(list(set(boundary_ids)))  # poor programming
                    i_list = [boundary_ids.index(bc_id) for bc_id in bc_ids]
                    normals = []
                    for i in i_list:
                        normals.extend(
                            [
                                float(s[12 + 4 * s.count("-1") + i * 2]),
                                float(s[12 + 4 * s.count("-1") + i * 2 + 1]),
                            ]
                        )

                else:
                    print("Boundary Condition Reading Error - More than two at one point")

                # check for corner points and add them a second time to the boundaries  -> very bad coding
                corner_distri_id = None
                if number_of_boundary_conditions == 2:
                    if 1 in distribution_id_list and 4 in distribution_id_list:  # top left -> 8 double
                        corner_distri_id = 8
                    elif 2 in distribution_id_list and 1 in distribution_id_list:  # bottom left -> 5 double
                        corner_distri_id = 5
                    elif 3 in distribution_id_list and 4 in distribution_id_list:  # top right -> 7 double
                        corner_distri_id = 7
                    elif 3 in distribution_id_list and 2 in distribution_id_list:  # bottom right -> 6 double
                        corner_distri_id = 6
                    if corner_distri_id:
                        self.corner_points.append(point_id)
                        self.corner_distris.append(corner_distri_id)

                if corner_distri_id != None:
                    for distri_id, p_bc_id in zip(distribution_id_list, boundary_ids):
                        if distri_id == corner_distri_id:
                            missing_double_boundaries = list(np.unique(boundary_ids, axis=0))
                            missing_double_boundaries.remove(p_bc_id)
                            for mdb in missing_double_boundaries:
                                distribution_id_list.append(corner_distri_id)
                                boundary_ids.append(mdb)
                            break

                # add point to detected boundary condition
                if bc_ids != None:
                    for BC in self.boundary_conditions:
                        for i, bc_id in enumerate(bc_ids):
                            if bc_id == BC.ID:
                                tmp_missing_distri = []
                                BC.point_ids.append(point_id)
                                for distri_id, p_bc_id in zip(distribution_id_list, boundary_ids):
                                    if p_bc_id == bc_id:
                                        tmp_missing_distri.append(distri_id)
                                BC.Amissingdisti.append(tmp_missing_distri)
                                BC.An.append([normals[i * 2], normals[i * 2 + 1]])

                            if number_of_boundary_conditions > 1:
                                BC.double_ids.append(point_id)

            # make numpy arrays from the point id list for later computations and clean up
            for BC in self.boundary_conditions:
                if len(BC.point_ids) > 0:
                    BC.point_ids = cp.array(BC.point_ids)
                    BC.An = cp.array(BC.An) / self.par["L"]
                    max_missing = max([len(x) for x in BC.Amissingdisti])
                    for distri_list in BC.Amissingdisti:
                        while len(distri_list) < max_missing:
                            distri_list.append(-1)
                    # clean for cupy
                    BC.Amissingdisti = [[y if type(y) == int else y.get() for y in x] for x in BC.Amissingdisti]
                    BC.Amissingdisti = cp.array(BC.Amissingdisti)
                    if len(BC.double_ids) > 0:
                        BC.double_ids = cp.unique(cp.array(BC.double_ids))
                    else:
                        BC.double_ids = cp.array([])

            self.corner_points = cp.array(self.corner_points)
            self.corner_distris = cp.array(self.corner_distris)

    def _add_points_to_bc_2(self):
        """Reads in the .msh file and adds all point id's to their respective boundary. In addition the boundary point
        normals are fetched from the .msh file and added to the Boundary Condition as well"""

        mesh_file = os.path.join(self.working_dir, self.name + ".msh")
        with open(mesh_file, "r") as current_file:
            for line in current_file:  # create point objects
                if line.startswith("#"):
                    continue
                s = line.split()
                bc_ids = None
                point_id = int(s[0])
                if len(s) == 30:  # one boundary condition
                    bc_ids = [int(s[12])]
                    normals = [
                        float(s[12 + 4 * s.count("-1")]),
                        float(s[12 + 4 * s.count("-1") + 1]),
                    ]
                elif len(s) == 42:  # two boundary conditions
                    bc_ids_tmp = [int(s[i]) for i in [12, 13, 14, 15, 16]]
                    bc_ids = sorted(list(set(bc_ids_tmp)))  # poor programming
                    i_list = [bc_ids_tmp.index(bc_id) for bc_id in bc_ids]
                    normals = []
                    for i in i_list:
                        normals.extend(
                            [
                                float(s[12 + 4 * s.count("-1") + i * 2]),
                                float(s[12 + 4 * s.count("-1") + i * 2 + 1]),
                            ]
                        )

                # add point to detected boundary condition
                if bc_ids != None:
                    for BC in self.boundary_conditions:
                        for i, bc_id in enumerate(bc_ids):
                            if bc_id == BC.ID:
                                BC.point_ids.append(point_id)
                                BC.An.append([normals[i * 2], normals[i * 2 + 1]])

        # make numpy arrays from the point id list for later computations and clean up
        for BC in self.boundary_conditions:
            BC.point_ids = cp.array(BC.point_ids)
            BC.An = cp.array(BC.An) / self.par["L"]

    def init_boundary_conditions(self):
        # self._create_boun_geo_arr()
        self._read_boundary_condition_file()
        self._add_points_to_bc()
        # self._create_boundary_points_matrix()
        # self._create_boundary_points_matrix_2()

    def apply_boundary_conditions(self):
        # compute current value of boundary conditions
        for BC in self.boundary_conditions:
            BC.compute_current_value_par(self.current_time, self.par)  # TODO: Bad coding style

        for BC in self.boundary_conditions:
            if BC.BCType == "Dirichlet_Con" and len(BC.point_ids) > 0:
                n_max_missing = BC.Amissingdisti.shape[1]
                for i in range(n_max_missing):
                    MD = BC.Amissingdisti[:, i][BC.Amissingdisti[:, i] != -1]
                    MDO = self.opposite_directions[MD]
                    P = BC.point_ids[BC.Amissingdisti[:, i] != -1]

                    # set initial fields to 0 only if -1 (edge case may have an value from prior applied BC)
                    init_ids = cp.where(self.Af_tmp[P, MD] == -1)[0]
                    self.Af_tmp[P[init_ids], MD[init_ids]] = 0.0

                    # bounce back
                    self.Af_tmp[P, MD] = self.Af_tmp[P, MD] + self.Af[P, MDO]

                # sys.exit()
                # continue
                # weights  = self.weights
                # b = self.par['csh']

                # n_max_missing = BC.Amissingdisti.shape[1]

                # for i in range(n_max_missing): # care about edge cases etc.
                #    MD = BC.Amissingdisti[:,i][BC.Amissingdisti[:,i] != -1]
                #    MDO = self.opposite_directions[MD]
                #    P = BC.point_ids[BC.Amissingdisti[:,i] != -1]
                #    j_bd = BC.CurrentValue

                #    c_current = self.c[MD]

                #    #phase change bounce back
                #    self.Af_tmp[P,MD] = self.Af_tmp[P,MD] - self.Af[P,MDO] + 2 * weights[MDO] * rho_help

                #    #add density- and stress-dependent term (quasi-equilibrium-distribution-function)
                #    self.Af_tmp[P,MD] = self.Af_tmp[P,MD] + 2 * weights[MDO] * (P_n_bd * (c_current[:,:,None] @ c_current[:,None,:] - np.array([np.eye(2)]*sigma_tmp.shape[0]) * (b**2)) / (2 * b**4)).sum(axis = 1).sum(axis=1)

            elif BC.BCType == "Neumann_Con" and len(BC.point_ids) > 0:
                # some parameters
                weights = self.weights

                b = self.par["csh"]
                # change dim, rho0 -> rho0h
                rho0 = self.par["rho0h"]
                mue_ = self.par["mueh"]
                lambda_ = self.par["lambdah"]
                t = BC.CurrentValue

                # tangent vector
                At = cp.zeros((len(BC.point_ids), 2))
                At[:, 0] = -BC.An[:, 1]
                At[:, 1] = BC.An[:, 0]

                # transformation Matrix
                m_bwd = cp.zeros((len(BC.point_ids), 2, 2))
                m_bwd[:, 0, :] = BC.An
                m_bwd[:, 1, :] = At

                # transform from x,y into n,t system
                t_new = m_bwd @ cp.array(t)

                # insert into local sigma
                sigma_lok = cp.zeros((len(BC.point_ids), 2, 2))
                sigma_lok[:, 0, 0] = t_new[:, 0]
                sigma_lok[:, 1, 0] = t_new[:, 1]
                sigma_lok[:, 0, 1] = t_new[:, 1]

                n_max_missing = BC.Amissingdisti.shape[1]

                for i in range(n_max_missing):
                    filter_ind = BC.Amissingdisti[:, i] != -1
                    MD = BC.Amissingdisti[:, i][filter_ind]
                    # filter -1 indices
                    tmpMD = BC.Amissingdisti.copy()
                    tmpMD[cp.where(tmpMD < 0)] = 100  # should be bigger than all distri functions indices
                    minMD = cp.min(
                        tmpMD, axis=1
                    )[
                        filter_ind
                    ]  # kinda hardcoded since -1 (point where less than 3 missing disti functions are present) are not filtered
                    MDO = self.opposite_directions[MD]
                    P = BC.point_ids[BC.Amissingdisti[:, i] != -1]

                    rho_self = cp.sum(self.Af[P, :], axis=1)

                    Pself = cp.einsum("xi,ia,ib->xab", self.Af[P, :], self.c, self.c)
                    Pnb = cp.einsum("xi,ia,ib->xab", self.Af[self.Ageo[P, minMD], :], self.c, self.c)

                    # calculate neigbor density get neighbors
                    rho_nbb = cp.sum(self.Af[self.Ageo[P, minMD], :], axis=1)  # this needs to be done correctly

                    sigma_self = (
                        -Pself
                        + cp.array([cp.eye(2)] * P.shape[0])
                        * (mue_ - lambda_)
                        * (rho_self[:, None, None] - rho0)
                        / rho0
                    )
                    sigma_nb = (
                        -Pnb
                        + cp.array([cp.eye(2)] * P.shape[0]) * (mue_ - lambda_) * (rho_nbb[:, None, None] - rho0) / rho0
                    )

                    # transform into local coordinate system
                    sigma_self = m_bwd[filter_ind] @ sigma_self @ m_bwd[filter_ind].transpose(0, 2, 1)
                    sigma_nb = m_bwd[filter_ind] @ sigma_nb @ m_bwd[filter_ind].transpose(0, 2, 1)

                    # interpolate new entry
                    sigma_t = 3.0 * sigma_self[:, 1, 1] / 2 - sigma_nb[:, 1, 1] / 2.0

                    ################################################################################

                    # insert into local sigma
                    sigma_lok[filter_ind, 1, 1] = sigma_t

                    # transform back into global coordinate system
                    sigma_tmp = m_bwd[filter_ind] @ sigma_lok[filter_ind] @ m_bwd[filter_ind].transpose(0, 2, 1)

                    # set initial fields to 0 only if -1 (edge case may have an value from prior applied BC)
                    init_ids = cp.where(self.Af_tmp[P, MD] == -1)[0]
                    self.Af_tmp[P[init_ids], MD[init_ids]] = 0.0

                    # calculate neigbor density get neighbors
                    rho_nb = cp.sum(self.Af[self.Ageo[P, MD], :], axis=1)  # this needs to be done correctly

                    # check for ids where opposite functions are missing (edge)
                    re = cp.where(self.Ageo[P, MD] == -1)  # fitlers error ids

                    # density at boundary for stress calculation
                    rho_help = 1.5 * rho_self - 0.5 * rho_nb  # this works for edge case too, since rho_nb = rho_self

                    # correct wrong interpoalted values by own value
                    rho_help[re] = rho_self[re]

                    # Note np.array([np.eye(2)]*sigma_tmp.shape[0]) -> 3 dimension ident matrix
                    P_bd = (
                        -sigma_tmp
                        + cp.array([cp.eye(2)] * sigma_tmp.shape[0])
                        * (mue_ - lambda_)
                        * (rho_help[:, None, None] - rho0)
                        / rho0
                    )

                    # P^n
                    P_n_bd = P_bd - rho_help[:, None, None] * (b**2) * cp.array([cp.eye(2)] * sigma_tmp.shape[0])

                    # current grid velocity
                    c_current = self.c[MD]

                    # phase change bounce back
                    self.Af_tmp[P, MD] = self.Af_tmp[P, MD] - self.Af[P, MDO] + 2 * weights[MDO] * rho_help

                    # add density- and stress-dependent term (quasi-equilibrium-distribution-function)
                    self.Af_tmp[P, MD] = self.Af_tmp[P, MD] + 2 * weights[MDO] * (
                        P_n_bd
                        * (
                            (c_current[:, :, None] @ c_current[:, None, :])
                            - cp.array([cp.eye(2)] * sigma_tmp.shape[0]) * (b**2)
                        )
                        / (2 * b**4)
                    ).sum(axis=1).sum(axis=1)

            else:
                print("BC with ID {} and type [{}] not implemented yet".format(BC.ID, BC.BCType))

        # corner distribution functions:
        if len(self.corner_points > 0):
            self.Af_tmp[self.corner_points, self.corner_distris] = (
                0.5 * self.Af_tmp[self.corner_points, self.corner_distris]
            )

    def integrate(self):
        self.rho = self.Af.sum(axis=1)
        SS = self._compute_SS()

        # compute first moment
        fm = cp.einsum("xi,ia->xa", self.Af, self.c)
        j_tpdt = fm + self.par["dth"] / 2.0 * SS
        self.output_dict["j"] = j_tpdt  # to write it into vtk object in another function
        wdot = j_tpdt / self.rho[:, None]

        # integration euler backwards
        self.Aw = self.Aw + self.par["dth"] * wdot
        self.output_dict["w"] = self.Aw

        # calculation of sigma
        P = cp.zeros((self.Af.shape[0], 2, 2))
        P[:, 0, 0] = cp.sum(self.Af * self.c_tensors[:, 0, 0], axis=1)
        P[:, 0, 1] = cp.sum(self.Af * self.c_tensors[:, 0, 1], axis=1)
        P[:, 1, 0] = cp.sum(self.Af * self.c_tensors[:, 1, 0], axis=1)
        P[:, 1, 1] = cp.sum(self.Af * self.c_tensors[:, 1, 1], axis=1)

        nu = 0.5 * self.par["lambdah"] / (self.par["lambdah"] + self.par["mueh"])

        sigma = cp.zeros((self.Af.shape[0], 2, 2))
        sigma = (
            -P
            + cp.identity(2)[None, :, :]
            * (self.par["mueh"] - self.par["lambdah"])
            * (self.rho[:, None, None] - self.par["rho0h"])
            / self.par["rho0h"]
        )
        self.output_dict["sigma_vm"] = cp.sqrt(
            (1 - nu + nu**2) * (sigma[:, 0, 0] ** 2 + sigma[:, 1, 1] ** 2)
            - (1.0 + 2.0 * nu - 2.0 * nu**2) * (sigma[:, 0, 0] * sigma[:, 1, 1])
            + 3.0 * sigma[:, 0, 1] ** 2
        )
        self.output_dict["sigma_vm"] = self.output_dict["sigma_vm"] * (self.par["mue"] * self.par["wr"] / self.par["L"])
        self.output_dict["sigma_x"] = sigma[:, 0, 0]
        self.output_dict["sigma_x"] = self.output_dict["sigma_x"] * (self.par["mue"] * self.par["wr"] / self.par["L"])

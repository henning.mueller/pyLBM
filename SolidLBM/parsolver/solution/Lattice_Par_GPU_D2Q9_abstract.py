import cupy as cp
import sys
import os
import logging

import SolidLBM.parsolver.solution.Lattice_Par_GPU_abstract as Lattice_Par_GPU_abstract_Module


class Lattice_Par_GPU_D2Q9_abstract(Lattice_Par_GPU_abstract_Module.Lattice_Par_GPU_abstract):
    """Specified Lattice Class for 2D Simulations

    TODO: to be defined
    """

    def __init__(self, working_dir, name):
        super(Lattice_Par_GPU_D2Q9_abstract, self).__init__(working_dir, name)
        # D2Q9 parameters
        self.weights = cp.array(
            [
                4.0 / 9.0,
                1.0 / 9.0,
                1.0 / 9.0,
                1.0 / 9.0,
                1.0 / 9.0,
                1.0 / 36.0,
                1.0 / 36.0,
                1.0 / 36.0,
                1.0 / 36.0,
            ]
        )
        self.lattice_directions = cp.array(
            [
                [0, 0],
                [1, 0],
                [0, 1],
                [-1, 0],
                [0, -1],
                [1, 1],
                [-1, 1],
                [-1, -1],
                [1, -1],
            ]
        )
        self.opposite_directions = cp.array([0, 3, 4, 1, 2, 7, 8, 5, 6])
        self.opp_interpol_dircetions = cp.array([0, 3, 4, 1, 2, 3, 4, 1, 2])

    def __init__(self, parameter_file="", mesh_file=""):
        super(Lattice_Par_CPU_D2Q9_abstract, self).__init__(parameter_file)

        self._read_lattice_geo_from_file(mesh_file)
        self.num_points = self.Ageo.shape[0]
        self._init_Af()
        self._init_Aw()

        self.grad_coeffs = self._compute_gradient_coefficients()

        # check if initial field file exists and read it

    def _read_initial_field(self, initial_file):
        """TODO: Complete this"""
        with open(initial_file) as f:
            for line in f:
                if "#" in line:
                    continue
            split = line.split()
            pass

    def _read_lattice_geo_from_file(self, mesh_file):
        """
        Reads geometry of Lattice from a specified file and stores it in Ageo.
        """

        with open(mesh_file, "r") as current_file:
            geo_list = []
            coord_list = []
            for line in current_file:  # create point objects
                if line.startswith("#"):
                    if "CellSize" in line:
                        self.cell_size = float(line.split()[3]) / self.par["L"]  # relate to total domain size
                    continue
                s = line.split()
                coord_list.append(
                    [
                        float(s[1]) / self.par["L"],
                        float(s[2]) / self.par["L"],
                        float(s[3]) / self.par["L"],
                    ]
                )
                nb_list = [
                    int(s[4]),
                    int(s[5]),
                    int(s[6]),
                    int(s[7]),
                    int(s[8]),
                    int(s[9]),
                    int(s[10]),
                    int(s[11]),
                ]
                if -1 in nb_list:
                    geo_list.append([1] + nb_list)
                else:
                    geo_list.append([0] + nb_list)
            self.Ageo = cp.array(geo_list)
            self.Acoord = cp.array(coord_list)
        logging.info('Reading mesh geometry and connective data from "{}"'.format(mesh_file))

    def _init_Af(self):
        self.Af = cp.zeros((self.num_points, 9))

    def _init_Aw(self):
        self.Aw = cp.zeros((self.num_points, 2))
        self.Awdot = cp.zeros((self.num_points, 2))

    def _get_neighbor_index(self, arr):
        condlist = [arr != -1, arr == -1]
        choicelist = [arr, np.arange(self.num_points)]
        return np.select(condlist, choicelist)

    def _compute_gradient_coefficients(self):
        # only first order at boundary
        # double cell size at inner nodes
        dx = np.ones((self.num_points, 2)) / (self.cell_size * 2)
        dy = np.ones((self.num_points, 2)) / (self.cell_size * 2)

        # single cell size at boundary nodes (first order)
        dx[:, 1] = dx[:, 1] * -1
        dy[:, 1] = dy[:, 1] * -1

        # set to one cell size at boundarys
        dx[self.Ageo[:, 1] == -1] = dx[self.Ageo[:, 1] == -1] * 2
        dx[self.Ageo[:, 3] == -1] = dx[self.Ageo[:, 3] == -1] * 2

        dy[self.Ageo[:, 2] == -1] = dy[self.Ageo[:, 2] == -1] * 2
        dy[self.Ageo[:, 4] == -1] = dy[self.Ageo[:, 4] == -1] * 2
        return dx, dy

import numpy as np
import logging

from SolidLBM.parsolver.solution.Lattice_Par_CPU_D2Q9_abstract import (
    Lattice_Par_CPU_D2Q9_abstract,
)
from SolidLBM.util.tools import ensure3d


class Lattice_Par_CPU_Ansumali(Lattice_Par_CPU_D2Q9_abstract):
    """Lattice_Par_CPU_Ansumali class for lattice Boltzmann method on CPU using Ansumali's approach.

    Attributes:
        rho (np.array): Array representing the density field.
        momentum (np.array): Array representing the momentum field.
        stress (np.array): Array representing the stress tensor field.
        corner_points (list): List of corner points for boundary conditions.
        corner_distris (list): List of corner distributions for boundary conditions.
        c_vec (np.array): Precomputed lattice velocities.
        c_tensors (np.array): Precomputed dyad of lattice velocities.
        Af (np.array): Distribution functions.
        Af_eq (np.array): Equilibrium distribution functions.
        Af_tmp (np.array): Temporary distribution functions for streaming.
        output_dict (dict): Dictionary for storing output data.

    Methods:
        __init__(parameter_file="", mesh_file="", *, switches=dict()):
            Initializes the lattice with given parameters and mesh files.
        __repr__():
            Returns a string representation of the class.
        _compute_dependent_parameters(**kwargs):
            Computes additional parameters needed for the numerical model.
        _compute_lattice_velocity():
            Precomputes the lattice velocities.
        _compute_c_tensors():
            Precomputes the dyad of lattice velocities.
        initialise_distribution_functions_and_fields():
            Initializes the distribution functions and fields.
        _compute_source():
            Computes the source term for the lattice Boltzmann equation.
        compute_eq_functions():
            Computes the equilibrium distribution functions from moments.
        regularize_precollision():
            Regularizes the distribution functions before collision.
        collide():
            Performs the collision step.
        stream():
            Performs the streaming step.
        _add_points_to_bc(boundary_conditions, mesh_file):
            Adds points to boundary conditions from the mesh file.
        _process_periodic_boundaries(bc_list):
            Processes periodic boundaries.
        _add_points_to_bc_2(boundary_conditions, mesh_file):
            Adds points to boundary conditions from the mesh file (alternative method).
        apply_boundary_conditions(bc):
            Applies the boundary conditions.
        _apply_dirichlet_bc(bc):
            Applies Dirichlet boundary conditions.
        _apply_neumann_bc(bc):
            Applies Neumann boundary conditions.
        _apply_neumann_x_bc(bc):
            Applies Neumann boundary conditions with additional processing.
        _process_corner_points():
            Processes corner points for boundary conditions.
        integrate(*, method="trapez"):
            Integrates the displacement field and sets related data for output.
        _compute_zeroth_moment():
            Computes the zeroth moment (density).
        _compute_first_moment():
            Computes the first moment (momentum).
        _compute_second_moment():
            Computes the second moment (stress tensor).
        _compute_second_moment_eq():
            Computes the equilibrium second moment.
        compute_cauchy():
            Computes the Cauchy stress tensor.
    """

    def __init__(self, parameter_file="", mesh_file="", *, switches=dict()):
        """Initialize the Lattice_Par_CPU_Ansumali class.

        Parameters:
        parameter_file (str): Path to the parameter file. Default is an empty string.
        mesh_file (str): Path to the mesh file. Default is an empty string.
        switches (dict): Optional keyword arguments for various switches. Default is an empty dictionary.

        Keyword Args:
        set_tau_symmetric (bool): Flag to set tau symmetric. Default is True.

        Initializes:
        self.rho (numpy.ndarray): Array of initial densities.
        self.momentum (numpy.ndarray): Array of initial momenta.
        self.stress (numpy.ndarray): Array of initial stresses.
        self.corner_points (list): List to handle corner points for boundary conditions.
        self.corner_distris (list): List to handle corner distributions for boundary conditions.

        Calls:
        self._compute_dependent_parameters(tau_sym): Computes dependent parameters based on tau_sym.
        self._compute_lattice_velocity(): Precomputes lattice velocities.
        self._compute_c_tensors(): Precomputes c tensors.

        Logs:
        "Initialization working" at debug level when initialization is complete.
        """
        super(Lattice_Par_CPU_Ansumali, self).__init__(parameter_file, mesh_file)

        # tau_sym = switches.get("set_tau_symmetric", True)
        # self._compute_dependent_parameters(tau_sym=tau_sym)
        self._compute_dependent_parameters()

        # initialize moments
        self.rho = np.array([self.par["rho0h"]] * self.num_points)
        self.momentum = np.zeros((self.num_points, 2))
        self.stress = np.zeros((self.num_points, 2, 2))

        # precompute lattice velocities
        self._compute_lattice_velocity()
        self._compute_c_tensors()

        # initialize corner handling for boundary conditions
        self.corner_points = []
        self.corner_distris = []

        logging.debug("Initialization working")

    def __repr__(self):
        return "Lattice_Par_CPU_Ansumali"

    def _compute_dependent_parameters(self, **kwargs):
        """compute additional parameters, that are needed for the numerical model"""
        self.par["rho0h"] = 1.0
        self.par["lambdah"] = self.par["lambda"] / self.par["mue"]
        self.par["mueh"] = 1.0

        self.par["cs"] = np.sqrt(self.par["mue"] / self.par["rho0"])
        self.par["cd"] = np.sqrt((self.par["lambda"] + 2 * self.par["mue"]) / self.par["rho0"])
        self.par["csh"] = 1.0
        self.par["cdh"] = self.par["cd"] / self.par["cs"]

        self.par["h"] = self.cell_size
        self.par["hh"] = self.par["h"] / self.par["L"]
        self.par["dth"] = 1 / np.sqrt(3) * self.par["hh"] / self.par["csh"]
        self.par["ch"] = self.par["hh"] / self.par["dth"]

        if "tau" not in self.par:
            self.par["tau"] = 0.55
        self.par["tauh"] = self.par["tau"] * self.par["dth"]

        logging.debug("SRT parameters set")

    def _compute_lattice_velocity(self):
        """precompute the lattice velocities"""
        self.c_vec = self.lattice_directions * self.par["ch"]

    def _compute_c_tensors(self):
        """precompute the dyad of lattice velocities"""
        cc = self.par["ch"]
        self.c_tensors = np.array(
            [
                [
                    [0.0, 0.0],  # c0c0
                    [0.0, 0.0],
                ],
                [
                    [cc**2, 0.0],  # c1c1
                    [0.0, 0.0],
                ],
                [
                    [0.0, 0.0],  # c2c2
                    [0.0, cc**2],
                ],
                [
                    [cc**2, 0.0],  # c3c3
                    [0.0, 0.0],
                ],
                [
                    [0.0, 0.0],  # c4c4
                    [0.0, cc**2],
                ],
                [
                    [cc**2, cc**2],  # c5c5
                    [cc**2, cc**2],
                ],
                [
                    [cc**2, -(cc**2)],  # c6c6
                    [-(cc**2), cc**2],
                ],
                [
                    [cc**2, cc**2],  # c7c7
                    [cc**2, cc**2],
                ],
                [
                    [cc**2, -(cc**2)],  # c8c8
                    [-(cc**2), cc**2],
                ],
            ]
        )

    def _compute_zeroth_moment(self):
        return self.Af.sum(axis=1)

    def _compute_first_moment(self):
        source = self._compute_source()
        fm = np.array(
            [
                (self.Af * self.c_vec[:, 0]).sum(axis=1),
                (self.Af * self.c_vec[:, 1]).sum(axis=1),
            ]
        ).T
        momentum = fm + self.par["dth"] / 2.0 * source
        return momentum

    def _compute_second_moment(self):
        mom_flux = np.zeros((self.Af.shape[0], 2, 2))
        mom_flux[:, 0, 0] = np.sum(self.Af * self.c_tensors[:, 0, 0], axis=1)
        mom_flux[:, 0, 1] = np.sum(self.Af * self.c_tensors[:, 0, 1], axis=1)
        mom_flux[:, 1, 0] = np.sum(self.Af * self.c_tensors[:, 1, 0], axis=1)
        mom_flux[:, 1, 1] = np.sum(self.Af * self.c_tensors[:, 1, 1], axis=1)

        return mom_flux

    def _compute_second_moment_eq(self):
        mom_flux_eq = np.zeros((self.Af_eq.shape[0], 2, 2))
        mom_flux_eq[:, 0, 0] = np.sum(self.Af_eq * self.c_tensors[:, 0, 0], axis=1)
        mom_flux_eq[:, 0, 1] = np.sum(self.Af_eq * self.c_tensors[:, 0, 1], axis=1)
        mom_flux_eq[:, 1, 0] = np.sum(self.Af_eq * self.c_tensors[:, 1, 0], axis=1)
        mom_flux_eq[:, 1, 1] = np.sum(self.Af_eq * self.c_tensors[:, 1, 1], axis=1)

        return mom_flux_eq

    def initialise_distribution_functions_and_fields(self):
        rho0 = self.par["rho0h"]
        b = self.par["csh"]
        j = rho0 * self.Awdot

        j0TimesCi = np.vstack([j[:, 0]] * 9).T * self.c_vec[:, 0] + np.vstack([j[:, 1]] * 9).T * self.c_vec[:, 1]
        OneTimesCiCi = self.c_tensors[:, 0, 0] + self.c_tensors[:, 1, 1]
        OneTimesOne = 2.0

        self.Af = self.weights * (
            rho0 + j0TimesCi / b**2 + 1 / (2 * b**4) * (-rho0 * b**2 * OneTimesCiCi + rho0 * b**4 * OneTimesOne)
        )
        # TODO set initial values for rho, j, P here from f

    def compute_eq_functions(self):
        """compute equilibrium functions from moments

        moments are taken from instance attributes, which are set at the last
        time step or from initialization
        """
        # get moments from integration of last time step
        b = self.par["csh"]
        rho = self.rho
        Ajx, Ajy = self.momentum.T
        P = self.stress

        # compute eq distribution
        jTimesCi = Ajx[:, None] * self.c_vec[:, 0] + Ajy[:, None] * self.c_vec[:, 1]
        PTimesCiCi = (
            P[:, 0, 0][:, None] * self.c_tensors[:, 0, 0]
            + P[:, 0, 1][:, None] * self.c_tensors[:, 1, 0]
            + P[:, 1, 0][:, None] * self.c_tensors[:, 0, 1]
            + P[:, 1, 1][:, None] * self.c_tensors[:, 1, 1]
        )
        OneTimesCiCi = self.c_tensors[:, 0, 0] + self.c_tensors[:, 1, 1]
        PTimesOne = (P[:, 0, 0] + P[:, 1, 1])[:, None]
        OneTimesOne = 2.0

        self.Af_eq = self.weights * (
            rho[:, None] + jTimesCi / (b**2)
            + 1  / (2 * b**4)
                * (PTimesCiCi - b**2 * PTimesOne - rho[:, None] * b**2 * OneTimesCiCi + rho[:, None] * b**4 * OneTimesOne)
        )

    def _compute_source(self):
        dx, dy = self.grad_coeffs

        sumf = self.Af.sum(axis=1)

        neighbor_indices_1 = self._get_neighbor_index(self.Ageo[:, 1])
        neighbor_indices_3 = self._get_neighbor_index(self.Ageo[:, 3])
        neighbor_indices_2 = self._get_neighbor_index(self.Ageo[:, 2])
        neighbor_indices_4 = self._get_neighbor_index(self.Ageo[:, 4])

        Arho_x = sumf[neighbor_indices_1] * dx[:, 0] + sumf[neighbor_indices_3] * dx[:, 1]
        Arho_y = sumf[neighbor_indices_2] * dy[:, 0] + sumf[neighbor_indices_4] * dy[:, 1]

        factor = (self.par["mueh"] - self.par["lambdah"]) / self.par["rho0h"]
        Asource = np.zeros((self.num_points, 2))
        Asource[:, 0] = factor * Arho_x
        Asource[:, 1] = factor * Arho_y

        # TODO: add BulkForce from Source
        return Asource

    def _compute_forcing(self):
        """project the source term onto the lattice directions"""
        Asource = self._compute_source()
        b = self.par["csh"]
        CiTimesS = np.einsum('ij,kj->ik', Asource, self.c_vec)
        Si = self.weights / b**2 * CiTimesS
        return Si
    
    def collide(self):
        """collision step"""
        Si = self._compute_forcing()

        dt_tauh = self.par["dth"] / self.par["tauh"]
        dt_tauh_half = self.par["dth"] / (2.0 * self.par["tauh"])
        one_minus_dt_tauh_half = 1.0 - dt_tauh_half

        self.Af = (
            self.Af
            - dt_tauh * (self.Af - self.Af_eq)
            + self.par["dth"] * one_minus_dt_tauh_half * Si
        )

    def stream(self):
        self.Af_tmp = self.Af.copy()
        self.Af_tmp[:, 1:9] = -1  # set missing distribution functions to -1
        for i in range(1, 9):
            np.put(
                self.Af_tmp[:, i],
                self.Ageo[:, i][self.Ageo[:, i] >= 0],
                self.Af[:, i][self.Ageo[:, i] >= 0],
            )

    def _process_periodic_boundaries(self, bc_list):
        """Find the neighbors for points which have a periodic boundary defined."""
        periodic_bc = tuple(bc for bc in bc_list if bc.BCType == "periodic")

        # fixed order of distributions along straight boundary
        _missing_distribs_ordered = {
            1: (1, 5, 8),
            2: (2, 6, 5),
            3: (3, 6, 7),
            4: (4, 7, 8),
        }

        if len(periodic_bc) == 2:
            oppposite_boun = [periodic_bc]
        else:
            # find opposite bcs via normals and put as tuple in list
            pass

        for bc_1, bc_2 in oppposite_boun:
            assert (bc_1.An[0] == -bc_2.An[0]).all()

            # get ordered indices from lateral index
            #   -> lateral, plus, minus
            # needed to assign point IDs properly in diagonal directions
            miss_distrib_1 = bc_1.Amissingdisti[0].min()
            k_1, l_1, m_1 = _missing_distribs_ordered[miss_distrib_1]
            miss_distrib_2 = bc_2.Amissingdisti[0].min()
            k_2, l_2, m_2 = _missing_distribs_ordered[miss_distrib_2]

            pt_ids_1 = bc_1.point_ids
            pt_ids_2 = bc_2.point_ids

            # assign point IDs in geo array for each direction
            # leave out diagonals at corners
            # note: streaming keys are are needed here, thus definition reversed
            self.Ageo[pt_ids_1, k_2] = pt_ids_2
            self.Ageo[pt_ids_2, k_1] = pt_ids_1
            self.Ageo[pt_ids_1[1:], l_2] = pt_ids_2[:-1]
            self.Ageo[pt_ids_2[1:], l_1] = pt_ids_1[:-1]
            self.Ageo[pt_ids_1[:-1], m_2] = pt_ids_2[1:]
            self.Ageo[pt_ids_2[:-1], m_1] = pt_ids_1[1:]

            # find all points in bc (converting to lists seems easier)
            # and remove points from corner handling
            periodic_pts = np.array([bc.point_ids for bc in periodic_bc]).flatten()
            corner_pts = self.corner_points.tolist()
            corner_dist = self.corner_distris.tolist()
            for c_pt, c_dist in zip(self.corner_points, self.corner_distris):
                if c_pt in periodic_pts:
                    corner_pts.remove(c_pt)
                    corner_dist.remove(c_dist)
            self.corner_points = np.array(corner_pts)
            self.corner_distris = np.array(corner_dist)

    def _add_points_to_bc(self, boundary_conditions, mesh_file):
        """Reads in the .msh file and adds all point id's to their respective boundary. In addition the boundary point
        normals are fetched from the .msh file and added to the Boundary Condition as well"""

        with open(mesh_file, "r") as current_file:
            for line in current_file:  # create point objects
                if line.startswith("#"):
                    continue
                s = line.split()
                bc_ids = None
                point_id = int(s[0])
                number_of_boundary_distributions = np.count_nonzero(self.Ageo[point_id] == -1)
                if number_of_boundary_distributions == 0:
                    continue  # not a boundary point

                # get id's of the boundary condition each distribution belongs to
                boundary_ids = [int(s[i]) for i in range(12, 12 + number_of_boundary_distributions)]
                # get id's of the missing distribution functions via lookup in Ageo
                # reverse the directions since the missing ones after streaming are the oppsoite ones
                distribution_id_list = list(self.opposite_directions[(self.Ageo[point_id] == -1).nonzero()[0]])

                number_of_boundary_conditions = len(np.unique(boundary_ids, axis=0))

                if number_of_boundary_conditions == 1:
                    normals = [
                        float(s[12 + 4 * s.count("-1")]),
                        float(s[12 + 4 * s.count("-1") + 1]),
                    ]
                    bc_ids = [boundary_ids[0]]

                elif number_of_boundary_conditions == 2:
                    bc_ids = sorted(list(set(boundary_ids)))  # poor programming
                    i_list = [boundary_ids.index(bc_id) for bc_id in bc_ids]
                    normals = []
                    for i in i_list:
                        normals.extend(
                            [
                                float(s[12 + 4 * s.count("-1") + i * 2]),
                                float(s[12 + 4 * s.count("-1") + i * 2 + 1]),
                            ]
                        )
                else:
                    print("Boundary Condition Reading Error - More than two at one point")

                # check for corner points and add them a second time to the boundaries  -> very bad coding
                corner_distri_id = None
                if number_of_boundary_conditions == 2:
                    if 1 in distribution_id_list and 4 in distribution_id_list:  # top left -> 8 double
                        corner_distri_id = 8
                    elif 2 in distribution_id_list and 1 in distribution_id_list:  # bottom left -> 5 double
                        corner_distri_id = 5
                    elif 3 in distribution_id_list and 4 in distribution_id_list:  # top right -> 7 double
                        corner_distri_id = 7
                    elif 3 in distribution_id_list and 2 in distribution_id_list:  # bottom right -> 6 double
                        corner_distri_id = 6

                    if corner_distri_id:
                        self.corner_points.append(point_id)
                        self.corner_distris.append(corner_distri_id)

                if corner_distri_id is not None:
                    for distri_id, p_bc_id in zip(distribution_id_list, boundary_ids):
                        if distri_id == corner_distri_id:
                            missing_double_boundaries = list(np.unique(boundary_ids, axis=0))
                            missing_double_boundaries.remove(p_bc_id)
                            for mdb in missing_double_boundaries:
                                distribution_id_list.append(corner_distri_id)
                                boundary_ids.append(mdb)
                            break

                # add point to detected boundary condition
                if bc_ids is not None:
                    for bc in boundary_conditions:
                        for i, bc_id in enumerate(bc_ids):
                            if bc_id == bc.ID:
                                tmp_missing_distri = []
                                bc.point_ids.append(point_id)
                                for distri_id, p_bc_id in zip(distribution_id_list, boundary_ids):
                                    if p_bc_id == bc_id:
                                        tmp_missing_distri.append(distri_id)
                                bc.Amissingdisti.append(tmp_missing_distri)
                                bc.An.append([normals[i * 2], normals[i * 2 + 1]])

                            if number_of_boundary_conditions > 1:
                                bc.double_ids.append(point_id)

            # make numpy arrays from the point id list for later computations and clean up
            for bc in boundary_conditions:
                if len(bc.point_ids) > 0:
                    bc.point_ids = np.array(bc.point_ids)
                    bc.An = np.array(bc.An) / self.par["L"]
                    max_missing = np.max([len(x) for x in bc.Amissingdisti])
                    for distri_list in bc.Amissingdisti:
                        while len(distri_list) < max_missing:
                            distri_list.append(-1)
                    bc.Amissingdisti = np.array(bc.Amissingdisti)
                    bc.double_ids = np.unique(np.array(bc.double_ids))

            self.corner_points = np.array(self.corner_points)
            self.corner_distris = np.array(self.corner_distris)

    def _add_points_to_bc_2(self, boundary_conditions, mesh_file):
        """Reads in the .msh file and adds all point id's to their respective boundary. In addition the boundary point
        normals are fetched from the .msh file and added to the Boundary Condition as well"""

        with open(mesh_file, "r") as current_file:
            for line in current_file:  # create point objects
                if line.startswith("#"):
                    continue
                s = line.split()
                bc_ids = None
                point_id = int(s[0])
                if len(s) == 30:  # one boundary condition
                    bc_ids = [int(s[12])]
                    normals = [
                        float(s[12 + 4 * s.count("-1")]),
                        float(s[12 + 4 * s.count("-1") + 1]),
                    ]
                elif len(s) == 42:  # two boundary conditions
                    bc_ids_tmp = [int(s[i]) for i in [12, 13, 14, 15, 16]]
                    bc_ids = sorted(list(set(bc_ids_tmp)))  # poor programming
                    i_list = [bc_ids_tmp.index(bc_id) for bc_id in bc_ids]
                    normals = []
                    for i in i_list:
                        normals.extend(
                            [
                                float(s[12 + 4 * s.count("-1") + i * 2]),
                                float(s[12 + 4 * s.count("-1") + i * 2 + 1]),
                            ]
                        )

                # add point to detected boundary condition
                if bc_ids is not None:
                    for BC in boundary_conditions:
                        for i, bc_id in enumerate(bc_ids):
                            if bc_id == BC.ID:
                                BC.point_ids.append(point_id)
                                BC.An.append([normals[i * 2], normals[i * 2 + 1]])

        # make numpy arrays from the point id list for later computations and clean up
        for BC in boundary_conditions:
            BC.point_ids = np.array(BC.point_ids)
            BC.An = np.array(BC.An) / self.par["L"]

    def apply_boundary_conditions(self, bc):
        bc_handlers = {
            "Dirichlet_Con": self._apply_dirichlet_bc,
            "Dirichlet_firstorder": self._apply_dirichlet_bc,
            "Neumann_Con": self._apply_neumann_bc,
            "Neumann_firstorder": self._apply_neumann_bc,
            "Neumann_X": self._apply_neumann_x_bc,
        }

        if bc.BCType in bc_handlers:
            if len(bc.point_ids) > 0:
                bc_handlers[bc.BCType](bc)
        else:
            print(f"BC with ID {bc.ID} and type [{bc.BCType}] not implemented yet")

    def _apply_dirichlet_bc(self, bc):
        weights = self.weights
        b = self.par["csh"]
        j_bd = np.asarray(bc.CurrentValue)

        n_max_missing = bc.Amissingdisti.shape[1]
        for i in range(n_max_missing):
            MD = bc.Amissingdisti[:, i][bc.Amissingdisti[:, i] != -1]
            MDO = self.opposite_directions[MD]
            P = bc.point_ids[bc.Amissingdisti[:, i] != -1]

            factor = -2 * weights[MDO] / b**2 * np.einsum("j,ij", j_bd, self.c_vec[MDO])
            self.Af_tmp[P, MD] = self.Af[P, MDO] + factor
        
    def _apply_neumann_bc(self, bc):
        weights = self.weights
        b = self.par["csh"]
        rho0 = self.par["rho0h"]
        mue_ = self.par["mueh"]
        lambda_ = self.par["lambdah"]
        
        b_2 = b**2
        b_4 = b**4
        
        traction = bc.CurrentValue

        At = np.zeros((len(bc.point_ids), 2))
        At[:, 0] = -bc.An[:, 1]
        At[:, 1] = bc.An[:, 0]

        mat_loc = np.stack([bc.An, At], axis=1)

        if "loc" not in bc.BCType.lower():
            t_new = mat_loc @ traction

        sigma_loc = np.empty((len(bc.point_ids), 2, 2))
        sigma_loc[:, 0, 0] = t_new[:, 0]
        sigma_loc[:, 1, 0] = t_new[:, 1]
        sigma_loc[:, 0, 1] = t_new[:, 1]

        n_max_missing = bc.Amissingdisti.shape[1]

        for i in range(n_max_missing):
            # get missing distribution functions and opposite directions
            valid_mask = bc.Amissingdisti[:, i] != -1
            MD = bc.Amissingdisti[:, i][valid_mask]
            MDO = self.opposite_directions[MD]
            P = bc.point_ids[valid_mask]

            # Identity tensor for each point
            n_points = len(P)
            eyes_3d = np.tile(np.eye(2), (n_points, 1, 1))
            
            # compute density and momentum flux at boundary node
            rho_bd = np.sum(self.Af[P, :], axis=1)

            momflux_bd = np.empty((P.shape[0], 2, 2))
            momflux_bd[:, 0, 0] = np.sum(self.Af[P, :] * self.c_tensors[:, 0, 0], axis=1)
            momflux_bd[:, 0, 1] = np.sum(self.Af[P, :] * self.c_tensors[:, 0, 1], axis=1)
            momflux_bd[:, 1, 0] = np.sum(self.Af[P, :] * self.c_tensors[:, 1, 0], axis=1)
            momflux_bd[:, 1, 1] = np.sum(self.Af[P, :] * self.c_tensors[:, 1, 1], axis=1)

            # compute stress tensor at boundary node and transform to local coordinates
            sigma_bd = -momflux_bd + eyes_3d * (mue_ - lambda_) * (
                (rho_bd / rho0 - 1)[:, None, None]
            )
            sigma_bd = np.einsum('nij,njk,nkl->nil',
                                    mat_loc[valid_mask],
                                    sigma_bd,  
                                    mat_loc[valid_mask].transpose(0, 2, 1))
            
            # Update tangential stress component and transform back to global coordinates
            sigma_loc[valid_mask, 1, 1] = sigma_bd[:, 1, 1]
            sigma_tmp = np.einsum('nij,njk,nkl->nil',
                                mat_loc[valid_mask],
                                sigma_loc[valid_mask],
                                mat_loc[valid_mask].transpose(0, 2, 1))

            # compute second moment from stress tensor
            momflux_n = (
                - sigma_tmp + eyes_3d * (mue_ - lambda_)
                  * (rho_bd / rho0 - 1)[:, None, None]
                - rho_bd[:, None, None] * b_2 * eyes_3d
            )

            # update distribution functions
            c_current = self.c_vec[MD]
            cc_tensor = np.einsum('ni,nj->nij', c_current, c_current) - b_2 * eyes_3d
            momflux_cc = momflux_n * cc_tensor
            mask = np.where(self.Af_tmp[P, MD] == -1)[0]
            self.Af_tmp[P[mask], MD[mask]] = 0.0
            self.Af_tmp[P, MD] += (
                -self.Af[P, MDO]
                + 2 * weights[MDO] * rho_bd
                + 2 * weights[MDO] * (momflux_cc / (2 * b_4)).sum(axis=1).sum(axis=1)
            )

    def _apply_neumann_x_bc(self, bc):
        """Applies Neumann boundary conditions with additional processing.
        
        experimental, not yet functional
        """
        weights = self.weights
        b = self.par["csh"]
        rho0 = self.par["rho0h"]
        mue_ = self.par["mueh"]
        lambda_ = self.par["lambdah"]
        traction = np.array(bc.CurrentValue)

        At = np.zeros((len(bc.point_ids), 2))
        At[:, 0] = -bc.An[:, 1]
        At[:, 1] = bc.An[:, 0]

        mat_loc = np.zeros((len(bc.point_ids), 2, 2))
        mat_loc[:, 0, :] = bc.An
        mat_loc[:, 1, :] = At

        t_new = mat_loc @ traction

        sigma_loc = np.zeros((len(bc.point_ids), 2, 2))
        sigma_loc[:, 0, 0] = t_new[:, 0]
        sigma_loc[:, 1, 0] = t_new[:, 1]
        sigma_loc[:, 0, 1] = t_new[:, 1]

        n_max_missing = bc.Amissingdisti.shape[1]

        for i in range(n_max_missing):
            filter_ind = bc.Amissingdisti[:, i] != -1
            corner_ind = np.full(np.size(filter_ind), False)

            for corner_pt, corner_dis in zip(self.corner_points, self.corner_distris):
                log1 = bc.point_ids == corner_pt
                log2 = bc.Amissingdisti[:, i] == corner_dis
                corner_ind = np.logical_or(corner_ind, np.logical_and(log1, log2))
            non_corner_ind = np.logical_not(corner_ind)
            MD = bc.Amissingdisti[:, i][filter_ind]

            tmpMD = bc.Amissingdisti.copy()
            tmpMD[np.where(tmpMD < 0)] = 100
            minMD = np.min(tmpMD, axis=1)[filter_ind]
            MDO = self.opposite_directions[MD]
            P = bc.point_ids[filter_ind]

            n_pts_filtered = P.shape[0]
            eyes = np.array([np.eye(2)] * n_pts_filtered)

            rho_self = np.sum(self.Af[P, :], axis=1)

            Pself = np.einsum("xi,ia,ib->xab", self.Af[P, :], self.c_vec, self.c_vec)
            Pnb = np.einsum("xi,ia,ib->xab", self.Af[self.Ageo[P, minMD], :], self.c_vec, self.c_vec)

            rho_nbb = np.sum(self.Af[self.Ageo[P, minMD], :], axis=1)

            sigma_self = -Pself + eyes * (mue_ - lambda_) * (rho_self[:, None, None] - rho0) / rho0
            sigma_nb = -Pnb + eyes * (mue_ - lambda_) * (rho_nbb[:, None, None] - rho0) / rho0

            sigma_self = mat_loc[filter_ind] @ sigma_self @ mat_loc[filter_ind].transpose(0, 2, 1)
            sigma_nb = mat_loc[filter_ind] @ sigma_nb @ mat_loc[filter_ind].transpose(0, 2, 1)

            sigma_t = np.zeros_like(filter_ind)
            sigma_t[filter_ind] = sigma_self[:, 1, 1]

            filtered_corner_ids = np.logical_and(filter_ind, non_corner_ind)
            sigma_loc[filtered_corner_ids, 1, 1] = sigma_t[filtered_corner_ids]

            sigma_tmp = np.einsum("xca,xcd,xdb->xab", mat_loc[filter_ind], sigma_loc[filter_ind], mat_loc[filter_ind])

            init_ids = np.where(self.Af_tmp[P, MD] == -1)[0]
            self.Af_tmp[P[init_ids], MD[init_ids]] = 0.0

            P_bd = -sigma_tmp + eyes * (mue_ - lambda_) * (rho_self[:, None, None] - rho0) / rho0
            P_n_bd = P_bd - rho_self[:, None, None] * (b**2) * np.array([np.eye(2)] * sigma_tmp.shape[0])

            c_current = self.c_vec[MD]

            self.Af_tmp[P, MD] = self.Af_tmp[P, MD] - self.Af[P, MDO] + 2 * weights[MDO] * rho_self
            self.Af_tmp[P, MD] = self.Af_tmp[P, MD] + 2 * weights[MDO] * (
                P_n_bd
                * (
                    (c_current[:, :, None] @ c_current[:, None, :])
                    - np.array([np.eye(2)] * sigma_tmp.shape[0]) * (b**2)
                )
                / (2 * b**4)
            ).sum(axis=1).sum(axis=1)

    def _process_corner_points(self):
        # corner distribution functions:
        if len(self.corner_points) > 0:
            self.Af_tmp[self.corner_points, self.corner_distris] = (
                0.5 * self.Af_tmp[self.corner_points, self.corner_distris]
            )

    def integrate(self, *, method="trapez"):
        """integrate displacement field and set or compute related data for output"""

        # update moments, keep previous values for integration
        self.rho, rho_prev = self._compute_zeroth_moment(), self.rho
        self.momentum, momentum_prev = self._compute_first_moment(), self.momentum
        self.stress = self._compute_second_moment()

        dth = self.par["dth"]
        if method == "trapez":
            # use trapezoid rule
            self.Aw += dth * (self.momentum / self.rho[:, None] + momentum_prev / rho_prev[:, None]) / 2
        else:
            # use euler backwards
            self.Aw += dth * self.momentum / self.rho[:, None]

        # set quantities for output in vtk
        self.output_dict["w"] = ensure3d(self.Aw)

        if "rho" in self.output_dict:
            self.output_dict["rho"] = self.rho

        if "j" in self.output_dict:
            self.output_dict["j"] = ensure3d(self.momentum)

        if "kinStress" in self.output_dict:
            pi = self.stress
            self.output_dict["kinStress"] = np.array([pi[:, 0, 0], pi[:, 1, 1], pi[:, 0, 1]]).T

        if "sigma" in self.output_dict or "sigma_vm" in self.output_dict:
            sigma = self.compute_cauchy()
            if "sigma" in self.output_dict:
                self.output_dict["sigma"] = (
                    np.array([sigma[:, 0], sigma[:, 1], sigma[:, 2]]).T * self.par["mue"] * self.par["wr"] / self.par["L"]
                )

            if "sigma_vm" in self.output_dict:
                nu = 0.5 * self.par["lambdah"] / (self.par["lambdah"] + self.par["mueh"])
                sigma_vm = np.sqrt(
                    (1 - nu + nu**2) * (sigma[:, 0] ** 2 + sigma[:, 1] ** 2)
                    - (1 + 2 * nu - 2 * nu**2) * (sigma[:, 0] * sigma[:, 1])
                    + 3 * sigma[:, 2] ** 2
                )
                self.output_dict["sigma_vm"] = sigma_vm * (self.par["mueh"] * self.par["wr"] / self.par["L"])

    def compute_cauchy(self):
        dx, dy = self.grad_coeffs

        neighbor_indices_1 = self._get_neighbor_index(self.Ageo[:, 1])
        neighbor_indices_3 = self._get_neighbor_index(self.Ageo[:, 3])
        neighbor_indices_2 = self._get_neighbor_index(self.Ageo[:, 2])
        neighbor_indices_4 = self._get_neighbor_index(self.Ageo[:, 4])

        dispx = self.Aw[:, 0]
        dispy = self.Aw[:, 1]

        grad_w = np.empty_like(self.stress)
        grad_w[:, 0, 0] = dispx[neighbor_indices_1] * dx[:, 0] + dispx[neighbor_indices_3] * dx[:, 1]
        grad_w[:, 0, 1] = dispy[neighbor_indices_1] * dx[:, 0] + dispy[neighbor_indices_3] * dx[:, 1]
        grad_w[:, 1, 0] = dispx[neighbor_indices_2] * dy[:, 0] + dispx[neighbor_indices_4] * dy[:, 1]
        grad_w[:, 1, 1] = dispy[neighbor_indices_2] * dy[:, 0] + dispy[neighbor_indices_4] * dy[:, 1]

        la = self.par["lambda"]
        mu = self.par["mue"]
        trace_grad_w = 0.5 * (grad_w[:, 0, 0] + grad_w[:, 1, 1])

        sigma = np.empty((self.Af.shape[0], 3))
        sigma[:, 0] = la * trace_grad_w + 2 * mu * grad_w[:, 0, 0]
        sigma[:, 1] = la * trace_grad_w + 2 * mu * grad_w[:, 1, 1]
        sigma[:, 2] = la * trace_grad_w + mu * (grad_w[:, 0, 1] + grad_w[:, 1, 0])

        return sigma

    def regularize_precollision(self) -> None:
        """regularize the distribution functions before collision

        https://doi.org/10.1016/j.matcom.2006.05.017
        """
        cc = self.par["csh"]

        mom_flux_neq = self._compute_second_moment() - self._compute_second_moment_eq()

        q_tensor = self.c_tensors - cc**2 * np.eye(2)

        w = self.weights
        f_reg = 0.5 * w / cc**4 * np.einsum("qij,nij->nq", q_tensor, mom_flux_neq) + self.Af_eq

        self.Af = f_reg


class Lattice_Par_CPU_TRT_Ansumali(Lattice_Par_CPU_Ansumali):
    """A class representing the TRT (Two-Relaxation-Time) Lattice Boltzmann Method 
    on a CPU using the Ansumali collision operator.

    This class extends the Lattice_Par_CPU_Ansumali class and implements the 
    TRT collision model, which uses two relaxation times to improve stability 
    and accuracy in simulations.

    Attributes:
        parameter_file (str): Path to the parameter file.
        mesh_file (str): Path to the mesh file.
        switches (dict): Dictionary of switches for various options.

    Methods:
        __repr__(): Returns a string representation of the class.
        _compute_dependent_parameters(tau_sym=True): Computes parameters 
            dependent on the relaxation times.
        collide(): Performs the collision step of the Lattice Boltzmann Method.
    """
    def __init__(self, parameter_file="", mesh_file="", *, switches=dict()):
        """TODO: to be defined."""
        super().__init__(parameter_file, mesh_file, switches=switches)

    def __repr__(self):
        return "Lattice_Par_CPU_TRT_Ansumali"

    def _compute_dependent_parameters(self, *, tau_sym=True):
        super()._compute_dependent_parameters()

        def parse_omega(par: dict, tau_sym: bool) -> tuple[float]:
            if "tau+" in par and "tau-" in par:
                tau_p = par["tau+"] * par["dth"]
                tau_m = par["tau-"] * par["dth"]
                return 1 / tau_p, 1 / tau_m

            omega_plus = 1 / par.get("tauh", 0.55)
            magic_par = par.get("magic_parameter", 0.18)

            omega_minus = (1 - omega_plus * self.par["dth"]) / (
                omega_plus * self.par["dth"] ** 2 * (magic_par - 1) + self.par["dth"]
            )

            if not tau_sym:
                # swap: get symmetric tau (omega +) from Lambda instead
                omega_plus, omega_minus = omega_minus, omega_plus
            return omega_plus, omega_minus

        omega_plus, omega_minus = parse_omega(self.par, tau_sym)

        self.par["omega_symh"] = omega_plus * self.par["dth"] / 2
        self.par["omega_asymh"] = omega_minus * self.par["dth"] / 2

        logging.debug(f"TRT parameters set; symmetric tau from input: {tau_sym}")

    def collide(self):
        dt = self.par["dth"]
        o_symm = self.par["omega_symh"]
        o_asym = self.par["omega_asymh"]

        Si = self._compute_forcing()

        qbar = self.opposite_directions
        Af_neq = self.Af - self.Af_eq
        Af_neq_op = Af_neq[:, qbar]
        self.Af += (
            - o_symm * (Af_neq + Af_neq_op)
            - o_asym * (Af_neq - Af_neq_op)
            + dt * (1 - o_asym) * Si
        )

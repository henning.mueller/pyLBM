import logging
from pathlib import Path
from typing import Optional, Union

import numpy as np

from SolidLBM.solver.solution.BoundaryCondition import BoundaryCondition
from SolidLBM.util.Output import Output_Interface
from SolidLBM.parsolver.solution.Lattice_Par_CPU_Ansumali import (
    Lattice_Par_CPU_Ansumali,
    Lattice_Par_CPU_TRT_Ansumali,
)
# import SolidLBM.solver.solution.CrackSet as CrackSet_Module


class Computation_Par:
    """
    represents a computation run, i.e. lattice with parameters and commands
    """

    # define flags with boolean values (and ensure existence with common case)
    Flags = {
        "dynamic_crack": False,
        "verbose": True,
        "set_tau_symmetric": True,
    }

    _LATTICE_TYPE = {
        "Ansumali_SRT": Lattice_Par_CPU_Ansumali,
        "Ansumali_TRT": Lattice_Par_CPU_TRT_Ansumali,
    }

    def __init__(
        self,
        working_dir: Union[str, Path],
        name: str,
        model: str,
        **kwargs,
    ) -> None:
        # INPUT
        self.name = name
        self.working_dir = Path(working_dir)
        self._process_flags()

        # OUTPUT
        self.Output = None  # instantiaion is handle in 'setup_output'

        # LATTICE
        Lattice_Par = self._LATTICE_TYPE.get(model, "Ansumali_TRT")
        self.Lattice = Lattice_Par(
            parameter_file=self.working_dir / (self.name + ".par"),
            mesh_file=self.working_dir / (self.name + ".msh"),
            switches=self.Flags,
        )
        self.Lattice.LatticeType = model
        self._initialise_lattice()

        # BOUNDARY CONDITIONS
        self.boundary_conditions = []
        self._initialise_boundary_conditions()

        # TIME
        self.time_step = 0
        self.current_time = 0

    def _process_flags(self) -> None:
        import tomli
        
        flag_arguments = list(Computation_Par.Flags.keys())
        par_file = self.working_dir / (self.name + ".toml")

        if not par_file.exists():
            logging.warning("TOML file not found. Cannot process flags.")
            return

        with open(par_file, "rb") as config_file:
            config = tomli.load(config_file)
        flags = config["Flags"]
        for k, v in flags.items():
            if k in flag_arguments:
                Computation_Par.Flags[k] = v

        logging.info(f'Reading Flags from "{par_file}"')

    def setup_output(
        self,
        quantities: list[str] = ["w", "sigma_vm"],
        time_interval: Optional[float] = None,
        *,
        output_dir: Union[str, Path] = "vtu",
        format: str = "vtu",
    ) -> None:
        """Create the vtk object for the output and set the requested quantities.
        Also initialises the output_dict in Lattice with the respective keys.

        Can also set the intervals to write output at via 'output_at_interval'.
        """

        if not self.Output:
            self.Output = Output_Interface(
                output_dir=self.working_dir / output_dir,
                format=format,
            )
        else:
            self.Output.fileformat = format

        self.Output._create_vertices(self.Lattice.Acoord)

        self.Output.request_quantities(quantities)
        for key in quantities:
            # set keys of requested quantities in dict
            self.Lattice.output_dict[key] = np.nan
        if time_interval:
            self.Output.request_intervals(time_interval, self.Lattice.par["max_time"])

    def _initialise_lattice(self) -> None:
        """Initialise the distributions and boundary conditions and prepare the vtk."""
        self.Lattice.initialise_distribution_functions_and_fields()

    def _read_boundary_condition_file(self) -> list:
        bc_file = self.working_dir / (self.name + ".bc")

        boundary_conditions = []
        with open(bc_file, "r") as current_file:
            for line in current_file:
                if line.startswith("#"):
                    continue
                tmp_bc = BoundaryCondition()
                tmp_bc.read_boundary_condition_from_string(line)
                boundary_conditions.append(tmp_bc)
        return boundary_conditions

    def _initialise_boundary_conditions(self) -> None:
        """Read the input file for boundary conditions and"""
        self.boundary_conditions: list = self._read_boundary_condition_file()
        self.Lattice._add_points_to_bc(
            self.boundary_conditions,
            self.working_dir / (self.name + ".msh"),
        )

        # handle points at periodic boundaries
        periodic_bc = [bc for bc in self.boundary_conditions if bc.BCType == "periodic"]
        if periodic_bc:
            self.Lattice._process_periodic_boundaries(self.boundary_conditions)
            for bc in periodic_bc:
                # periodic boundaries cannot actually be applied like bcs
                self.boundary_conditions.remove(bc)

    def _update_boundary_values(self) -> None:
        """Get the current value a each boundary."""
        for bc in self.boundary_conditions:
            bc.compute_current_value_par(self.current_time, self.Lattice.par)

    # commands for the simulation run
    def equilibrium(self) -> None:
        """Compute the equilibrium distribution functions."""
        self.Lattice.compute_eq_functions()

    def regularize(self) -> None:
        """Regularise the equilibrium distribution functions."""
        self.Lattice.regularize_precollision()

    def collision(self) -> None:
        """Perform the collision / relaxation computations."""
        self.Lattice.collide()

    def streaming(self) -> None:
        """Stream the distributions to the respective neighbors."""
        self.Lattice.stream()

    def boundary_handling(self) -> None:
        """Apply the boundary conditions to find missing distributions."""
        self._update_boundary_values()
        for bc in self.boundary_conditions:
            self.Lattice.apply_boundary_conditions(bc)
        self.Lattice._process_corner_points()

    def distribution_update(self) -> None:
        """Update the distributions after boundary handling."""
        self.Lattice.update_distribution_functions()

    def integration(self) -> None:
        """Compute the displacement from the momentum via integration."""
        self.Lattice.integrate()

    def time_update(self) -> None:
        """Move the simulation time forward."""
        par = self.Lattice.par
        self.current_time += par["dth"] * par["L"] / par["cs"]

    def time_condition(self) -> bool:
        """Check the current time against the maximum simulation time"""
        max_time = self.Lattice.par["max_time"]

        time_condition = self.current_time <= max_time

        if self.Flags["verbose"]:
            print("", end="\r")
            print(
                f"    iteration: {self.time_step: 5d}  -- ",
                f"time: {self.current_time: 2.4f} /",
                f"{max_time: >2.1f}",
                end="",
            )
            if not time_condition:
                # print a newline character at the end of time
                print("")
        self.time_step += 1
        return time_condition

    def _write_output_info(self) -> None:
        """Write current vtk-number to terminal."""
        if self.Flags["verbose"]:
            print(f"  --  output: {self.Output.output_nr: > 4d}", end="")

    def output(self) -> None:
        """Write the requested output from Lattice to .vtu- / .vtk-files."""
        self._write_output_info()
        self.Output.write_data(self.Lattice.output_dict, name=self.name)

    def output_at_interval(self) -> None:
        """Write the requested output to .vtu- / .vtk-files.

        Only writes at requested intervals of simulation time and is meant to
        be called in every time iteration.
        """
        self._write_output_info()
        self.Output.write_at_interval(self.current_time, self.Lattice.output_dict, name=self.name)

    # combined commands for convenience
    def collision_and_streaming(self) -> None:
        """combine steps for collision and streaming"""
        self.equilibrium()
        self.collision()
        self.streaming()
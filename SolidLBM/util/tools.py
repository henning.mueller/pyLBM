"""a collection of tools"""

from pathlib import Path

import numpy as np


def make_list(iterable):
    """Make list from chain of iterables"""
    from itertools import chain

    try:
        iterable = list(chain.from_iterable(iterable))
    except TypeError:
        iterable = list(chain(iterable))

    return iterable


def ensure3d(array: np.ndarray) -> np.ndarray:
    """Add dimmension to array if it is 2D"""
    
    _, d = array.shape
    return np.pad(array, ((0, 0), (0, 3 - d)))


def plot_mesh(
    points,
    boundary,
    out_path_name: Path,
    meshdim="2D",
    *,
    dpi=600,
    tex=False,
    plot_polys=True,
    interactive=False,
):
    """Plot the Mesh with points and boundaries

    in preparation of plotting / output-class
    TODO: should be moved to designated class, which stores some of the arguments as attributes
    """
    import matplotlib as mpl
    import matplotlib.pyplot as plt

    from SolidLBM.util.geometry.Line import Line
    from SolidLBM.util.geometry.Crack import Crack
    from SolidLBM.util.geometry.Circle import Circle, CircleSeg

    assert points

    def _plot_points():
        if meshdim == "2D":
            pointsx = []
            pointsy = []
            for pt in points:
                pointsx.append(pt.x)
                pointsy.append(pt.y)

            cell_size = abs(pointsx[1] - pointsx[0])
            msize = np.clip(cell_size * 100, 0.2, 0.5)
            ax.plot(pointsx, pointsy, "b.", markersize=msize)

    def _plot_polygon():
        for pt in points:
            if pt.VolumeSurfaceMeasure:
                cell_polygon = pt.VolumeSurfaceMeasure[2]
                for edge in cell_polygon.Edges:
                    ax.plot([edge.P1.x, edge.P2.x], [edge.P1.y, edge.P2.y], "r-")

    def _plot_boundary(color=""):
        """
        displays the boundary with matplotlib.
        :param color:
        :return:
        """
        colors = ["goldenrod", "mediumblue", "forestgreen", "blueviolet", "teal"]
        color_idx = 0
        for boundary_element in boundary.BoundingGeometry:
            # TODO: move to class of boundary element
            if not color:
                color_idx = boundary_element.BoundaryName % len(colors)
                boun_color = colors[color_idx]
            else:
                boun_color = color

            # TODO: should be done via duck-typing
            #       also register these elements as boundary_element-type
            if isinstance(boundary_element, Line):
                if isinstance(boundary_element, Crack):
                    boun_color = "firebrick"
                ax.plot(
                    [boundary_element.P1.x, boundary_element.P2.x],
                    [boundary_element.P1.y, boundary_element.P2.y],
                    boun_color,
                )
            elif type(boundary_element) is Circle:
                tmp_circle = plt.Circle(
                    (boundary_element.P1.x, boundary_element.P1.y),
                    boundary_element.R,
                    color=boun_color,
                    fill=False,
                )
                ax.add_artist(tmp_circle)
            elif type(boundary_element) is CircleSeg:
                from matplotlib.patches import Arc

                tmp_arc = Arc(
                    (boundary_element.P1.x, boundary_element.P1.y),
                    boundary_element.R * 2,
                    boundary_element.R * 2,
                    theta1=boundary_element.StartAngleRad * 180 / np.pi,
                    theta2=boundary_element.EndAngleRad * 180 / np.pi,
                    edgecolor=boun_color,
                )
                ax.add_patch(tmp_arc)

    assert points
    assert boundary

    if not meshdim == "2D":
        print("Class Mesh Error: 3D not yet implemented!")

    fig, ax = plt.subplots()

    _plot_points()
    if plot_polys:
        _plot_polygon()
    _plot_boundary()

    # aspect ratio of axes equal for square plot
    ax.set_aspect("equal", adjustable="box")
    ax.set_frame_on(False)

    if not tex:
        plot_filepath = f"{out_path_name}_mesh.png"
    else:
        # save as .pgf for LaTeX
        # does not work with pdflatex because of bug in matplotlib
        mpl.use("pgf")
        mpl.rcParams.update(
            {
                # 'pgf.texsystem': 'pdflatex',
                "pgf.texsystem": "xelatex",
                "font.family": "serif",
                "font.size": 8,
                "text.usetex": True,
                "pgf.rcfonts": False,
            }
        )
        plot_filepath = f"{out_path_name}_mesh.pgf"
    fig.savefig(plot_filepath, dpi=dpi)

    if interactive:
        return fig

    plt.close("all")


def find_link_keys(p1, p2, keys, inv_keys):
    """get neighbor keys of points joined by link"""
    key = next((k for k in p1.Neighbors.keys() if p1.Neighbors[k] == p2))
    idx = keys.index(key)
    inv_key = inv_keys[idx]

    return key, inv_key


def coord_array_from_points(points) -> np.ndarray:
    """get numpy array of coordinates from list of points"""
    import numpy as np

    Acoords = np.array([pt.coords() for pt in points])

    return Acoords


def data_array_from_points(points) -> dict[str, np.ndarray]:
    """get numpy array of data from list of points"""

    field_names = points[0].PPData.keys()

    data = {}
    for key in field_names:
        data[key] = np.array([pt.PPData[key] for pt in points])

    return data

import shutil
import logging
from pathlib import Path
from typing import Union

import meshio
import numpy as np
from numpy.typing import NDArray


class Output_Interface:
    """Manage output for different parts of the SolidLBM-package.

    Examples include writing vtu-files via vtk from Computation_Par,
    plotting the mesh or writing the data of CrackTips.
    """

    __file_formats = ("vtu", "vtk")
    __field_names = ("w", "rho", "j", "kinStress", "sigma", "sigma_vm", "f", "feq")

    # TODO: do consistent re-dimensionalisation of the output data
    #       depending on quantities (keys) in output dictionary

    def __init__(
        self,
        output_dir: Union[Path, str] = "./",
        format: str = "vtu",
    ) -> None:
        self.output_dir = Path(output_dir)
        self._setup_output_directories(self.output_dir)

        self.output_times = None
        self.next_output_time: float = None
        self.output_nr: int = 0
        
        if format not in self.__file_formats:
            logging.warning(f"Output format {format} not supported. Using vtu instead.")
            format = "vtu"
        self.fileformat: str = format

    def _setup_output_directories(self, dir_path) -> None:
        """Create the directory for the output.
        If it exists already, remove first to ensure an empty directory.
        """
        if dir_path.exists():
            shutil.rmtree(dir_path, ignore_errors=True)
        Path.mkdir(dir_path)

    def _create_vertices(self, coords: NDArray) -> None:
        self.points = coords
        self.vertices = [
            ("vertex", np.array([[i,] for i in range (coords.shape[0])]))
        ]
    
    def request_quantities(self, output_quantities: list[str] = ["w"]) -> None:
        """Set the quantities that should be written as output."""
        self.output_quantities = output_quantities

    def request_intervals(self, delta_t: float = 0.1, t_max: float = 99) -> None:
        """Set the time intervals at which to write output."""
        self.output_times = (delta_t * i for i in range(20 + int(t_max // delta_t)))
        self.next_output_time = next(self.output_times)
    
    def write_data(self, data: dict, name: str = "mesh") -> None:
        """Write the mesh data to disk."""
        format = self.fileformat
        filepath = self.output_dir / f"{name}_{self.output_nr:04d}.{format}"
        
        mesh = meshio.Mesh(
            self.points,
            self.vertices,
            point_data=data,
        )
        meshio.write(filepath, mesh, file_format=format)

        self.output_nr += 1

    def write_at_interval(self, time: float, data: dict, name: str = "mesh") -> None:
        def write():
            self.write_data(data, name)
            
        if not self.output_times:
            write()
            logging.info("No interval for output requested. Writing output anyway.")

        if self.next_output_time <= time:
            write()
            self.next_output_time = next(self.output_times)

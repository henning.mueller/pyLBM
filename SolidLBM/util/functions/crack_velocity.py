import numpy as np


def tanh_order4(k: float, kc: float) -> float:
    """function for crack growth velocity to compensate overshoot of SIF"""
    print("func")

    q = k / kc
    v = np.tanh(np.sqrt(q**4 - 1))
    return v


def tanh_order_4_inverse(k: float, kc: float) -> float:
    """function for crack growth velocity to compensate overshoot of SIF"""
    q = (k - kc) / k
    v = np.tanh(np.sqrt(q**4))
    return v


def energy_balance(release: float, rate: float, flux: float) -> float:
    a_dot = 0.5 * (rate - flux) / release
    return a_dot


def from_g_static(g: float, g_c: float, crs: float) -> float:
    v = crs * (g / g_c - 1)
    return v


def from_g_static_regular(g: float, g_c: float, crs: float) -> float:
    v = crs * np.tanh(g / g_c - 1)
    return v

# The following Python code uses the abc module and defines an abstract base class
from abc import ABC, abstractmethod


# define a abstract class
class Geometric_Object(ABC):
    # # initialize the abstract class
    # def __init__(self):
    #     super().__init__()

    @abstractmethod
    def compute_normal(self, point=None, key=""):
        pass

    @abstractmethod
    def decide_if_point_is_on_element(self, point, tol=1.0e-15):
        pass

    @abstractmethod
    def compute_shortest_distance_to_point(self, point):
        pass

    @abstractmethod
    def decide_if_intersects_lineseg(self, other, tol=0.0):
        pass

    @abstractmethod
    def find_intersection_point_with_line_seg(self, other):
        pass

    @abstractmethod
    def test_if_inside_boundary(self, point, tol=1.0e-14, **kwargs):
        pass


############################################################################
# cannot use abstract class since only exists in "Line"
############################################################################
# @abstractmethod
# def decide_if_point_is_on_left_or_right_side_of_line(self, point=None):
# pass


# @abstractmethod
# def compute_length(self):
#     pass

# import matplotlib.pyplot as plt
import numpy as np

from shapely.geometry import Point as shapePoint
from shapely.geometry.polygon import Polygon as shapePolygon

import SolidLBM.util.geometry.Vector as Vector_Module
import SolidLBM.util.geometry.Line as Line_Module
import SolidLBM.util.geometry.Crack as Crack_Module
import SolidLBM.util.geometry.Point as Point_Module


class Polygon(object):
    def __init__(self, points=None):
        points = Point_Module.aspoint(points)
        if points is not None:
            self.Points = points
        self.PointsOnBoundary = list()
        self.BoundaryNamesForAllPointsOnBoundary = list()
        # self.Points = self.sort_points_by_counter_clock_wise_angle(in_origin=self.get_center())
        self.ConvexHull = list()
        self.Edges = list()
        self.EdgesBoundaryNames = list()
        self.EdgesToInteriorSizes = dict()
        if self.Points[0].x is not None:
            self.get_edges()
            self.Center = self.get_center()

    def remove_invalid_boundary_name_from_edges_that_are_on_cracks(self, boundary=None, crack_face_identifier=""):
        # remove invalid boundary name from edges that are on cracks
        need_to_remove_invalid_boundary_names_from_crack_edges = False

        # edge_name_lists_with_multiple_boundary_names = [edge in tmp_polygon.EdgesBoundaryNames]
        for edge_boundary_name_list in self.EdgesBoundaryNames:
            if len(edge_boundary_name_list) > 1:
                need_to_remove_invalid_boundary_names_from_crack_edges = True
                break

        if need_to_remove_invalid_boundary_names_from_crack_edges:
            crack_list = [
                boundary_element
                for boundary_element in boundary.BoundingGeometry
                if isinstance(boundary_element, Crack_Module.Crack)
            ]  # TODO does not need to be done more than once
            for edge_boundary_name_list in self.EdgesBoundaryNames:
                if len(edge_boundary_name_list) > 1:
                    matching_cracks = [
                        crack
                        for crack in crack_list
                        if (
                            crack.BoundaryName in edge_boundary_name_list
                            and crack.BoundaryName2Crack in edge_boundary_name_list
                        )
                    ]
                    if matching_cracks:
                        if crack_face_identifier == "R":
                            edge_boundary_name_list.remove(matching_cracks[0].BoundaryName2Crack)
                        elif crack_face_identifier == "L":
                            edge_boundary_name_list.remove(matching_cracks[0].BoundaryName)
                        else:
                            print(
                                "Class polyon Method remove_invalid_boundary_name_from_edges_that_are_on_cracks Error: decide of point is LeftOrRighFaceOfCrack"
                            )

    def get_area(self):
        # shoelace formula https://codereview.stackexchange.com/questions/88010/area-of-an-irregular-polygon
        if self.ConvexHull:
            points = self.ConvexHull
            area = 0.0
            q = points[-1]
            for p in points:
                area += p.x * q.y - p.y * q.x
                q = p
            return abs(area / 2)
        else:
            print("Class Polygon / Method get_area: Cannot compute area! Complex hull is not computed")

    def compute_surface_measure_and_id(self):
        """
        computes surface measure and associated boundary id for edges
        :return: a list of boundary names, a list of associated surface measure boundary, a dict of surface measure to inner neighbors
        """
        tmp_boundary_names = list()
        tmp_boundary_measure = list()
        tmp_inner_surface_measure = {
            "x+": None,
            "y+": None,
            "x-": None,
            "y-": None,
            "x+y+": None,
            "x+y-": None,
            "x-y+": None,
            "x-y-": None,
        }

        for edge_boundary_name_list in self.EdgesBoundaryNames:  # gives list of all boundary names of polygon's edges
            for boundary_name in edge_boundary_name_list:
                if boundary_name is not None:
                    if boundary_name not in tmp_boundary_names:
                        tmp_boundary_names.append(boundary_name)

        for boundary_name in tmp_boundary_names:
            if boundary_name is not None:
                tmp_surface_measure_of_boundary_name = 0.0
                for i in range(0, len(self.Edges)):
                    for j in range(0, len(self.EdgesBoundaryNames[i])):
                        if self.EdgesBoundaryNames[i][j] == boundary_name:
                            tmp_surface_measure_of_boundary_name = (
                                tmp_surface_measure_of_boundary_name + self.Edges[i].compute_length()
                            )
            tmp_boundary_measure.append(tmp_surface_measure_of_boundary_name)

        tol = 1.0e-6
        sqrt2two = 1.0 * 2**0.5

        for i in range(0, len(self.Edges)):
            if not self.EdgesBoundaryNames[i]:  # no external boundary associated -> inner boundary
                normal = self.Edges[i].compute_normal()[0]

                # immediate neighbors
                if abs(normal.x * 1.0 + normal.y * 0.0 - 1.0) < tol:
                    if tmp_inner_surface_measure["x+"] is None:
                        tmp_inner_surface_measure["x+"] = 0.0
                    tmp_inner_surface_measure["x+"] = tmp_inner_surface_measure["x+"] + self.Edges[i].compute_length()
                elif abs(normal.x * 0.0 + normal.y * 1.0 - 1.0) < tol:
                    if tmp_inner_surface_measure["y+"] is None:
                        tmp_inner_surface_measure["y+"] = 0.0
                    tmp_inner_surface_measure["y+"] = tmp_inner_surface_measure["y+"] + self.Edges[i].compute_length()
                elif abs(normal.x * (-1.0) + normal.y * 0.0 - 1.0) < tol:
                    if tmp_inner_surface_measure["x-"] is None:
                        tmp_inner_surface_measure["x-"] = 0.0
                    tmp_inner_surface_measure["x-"] = tmp_inner_surface_measure["x-"] + self.Edges[i].compute_length()
                elif abs(normal.x * 0.0 + normal.y * (-1.0) - 1.0) < tol:
                    if tmp_inner_surface_measure["y-"] is None:
                        tmp_inner_surface_measure["y-"] = 0.0
                    tmp_inner_surface_measure["y-"] = tmp_inner_surface_measure["y-"] + self.Edges[i].compute_length()
                # diagonal neighbors
                elif abs(normal.x * 1.0 + normal.y * 1.0 - sqrt2two) < tol:
                    if tmp_inner_surface_measure["x+y+"] is None:
                        tmp_inner_surface_measure["x+y+"] = 0.0
                    tmp_inner_surface_measure["x+y+"] = (
                        tmp_inner_surface_measure["x+y+"] + self.Edges[i].compute_length()
                    )
                elif abs(normal.x * 1.0 + normal.y * (-1.0) - sqrt2two) < tol:
                    if tmp_inner_surface_measure["x+y-"] is None:
                        tmp_inner_surface_measure["x+y-"] = 0.0
                    tmp_inner_surface_measure["x+y-"] = (
                        tmp_inner_surface_measure["x+y-"] + self.Edges[i].compute_length()
                    )
                elif abs(normal.x * (-1.0) + normal.y * 1.0 - sqrt2two) < tol:
                    if tmp_inner_surface_measure["x-y+"] is None:
                        tmp_inner_surface_measure["x-y+"] = 0.0
                    tmp_inner_surface_measure["x-y+"] = (
                        tmp_inner_surface_measure["x-y+"] + self.Edges[i].compute_length()
                    )
                elif abs(normal.x * (-1.0) + normal.y * (-1.0) - sqrt2two) < tol:
                    if tmp_inner_surface_measure["x-y-"] is None:
                        tmp_inner_surface_measure["x-y-"] = 0.0
                    tmp_inner_surface_measure["x-y-"] = (
                        tmp_inner_surface_measure["x-y-"] + self.Edges[i].compute_length()
                    )

        return list([tmp_boundary_names, tmp_boundary_measure, tmp_inner_surface_measure])

    def get_center(self):
        if self.Points:
            length = len(self.Points)
            center_x = 0.0
            center_y = 0.0
            for point in self.Points:
                center_x = center_x + 1.0 / length * point.x
                center_y = center_y + 1.0 / length * point.y
            return Vector_Module.Vector(x=center_x, y=center_y)

    def get_edges(self, list_of_boundary_names=None):
        # computes self edges for polygon of ordered points and saves them to self.Edges
        # returns a list of valid boundary names of edges as well as a list of the lengths of the edges
        out_edges = list()
        # out_edges_lengths = list()
        edges_boundary_names = list()
        if self.Points:
            for i in range(0, len(self.Points) - 1):
                tmp_boundary_name_of_edges = list()
                tmp_edge = Line_Module.Line(self.Points[i], self.Points[i + 1])
                # out_edges_lengths.append(tmp_edge.compute_length())
                out_edges.append(tmp_edge)
                if list_of_boundary_names:
                    if list_of_boundary_names[i] and list_of_boundary_names[i + 1]:
                        for boundary_name in list_of_boundary_names[i]:
                            if (
                                boundary_name in list_of_boundary_names[i + 1]
                            ):  # any boundary in both lists connects points i and i+1
                                tmp_boundary_name_of_edges.append(boundary_name)
                                # break
                    edges_boundary_names.append(tmp_boundary_name_of_edges)
            tmp_boundary_names_of_edge = list()
            tmp_edge = Line_Module.Line(self.Points[len(self.Points) - 1], self.Points[0])
            # out_edges_lengths.append(tmp_edge.compute_length())
            out_edges.append(tmp_edge)
            if list_of_boundary_names:
                if list_of_boundary_names[len(self.Points) - 1] and list_of_boundary_names[0]:
                    for boundary_name in list_of_boundary_names[len(self.Points) - 1]:
                        if boundary_name in list_of_boundary_names[0]:
                            tmp_boundary_names_of_edge.append(boundary_name)
            edges_boundary_names.append(tmp_boundary_names_of_edge)
            self.Edges = out_edges
            self.EdgesBoundaryNames = edges_boundary_names
            return list([edges_boundary_names])
        else:
            print("Class Polygon Method get_edges Error: Points attribute of polygon not allocated yet")

    def sort_points_by_counter_clock_wise_angle(self, in_origin=None):
        # https: // stackoverflow.com / questions / 41855695 / sorting - list - of - two - dimensional - coordinates - by - clockwise - angle - using - python
        def clockwiseangle_and_distance(in_point):
            point = list([in_point.x, in_point.y])
            origin = list([in_origin.x, in_origin.y])
            # Vector between point and the origin: v = p - o
            vector = [point[0] - origin[0], point[1] - origin[1]]
            # Length of vector: ||v||
            lenvector = np.hypot(vector[0], vector[1])
            # If length is zero there is no angle
            if lenvector == 0:
                return -np.pi, 0
            # Normalize vector: v/||v||
            normalized = [vector[0] / lenvector, vector[1] / lenvector]
            dotprod = normalized[0] * refvec[0] + normalized[1] * refvec[1]  # x1*x2 + y1*y2
            diffprod = refvec[1] * normalized[0] - refvec[0] * normalized[1]  # x1*y2 - y1*x2
            angle = np.arctan2(diffprod, dotprod)
            # Negative angles represent counter-clockwise angles so we need to subtract them
            # from 2*pi (360 degrees)
            if angle < 0:
                return 2 * np.pi + angle, lenvector
            # I return first the angle because that's the primary sorting criterium
            # but if two vectors have the same angle then the shorter distance should come first.
            return angle, lenvector

        refvec = list([1, 0])
        sorted_clock_wise = sorted(self.Points, key=clockwiseangle_and_distance)
        sorted_clock_wise.reverse()
        self.Points = sorted_clock_wise
        if self.ConvexHull:
            sorted_clock_wise_convex_hull = sorted(self.ConvexHull, key=clockwiseangle_and_distance)
            sorted_clock_wise.reverse()
            self.ConvexHull = sorted_clock_wise
        return sorted_clock_wise

    def compute_side_lengths(self):
        for i in range(0, len(self.Points) - 1):
            tmp_vec = self.Points[i + 1] - self.Points[i]
            self.SideLenghts.append(tmp_vec.absolute_value())
        tmp_vec = self.Points[0] - self.Points[len(self.Points) - 1]
        self.SideLenghts.append(tmp_vec.absolute_value())

    def find_left_most_point(self):
        left_most_point = self.Points[0]
        for point in self.Points:
            if point.x < left_most_point.x:
                left_most_point = point
        return left_most_point

    def compute_convex_hull(self):
        # https: // en.wikipedia.org / wiki / Gift_wrapping_algorithm
        def find_left_most_point(points=None):
            left_most_point = points[0]
            for point in points:
                if point.x < left_most_point.x:
                    left_most_point = point
            return left_most_point

        self.ConvexHull = list()
        pointOnHull = find_left_most_point(points=self.Points)
        i = 0
        quit = 0
        while not quit:
            self.ConvexHull.append(pointOnHull)
            tmp_Pi = pointOnHull
            endpoint = self.Points[0]
            for j in range(1, len(self.Points)):
                tmp_Pj = self.Points[j]
                if tmp_Pi != endpoint:
                    tmp_line = Line_Module.Line(tmp_Pi, endpoint)
                    if tmp_line.test_if_inside_boundary(tmp_Pj) and not tmp_line.decide_if_point_is_on_element(tmp_Pj):
                        endpoint = tmp_Pj
                else:
                    endpoint = tmp_Pj
            i = i + 1
            pointOnHull = endpoint
            if endpoint == self.ConvexHull[0]:
                quit = 1

    def find_intersection_points_of_edges_with_boundary(self, boundary=None):
        tmp_edge_boundary_intersection_points = list()
        # tmp_name_of_intersected_boundary_at_edge_boundary_intersection_points = list()
        for edge in self.Edges:
            for boundary_element in boundary.BoundingGeometry:
                if boundary_element.decide_if_intersects_lineseg(edge):
                    if (
                        isinstance(boundary_element, Line_Module.Line)
                        and boundary_element.decide_if_point_is_on_element(point=edge.P1)
                        and boundary_element.decide_if_point_is_on_element(point=edge.P2)
                    ):
                        tmp_edge_boundary_intersection_points.append(list([edge.P1, edge.P2]))
                        # tmp_edge_boundary_intersection_points.append(edge.P2)
                    else:
                        tmp_vecs = boundary_element.find_intersection_point_with_line_seg(edge)
                        tmp_edge_boundary_intersection_points.append(Point_Module.aspoint(tmp_vecs))
                        # tmp_edge_boundary_intersection_points.append(Point_Module.Point(x=tmp_vec.x, y=tmp_vec.y))
                        # tmp_intersection_point = Point_Module.Point(x=tmp_vec.x, y=tmp_vec.y)
                        # tmp_edge_boundary_intersection_points.append(boundary_element.find_intersection_point_with_line_seg(edge))  # more than one boundary element could intersect an edge at the same point

                    # tmp_name_of_intersected_boundary_at_edge_boundary_intersection_points.append(boundary_element.BoundaryName)
        # return list([tmp_edge_boundary_intersection_points, tmp_name_of_intersected_boundary_at_edge_boundary_intersection_points])
        return tmp_edge_boundary_intersection_points

    def find_corner_points_in_domain(self, boundary):
        tmp_corner_points_in_domain = list()
        for point in self.Points:
            domain_contains_point = boundary.check_if_domain_contains_point(point=point, generator=self.CenterPoint)
            if domain_contains_point:
                tmp_corner_points_in_domain.append(point)
        return tmp_corner_points_in_domain

    def find_boundary_names_for_points(self, boundary, tol=1.0e-14):
        """

        :param boundary:
        :param tol:
        :return: a list of boundary names for all points of polygon that are on a boundary element of boundary. the entry for interior points is an empty list
        """
        # operates on convex hull of Polygon -> reduced set of points
        # also computes self.PointsOnBoundary

        out_boundary_names_for_all_points = list()

        for point in self.Points:
            tmp_boundary_names_at_point = list()
            for boundary_element in boundary.BoundingGeometry:
                points_on_cirlce = [
                    point
                    for point in self.Points
                    if boundary_element.decide_if_point_is_on_element(point=point, tol=tol)
                ]
                if boundary_element.decide_if_point_is_on_element(point=point, tol=tol):
                    tmp_boundary_names_at_point.append(
                        boundary_element.BoundaryName
                    )  # might be more than one boundary point if boundary_elements intersect at point
                    if type(boundary_element) == Crack_Module.Crack:
                        tmp_boundary_names_at_point.append(boundary_element.BoundaryName2Crack)
            if not tmp_boundary_names_at_point:  # must be interior point
                out_boundary_names_for_all_points.append(tmp_boundary_names_at_point)
            else:  # point on boundary
                self.PointsOnBoundary.append(point)
                out_boundary_names_for_all_points.append(tmp_boundary_names_at_point)
        self.BoundaryNamesForAllPointsOnBoundary = out_boundary_names_for_all_points
        return out_boundary_names_for_all_points

    def decide_if_point_is_in_polygon(self, in_point):
        test_shape_point = shapePoint(in_point.x, in_point.y)
        shape_point_list = list()
        for point in self.Points:
            shape_point = shapePoint(point.x, point.y)
            shape_point_list.append(shape_point)
        poly = shapePolygon(shape_point_list)
        return poly.contains(test_shape_point)

    def find_intersection_points_of_boundary_elements_that_have_points_in_polygon(self, boundary):
        def a_point_of_polygon_is_on_boundary_element(polygon=None, boundary_element=None):
            point_of_polygon_is_on_boundary_element = False
            for point in polygon.Points:
                if boundary_element.decide_if_point_is_on_element(point=point):
                    point_of_polygon_is_on_boundary_element = True
                    break
            return point_of_polygon_is_on_boundary_element

        list_of_boundary_names_of_polygon_points = list()
        list_of_boundary_elements = list()
        # determine boundary names for all points in polygon (only points on a boundary have a boundary name)
        for boundary_names in self.BoundaryNamesForAllPointsOnBoundary:
            for boundary_name in boundary_names:
                if boundary_name not in list_of_boundary_names_of_polygon_points:
                    list_of_boundary_names_of_polygon_points.append(boundary_name)
                    boundary_elements_with_boundary_name = [
                        boundary_element
                        for boundary_element in boundary.BoundingGeometry
                        if (
                            boundary_element.BoundaryName == boundary_name
                            and a_point_of_polygon_is_on_boundary_element(
                                boundary_element=boundary_element, polygon=self
                            )
                        )
                    ]

                    if not boundary_elements_with_boundary_name:  # correct boundary element is crack
                        cracks_list = [
                            boundary_element
                            for boundary_element in boundary.BoundingGeometry
                            if type(boundary_element) == Crack_Module.Crack
                        ]
                        boundary_elements_with_boundary_name = [
                            crack
                            for crack in cracks_list
                            if (
                                crack.BoundaryName2Crack == boundary_name
                                and a_point_of_polygon_is_on_boundary_element(boundary_element=crack, polygon=self)
                            )
                        ]
                    list_of_boundary_elements.append(boundary_elements_with_boundary_name[0])

        # determine intersection points of the associated boundary elements if any
        list_of_intersection_points = list()
        list_of_intersection_points_boundary_names = list()
        if len(list_of_boundary_names_of_polygon_points) > 1:  # at least to boundary names/elements
            # one of the elements needs to be a line
            # find line elements
            tmp_line_elements = list()
            for boundary_element in list_of_boundary_elements:
                if type(boundary_element) == Line_Module.Line:
                    tmp_line_elements.append(boundary_element)
            # for each line element find the intersection points with all other valid boundary_elements
            already_tested_combinations = list()
            for line_element in tmp_line_elements:
                for boundary_element in list_of_boundary_elements:
                    if (
                        str(line_element.BoundaryName) + "c" + str(boundary_element.BoundaryName)
                    ) not in already_tested_combinations:
                        already_tested_combinations.append(
                            str(line_element.BoundaryName) + "c" + str(boundary_element.BoundaryName)
                        )
                        already_tested_combinations.append(
                            str(boundary_element.BoundaryName) + "c" + str(line_element.BoundaryName)
                        )
                        if line_element is not boundary_element:
                            if boundary_element.decide_if_intersects_lineseg(line_element):
                                # tmp_intersection_point = boundary_element.find_intersection_point_with_line_seg(line_element)
                                tmp_vecs = boundary_element.find_intersection_point_with_line_seg(line_element)
                                tmp_intersection_points = Point_Module.aspoint(tmp_vecs)
                                # if boundary.check_if_domain_contains_point(tmp_intersection_point):
                                list_of_intersection_points.append(tmp_intersection_points)
                                list_of_intersection_points_boundary_names.append(
                                    [
                                        line_element.BoundaryName,
                                        boundary_element.BoundaryName,
                                    ]
                                )
        return list([list_of_intersection_points, list_of_intersection_points_boundary_names])

    def remove_duplicate_points(self):
        tmp_points = list()
        for point in self.Points:
            found = False
            for point_i in tmp_points:
                tol = 1.0e-15
                if ((point.x - point_i.x) ** 2 + (point.y - point_i.y) ** 2) ** 0.5 < tol:
                    found = True
            if not found:
                tmp_points.append(point)
        tmp_points = Point_Module.aspoint(tmp_points)
        self.Points = tmp_points

    def decide_if_point_is_on_convex_hull(self, test_point=None, tol=1.0e-14):
        point_is_on_convex_hull = False
        if self.ConvexHull:
            for i in range(0, len(self.ConvexHull) - 1):
                tmp_line = Line_Module.Line(self.ConvexHull[i], self.ConvexHull[i + 1])
                if tmp_line.decide_if_point_is_on_element(test_point, tol=tol):
                    point_is_on_convex_hull = True
                    break
            tmp_line = Line_Module.Line(self.ConvexHull[len(self.ConvexHull) - 1], self.ConvexHull[0])
            if tmp_line.decide_if_point_is_on_element(test_point, tol=tol):
                point_is_on_convex_hull = True
        return point_is_on_convex_hull

    def compute_outer_polygon_points(
        self,
    ):  # convex hull + points that are not directly on convex hull
        tmp_outer_polygon_points = list()
        for point in self.Points:
            if self.decide_if_point_is_on_convex_hull(test_point=point, tol=1.0e-8):
                tmp_outer_polygon_points.append(point)
        if len(self.ConvexHull) > len(tmp_outer_polygon_points):
            tmp_outer_polygon_points = self.ConvexHull
        return tmp_outer_polygon_points

    # def associate_boundary_names_to_edges_of_polygon(self, list_of_boundary_names_for_points = None):
    # operates on polygon consisting of
    # for point in self.Points:

    # def plot_polygon(self, color = 'r'):
    #     for edge in self.Edges:
    #         plt.plot([edge.P1.x, edge.P2.x],
    #                  [edge.P1.y, edge.P2.y], color + '-')

    def plot_convex_hull_of_polygon(self, color="g"):
        import matplotlib.pyplot as plt

        tmp_edges = list()
        for i in range(0, len(self.ConvexHull) - 1):
            tmp_edge = Line_Module.Line(self.ConvexHull[i], self.ConvexHull[i + 1])
            tmp_edges.append(tmp_edge)
        tmp_edge = Line_Module.Line(self.ConvexHull[len(self.ConvexHull) - 1], self.ConvexHull[0])
        tmp_edges.append(tmp_edge)
        for edge in tmp_edges:
            plt.plot([edge.P1.x, edge.P2.x], [edge.P1.y, edge.P2.y], color + "-")

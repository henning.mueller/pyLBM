import logging
from typing import Optional, Any

from SolidLBM.util.geometry.Vector import Vector
import SolidLBM.util.geometry.Geometric_Object as Geometric_Object_Module


class Line(Geometric_Object_Module.Geometric_Object):
    __slots__ = (
        "P1",
        "P2",
        "BoundaryName",
        "Normal",
    )

    def __init__(self, p1=None, p2=None):
        """initialize Line instance
        p1, p2 can be Vector-instances (i.e. points) or
        tuples of coordinates, which must be castable to float, to create Vectors from
        """
        if isinstance(p1, Vector) and isinstance(p2, Vector) and not p1 == p2:
            self.P1 = p1
            self.P2 = p2
            self.BoundaryName = None
            self.Normal = None
        elif isinstance(p1, tuple) and isinstance(p2, tuple) and not p1 == p2:
            xy1 = map(float, p1)
            xy2 = map(float, p2)
            self.P1 = Vector(*xy1)
            self.P2 = Vector(*xy2)
        else:
            return None

        # TODO : indicate somehow, if no object is created, or throw some exception

        self.BoundaryName = None
        self.Normal = None

    def __repr__(self):
        return f"{self.__class__.__name__} {self.P1.coords()} --> {self.P2.coords()}"

    def __neg__(self):
        """switch direction of Line"""
        self.P1, self.P2 = self.P2, self.P1
        return self

    def compute_normal(self, point=None, key=""):
        # Compute tangent
        tangent = (self.P2 - self.P1).normalized()
        #  Compute Normal (order of points matters, counter clock wise)
        normal = -tangent.perp().normalized()
        return (normal,)

    def compute_length(self):
        if hasattr(self, "P2") and hasattr(self, "P1"):
            tmp_vec = self.P2 - self.P1
            return tmp_vec.absolute_value()
        else:
            pass

    def compute_shortest_distance_to_point(self, point):
        if isinstance(point, Vector):
            vector_p1p2 = self.P2 - self.P1
            tangent = vector_p1p2 / vector_p1p2.absolute_value()
            u = ((point - self.P1) * tangent) / vector_p1p2.absolute_value()
            if u > 1:
                u = 1.0
            elif u < 0:
                u = 0.0
            closest_point = self.P1 + (self.P2 - self.P1) * u
            tmp_vector = closest_point - point
            shortest_distance_to_point = tmp_vector.absolute_value()
            return [shortest_distance_to_point, closest_point]
        else:
            raise TypeError(
                "Can't compute shortest distance between Point-object and {0}-object".format(
                    type(point)
                )
            )

    def find_intersection_point_with_line_seg(self, other):
        # computes the intersection point of two lines
        # https://stackoverflow.com/questions/3252194/numpy-and-line-intersections
        assert isinstance(
            other, Line
        ), f"Can't compute intersection of Line-object and {type(other)}-object"
        da = self.P2 - self.P1
        db = other.P2 - other.P1
        dp = self.P1 - other.P1
        dap = da.perp()
        denominator = dap * db
        num = dap * dp
        if not denominator == 0.0:
            tmp_vector = (num / denominator) * db + other.P1
            return [tmp_vector]
        else:  # lines are parallel ->
            if (
                other.compute_shortest_distance_to_point(self.P1)[0]
                < other.compute_shortest_distance_to_point(self.P2)[0]
            ):
                return [self.P1]
            else:
                return [self.P2]

    def decide_if_intersects_lineseg(self, other: Any, tol: float = 0.0) -> bool:
        # decides if two lines intersect
        assert isinstance(
            other, Line
        ), f"Can't compute intersection of Line-object and {type(other)}-object"
        return (
            self.P1.ccw(self.P2, other.P1) * self.P1.ccw(self.P2, other.P2) <= tol
            and other.P1.ccw(other.P2, self.P1) * other.P1.ccw(other.P2, self.P2) <= tol
        )

    def decide_if_point_is_on_element(
        self, point: Vector, tol: float = 1.0e-14
    ) -> bool:
        """Check, if a given point lies on the line element."""
        assert isinstance(point, Vector)

        if not self.P1 == self.P2:
            r12 = self.P2 - self.P1
            e12 = r12 / r12.absolute_value()
        else:
            return False
        if (self.P1 - point).absolute_value() > tol:
            r13 = point - self.P1
            e13 = r13 / r13.absolute_value()
        else:
            return True

        on_line = abs(e12 * e13) >= 1.0 - tol
        if on_line:
            t = e12 * r13 / (r12.absolute_value())
            return t >= 0.0 and t <= 1.0
        else:
            return False

    def decide_if_point_is_on_left_or_right_side_of_line(
        self, point: Optional[Vector] = None
    ) -> bool:
        """Check, if a point lies to the left or right of the line element, or directly on it.
        Direction of line is given by the direction of the vector from P1 to P2.
        """
        assert isinstance(point, Vector)

        if not self.decide_if_point_is_on_element(point):
            if self.P2 != self.P1:
                e12 = self.P2 - self.P1
                e12 = e12 / e12.absolute_value()
                r13 = point - self.P1
                t = e12 * r13
                closest_point_on_line = t * e12
                r_closest_point_on_line_3 = point - closest_point_on_line
                e_closest_point_on_line_3 = (
                    r_closest_point_on_line_3
                    / r_closest_point_on_line_3.absolute_value()
                )
                normal = self.compute_normal()[0]
                tol = 1.0e-14
                if normal * e_closest_point_on_line_3 > tol:
                    return "R"
                elif normal * e_closest_point_on_line_3 < -tol:
                    return "L"
        else:
            return "O"

    def test_if_inside_boundary(self, point, tol=1.0e-14, **kwargs):
        normal = self.compute_normal()[0]
        inside = ((point - self.P1) * normal <= tol) and (
            point - self.P2
        ) * normal <= tol
        return inside

    def add(self, other):
        """try to reduce two lines to one, returns list of line(s)"""
        return join(self, other)

    def points(self):
        return self.P1, self.P2


def join(line1, line2) -> list[Line]:
    """Tries to join two line-objects together.
    If they form one consecutive line, that line is returned in a list.
    Otherwise, the line elements are returned in a list.
    Keeps properties like BoundaryName from first input line.
    """
    pts = set((line1.P1, line1.P2, line2.P1, line2.P2))
    if len(pts) == 4:  # lines are disjunct
        return [line1, line2]
    elif len(pts) == 2:  # lines coincide
        return [line1]
    elif len(pts) == 3:  # lines share one point
        pts = list(pts)
        pts.sort(key=lambda p: p.coords())  # sort points by coordinates
        pt_l, pt_m, pt_r = pts
        new_line = Line(pt_l, pt_r)

        # build tangents and check angle
        tang1 = line1.P2 - line1.P1
        tang_new = new_line.P2 - new_line.P1
        dot = tang1 * tang_new

        if new_line.decide_if_point_is_on_element(pt_m):
            if dot < 1:
                new_line = -new_line
            new_line.BoundaryName = line1.BoundaryName

            if not line2.BoundaryName == new_line.BoundaryName:
                logging.warning(
                    "Boundaryname of 2nd line differs from that of joined line."
                )

            return [new_line]

        return [line1, line2]

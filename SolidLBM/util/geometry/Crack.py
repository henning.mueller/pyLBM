from collections.abc import Iterable
from typing import Optional, TypeVar

import SolidLBM.util.geometry.Vector as Vector_Module
import SolidLBM.util.geometry.Line as Line_Module
import SolidLBM.util.geometry.Point as Point_Module
from SolidLBM.util.geometry.Geometric_Object import Geometric_Object
from SolidLBM.util.tools import find_link_keys


Vector = Vector_Module.Vector
Point = Point_Module.Point
Line = Line_Module.Line
Boundary = TypeVar("Boundary")


link_directions = {
    "x+": (1, 0, 0),
    "x-": (-1, 0, 0),
    "y+": (0, 1, 0),
    "y-": (0, -1, 0),
    "x+y+": (1, 1, 0),
    "x+y-": (1, -1, 0),
    "x-y+": (-1, 1, 0),
    "x-y-": (-1, -1, 0),
}


class Crack(Line, Geometric_Object):
    __slots__ = ("BoundaryName2Crack",)

    def __init__(self, p1=None, p2=None):
        super(Crack, self).__init__(p1, p2)
        self.BoundaryName2Crack = None

    def __neg__(self):
        """switch direction of Crack (and BoundaryNames)"""
        self.P1, self.P2 = self.P2, self.P1
        self.BoundaryName, self.BoundaryName2Crack = (
            self.BoundaryName2Crack,
            self.BoundaryName,
        )
        return self

    def compute_normal(self, point=None, key=""):
        """
        compute normal vector of crack
        also decide on which side a point lies (in key-direction from crack)
        """
        if not key:
            return super(Crack, self).compute_normal()

        # counter clockwise, order of points matters
        tangent = (self.P2 - self.P1).normalized()
        normal = -tangent.perp().normalized()

        # missing key is inverse of key pointing towards point from crack -> in push direction

        e_lattice_link = Vector(*link_directions[key])
        side_of_crack = "R"
        if normal * e_lattice_link < 0:
            normal = -normal
            side_of_crack = "L"

        return normal, side_of_crack

    def decide_if_point_is_on_R_or_L_side_of_crack(self, key=""):
        if key:
            _, side = self.compute_normal(key=key)
        else:
            side = "R"
        return side

    def decide_if_intersects_crackseg(self, other, tol=0.0, keys=None, n_keys=None):
        """Decide, if a line intersects the crack.
        If an endpoint of the line lies on the crack, it must be on the same side as the
        other point for an intersection.
        Otherwise, the corresponding method in Line is called.
        """

        assert isinstance(
            other, Line
        ), "Can only compare to Line-object; found {0}-object".format(type(other))

        intersect = self.decide_if_intersects_lineseg(other, tol=tol)

        if intersect:
            # Line intersects with Crack, check if ends on Crack
            pt1, pt2 = other.points()  # start and end of line segment
            pt1_on_crack = self.decide_if_point_is_on_element(pt1)
            pt2_on_crack = self.decide_if_point_is_on_element(pt2)

            if pt1_on_crack or pt2_on_crack:
                # one of the points lies on Crack, check if on same side
                if not (keys and n_keys):
                    # must pass keys for usage in Mesher
                    keys = pt1.neighbor_keys
                    n_keys = pt1.neighbor_keys

                # find key of Neighbor first
                key = next((k for k in pt1.Neighbors.keys() if pt1.Neighbors[k] == pt2))
                idx = keys.index(key)
                inv_key = n_keys[idx]

                # always have point on crack on the right side
                if pt1_on_crack:
                    pt1.IsLeftOrRightFaceOfCrack = "R"
                    pt2.IsLeftOrRightFaceOfCrack = (
                        self.decide_if_point_is_on_R_or_L_side_of_crack(key=inv_key)
                    )
                elif pt2_on_crack:
                    pt1.IsLeftOrRightFaceOfCrack = (
                        self.decide_if_point_is_on_R_or_L_side_of_crack(key=key)
                    )
                    pt2.IsLeftOrRightFaceOfCrack = "R"
                intersect = pt1.IsLeftOrRightFaceOfCrack != pt2.IsLeftOrRightFaceOfCrack
        return intersect

    def test_if_inside_boundary(
        self, point, tol=1.0e-14, generator_key="", in_generator=None
    ):
        # two options:  key for normal lattice points
        #               in_generator point object for other cases

        if generator_key in point.Neighbors.keys():
            tmp_generator = point.Neighbors[generator_key]
        elif (
            in_generator
        ):  # tested point might not be a lattice point -> create "virtual" generator
            tmp_generator = in_generator
            if not tmp_generator.IsLeftOrRightFaceOfCrack:
                # should be known since generator is lattice point object
                # can happen, when Crack reaches within Polygon without severing lattice links
                # -> just ignore, Crack does serve as boundary
                return True  # just ignore Crack
        else:
            raise TypeError(
                "Tested against crack element! generator_key or generator Point-object required"
            )
        tmp_line = Line(tmp_generator, point)
        inv_keys = {
            "x+": "x-",
            "y+": "y-",
            "x-": "x+",
            "y-": "y+",
        }

        inside = True
        if self.decide_if_intersects_lineseg(
            tmp_line
        ):  # is only called for lineseqs that intersect crack?
            if self.decide_if_point_is_on_element(
                point
            ):  # generated point is on crack -> inside the domain
                inside = True
            elif self.decide_if_point_is_on_element(
                tmp_generator
            ):  # generator is on crack
                if (
                    tmp_generator.IsLeftOrRightFaceOfCrack
                    == self.decide_if_point_is_on_R_or_L_side_of_crack(
                        key=inv_keys[generator_key]
                    )
                ):  # generated point is on same side of crack as generator
                    inside = True
                elif not self.decide_if_point_is_on_element(
                    point
                ):  # special case at the crack tip for nodes aligned with crack
                    tmp_vec = point - tmp_generator
                    tmp_vec = tmp_vec / tmp_vec.absolute_value()
                    normal = self.compute_normal()[0]  # key not needed
                    inside = abs(normal * tmp_vec) < tol
                    # if abs(normal*tmp_vec) < tol:
                    # return True
                    # else:
                    # return False
                else:
                    inside = False
            else:  # link intersects but both points are on opposite sides of crack -> point is not part of body
                inside = False

        return inside

    def add(self, other):
        """
        Adds the current Crack object with another Crack object.

        Parameters:
        other (Crack): The other Crack object to be added.

        Returns:
        Crack: A new Crack object representing the addition of the two Crack objects.
        """
        return join(self, other)


def join(crack1, crack2):
    """Join Cracks as Line-objects, convert to Crack and set BoundaryName2Crack"""
    joined = Line_Module.join(crack1, crack2)

    if len(joined) == 1:
        joined_line = joined[0]
        joined_crack = Crack(joined_line.P1, joined_line.P2)
        joined_crack.BoundaryName = crack1.BoundaryName
        joined_crack.BoundaryName2Crack = crack1.BoundaryName2Crack
        joined = [joined_crack]

    return joined


def find_boun_points_at_crack(
    crack: Crack,
    initial_seeds: list[Point],
    keys: Optional[list] = None,
    n_keys: Optional[list] = None,
) -> list[tuple[Point]]:
    """find the lattice points with links intersected by new crack segment;
    basically flood fill with initial queue populated by seed and its neighbors
    """
    queue = set()
    for pt in initial_seeds:
        neighbors = list(pt.Neighbors.values())
        queue.update(neighbors)

    visited = set()
    intersected_pts = []

    while queue:
        pt = queue.pop()
        visited.add(pt)
        for n_pt in set(pt.Neighbors.values()) - visited:
            # check each lattice link of pt for intersection with crack
            link = Line(pt, n_pt)
            intersect = crack.decide_if_intersects_crackseg(
                link, keys=keys, n_keys=n_keys
            )
            if intersect:
                intersected_pts.append((pt, n_pt))
                queue |= set(n_pt.Neighbors.values()) - visited

    return intersected_pts


def process_links_of_boun_points(
    point_tuples: list[tuple[Vector]],
    crack: Crack,
    boundary: Boundary,
    bc_imp: Optional[tuple[int]] = None,
    keys: Optional[tuple[int]] = None,
    n_keys: Optional[tuple[int]] = None,
) -> Iterable[Vector]:
    """sever links and make lattice points into boundary lattice points;
    set BoundaryName, compute normals and create AuxPoints for each point,
    also process points according to BC impementation
    """

    def geometrics():
        """get distance to boundary per intersected link"""
        link = Line(pt, n_pt)
        (pt_x,) = link.find_intersection_point_with_line_seg(crack)

        q = pt.dist(pt_x)
        pt.DistanceToBoundary[key] = q

        q = n_pt.dist(pt_x)
        n_pt.DistanceToBoundary[n_key] = q

    def process_pt(pt, key, n_key):
        """set BoundaryName, compute normals, create AuxPoints for each point"""
        if not pt.IsLeftOrRightFaceOfCrack:
            # keys of directions pointing towards LatticePoint needed here (streaming key)
            pt.IsLeftOrRightFaceOfCrack = (
                crack.decide_if_point_is_on_R_or_L_side_of_crack(n_key)
            )
        boundary.compute_rectangular_distance_to_boundary(
            point=pt, key=key
        )  # sets BoundaryName

        return pt.BoundaryClosestPoints[key]  # AuxPoint on crack

    aux_pts = []
    for pt, n_pt in point_tuples:
        if not (keys and n_keys):  # Mesher needs to provide keys
            keys = pt.neighbor_keys
            n_keys = pt.inv_keys

        key, n_key = find_link_keys(pt, n_pt, keys, n_keys)
        assert key in pt.Neighbors and n_key in n_pt.Neighbors

        aux1 = process_pt(pt, key, n_key)
        aux2 = process_pt(n_pt, n_key, key)
        aux_pts.extend([aux1, aux2])

        # sever link by removing neighbors
        pt.Neighbors.pop(key)
        n_pt.Neighbors.pop(n_key)
        geometrics()

        if bc_imp:  # key for BC implementation type is provided
            if pt.IsLeftOrRightFaceOfCrack == "R":
                pt.BCImplementationSwitch, n_pt.BCImplementationSwitch = bc_imp
            else:
                n_pt.BCImplementationSwitch, pt.BCImplementationSwitch = (
                    bc_imp  # switched
                )
    return aux_pts
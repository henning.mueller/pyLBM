import numpy as np


class Vector(object):
    def __init__(self, x=0.0, y=0.0, z=0.0):
        self.x = x
        self.y = y
        self.z = z

    def _is_valid_operand(self, other):
        return hasattr(other, "x") and hasattr(other, "y") and hasattr(other, "z")

    def __hash__(self):
        return hash((self.x, self.y, self.z))

    def __repr__(self):
        return f"{self.__class__.__name__}-object (x: {self.x:.4f}, y: {self.y:.4f}, z: {self.z:.4f})"

    def __str__(self):
        return repr(self)

    def __neg__(self):
        """negative Vector"""
        self.x *= -1
        self.y *= -1
        self.z *= -1
        return self

    def __lt__(self, other):
        # compare by absoulte value
        if not isinstance(other, Vector):
            raise TypeError(f"Can't compare Vector-object and {type(other)}-object")
        return self.absolute_value() < other.absolute_value()

    def __le__(self, other):
        # compare by absoulte value
        if not isinstance(other, Vector):
            raise TypeError(f"Can't compare Vector-object and {type(other)}-object")
        return self.absolute_value() <= other.absolute_value()
    
    def __gt__(self, other):
        # compare by absoulte value
        if not isinstance(other, Vector):
            raise TypeError(f"Can't compare Vector-object and {type(other)}-object")
        return self.absolute_value() > other.absolute_value()
    
    def __ge__(self, other):
        # compare by absoulte value
        if not isinstance(other, Vector):
            raise TypeError(f"Can't compare Vector-object and {type(other)}-object")
        return self.absolute_value() >= other.absolute_value()

    def __eq__(self, other):
        if not self._is_valid_operand(other):
            raise TypeError(f"Can't compare Vector-object and {type(other)}-object")
        return self.x == other.x and self.y == other.y and self.z == other.z

    def __ne__(self, other):
        if not self._is_valid_operand(other):
            raise TypeError(f"Can't compare Vector-object and {type(other)}-object")
        return not self.__eq__(other)

    def __sub__(self, other):
        if not self._is_valid_operand(other):
            raise TypeError(f"Can't compare Vector-object and {type(other)}-object")
        return Vector(self.x - other.x, self.y - other.y, self.z - other.z)

    def __add__(self, other):
        if not self._is_valid_operand(other):
            raise TypeError(f"Can't add {type(other)}-object to Vector-object")
        return Vector(self.x + other.x, self.y + other.y, self.z + other.z)

    def __iadd__(self, other):
        if not self._is_valid_operand(other):
            raise TypeError(f"Can't add {type(other)}-object to Vector-object")
        self.x = other.x + self.x
        self.y = other.y + self.x
        self.z = other.z + self.x
        return self

    def __mul__(self, other):
        if isinstance(other, Vector):
            return self.x * other.x + self.y * other.y + self.z * other.z
        else:
            try:
                return Vector(self.x * other, self.y * other, self.z * other)
            except TypeError:
                ot = type(other)
                raise TypeError("Can't multiply Vector by {0}".format(ot))

    def __rmul__(self, other):
        if isinstance(other, Vector):
            return self.x * other.x + self.y * other.y + self.z * other.z
        else:
            try:
                return Vector(self.x * other, self.y * other, self.z * other)
            except TypeError:
                ot = type(other)
                raise TypeError("Can't multiply Vector by {0}".format(ot))

    def __div__(self, other):
        return self.__truediv__(other)

    def __truediv__(self, other):
        try:
            return Vector(self.x / other, self.y / other, self.z / other)
        except TypeError:
            ot = type(other)
            raise TypeError("Can't divide Vector by {0}".format(ot))

    def __rtruediv__(self, other):
        try:
            return Vector(self.x / other, self.y / other, self.z / other)
        except TypeError:
            ot = type(other)
            raise TypeError("Can't divide Vector by {0}".format(ot))

    def cross_product(self, other):
        if isinstance(other, Vector):
            return Vector(
                self.y * other.z - self.z * other.y,
                self.z * other.x - self.x * other.z,
                self.x * other.y - self.y * other.x,
            )
        else:
            raise TypeError("Can't compute cross product with {}".format(type(other)))

    def absolute_value(self):
        return (self.x**2 + self.y**2 + self.z**2) ** 0.5

    def perp(self):
        return Vector(x=-self.y, y=self.x, z=self.z)

    def normalized(self):
        return self / self.absolute_value()

    def coords(self):
        """return components of Vector (coordinates of Point) as tuple"""
        return (self.x, self.y, self.z)

    def angle(self, other):
        """angle of other vector to this vector"""
        if isinstance(other, Vector):
            # to x-axis
            gamma, theta = np.arctan2([self.y, other.y], [self.x, other.x])
            return theta - gamma
        else:
            raise TypeError("Can't compute angle with {}".format(type(other)))

    def to_array(self):
        """return coordinates as NumPy array"""
        return np.array(self.coords())


class Vector_With_Displacement(Vector):
    def __init__(self, x=0.0, y=0.0, z=0.0, dim_of_w=1):
        super(Vector_With_Displacement, self).__init__(x, y, z)
        self.ID = -1
        self.w = [0.0] * dim_of_w
        self.PPData = dict()

    def __hash__(self):
        return hash((self.x, self.y, self.z, self.ID**2))

    def write_position_2_vtk(self, computation):
        L = computation.Parameters["L"]
        s1 = "{0:16.8e} ".format(self.x * L)
        s1 += "{0:16.8e} ".format(self.y * L)
        s1 += "{0:16.8e} \n".format(self.z * L)
        return s1

    def write_data_2_vtk(self, computation):
        s = ""
        for wcomp in self.w:
            s = s + "{0:16.8e} ".format(wcomp * computation.Parameters["wr"])
        return s

    def write_PPData_2_vtk(self, key):
        if hasattr(self.PPData[key], "__len__"):
            tmp_string = ""
            for i in range(0, len(self.PPData[key])):
                tmp_string2 = "{0" + ":16.8e} "
                tmp_string = tmp_string + tmp_string2.format(self.PPData[key][i])
                # tmp_string += '{0:16.8e} '.format(self.PPData[key][i])
        else:
            tmp_string = "{0:16.8e}".format(self.PPData[key])
        return tmp_string

    def write_cell_connectivity_2_vtk(self):
        return "1 {0:6d} \n".format(self.ID)

    def write_cell_type_2_vtk(self):
        return "1\n"

    @classmethod
    def from_vector(cls, vector, dim_of_w=1):
        """create Vector_With_Displacement from Vector-type object"""
        assert isinstance(vector, Vector)
        vec_with_disp = cls(*vector.coords(), dim_of_w)
        return vec_with_disp

import SolidLBM.util.geometry.Vector as Vector_Module
import SolidLBM.util.geometry.Line as Line_Module
import SolidLBM.util.geometry.Polygon as Polygon_Module

import SolidLBM.util.geometry.Point as Point_Module


class Square(Polygon_Module.Polygon):
    def __init__(
        self,
        points=[Vector_Module.Vector(x=None, y=None, z=None)] * 4,
        center_point=None,
        side_length=0.0,
        seed_point=Vector_Module.Vector(),
    ):
        super(Square, self).__init__(points=points)
        if points[0].x is not None:
            self.Points = points
            self.get_edges()
        elif center_point is not None:
            self.create_square_around_center_point(
                center_point=center_point,
                side_length=side_length,
                seed_point=seed_point,
            )
        else:
            print("Class square error: cannot create square! input points explicitly or center_point")
        # self.Points = self.sort_points_by_counter_clock_wise_angle(in_origin=self.get_center())
        self.ConvexHull = list()
        self.Edges = list()
        self.AuxLines = list()
        self.CenterPoint = center_point

    def create_square_around_center_point(
        self,
        center_point=Vector_Module.Vector(x=None, y=None, z=None),
        side_length=0.0,
        seed_point=Vector_Module.Vector(),
    ):
        tmp_points = [None, None, None, None]

        xm = float(round((center_point.x - seed_point.x) / side_length, 0) * 2 - 1) * side_length / 2.0 + seed_point.x
        xp = float(round((center_point.x - seed_point.x) / side_length, 0) * 2 + 1) * side_length / 2.0 + seed_point.x
        ym = float(round((center_point.y - seed_point.y) / side_length, 0) * 2 - 1) * side_length / 2.0 + seed_point.y
        yp = float(round((center_point.y - seed_point.y) / side_length, 0) * 2 + 1) * side_length / 2.0 + seed_point.y

        # xp = center_point.x + side_length/2.0
        # ym = center_point.y - side_length/2.0
        # yp = center_point.y + side_length/2.0

        # xm = float(round(((center_point.x - seed_point.x) - side_length/2.0) / (side_length/2.0), 0)) * side_length/2.0 + seed_point.x
        # xp = float(round(((center_point.x + seed_point.x) + side_length/2.0) / (side_length/2.0), 0)) * side_length/2.0 + seed_point.x
        # ym = float(round(((center_point.y - seed_point.y) - side_length/2.0) / (side_length/2.0), 0)) * side_length/2.0 + seed_point.y
        # yp = float(round(((center_point.y + seed_point.y) + side_length/2.0) / (side_length/2.0), 0)) * side_length/2.0 + seed_point.y

        # point = Point(float(round(((self.x - seed_point.x) - delta) / delta, 0)) * delta + seed_point.x, self.y,
        #              self.z)

        tmp_points[0] = Point_Module.Point(x=xm, y=ym)
        tmp_points[1] = Point_Module.Point(x=xp, y=ym)
        tmp_points[2] = Point_Module.Point(x=xp, y=yp)
        tmp_points[3] = Point_Module.Point(x=xm, y=yp)

        # tmp_points[0] = Point(x=center_point.x - side_length/2.0, y=center_point.y - side_length/2.0)
        # tmp_points[1] = Point(x=center_point.x + side_length/2.0, y=center_point.y - side_length/2.0)
        # tmp_points[2] = Point(x=center_point.x + side_length/2.0, y=center_point.y + side_length/2.0)
        # tmp_points[3] = Point(x=center_point.x - side_length/2.0, y=center_point.y + side_length/2.0)
        self.Points = tmp_points
        self.CenterPoint = center_point
        # self.CenterPoint = Point(x=center_point.x, y=center_point.y, z=center_point.z)

    def decide_which_aux_lines_to_create(self, boundary):
        def compare_lists(a, b):
            lists_are_the_same = False
            if len(a) == len(b):
                for i in range(0, len(a)):
                    if a[i] == b[i]:
                        lists_are_the_same = True
            return lists_are_the_same

        square_cells_around_cell = self.CenterPoint.test_if_squares_around_point_exist()[1]

        # top right point, consider the three cells adjacent to point
        aux_line_at_top_right = None
        if boundary.check_if_domain_contains_point(self.Points[2], generator=self.CenterPoint):
            if (
                square_cells_around_cell["north"]
                and square_cells_around_cell["northeast"]
                and (not square_cells_around_cell["east"])
            ):
                aux_line_at_top_right = "southeast"
            elif (
                square_cells_around_cell["north"]
                and (not square_cells_around_cell["northeast"])
                and (not square_cells_around_cell["east"])
            ):
                aux_line_at_top_right = "east"
            elif (
                (not square_cells_around_cell["north"])
                and (not square_cells_around_cell["northeast"])
                and (not square_cells_around_cell["east"])
            ):
                aux_line_at_top_right = "northeast"
            elif (
                (not square_cells_around_cell["north"])
                and (not square_cells_around_cell["northeast"])
                and square_cells_around_cell["east"]
            ):
                aux_line_at_top_right = "north"
            elif (
                (not square_cells_around_cell["north"])
                and square_cells_around_cell["northeast"]
                and square_cells_around_cell["east"]
            ):
                aux_line_at_top_right = "northwest"

        # top left point, consider the three cells adjacent to point
        aux_line_at_top_left = None
        if boundary.check_if_domain_contains_point(self.Points[3], generator=self.CenterPoint):
            if (
                (not square_cells_around_cell["north"])
                and square_cells_around_cell["northwest"]
                and (square_cells_around_cell["west"])
            ):
                aux_line_at_top_left = "northeast"
            elif (
                (not square_cells_around_cell["north"])
                and (not square_cells_around_cell["northwest"])
                and (square_cells_around_cell["west"])
            ):
                aux_line_at_top_left = "north"
            elif (
                (square_cells_around_cell["north"])
                and (not square_cells_around_cell["northwest"])
                and (square_cells_around_cell["west"])
            ):
                aux_line_at_top_left = "northwest"
            elif (
                (square_cells_around_cell["north"])
                and (not square_cells_around_cell["northwest"])
                and (not square_cells_around_cell["west"])
            ):
                aux_line_at_top_left = "west"
            elif (
                (square_cells_around_cell["north"])
                and square_cells_around_cell["northwest"]
                and (not square_cells_around_cell["west"])
            ):
                aux_line_at_top_left = "southwest"

        # bottom left point
        aux_line_at_bottom_left = None
        if boundary.check_if_domain_contains_point(self.Points[0], generator=self.CenterPoint):
            if (
                (not square_cells_around_cell["west"])
                and square_cells_around_cell["southwest"]
                and (square_cells_around_cell["south"])
            ):
                aux_line_at_bottom_left = "northwest"
            elif (
                (not square_cells_around_cell["west"])
                and (not square_cells_around_cell["southwest"])
                and (square_cells_around_cell["south"])
            ):
                aux_line_at_bottom_left = "west"
            elif (
                (square_cells_around_cell["west"])
                and (not square_cells_around_cell["southwest"])
                and (square_cells_around_cell["south"])
            ):
                aux_line_at_bottom_left = "southwest"
            elif (
                (square_cells_around_cell["west"])
                and (not square_cells_around_cell["southwest"])
                and (not square_cells_around_cell["south"])
            ):
                aux_line_at_bottom_left = "south"
            elif (
                (square_cells_around_cell["west"])
                and square_cells_around_cell["southwest"]
                and (not square_cells_around_cell["south"])
            ):
                aux_line_at_bottom_left = "southeast"

        # bottom right point
        aux_line_at_bottom_right = None
        if boundary.check_if_domain_contains_point(self.Points[1], generator=self.CenterPoint):
            if (
                (not square_cells_around_cell["south"])
                and square_cells_around_cell["southeast"]
                and (square_cells_around_cell["east"])
            ):
                aux_line_at_bottom_right = "southwest"
            elif (
                (not square_cells_around_cell["south"])
                and (not square_cells_around_cell["southeast"])
                and (square_cells_around_cell["east"])
            ):
                aux_line_at_bottom_right = "south"
            elif (
                (square_cells_around_cell["south"])
                and (not square_cells_around_cell["southeast"])
                and (square_cells_around_cell["east"])
            ):
                aux_line_at_bottom_right = "southeast"
            elif (
                (square_cells_around_cell["south"])
                and (not square_cells_around_cell["southeast"])
                and (not square_cells_around_cell["east"])
            ):
                aux_line_at_bottom_right = "east"
            elif (
                (square_cells_around_cell["south"])
                and square_cells_around_cell["southeast"]
                and (not square_cells_around_cell["east"])
            ):
                aux_line_at_bottom_right = "northeast"

        return [
            aux_line_at_bottom_left,
            aux_line_at_bottom_right,
            aux_line_at_top_right,
            aux_line_at_top_left,
        ]

    def create_aux_line(self, point_key="", aux_line_direction_key="", cellsize=0.0):
        if point_key == "northeast":
            if aux_line_direction_key == "southeast":
                tmp_aux_line = Line_Module.Line(
                    self.Points[2],
                    Point_Module.Point(x=self.Points[2].x + cellsize, y=self.Points[2].y - cellsize),
                )
            elif aux_line_direction_key == "east":
                tmp_aux_line = Line_Module.Line(
                    self.Points[2],
                    Point_Module.Point(x=self.Points[2].x + cellsize, y=self.Points[2].y),
                )
            elif aux_line_direction_key == "northeast":
                tmp_aux_line = Line_Module.Line(
                    self.Points[2],
                    Point_Module.Point(x=self.Points[2].x + cellsize, y=self.Points[2].y + cellsize),
                )
            elif aux_line_direction_key == "north":
                tmp_aux_line = Line_Module.Line(
                    self.Points[2],
                    Point_Module.Point(x=self.Points[2].x, y=self.Points[2].y + cellsize),
                )
            elif aux_line_direction_key == "northwest":
                tmp_aux_line = Line_Module.Line(
                    self.Points[2],
                    Point_Module.Point(x=self.Points[2].x - cellsize, y=self.Points[2].y + cellsize),
                )
            else:
                print(
                    "Class square method create_aux_line error: aux_line_direction_key is not valid for specified point"
                )
        elif point_key == "northwest":
            if aux_line_direction_key == "northeast":
                tmp_aux_line = Line_Module.Line(
                    self.Points[3],
                    Point_Module.Point(x=self.Points[3].x + cellsize, y=self.Points[3].y + cellsize),
                )
            elif aux_line_direction_key == "north":
                tmp_aux_line = Line_Module.Line(
                    self.Points[3],
                    Point_Module.Point(x=self.Points[3].x, y=self.Points[3].y + cellsize),
                )
            elif aux_line_direction_key == "northwest":
                tmp_aux_line = Line_Module.Line(
                    self.Points[3],
                    Point_Module.Point(x=self.Points[3].x - cellsize, y=self.Points[3].y + cellsize),
                )
            elif aux_line_direction_key == "west":
                tmp_aux_line = Line_Module.Line(
                    self.Points[3],
                    Point_Module.Point(x=self.Points[3].x - cellsize, y=self.Points[3].y),
                )
            elif aux_line_direction_key == "southwest":
                tmp_aux_line = Line_Module.Line(
                    self.Points[3],
                    Point_Module.Point(x=self.Points[3].x - cellsize, y=self.Points[3].y - cellsize),
                )
            else:
                print(
                    "Class square method create_aux_line error: aux_line_direction_key is not valid for specified point"
                )
        elif point_key == "southwest":
            if aux_line_direction_key == "northwest":
                tmp_aux_line = Line_Module.Line(
                    self.Points[0],
                    Point_Module.Point(x=self.Points[0].x - cellsize, y=self.Points[0].y + cellsize),
                )
            elif aux_line_direction_key == "west":
                tmp_aux_line = Line_Module.Line(
                    self.Points[0],
                    Point_Module.Point(x=self.Points[0].x - cellsize, y=self.Points[0].y),
                )
            elif aux_line_direction_key == "southwest":
                tmp_aux_line = Line_Module.Line(
                    self.Points[0],
                    Point_Module.Point(x=self.Points[0].x - cellsize, y=self.Points[0].y - cellsize),
                )
            elif aux_line_direction_key == "south":
                tmp_aux_line = Line_Module.Line(
                    self.Points[0],
                    Point_Module.Point(x=self.Points[0].x, y=self.Points[0].y - cellsize),
                )
            elif aux_line_direction_key == "southeast":
                tmp_aux_line = Line_Module.Line(
                    self.Points[0],
                    Point_Module.Point(x=self.Points[0].x + cellsize, y=self.Points[0].y - cellsize),
                )
            else:
                print(
                    "Class square method create_aux_line error: aux_line_direction_key is not valid for specified point"
                )
        elif point_key == "southeast":
            if aux_line_direction_key == "southwest":
                tmp_aux_line = Line_Module.Line(
                    self.Points[1],
                    Point_Module.Point(x=self.Points[1].x - cellsize, y=self.Points[1].y - cellsize),
                )
            elif aux_line_direction_key == "south":
                tmp_aux_line = Line_Module.Line(
                    self.Points[1],
                    Point_Module.Point(x=self.Points[1].x, y=self.Points[1].y - cellsize),
                )
            elif aux_line_direction_key == "southeast":
                tmp_aux_line = Line_Module.Line(
                    self.Points[1],
                    Point_Module.Point(x=self.Points[1].x + cellsize, y=self.Points[1].y - cellsize),
                )
            elif aux_line_direction_key == "east":
                tmp_aux_line = Line_Module.Line(
                    self.Points[1],
                    Point_Module.Point(x=self.Points[1].x + cellsize, y=self.Points[1].y),
                )
            elif aux_line_direction_key == "northeast":
                tmp_aux_line = Line_Module.Line(
                    self.Points[1],
                    Point_Module.Point(x=self.Points[1].x + cellsize, y=self.Points[1].y + cellsize),
                )
            else:
                print(
                    "Class square method create_aux_line error: aux_line_direction_key is not valid for specified point"
                )
        self.AuxLines.append(tmp_aux_line)

    def create_all_auxlines_for_square(self, boundary=None, cellsize=None):
        # boundary = in_mesh.Boundary
        keys_of_auxlines_to_create = self.decide_which_aux_lines_to_create(boundary=boundary)
        keys = ["southwest", "southeast", "northeast", "northwest"]
        for i in range(0, len(keys_of_auxlines_to_create)):
            key = keys_of_auxlines_to_create[i]
            if key is not None:
                self.create_aux_line(
                    point_key=keys[i],
                    aux_line_direction_key=keys_of_auxlines_to_create[i],
                    cellsize=cellsize,
                )

    def find_intersection_points_of_aux_lines_with_boundary(self, boundary=None):
        tmp_aux_lines_boundary_intersection_points = list()
        tmp_aux_lines_names_of_boundary_intersection_points = list()
        # tmp_name_of_intersected_boundary_at_edge_boundary_intersection_points = list()
        for aux_line in self.AuxLines:
            for boundary_element in boundary.BoundingGeometry:
                if boundary_element.decide_if_intersects_lineseg(aux_line):
                    tmp_aux_lines_boundary_intersection_points.append(
                        boundary_element.find_intersection_point_with_line_seg(aux_line)
                    )  # more than one boundary element could intersect an aux_line at the same point
                    tmp_aux_lines_names_of_boundary_intersection_points.append(boundary_element.BoundaryName)
        tmp_aux_lines_boundary_intersection_points = Point_Module.aspoint(tmp_aux_lines_boundary_intersection_points)
        return list(
            [
                tmp_aux_lines_boundary_intersection_points,
                tmp_aux_lines_names_of_boundary_intersection_points,
            ]
        )

from collections.abc import Iterable

import SolidLBM.util.geometry.Vector as Vector_Module


class Point(Vector_Module.Vector):
    __slots__ = (
        "x",
        "y",
        "z",
        "Neighbors",
        "BoundaryName",
        "DistanceToBoundary",
        "BoundaryNormals",
        "BoundaryClosestPoints",
        "ID",
        "IsLeftOrRightFaceOfCrack",
        "VolumeSurfaceMeasure",
    )

    def __init__(self, x=0.0, y=0.0, z=0.0, number=None):
        super(Point, self).__init__(x, y, z)
        self.Neighbors = dict()
        self.BoundaryName = dict()
        self.DistanceToBoundary = dict()
        self.BoundaryNormals = dict()
        self.BoundaryClosestPoints = dict()
        self.ID = number
        self.IsLeftOrRightFaceOfCrack = None
        self.VolumeSurfaceMeasure = None

    def __repr__(self):
        return "{}-object (ID: {}; x: {}, y: {}, z: {})".format(
            self.__class__.__name__, self.ID, self.x, self.y, self.z
        )

    def __hash__(self):
        return hash((self.x**3, self.y**2, self.z, self.ID))

    def __iadd__(self, other):
        if isinstance(other, Vector_Module.Vector):
            # as in Vector, but ensure returning a Point-instance
            result = super().__add__(other)
            return Point.cast(result)
        else:
            raise TypeError(f"Can't add {type(other)}-object to Point-object")

    @classmethod
    def cast(cls, obj):
        """
        cast a Vector-type object to a Point-type object
        """
        if isinstance(obj, cls):
            point = obj
        else:
            x = obj.x
            y = obj.y
            z = obj.z
            num = getattr(obj, "ID", None)
            point = cls(x=x, y=y, z=z, number=num)
        return point

    def test_if_squares_around_point_exist(self):
        if self.BoundaryName:
            keys = ["x+", "y+", "x-", "y-"]
            tmp_list_of_cells = [1, 1, 1, 1, 1, 1, 1, 1]
            tmp_dict_of_cells = {
                "east": 0,
                "northeast": 0,
                "north": 0,
                "northwest": 0,
                "west": 0,
                "southwest": 0,
                "south": 0,
                "southeast": 0,
            }

            for i in range(0, 4):
                if keys[i] in self.Neighbors:
                    if keys[i] == "x+":
                        tmp_dict_of_cells["east"] = 1
                    elif keys[i] == "y+":
                        tmp_dict_of_cells["north"] = 1
                    elif keys[i] == "x-":
                        tmp_dict_of_cells["west"] = 1
                    elif keys[i] == "y-":
                        tmp_dict_of_cells["south"] = 1
                    # tmp_string = tmp_string + keys[i]
                    tmp_list_of_cells[i] = 1
                    # else:                     # keys[i] exists in Neighbors
                    if (
                        keys[i] == "x+"
                    ):  # right neighbor exists -> check for top and bottom neighbors of that cell
                        if "y+" in self.Neighbors["x+"].Neighbors:
                            tmp_list_of_cells[4] = 1
                            tmp_dict_of_cells["northeast"] = 1
                        if "y-" in self.Neighbors["x+"].Neighbors:
                            tmp_list_of_cells[7] = 1
                            tmp_dict_of_cells["southeast"] = 1
                    elif keys[i] == "y+":
                        if "x+" in self.Neighbors["y+"].Neighbors:
                            tmp_list_of_cells[4] = 1
                            tmp_dict_of_cells["northeast"] = 1
                        if "x-" in self.Neighbors["y+"].Neighbors:
                            tmp_list_of_cells[5] = 1
                            tmp_dict_of_cells["northwest"] = 1
                    elif keys[i] == "x-":
                        if "y+" in self.Neighbors["x-"].Neighbors:
                            tmp_list_of_cells[5] = 1
                            tmp_dict_of_cells["northwest"] = 1
                        if "y-" in self.Neighbors["x-"].Neighbors:
                            tmp_list_of_cells[6] = 1
                            tmp_dict_of_cells["southwest"] = 1
                    elif keys[i] == "y-":
                        if "x-" in self.Neighbors["y-"].Neighbors:
                            tmp_list_of_cells[6] = 1
                            tmp_dict_of_cells["southwest"] = 1
                        if "x+" in self.Neighbors["y-"].Neighbors:
                            tmp_list_of_cells[7] = 1
                            tmp_dict_of_cells["southeast"] = 1
            return [tmp_list_of_cells, tmp_dict_of_cells]

    def create_neighbor(self, key, delta, seed_point):
        # generates neighboring Point-objects for each existing link in Neighbors
        if key not in self.Neighbors:
            if key == "x+":
                point = Point(
                    float(round(((self.x - seed_point.x) + delta) / delta, 0)) * delta
                    + seed_point.x,
                    self.y,
                    self.z,
                )
                n_key = "x-"
            elif key == "x-":
                point = Point(
                    float(round(((self.x - seed_point.x) - delta) / delta, 0)) * delta
                    + seed_point.x,
                    self.y,
                    self.z,
                )
                n_key = "x+"
            elif key == "y+":
                point = Point(
                    self.x,
                    float(round(((self.y - seed_point.y) + delta) / delta, 0)) * delta
                    + seed_point.y,
                    self.z,
                )
                n_key = "y-"
            elif key == "y-":
                point = Point(
                    self.x,
                    float(round(((self.y - seed_point.y) - delta) / delta, 0)) * delta
                    + seed_point.y,
                    self.z,
                )
                n_key = "y+"
            elif key == "z+":
                point = Point(self.x, self.y, self.z + delta)
                n_key = "z-"  # TODO
            elif key == "z-":
                point = Point(self.x, self.y, self.z - delta)
                n_key = "z+"  # TODO
            # D2Q9
            elif key == "x+y+" or key == "y+x+":
                point = Point(
                    float(round(((self.x - seed_point.x) + delta) / delta, 0)) * delta
                    + seed_point.x,
                    float(round(((self.y - seed_point.y) + delta) / delta, 0)) * delta
                    + seed_point.y,
                    self.z,
                )
                n_key = "x-y-"
            elif key == "x+y-" or key == "y-x+":
                point = Point(
                    float(round(((self.x - seed_point.x) + delta) / delta, 0)) * delta
                    + seed_point.x,
                    float(round(((self.y - seed_point.y) - delta) / delta, 0)) * delta
                    + seed_point.y,
                    self.z,
                )
                n_key = "x-y+"
            elif key == "x-y-" or key == "y-x-":
                point = Point(
                    float(round(((self.x - seed_point.x) - delta) / delta, 0)) * delta
                    + seed_point.x,
                    float(round(((self.y - seed_point.y) - delta) / delta, 0)) * delta
                    + seed_point.y,
                    self.z,
                )
                n_key = "x+y+"
            elif key == "x-y+" or key == "y+x-":
                point = Point(
                    float(round(((self.x - seed_point.x) - delta) / delta, 0)) * delta
                    + seed_point.x,
                    float(round(((self.y - seed_point.y) + delta) / delta, 0)) * delta
                    + seed_point.y,
                    self.z,
                )
                n_key = "x+y-"

            self.Neighbors[key] = point
            point.Neighbors[n_key] = self
            return point
        else:
            return None

    def remove_neighbor(self, key):
        del self.Neighbors[key]

    def ccw(self, point1, point2):
        """checks if points are arranged counter-clockwise
        http://ssw.jku.at/Teaching/Lectures/UZR/Algo2/2006S/Unterlagen/Algo2-07-Geometrie_4.pdf
        """
        if isinstance(point1, Point) and isinstance(point2, Point):
            dx1 = point1.x - self.x
            dy1 = point1.y - self.y
            dx2 = point2.x - self.x
            dy2 = point2.y - self.y

            if dy1 * dx2 < dy2 * dx1:
                return 1
            elif dy1 * dx2 > dy2 * dx1:
                return -1
            else:  # -> kollinear
                if dx1 * dx2 < 0 or dy1 * dy2 < 0:  # p0 in der Mitte
                    return -1
                elif (dx1**2 + dy1**2) >= dx2**2 + dy2**2:  # p2 in der Mitte
                    return 0
                else:
                    return 1  # p1 in der Mitte

    def dist(self, other):
        """absolute distance from Point to other"""
        assert isinstance(other, Vector_Module.Vector)
        d = (other - self).absolute_value()
        return abs(d)


def aspoint(obj):
    """
    ensures having objects of Point-type
    """
    if isinstance(obj, Point):
        # return Point-object unaltered
        return obj
    elif isinstance(obj, Vector_Module.Vector):
        # cast Vector
        return Point.cast(obj)
    elif hasattr(obj, "P1") and hasattr(obj, "P2"):
        # obj (e.g. Line) has two Points
        return (obj.P1, obj.P2)
    elif hasattr(obj, "x") and hasattr(obj, "y") and hasattr(obj, "z"):
        # any object with coordinates, e.g. from namedtuple
        return Point.cast(obj)
    elif isinstance(obj, Iterable):
        # for iterable objects (e.g. lists), call method recursively
        # must be last, since Iterables might have coordinates (e.g. namedtuple)
        obj_list = []
        for e in obj:
            e = aspoint(e)
            obj_list.append(e)
        return obj_list
    else:
        raise TypeError(
            "Cannot cast {}-object as Point-object".format(obj.__class__.__name__)
        )
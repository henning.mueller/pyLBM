import SolidLBM.util.geometry.Vector as Vector_Module


class Triangle(object):
    def __init__(self, points=[Vector_Module.Vector(x=None, y=None, z=None)] * 3):
        data_types = [isinstance(point, Vector_Module.Vector) for point in points]
        if False in data_types:
            print("Class Box Error: Input Vector-objects!")
        else:
            self.Points = points

    def test_if_point_is_in_box(self, test_point, tol):
        # https://stackoverflow.com/questions/2049582/how-to-determine-if-a-point-is-in-a-2d-triangle
        if isinstance(test_point, Vector_Module.Vector):
            px = test_point.x
            py = test_point.y

            if self.Points[0].x is None:
                print("hi")

            p0x = self.Points[0].x
            p0y = self.Points[0].y
            p1x = self.Points[1].x
            p1y = self.Points[1].y
            p2x = self.Points[2].x
            p2y = self.Points[2].y

            area = 0.5 * (-p1y * p2x + p0y * (-p1x + p2x) + p0x * (p1y - p2y) + p1x * p2y)
            abs_area = abs(area)

            if area > 0:
                sp = p0y * p2x - p0x * p2y + (p2y - p0y) * px + (p0x - p2x) * py
                tp = p0x * p1y - p0y * p1x + (p0y - p1y) * px + (p1x - p0x) * py
            else:
                sp = -(p0y * p2x - p0x * p2y + (p2y - p0y) * px + (p0x - p2x) * py)
                tp = -(p0x * p1y - p0y * p1x + (p0y - p1y) * px + (p1x - p0x) * py)

            if (sp > -tol) & ((tp > -tol) & ((sp + tp) - tol < 2.0 * abs_area)):
                return True
            else:
                return False

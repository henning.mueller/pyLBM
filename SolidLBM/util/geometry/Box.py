import SolidLBM.util.geometry.Point as Point_Module


class Box(object):
    def __init__(self, points=None):
        data_types = [type(point) == Point_Module.Point for point in points]
        if False in data_types:
            print("Class Box Error: Input Point-objects!")
        else:
            self.Points = points

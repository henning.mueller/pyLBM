import SolidLBM.util.geometry.Vector as Vector_Module


class Quadrilateral(object):
    def __init__(self, points=[Vector_Module.Vector(x=None, y=None, z=None)] * 4):
        self.Points = points
        self.SideLenghts = list()

    def get_area(self):
        # shoelace formula https://codereview.stackexchange.com/questions/88010/area-of-an-irregular-polygon
        points = self.Points
        area = 0.0
        q = points[-1]
        for p in points:
            area += p.x * q.y - p.y * q.x
            q = p
        return abs(area / 2)

    def create_square_around_center_point(
        self, center_point=Vector_Module.Vector(x=None, y=None, z=None), side_length=0.0
    ):
        self.Points[0] = Vector_Module.Vector(x=center_point.x - side_length, y=center_point.y - side_length)
        self.Points[1] = Vector_Module.Vector(x=center_point.x + side_length, y=center_point.y - side_length)
        self.Points[2] = Vector_Module.Vector(x=center_point.x + side_length, y=center_point.y + side_length)
        self.Points[3] = Vector_Module.Vector(x=center_point.x - side_length, y=center_point.y + side_length)

    def compute_side_lengths(self):
        for i in range(0, len(self.Points) - 1):
            tmp_vec = self.Points[i + 1] - self.Points[i]
            self.SideLenghts.append(tmp_vec.absolute_value())
        tmp_vec = self.Points[0] - self.Points[len(self.Points) - 1]
        self.SideLenghts.append(tmp_vec.absolute_value())

import numpy as np

import SolidLBM.util.geometry.Vector as Vector_Module
import SolidLBM.util.geometry.Line as Line_Module
import SolidLBM.util.geometry.Geometric_Object as Geometric_Object_Module


class Circle(Geometric_Object_Module.Geometric_Object):
    def __init__(self, p1=None, r=None, hole=False):
        if isinstance(p1, Vector_Module.Vector) and r > 0.0:
            # if type(p1) == Vector_Module.Vector and r > 0.0:
            self.P1 = p1
            self.R = r
            self.Hole = hole  # True: outward normal points inside circle ( -> hole -> no mesh in circle)
        # False: outward normal points outside circle ( -> no hole -> mesh inside circle)
        else:
            print("Class Circle Error: Input Point-objects and make sure that radius is > 0!")

    def compute_normal(self, vector, key="", tol=1.0e-15):
        if isinstance(vector, Vector_Module.Vector):
            if (
                abs(((vector.x - self.P1.x) ** 2 + (vector.y - self.P1.y) ** 2 - self.R**2) / self.R) < tol
            ):  # point is part of circle
                radial_vec = vector - self.P1
                normal = radial_vec.normalized()
                if self.Hole:
                    return [-normal]
                else:
                    return [normal]
            else:
                print("Class Circle / Method compute normal Error: Point is not part of circle cannot compute normal")
        else:
            raise TypeError("Class Circle/ Method compute_normal Error: Input Point-objects!")

    def compute_shortest_distance_to_point(self, point):
        if isinstance(point, Vector_Module.Vector):
            v = point - self.P1
            if v.absolute_value() != 0.0:
                closest_point = self.P1 + v / v.absolute_value() * self.R
                tmp_vec = point - closest_point
                shortest_distance_to_point = tmp_vec.absolute_value()
                return [shortest_distance_to_point, closest_point]
            else:
                print("Class Circle / Method compute_shortest_distance_to_point Error: ")
        else:
            raise TypeError("Can't compute shortest distance between Point-object and {}-object".format(type(point)))

    def find_intersection_point_with_line_seg(self, line_seg):
        if isinstance(line_seg, Line_Module.Line):
            ab = line_seg.P2 - line_seg.P1
            abs_ab = ab.absolute_value()
            ac = self.P1 - line_seg.P1
            e_ab = line_seg.P2 - line_seg.P1
            if e_ab.absolute_value() != 0.0:
                e_ab = e_ab / e_ab.absolute_value()
            else:
                print(
                    "Class circle/ method find_intersection_point_with_line_seg: Points of line segment cannot be identical"
                )
            abs_ad = ac * e_ab
            ad = abs_ad * e_ab  # project on ab
            td = abs_ad / abs_ab
            d = line_seg.P1 + ad  # closest point on line to center of circle
            cd = d - self.P1  #
            abs_cd = cd.absolute_value()

            if (
                abs_cd <= self.R
            ):  # there is an intersection of the line AB and the circle (maybe no intersection with lineseg)
                if abs_cd == self.R:  # line segment is tangent to circle -> d is closest point
                    if td <= 1.0 and td >= 0.0:  # d (closest point) is part of lineseg
                        return list([Vector_Module.Vector(x=d.x, y=d.y)])
                    else:  # no intersection
                        return None
                elif abs_cd < self.R:  # line segment may intersect circle up to two times
                    ti = np.sqrt(self.R**2 - abs_cd**2) / ab.absolute_value()
                    i1 = d + ti * ab.absolute_value() * e_ab
                    t1 = td + ti
                    i1_is_part_of_line_seg = t1 >= 0.0 and t1 <= 1.0  # intersection point is part of circle_seg
                    i2 = d - ti * ab.absolute_value() * e_ab
                    t2 = td - ti
                    i2_is_part_of_line_seg = t2 >= 0.0 and t2 <= 1.0
                    if i1_is_part_of_line_seg and i2_is_part_of_line_seg:
                        return list(
                            [
                                Vector_Module.Vector(x=i1.x, y=i1.y),
                                Vector_Module.Vector(x=i2.x, y=i2.y),
                            ]
                        )
                    elif i1_is_part_of_line_seg:
                        return list([Vector_Module.Vector(x=i1.x, y=i1.y)])
                    elif i2_is_part_of_line_seg:
                        return list([Vector_Module.Vector(x=i2.x, y=i2.y)])
                    else:  # line segment completely inside circle
                        return None
            else:  # no intersection possible
                return None

    def decide_if_intersects_lineseg(self, line_seg):
        if isinstance(line_seg, Line_Module.Line):
            return self.find_intersection_point_with_line_seg(line_seg) is not None

    def decide_if_point_is_on_element(self, point, tol=1.0e-15):
        if isinstance(point, Vector_Module.Vector):
            # tol = 1.0e-15
            r_vec = point - self.P1
            # angle = np.arctan2(r_vec.y, r_vec.x)

            if abs(r_vec.absolute_value() - self.R) < tol:
                return True
            else:
                return False

    def test_if_inside_boundary(self, point, tol=0.0, **kwargs):
        r_vec = point - self.P1
        if self.Hole:
            inside = r_vec.absolute_value() ** 2 >= self.R**2 - tol
        else:
            inside = r_vec.absolute_value() ** 2 <= self.R**2 + tol
        return inside


class CircleSeg(Circle, Geometric_Object_Module.Geometric_Object):
    def __init__(self, p1=None, r=None, hole=False, start_angle=0.0, end_angle=2.0 * np.pi):
        # super(Point, self).__init__(x, y, z)
        super(CircleSeg, self).__init__(p1, r, hole)
        if start_angle < 0.0:
            start_angle = start_angle + 2.0 * np.pi
        if end_angle < 0.0:
            end_angle = end_angle + 2.0 * np.pi
        self.StartAngleRad = start_angle
        self.EndAngleRad = end_angle

    def decide_if_point_is_on_element(self, point, tol=1.0e-15):
        if type(point) == Vector_Module.Vector:
            # tol = 1.0e-15
            r_vec = point - self.P1
            angle = np.arctan2(r_vec.y, r_vec.x)

            if angle < 0.0:
                angle = 2.0 * np.pi + angle

            bool1 = abs(((point.x - self.P1.x) ** 2 + (point.y - self.P1.y) ** 2 - self.R**2) / (self.R)) < tol

            if self.StartAngleRad < self.EndAngleRad:
                bool2 = angle >= self.StartAngleRad and angle <= self.EndAngleRad
            if self.StartAngleRad > self.EndAngleRad:
                bool2 = (angle >= 0 and angle <= self.EndAngleRad) or (
                    angle >= self.StartAngleRad and angle <= 2.0 * np.pi
                )
            return bool1 and bool2

    def compute_shortest_distance_to_point(self, point):
        if isinstance(point, Vector_Module.Vector):
            v = point - self.P1
            if v.absolute_value() != 0.0:
                closest_point = self.P1 + v / v.absolute_value() * self.R
                if self.decide_if_point_is_on_element(closest_point):
                    tmp_vec = point - closest_point
                else:
                    point_start = (
                        self.P1
                        + Vector_Module.Vector(x=np.cos(self.StartAngleRad), y=np.sin(self.StartAngleRad)) * self.R
                    )
                    point_end = (
                        self.P1 + Vector_Module.Vector(x=np.cos(self.EndAngleRad), y=np.sin(self.EndAngleRad)) * self.R
                    )
                    tmp_vec1 = point - point_start
                    tmp_vec2 = point - point_end
                    if tmp_vec1.absolute_value() < tmp_vec2.absolute_value():
                        tmp_vec = tmp_vec1
                        closest_point = point_start
                    else:
                        tmp_vec = tmp_vec2
                        closest_point = point_end
                shortest_distance_to_point = tmp_vec.absolute_value()
                return [shortest_distance_to_point, closest_point]
            else:
                print(
                    "Class Circle Seg / Method compute_shortest_distance_to_point Error: Point must be different from center point"
                )
        else:
            raise TypeError("Can't compute shortest distance between Point-object and {}-object".format(type(point)))

    def find_intersection_point_with_line_seg(self, line_seg):
        if isinstance(line_seg, Line_Module.Line):
            ab = line_seg.P2 - line_seg.P1
            abs_ab = ab.absolute_value()
            ac = self.P1 - line_seg.P1
            e_ab = line_seg.P2 - line_seg.P1
            if e_ab.absolute_value() != 0.0:
                e_ab = e_ab / e_ab.absolute_value()
            else:
                print(
                    "Class circle/ method find_intersection_point_with_line_seg: Points of line segment cannot be identical"
                )
            abs_ad = ac * e_ab
            ad = abs_ad * e_ab  # project on ab
            td = abs_ad / abs_ab
            d = line_seg.P1 + ad  # closest point on line to center of circle
            cd = d - self.P1  #
            abs_cd = cd.absolute_value()

            if (
                abs_cd <= self.R
            ):  # there is an intersection of the line AB and the circle (maybe no intersection with lineseg)
                if abs_cd == self.R:  # line segment is tangent to circle -> d is closest point
                    if td <= 1.0 and td >= 0.0:  # d (closest point) is part of lineseg
                        if self.decide_if_point_is_on_element(d):  # intersection point is part of circle_seg
                            return list([d])
                        else:
                            return None
                    else:  # no intersection
                        return None
                elif abs_cd < self.R:  # line segment may intersect circle up to two times
                    ti = np.sqrt(self.R**2 - abs_cd**2) / ab.absolute_value()
                    i1 = d + ti * ab.absolute_value() * e_ab
                    if self.decide_if_point_is_on_element(i1):  # intersection point needs to be part of circle_seg
                        t1 = td + ti
                        i1_is_part_of_line_seg = t1 >= 0.0 and t1 <= 1.0  # intersection point is part of circle_seg
                    else:
                        i1_is_part_of_line_seg = False  # aux just not part of circle
                    i2 = d - ti * ab.absolute_value() * e_ab
                    if self.decide_if_point_is_on_element(i2):  # intersection point needs to be part of circle_seg
                        t2 = td - ti
                        i2_is_part_of_line_seg = t2 >= 0.0 and t2 <= 1.0
                    else:
                        i2_is_part_of_line_seg = False  # aux just not part of circle_seg

                    if i1_is_part_of_line_seg and i2_is_part_of_line_seg:
                        return list([i1, i2])
                    elif i1_is_part_of_line_seg:
                        return list([i1])
                    elif i2_is_part_of_line_seg:
                        return list([i2])
                    else:  # line segment completely inside circle
                        return None
            else:  # no intersection possible
                return None

def tensordot(A, B):
    """Calculate the tensor dot product of two tensors."""
    if len(A) != len(B):
        raise ValueError("The tensors must have the same dimensions.")
    return sum(A[i][j] * B[i][j] for i in range(len(A)) for j in range(len(A)))

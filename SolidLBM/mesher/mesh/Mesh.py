from collections import deque
import logging
from pathlib import Path

# from SolidLBM.util.geometry.Vector import Vector
from SolidLBM.util.geometry.Point import Point
import SolidLBM.util.geometry.Crack as CrackModule
from SolidLBM.mesher.mesh.Boundary import (
    Boundary,
    compute_cell_volumes_and_areas_at_boundary_points,
)
import SolidLBM.mesher.mesh.InitialCondition as InitialConditionModule


class Mesh(object):
    neighbor_keys = {
        "D2Q5": ["x+", "x-", "y+", "y-"],
        "D2Q9": ["x+", "x-", "y+", "y-", "x+y+", "x-y+", "x-y-", "x+y-"],
    }
    inverse_keys = {
        "D2Q5": ["x-", "x+", "y-", "y+"],
        "D2Q9": ["x-", "x+", "y-", "y+", "x-y-", "x+y-", "x+y+", "x-y+"],
    }

    def __init__(
        self,
        name="",
        cell_size: float = 0.0,
        seed_point=None,
        working_directory: Path | str = "",
        mesh_type="D2Q5",
        **kwargs,
    ):
        self.Name = name
        self.WorkingDir = Path(working_directory)
        self.Boundary = Boundary(
            input_file_path=self.WorkingDir / f"{self.Name}.geo",
            hmeshable=cell_size,
        )
        self.CellSize = cell_size
        self.Points = list()
        self.Cells = None

        self.Seed_Point = self._get_seed_point(seed_point)
        self.MeshType = mesh_type
        self.vtuCells = list()
        self.InitialCondition = None

    @staticmethod
    def _get_seed_point(seed_point) -> Point:
        """Ensure seed_point is instance of Point-class

        Can be created from list or tuple of coordinates
        """
        if not isinstance(seed_point, Point):
            assert isinstance(seed_point, (list, tuple)) and 2 <= len(seed_point) <= 3
            seed_point = Point(*seed_point)
        return seed_point

    def compute_cell_volumes_areas_boundary_names_at_boundary_points(self):
        """call the static method and pass correctly instance attributes"""
        vsm = compute_cell_volumes_and_areas_at_boundary_points(
            cellsize=self.CellSize,
            points=self.Points,
            seedpoint=self.Seed_Point,
            boundary=self.Boundary,
        )
        return vsm

    def create_mesh_neighbor_points(self, verbose=False):
        """
        Create the mesh points in the domain with a flood fill algorithm.
        Cracks are ignored at first and handled later.
        """

        keys = self.neighbor_keys[self.MeshType]
        n_keys = self.inverse_keys[self.MeshType]

        if self.Boundary.Dimensions == "2D":
            # create all neighbors of seed point and save to Points
            deltah = self.CellSize
            seed_pt = self.Seed_Point
            seed_pt.ID = len(self.Points)
            self.Points.append(seed_pt)
            queue = deque()
            queue.append(seed_pt)

            # flood fill algorithm
            while queue:
                tmp_point = queue.popleft()
                for key, n_key in zip(keys, n_keys):
                    tmp_neighbor = tmp_point.create_neighbor(
                        key, deltah, seed_point=seed_pt
                    )
                    if tmp_neighbor:  # point created successfully else None is returned
                        existing_matching_points = (
                            pt for pt in queue if pt == tmp_neighbor
                        )

                        if tmp_neighbor in queue:
                            # tmp_neighbor already exists in Points -> link to tmp_point
                            match_pt = next(existing_matching_points)
                            tmp_point.Neighbors[key] = match_pt
                            match_pt.Neighbors[n_key] = tmp_point
                        elif self.Boundary.check_if_domain_contains_point(
                            tmp_neighbor, key=n_key
                        ):
                            # tmp_neighbor is in domain -> add to queue to create its neighbors
                            queue.append(tmp_neighbor)
                            tmp_neighbor.ID = len(self.Points)
                            self.Points.append(tmp_neighbor)
                        else:
                            # tmp_neighbor out of domain -> tmp_point next to boundary
                            self.Boundary.compute_rectangular_distance_to_boundary(
                                point=tmp_point, key=key
                            )
                            tmp_point.remove_neighbor(
                                key
                            )  # keep tmp_neighbor until distance to boundary is computed

                if __debug__ and verbose:
                    print(
                        "Creating Mesh -- current length of queue: {:04d} points ==> {:06d} points total".format(
                            len(queue), len(self.Points)
                        ),
                        end="\r",
                    )

            # add cracks to boundaries, as is expected by other methods
            self.Boundary.BoundingGeometry += self.Boundary.CrackBoundaries

            for crack in self.Boundary.CrackBoundaries:
                intersected_pts = CrackModule.find_boun_points_at_crack(
                    crack, self.Points, keys=keys, n_keys=n_keys
                )
                _ = CrackModule.process_links_of_boun_points(
                    intersected_pts, crack, self.Boundary, keys=keys, n_keys=n_keys
                )
            if verbose:
                print("")

    def plot_mesh(self, *, interactive=False, **kwargs):
        from SolidLBM.util.tools import plot_mesh as pltmsh

        fig = pltmsh(
            self.Points,
            self.Boundary,
            self.WorkingDir / self.Name,
            self.Boundary.Dimensions,
            **kwargs,
        )
        if interactive:
            return fig

    def print_to_file(self):
        # check if necessary fields of mesh are defined
        try:
            my_file = open(self.WorkingDir / f"{self.Name}.msh", "w")
        except IOError:
            print("Class Mesh/ Method print_to_file: The file cannot be opened!")

        #  write header info
        my_file.write("# CellSize = {0} \n".format(self.CellSize))
        my_file.write(
            "# n is number of missing lattice links at boundary lattice sites \n"
        )
        if self.MeshType == "D2Q5":
            my_file.write("# velocity set D2Q5 \n")
            my_file.write(
                "# ID | x | y | z | ID x+ | ID y+ | ID x- | ID y- [| n x Bnd IDs | n x Bnd Distance | n x [x,y] Closest Point on Boundary | n x [x,y] Bnd Normal at Closest Point] \n"
            )
            keys = ["x+", "y+", "x-", "y-"]
        elif self.MeshType == "D2Q9":
            my_file.write("# velocity set D2Q9 \n")
            my_file.write(
                "# ID | x | y | z | ID x+ | ID y+ | ID x- | ID y- | ID x+y+ | ID x-y+ | ID x-y- | ID x+y- [| n x Bnd IDs | n x Bnd Distance | n x [x,y] Closest Point on Boundary | n x [x,y] Bnd Normal at Closest Point] \n"
            )
            keys = ["x+", "y+", "x-", "y-", "x+y+", "x-y+", "x-y-", "x+y-"]

        for point in self.Points:
            my_file.write(
                "{0:6d} {1:16.8e} {2:16.8e} {3:16.8e} ".format(
                    point.ID, point.x, point.y, point.z
                )
            )
            for n_key in keys:  # set order of neighbors
                if n_key in point.Neighbors.keys():
                    my_file.write("{0:6d} ".format(point.Neighbors[n_key].ID))
                else:
                    my_file.write("{0:6d} ".format(-1))

            for key in keys:
                if key in point.BoundaryName.keys():
                    my_file.write("{0:6d} ".format(int(point.BoundaryName[key])))

            for key in keys:  # set order of boundary_names
                if key in point.DistanceToBoundary.keys():
                    my_file.write(
                        "{0:16.8e} ".format(float(point.DistanceToBoundary[key]))
                    )
            for key in keys:  # set order of boundary_names
                if key in point.BoundaryClosestPoints.keys():
                    my_file.write(
                        "{0:16.8e} {1:16.8e}".format(
                            float(point.BoundaryClosestPoints[key].x),
                            float(point.BoundaryClosestPoints[key].y),
                        )
                    )
            for key in keys:  # set order of boundary_names
                if key in point.BoundaryNormals.keys():
                    my_file.write(
                        "{0:16.8e} {1:16.8e}".format(
                            float(point.BoundaryNormals[key].x),
                            float(point.BoundaryNormals[key].y),
                        )
                    )
            my_file.write("\n")
        my_file.close()

    def print_alt_bc_to_file(self):
        # check if necessary fields of mesh are defined
        try:
            my_file = open(self.WorkingDir / f"{self.Name}.bc_alt", "w")
        except IOError:
            print("Class Mesh/ Method print_to_file: The file cannot be opened!")

        #  write header info
        my_file.write(
            "# This file contains cell volumes and associated surfaces for alternative implementation of bc \n"
        )
        my_file.write(
            "# ID | surface measure to interior x+ y+ x- y- x+y+ x+y- x-y+ x-y- | cell volume | n x [Bnd IDs, surface measure] \n"
        )

        # tmp_point_volume_surface_measure = list([volume, surface_measures_of_boundary_ids_at_point, tmp_polygon])

        for point in self.Points:
            if point.VolumeSurfaceMeasure:
                my_file.write("{0:6d} ".format(point.ID))

                inner_surface_measures = point.VolumeSurfaceMeasure[3]

                keys = ("x+", "y+", "x-", "y-", "x+y+", "x+y-", "x-y+", "x-y-")

                tmp_measures = (
                    inner_surface_measures.get(key, -1)
                    if inner_surface_measures[key] is not None
                    else -1
                    for key in keys
                )
                my_file.write(
                    " {0:16.8e} {1:16.8e} {2:16.8e} {3:16.8e} {4:16.8e} {5:16.8e} {6:16.8e} {7:16.8e}".format(
                        *tmp_measures
                    )
                )

                # write volume
                my_file.write("{0:16.8e} ".format(point.VolumeSurfaceMeasure[0]))

                surface_measures_of_boundary_ids_at_point = point.VolumeSurfaceMeasure[
                    1
                ]
                # write boundary ids and associated boundary measure
                for i in range(0, len(surface_measures_of_boundary_ids_at_point[0])):
                    my_file.write(
                        "{0:6d} {1:16.8e} ".format(
                            surface_measures_of_boundary_ids_at_point[0][i],
                            surface_measures_of_boundary_ids_at_point[1][i],
                        )
                    )
                my_file.write("\n")
        my_file.close()

    def read_alt_bc_from_file(self):
        with open(self.WorkingDir / f"{self.Name}.bc_alt", "r") as my_file:
            my_line = my_file.readline()  # read header info
            while "#" in my_line:
                my_current_pos = my_file.tell()
                my_line = my_file.readline()

            my_file.seek(my_current_pos)
            for my_line in my_file:  # create point objects
                my_strings = my_line.split()

                tmp_id = int(my_strings[0])
                tmp_volume = float(my_strings[1])
                number_of_boundary_elements = int((len(my_strings) - 2) / 2)
                tmp_boundary_ids_at_point = list()
                tmp_boundary_surface_measure_at_point = list()
                for i in range(0, number_of_boundary_elements):
                    tmp_boundary_id = int(my_strings[2 + 2 * i])
                    tmp_boundary_ids_at_point.append(tmp_boundary_id)
                    tmp_surface_measure = float(my_strings[2 + 2 * i + 1])
                    tmp_boundary_surface_measure_at_point.append(tmp_surface_measure)
                tmp_volume_boundary_surface_measure_at_point = list(
                    [
                        tmp_volume,
                        [
                            tmp_boundary_ids_at_point,
                            tmp_boundary_surface_measure_at_point,
                        ],
                    ]
                )
                self.Points[
                    tmp_id
                ].VolumeSurfaceMeasure = tmp_volume_boundary_surface_measure_at_point

    def read_from_file(self):
        try:
            my_file = open(self.WorkingDir / f"{self.Name}.msh", "r")
        except IOError:
            logging.error("The file cannot be opened!")

        my_line = my_file.readline()  # read header info
        while "#" in my_line:
            my_strings = my_line.split()
            if "CellSize" in my_strings:
                self.CellSize = float(my_strings[3])
            if "MeshType" in my_strings:
                self.MeshType = my_strings[3]
            my_current_pos = my_file.tell()
            my_line = my_file.readline()
        #
        my_file.seek(my_current_pos)
        for my_line in my_file:  # create point objects
            my_strings = my_line.split()
            tmp_point = Point(
                number=int(my_strings[0]),
                x=float(my_strings[1]),
                y=float(my_strings[2]),
                z=float(my_strings[3]),
            )
            self.Points.append(tmp_point)

        my_file.seek(my_current_pos)  # go back to start of file
        for my_line in (
            my_file
        ):  # read neighbor information/ distances to boundary for boundary points
            neighbor_keys = list(
                ["x+", "y+", "x-", "y-"]
            )  # TODO: use neighbor keys from dict
            my_strings = my_line.split()
            tmp_distance_to_boundary = dict()
            tmp_boundary_name = dict()
            tmp_boundary_closest_points = dict()
            tmp_boundary_normals = dict()
            for i in range(0, 4):
                if int(my_strings[4 + i]) != -1:
                    self.Points[int(my_strings[0])].Neighbors[neighbor_keys[i]] = (
                        self.Points[int(my_strings[4 + i])]
                    )
                else:
                    tmp_distance_to_boundary[neighbor_keys[i]] = -1
            for i in range(0, my_strings.count("-1")):
                tmp_boundary_name[list(tmp_distance_to_boundary.keys())[i]] = int(
                    my_strings[8 + i]
                )
                tmp_distance_to_boundary[list(tmp_distance_to_boundary.keys())[i]] = (
                    float(my_strings[8 + my_strings.count("-1") + i])
                )
                tmp_boundary_closest_points[
                    list(tmp_distance_to_boundary.keys())[i]
                ] = Point(
                    x=float(my_strings[8 + 2 * my_strings.count("-1") + 2 * i]),
                    y=float(my_strings[8 + 2 * my_strings.count("-1") + 2 * i + 1]),
                    z=0.0,
                )
                tmp_boundary_normals[list(tmp_distance_to_boundary.keys())[i]] = Point(
                    x=float(my_strings[8 + 4 * my_strings.count("-1") + 2 * i]),
                    y=float(my_strings[8 + 4 * my_strings.count("-1") + 2 * i + 1]),
                    z=0.0,
                )
            self.Points[int(my_strings[0])].BoundaryName = tmp_boundary_name
            self.Points[
                int(my_strings[0])
            ].DistanceToBoundary = tmp_distance_to_boundary
            self.Points[
                int(my_strings[0])
            ].BoundaryClosestPoints = tmp_boundary_closest_points
            self.Points[int(my_strings[0])].BoundaryNormals = tmp_boundary_normals
        my_file.close()

    def tmp_print_initial_conditions_to_file(self):
        self.InitialCondition = InitialConditionModule.InitialCondition(
            Name=self.Name, WorkingDir=self.WorkingDir, Points=self.Points
        )
        self.InitialCondition.tmp_print_initial_conditions_to_file()

    def associated_to_quad_mesh(self):
        """??? comments
        creates vtu - cells and saves them to self.vtuCells
        :return:
        """
        if len(self.Points) > 1:
            xMin = self.Points[0].x
            xMax = self.Points[0].x
            yMin = self.Points[0].y
            yMax = self.Points[0].y

            for pkt in self.Points:
                xMin = min(xMin, pkt.x)
                yMin = min(yMin, pkt.y)
                xMax = max(xMax, pkt.x)
                yMax = max(yMax, pkt.y)

            w, h = (
                int(round((xMax - xMin) / self.CellSize + 1)),
                int(round((yMax - yMin) / self.CellSize + 1)),
            )
            tmp_grid = [[Point(number=-1) for y in range(h)] for x in range(w)]

            for pkt in self.Points:
                tmp_grid[int(round((pkt.x - xMin) / self.CellSize))][
                    int(round((pkt.y - yMin) / self.CellSize))
                ] = pkt

            for j in range(h - 1):
                for i in range(w - 1):
                    if (
                        # tmp_grid[i][j] > -1 and tmp_grid[i+1][j] > -1 and
                        # tmp_grid[i+1][j+1] > -1 and tmp_grid[i][j+1] > -1 and
                        tmp_grid[i][j]
                        in self.Points[tmp_grid[i + 1][j].ID].Neighbors.values()
                        and tmp_grid[i][j]
                        in self.Points[tmp_grid[i][j + 1].ID].Neighbors.values()
                        and tmp_grid[i + 1][j]
                        in self.Points[tmp_grid[i][j].ID].Neighbors.values()
                        and tmp_grid[i + 1][j]
                        in self.Points[tmp_grid[i + 1][j + 1].ID].Neighbors.values()
                        and tmp_grid[i][j + 1]
                        in self.Points[tmp_grid[i][j].ID].Neighbors.values()
                        and tmp_grid[i][j + 1]
                        in self.Points[tmp_grid[i + 1][j + 1].ID].Neighbors.values()
                        and tmp_grid[i + 1][j + 1]
                        in self.Points[tmp_grid[i + 1][j].ID].Neighbors.values()
                        and tmp_grid[i + 1][j + 1]
                        in self.Points[tmp_grid[i][j + 1].ID].Neighbors.values()
                    ):
                        self.vtuCells.append(
                            [
                                tmp_grid[i][j].ID,
                                tmp_grid[i + 1][j].ID,
                                tmp_grid[i + 1][j + 1].ID,
                                tmp_grid[i][j + 1].ID,
                            ]
                        )
                    else:
                        print("no Cell")
        else:
            print("Error: there is no mesh to generate cells from")

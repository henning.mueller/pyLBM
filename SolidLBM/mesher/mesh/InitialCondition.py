import logging
import numpy as np

from pathlib import Path


class InitialCondition(object):
    __slots__ = ("Name", "WorkingDir", "Points", "Config")

    def __init__(self, Name, WorkingDir, Points):
        self.Name = Name
        self.WorkingDir = Path(WorkingDir)

        self.Points = Points
        self.Config = self.read_initial_configuration()

    def read_initial_configuration(self) -> list[str]:
        cfg = []
        config_file = self.WorkingDir / f"{self.Name}.cfg"
        if config_file.is_file():
            with open(config_file, "r") as cfg_file:
                lines = cfg_file.readlines()
                for tmp_line in lines:
                    cfg.append(tmp_line.strip().split())
        return cfg

    def tmp_print_initial_conditions_to_file(self):
        try:
            my_file = open(self.WorkingDir / f"{self.Name}.init", "w")
        except IOError:
            logging.info("The IC file cannot be opened!")

        my_file.write("# This file describes the initial fields in a LBM lattice \n")
        my_file.write("# ID | w | wdot \n")

        bc_type = "1"   # assume zero BC as default

        for tmp_BC in self.Config:
            if "initial_condition:" in tmp_BC:
                bc_type = tmp_BC[1]
            if "phase_change_1:" in tmp_BC:
                w_phase_change_1 = tmp_BC[1]
                wdot_phase_change_1 = tmp_BC[2]
            if "phase_change_2:" in tmp_BC:
                w_phase_change_2 = tmp_BC[1]
                wdot_phase_change_2 = tmp_BC[2]
            if "period_1:" in tmp_BC:
                w_period_1 = tmp_BC[1]
                wdot_period_1 = tmp_BC[2]
            if "period_2:" in tmp_BC:
                w_period_2 = tmp_BC[1]
                wdot_period_2 = tmp_BC[2]
            if "amplitude_lin_1:" in tmp_BC:
                w_amplitude_lin_1 = tmp_BC[1]
                wdot_amplitude_lin_1 = tmp_BC[2]
            if "amplitude_lin_2:" in tmp_BC:
                w_amplitude_lin_2 = tmp_BC[1]
                wdot_amplitude_lin_2 = tmp_BC[2]
            if "amplitude_lin_3:" in tmp_BC:
                w_amplitude_lin_3 = tmp_BC[1]
                wdot_amplitude_lin_3 = tmp_BC[2]
            if "amplitude_1:" in tmp_BC:
                w_amplitude_1 = tmp_BC[1]
                wdot_amplitude_1 = tmp_BC[2]
            if "amplitude_2:" in tmp_BC:
                w_amplitude_2 = tmp_BC[1]
                wdot_amplitude_2 = tmp_BC[2]
            if "translation:" in tmp_BC:
                w_translation = tmp_BC[1]
                wdot_translation = tmp_BC[2]
            if "amplitude_navier_lin_1:" in tmp_BC:
                u_amplitude_lin_1 = tmp_BC[1]
            if "amplitude_navier_lin_2:" in tmp_BC:
                v_amplitude_lin_1 = tmp_BC[1]

        if bc_type == "1":
            my_file.write("# ZERO_BC \n")

            for point in self.Points:
                w = 0.0
                wdot = 0.0
                my_file.write("{0} {1} {2} \n".format(point.ID, w, wdot))

        elif bc_type == "2":
            my_file.write("# LINE_BC \n")

            for point in self.Points:
                w = (
                    float(w_amplitude_lin_1) * point.x
                    + float(w_amplitude_lin_2) * point.y
                    + float(w_amplitude_lin_3) * point.x * point.y
                )
                wdot = (
                    float(wdot_amplitude_lin_1) * point.x
                    + float(wdot_amplitude_lin_2) * point.y
                    + float(wdot_amplitude_lin_3) * point.x * point.y
                )

                my_file.write("{0} {1} {2} \n".format(point.ID, w, wdot))

        elif bc_type == "3":
            my_file.write("# COS_BC \n")
            i = 0
            for point in self.Points:
                w = (
                    float(w_amplitude_1)
                    * np.cos(
                        self.Points[i].x * 2.0 * np.pi / float(w_period_1)
                        + float(w_phase_change_1)
                    )
                    + float(w_amplitude_2)
                    * np.cos(
                        self.Points[i].y * 2.0 * np.pi / float(w_period_2)
                        + float(w_phase_change_2)
                    )
                    + float(w_translation)
                )

                wdot = (
                    float(wdot_amplitude_1)
                    * np.cos(
                        self.Points[i].x * 2.0 * np.pi / float(wdot_period_1)
                        + float(wdot_phase_change_1)
                    )
                    + float(wdot_amplitude_2)
                    * np.cos(
                        self.Points[i].y * 2.0 * np.pi / float(wdot_period_2)
                        + float(wdot_phase_change_2)
                    )
                    + float(wdot_translation)
                )

                my_file.write("{0} {1} {2} \n".format(point.ID, w, wdot))
                i += 1

        # this is just a test
        elif bc_type == "4":
            my_file.write("# LINE_BC \n")
            for point in self.Points:
                u = float(u_amplitude_lin_1) * point.x
                v = float(v_amplitude_lin_1) * point.y
                wdot = 0.0

                my_file.write(
                    "{0} {1} {2} {3} {4} \n".format(point.ID, u, v, wdot, wdot)
                )

        elif bc_type == "5":
            my_file.write("# USER_BC\n")
            for point in self.Points:
                u = point.x + point.y
                v = point.x**2 - point.y**2 + 2.0 * point.x * point.y
                wdot = 0.0

                my_file.write(
                    "{0} {1} {2} {3} {4} \n".format(point.ID, u, v, wdot, wdot)
                )

        my_file.close()

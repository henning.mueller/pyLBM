import inspect
import sys
# import numpy as np

from SolidLBM.util.geometry.Vector import Vector, Vector_With_Displacement
from SolidLBM.util.geometry.Point import Point
from SolidLBM.util.geometry.Line import Line
from SolidLBM.util.geometry.Crack import Crack
from SolidLBM.util.geometry.Polygon import Polygon
from SolidLBM.util.geometry.Square import Square
from SolidLBM.util.geometry.Circle import Circle, CircleSeg


# PI = np.pi
PI = 3.141592653589793


class Boundary(object):
    """
    container for geometric elements that define the boundary of a solid
    """

    def __init__(self, input_file_path=None, hmeshable=0.05):
        self.Points = None  # used to define higher order geometric elements
        self.BoundingGeometry = None  # container for all boundary geometric objects except for cracks
        self.CrackBoundaries = None  # container for crack objects
        self.Dimensions = None  # Dimensions of the geometry "2D" or "3D"
        # self.BoundingBox = None                 # ?
        self.InputFilePath = input_file_path
        self.read_input_file()
        # self.correctBoundary(deltah=hmeshable)

    def read_input_file(self):
        """
            reads boundary geometry from InputFilePath.geo and creates
            connected geometric objects that represent the boundary
        :return:
        """
        try:
            my_file = open(self.InputFilePath, "r")
            my_file_lines = [f_line.rstrip("\n") for f_line in my_file]
            if not my_file_lines[0].find("2D") == -1:  # 2D
                self.Dimensions = "2D"
                my_points = [
                    f_line.split()
                    for f_line in my_file_lines[
                        (my_file_lines.index("POINTS") + 1) : (my_file_lines.index("ENDPOINTS"))
                    ]
                ]
                point_list = []
                line_list = []
                circle_list = []
                circleseg_list = []
                crack_list = []

                for point in my_points:
                    tmp_point = Point(float(point[0]), float(point[1]), float(point[2]))
                    point_list.append(tmp_point)

                if "LINES" in my_file_lines:
                    my_lines = [
                        f_line.split()
                        for f_line in my_file_lines[
                            (my_file_lines.index("LINES") + 1) : (my_file_lines.index("ENDLINES"))
                        ]
                    ]
                    for line in my_lines:
                        tmp_line = Line(point_list[int(line[0]) - 1], point_list[int(line[1]) - 1])
                        tmp_line.BoundaryName = int(line[2])  # read boundary condition key
                        tmp_line.Normal = tmp_line.compute_normal()[
                            0
                        ]  # not here (may be dependent on exact point on line TODO)
                        line_list.append(tmp_line)

                if "CRACKS" in my_file_lines:
                    my_lines = [
                        f_line.split()
                        for f_line in my_file_lines[
                            (my_file_lines.index("CRACKS") + 1) : (my_file_lines.index("ENDCRACKS"))
                        ]
                    ]
                    for line in my_lines:
                        tmp_line = Crack(point_list[int(line[0]) - 1], point_list[int(line[1]) - 1])
                        tmp_line.BoundaryName = int(line[2])  # read boundary condition key
                        tmp_line.BoundaryName2Crack = int(line[3])
                        # tmp_line.Normal = tmp_line.compute_normal()  # not here (may be dependent on exact point on line TODO)
                        crack_list.append(tmp_line)

                if "CIRCLES" in my_file_lines:
                    my_circles = [
                        f_line.split()
                        for f_line in my_file_lines[
                            (my_file_lines.index("CIRCLES") + 1) : (my_file_lines.index("ENDCIRCLES"))
                        ]
                    ]
                    for circle in my_circles:
                        tmp_circle = Circle(p1=point_list[int(circle[0]) - 1], r=float(circle[1]))
                        tmp_circle.BoundaryName = int(circle[3])
                        if int(circle[2]):
                            tmp_circle.Hole = True
                        else:
                            tmp_circle.Hole = False
                        circle_list.append(tmp_circle)

                if "CIRCLESEGMENTS" in my_file_lines:
                    my_circle_segs = [
                        f_line.split()
                        for f_line in my_file_lines[
                            (my_file_lines.index("CIRCLESEGMENTS") + 1) : (my_file_lines.index("ENDCIRCLESEGMENTS"))
                        ]
                    ]
                    for circle_seg in my_circle_segs:
                        if "PI" in circle_seg[2]:
                            tmp_string_list = circle_seg[2].split("P")
                            tmp_start_angle = float(tmp_string_list[0]) * PI
                        else:
                            tmp_start_angle = float(circle_seg[2])

                        if "PI" in circle_seg[3]:
                            tmp_string_list = circle_seg[3].split("P")
                            tmp_end_angle = float(tmp_string_list[0]) * PI
                        else:
                            tmp_end_angle = float(circle_seg[3])

                        tmp_circle_seg = CircleSeg(
                            p1=point_list[int(circle_seg[0]) - 1],
                            r=float(circle_seg[1]),
                            start_angle=tmp_start_angle,
                            end_angle=tmp_end_angle,
                        )
                        tmp_circle_seg.BoundaryName = int(circle_seg[5])
                        if int(circle_seg[4]):  # domain is "inside the circle"
                            tmp_circle_seg.Hole = True
                        else:  # domain is "outside the circle"
                            tmp_circle_seg.Hole = False
                        circleseg_list.append(tmp_circle_seg)

                self.Points = point_list

                # seperate cracks from other boundaries for point creation in Mesh
                self.BoundingGeometry = line_list + circle_list + circleseg_list
                self.CrackBoundaries = crack_list

                if not self.BoundingGeometry:
                    print("Class Boundary Error / Method read_input_file Error: No boundary elements added!")

            elif not my_file_lines[0].find("3D") == -1:  # 3D
                print("Class Boundary Error: 3D not implemented!")
            else:
                print('Class Boundary Error: file.bc header cannot be identified. "2D" or "3D" missing!')
            my_file.close()
        except IOError:
            print("Class Boundary Error: File '{}' cannot be opened!".format(self.InputFilePath))

    def check_if_domain_contains_point(self, point, key="", generator=None):
        """checks if this boundary contains a given point generated by the flood fill algorithm from point generator
        :param point:           the candidate point that is checked
        :param key:             a key that indicates from which direction the point was generated
        :param generator:       the point from which the candidate point parameter was generated
        :return:                a boolean that is true if this point is inside the domain
        """
        # checks if domain contains point and assigns boundary name if not in domain
        if self.Dimensions == "2D":
            closest_boundary_elements = self.find_closest_boundary_element_to_point(point)
            is_in_domain = True
            for boundary_element in closest_boundary_elements:
                is_in_domain_for_boundary_element = boundary_element.test_if_inside_boundary(
                    point, generator_key=key, in_generator=generator
                )
                if not is_in_domain_for_boundary_element:
                    is_in_domain = False
        else:
            raise NotImplementedError("3D not implemented in " + inspect.currentframe().f_code.co_name)
        return is_in_domain

    def find_closest_boundary_element_to_point(self, point):
        """
        determines the closest boundary elements in this boundary to a given point, which is used to determine whether this point is in the domain.
        Multiple boundary elements can be returned if they have the same distance.
        :param point: the point
        :return:
        """
        if self.Dimensions == "2D":
            shortest_distances_to_point = [
                boundary_element.compute_shortest_distance_to_point(point)[0]
                for boundary_element in self.BoundingGeometry
            ]  # could be more than one boundary_element if closest lines are at equal distance
            indices = [
                i
                for i in range(0, len(shortest_distances_to_point))
                if shortest_distances_to_point[i] == min(shortest_distances_to_point)
            ]
            closest_boundary_elements = []
            min_boundary_name = self.BoundingGeometry[indices[0]].BoundaryName
            # closest_boundary_elements.append(self.BoundingGeometry[indices[0]])
            for i in range(0, len(indices)):  # first boundary element has valid boundary name
                if self.BoundingGeometry[indices[0]].BoundaryName <= min_boundary_name:
                    closest_boundary_elements.insert(0, self.BoundingGeometry[indices[i]])
                else:
                    closest_boundary_elements.append(self.BoundingGeometry[indices[i]])
            return closest_boundary_elements
        else:
            raise NotImplementedError("3D not implemented in " + inspect.currentframe().f_code.co_name)

    # def plot_boundary(self, color=''):
    #     """
    #     displays the boundary with matplotlib.
    #     :param color:
    #     :return:
    #     """
    #     if self.Dimensions == "2D":
    #         # colors = ['y', 'r', 'g', 'c']
    #         colors = ['goldenrod', 'mediumblue', 'forestgreen', 'blueviolet', 'teal']
    #         ax = plt.gca()
    #         color_idx = 0
    #         for boundary_element in self.BoundingGeometry:
    #             if not color:
    #                 color_idx = boundary_element.BoundaryName % len(colors)
    #                 boun_color = colors[color_idx]
    #             else:
    #                 boun_color = color

    #             if isinstance(boundary_element, Line):
    #                 if isinstance(boundary_element, Crack):
    #                     boun_color = 'firebrick'
    #                 plt.plot(
    #                     [boundary_element.P1.x, boundary_element.P2.x],
    #                     [boundary_element.P1.y, boundary_element.P2.y],
    #                     boun_color,
    #                 )
    #             elif type(boundary_element) == Circle:
    #                 tmp_circle = plt.Circle(
    #                     (boundary_element.P1.x, boundary_element.P1.y),
    #                     boundary_element.R,
    #                     color=boun_color,
    #                     fill=False,
    #                 )
    #                 ax.add_artist(tmp_circle)
    #             elif type(boundary_element) == CircleSeg:
    #                 tmp_arc = Arc(
    #                     (boundary_element.P1.x, boundary_element.P1.y), boundary_element.R * 2.0,
    #                     boundary_element.R * 2.0,
    #                     theta1=boundary_element.StartAngleRad * 180 / PI,
    #                     theta2=boundary_element.EndAngleRad * 180 / PI,
    #                     edgecolor=boun_color,
    #                 )
    #                 ax.add_patch(tmp_arc)

    def compute_rectangular_distance_to_boundary(self, point=None, key=""):
        """
        Computes the distance to the next boundary along the lattice link defined by the direction key. The correct boundary element is the boundary
        element with minimal boundary name that is intersected by the lattice link between this point and its neighbor in the key direction.
        Also sets the fields
            point.BoundaryName[key]
            point.BoundaryClosestPoints[key]
            point.BoundaryNormals[key]
            point.DistanceToBoundary[key] = delta
        of this point.
        :param point: the boundary lattice point
        :param key: the lattice direction in which to compute distances
        :return: [ BoundaryName BoundaryClosestPoints BoundaryNormals DistanceToBoundary ]
        """
        # works for 2D line segments
        # also assigns boundary name for each lattice link of out of boundary points

        assert isinstance(point, Vector), "Cannot compute distance of {} to boundary".format(type(point))

        tmp_line = Line(point, point.Neighbors[key])
        intersected_boundary_elements = [
            boundary_element
            for boundary_element in self.BoundingGeometry
            if boundary_element.decide_if_intersects_lineseg(tmp_line)
        ]
        tmp_boundary_name = sys.maxsize  # choose smallest BoundaryName if more than one boundary_element is intersected
        tmp_closest_point = None

        for boundary_element in intersected_boundary_elements:
            if boundary_element.BoundaryName < tmp_boundary_name:
                tmp_boundary_name = boundary_element.BoundaryName
                if type(boundary_element) == Crack:
                    if point.IsLeftOrRightFaceOfCrack == "L":
                        tmp_boundary_name = boundary_element.BoundaryName2Crack
                tmp_closest_point = boundary_element.compute_shortest_distance_to_point(point)[1]
                tmp_normal = boundary_element.compute_normal(tmp_closest_point, key)[0]

        if tmp_closest_point is None:
            closest_boundaries = self.find_closest_boundary_element_to_point(point)
            if len(closest_boundaries) > 0:
                tmp_closest_point = point
                if type(closest_boundaries[0]) == Crack:  # cracks may have two boundary names
                    if point.IsLeftOrRightFaceOfCrack == "R":
                        tmp_boundary_name = closest_boundaries[0].BoundaryName
                    elif point.IsLeftOrRightFaceOfCrack == "L":
                        tmp_boundary_name = closest_boundaries[0].BoundaryName2Crack
                else:
                    tmp_boundary_name = closest_boundaries[0].BoundaryName
                tmp_normal = closest_boundaries[0].compute_normal(tmp_closest_point, key)[0]
        point.BoundaryName[key] = tmp_boundary_name
        point.BoundaryClosestPoints[key] = Vector_With_Displacement.from_vector(tmp_closest_point)
        point.BoundaryNormals[key] = tmp_normal
        #      point.ClosestPointOnBoundaryElementsThatIntersectLatticeLink[key] = tmp_closest_point
        if not intersected_boundary_elements:
            if len(closest_boundaries) > 1:
                raise ValueError("Closest boundary element intersects lattice link more than once!")
            tmp_intersect_point = tmp_closest_point
        else:
            if len(intersected_boundary_elements[0].find_intersection_point_with_line_seg(tmp_line)) > 1:
                raise ValueError("Closest boundary element intersects lattice link more than once!")
            tmp_intersect_point = intersected_boundary_elements[0].find_intersection_point_with_line_seg(tmp_line)[0]

        delta = (point - tmp_intersect_point).absolute_value()

        if key not in point.DistanceToBoundary.keys():
            point.DistanceToBoundary[key] = delta

    # decide if any of the boundary segments intersects with line segment from point to neighbours

    # compute the distance to boundary for each intersection point of each link

    # save these into a dictionary

    # call this method for every out of bound node of the mesh


def compute_cell_volumes_and_areas_at_boundary_points(boundary=None, cellsize=0, points=[], seedpoint=None):
    """compute the volume and boundary area of boundary points for
    mesoscopic (Guangwu wave eq.) implementation of boundary conditions
    shifted here from Mesh; meant to be importable
    """
    for point in points:
        if point.BoundaryName:
            # > original square <
            boundary_square = Square(center_point=point, side_length=cellsize, seed_point=seedpoint)  # original square
            corner_points_of_square_in_domain = boundary_square.find_corner_points_in_domain(boundary=boundary)
            boundary_square.create_all_auxlines_for_square(boundary=boundary, cellsize=cellsize)
            intersection_points_of_auxlines_with_boundary = (
                boundary_square.find_intersection_points_of_aux_lines_with_boundary(boundary=boundary)[0]
            )
            boundary_square.get_edges()
            intersection_points_of_edges_with_boundary = (
                boundary_square.find_intersection_points_of_edges_with_boundary(boundary=boundary)
            )

            # > intermediate polygon <
            tmp_points = []
            for point_i in corner_points_of_square_in_domain:
                tmp_points.append(point_i)
            for point_i in intersection_points_of_auxlines_with_boundary + intersection_points_of_edges_with_boundary:
                tmp_points.append(point_i[0])
            # for point_i in intersection_points_of_edges_with_boundary:
            # tmp_points.append(point_i[0])

            tmp_polygon = Polygon(points=tmp_points)

            tmp_polygon.remove_duplicate_points()
            center_of_polygon = tmp_polygon.get_center()
            tmp_polygon.find_boundary_names_for_points(boundary=boundary)
            tmp_polygon.sort_points_by_counter_clock_wise_angle(in_origin=center_of_polygon)
            # tmp_polygon.compute_convex_hull()
            # outer_polygon_points = tmp_polygon.compute_outer_polygon_points()

            intersection_points_of_boundary_elements_that_have_points_in_polygon = (
                tmp_polygon.find_intersection_points_of_boundary_elements_that_have_points_in_polygon(
                    boundary=boundary
                )[0]
            )

            # next intermediate polygon
            tmp_points = tmp_polygon.Points
            if intersection_points_of_boundary_elements_that_have_points_in_polygon:
                for point_i in intersection_points_of_boundary_elements_that_have_points_in_polygon[
                    0
                ]:  # add intersections of boundary elements
                    tmp_points.append(point_i)
            tmp_polygon = Polygon(points=tmp_points)

            tmp_polygon.remove_duplicate_points()
            center_of_polygon = tmp_polygon.get_center()
            tmp_polygon.sort_points_by_counter_clock_wise_angle(in_origin=center_of_polygon)
            tmp_polygon.compute_convex_hull()
            final_outer_polygon_points = tmp_polygon.compute_outer_polygon_points()

            # final polygon
            tmp_polygon = Polygon(points=final_outer_polygon_points)
            list_of_boundary_names_at_points = tmp_polygon.find_boundary_names_for_points(
                boundary=boundary, tol=1.0e-14
            )
            tmp_polygon.get_edges(list_of_boundary_names=list_of_boundary_names_at_points)

            if point.IsLeftOrRightFaceOfCrack:  # only for points adjacent to cracks
                tmp_polygon.remove_invalid_boundary_name_from_edges_that_are_on_cracks(
                    boundary=boundary,
                    crack_face_identifier=point.IsLeftOrRightFaceOfCrack,
                )

            tmp_polygon.compute_convex_hull()

            volume = tmp_polygon.get_area()
            surface_measures_of_boundary_ids_at_point = tmp_polygon.compute_surface_measure_and_id()

            """
            [
                volume: float,
                (
                    Boundary Names,
                    Boundary Measures,
                ),
                polygonal cell,
                interior surface length,
            ]
            """

            tmp_point_volume_surface_measure = (
                volume,
                (
                    surface_measures_of_boundary_ids_at_point[0],
                    surface_measures_of_boundary_ids_at_point[1],
                ),  # (Boundary Names, Boundary Measure)
                tmp_polygon,
                surface_measures_of_boundary_ids_at_point[2],  # Inner Surface Measure
            )
            point.VolumeSurfaceMeasure = tmp_point_volume_surface_measure

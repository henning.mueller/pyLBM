import SolidLBM.solver.solution.LatticePointD2_abstract as LatticePointD2_abstract_Module


class LatticePointD2Q5_abstract(LatticePointD2_abstract_Module.LatticePointD2_abstract):
    __slots__ = ()
    # static attributes
    neighbor_keys = ["x+", "y+", "x-", "y-"]  # neighbor keys for D2Q5
    inv_keys = ["x-", "y-", "x+", "y+"]  # inverse to neighbor keys
    stream_keys = [3, 4, 1, 2]
    no_f = 5

    def __init__(self, x=0.0, y=0.0, z=0.0, number=None):
        super(LatticePointD2Q5_abstract, self).__init__(x, y, z)
        self.Neighbors = dict()
        self.NeighborsDiagonal = dict()
        self.BoundaryName = dict()  # valid boundary name for each lattice link without a corresponding neighbor
        self.DistanceToBoundary = dict()
        self.BoundaryClosestPoints = dict()  # the closest points for each boundary lattice point
        self.BoundaryNormals = dict()  # normals at this point
        self.ID = number
        self.f = [None] * 5
        self.f_temp = [None] * 5
        self.f_eq = [None] * 5

        self.w = [None] * 1  # macroscopic field
        self.wdot = [None] * 1  # rate of macroscopic field

        # data for postprocessing
        self.PPData = dict()  # can be written for PP name: values (scalar or list)

        self.BCImplementationSwitch = -1  # for multiple BC implementations in same computation

        # non-mesh conforming BC macroscopic matrix (TODO which type?)
        self.BoundaryPointListID = -1
        self.BoundaryCoefficients = list()

        # non-mesh conforming BC local
        self.VolumeSurfaceMeasure = list()

        # gradient coefficients
        self.gradient_coefficients = list()

    def collide(self, computation):
        """
        :param computation: relaxation time
        :return: update distribution
        """
        for i in range(0, 5):
            self.f[i] = self.f[i] - 1.0 / computation.Parameters["tauh"] * (self.f[i] - self.f_eq[i])

    def stream(self):
        """
        :return: update distribution functions at neighbors after streaming
        """
        # push implementation
        self.f_temp[0] = self.f[0]
        n = 1
        if not self.BoundaryName:  # interior nodes
            for n_key in LatticePointD2Q5_abstract.neighbor_keys:
                self.Neighbors[n_key].f_temp[n] = self.f[n]
                n += 1
        else:  # boundary nodes only stream to existing neighbors
            for n_key in LatticePointD2Q5_abstract.neighbor_keys:
                if n_key in self.Neighbors.keys():
                    self.Neighbors[n_key].f_temp[n] = self.f[n]
                n += 1

    def update_distribution_function_after_streaming(self):
        """
        f_temp necessary to prevent overwriting f
        :return: updates f
        """
        for i in range(0, 5):
            self.f[i] = self.f_temp[i]
            self.f_temp[i] = None  # invalidate distributions functions from current time step

import numpy as np
import copy


class Matrix:
    @staticmethod
    def _list_to_Matrix(list0):
        matrix = Matrix(len(list0), len(list0[0]))
        matrix.S = list0
        return matrix

    def __init__(self, m, n):
        self.m = m
        self.n = n
        self.S = self._allocate_zero_coefficient_matrix(m, n)

    def _allocate_zero_coefficient_matrix(self, m, n):
        tmp_array = [0.0] * int(n)  # row
        tmp_matrix = list()
        for i in range(0, int(m)):
            tmp_matrix.append(copy.copy(tmp_array))
        return tmp_matrix

    def multiply_left(self, A):
        Snp = np.array(self.S)
        Anp = np.array(A)
        Res = Anp.dot(Snp)
        return Res.tolist()

    def multiply_right(self, A):
        Snp = np.array(self.S)
        Anp = np.array(A)
        Res = Snp.dot(A)
        return Res.tolist()

    def add(self, A):
        Snp = np.array(self.S)
        Anp = np.array(A)
        Res = Snp + Anp
        return Res.tolist()

    def inverse_of_A(self):
        if not (self.m == self.n):
            raise Exception("Cannot compute inverse")
        Snp = np.array(self.S)
        Res = np.linalg.inv(Snp)
        return Res.tolist()

    def write_A(self, row, col, entry):
        self.S[row][col] = self.S[row][col] + entry

    def get_A(self):
        return self.S

import SolidLBM.solver.solution.IntegrationRule_abstract as IntegrationRule_abstract_Module


class IntegrationRule_EulerBackward(IntegrationRule_abstract_Module.IntegrationRule_abstract):
    def __init__(self, parameters=dict()):
        self.Parameters = parameters

    def integrateW(self, w, wdot_np1):
        """
        computes current w from old w, wdot
        :param w:
        :param wdot:
        :param wddot:
        :return:
        """
        # Newmark parameters
        # beta = self.Parameters['beta_Newmark']
        # gamma = self.Parameters['gamma_Newmark']
        dt = self.Parameters["dth"]
        if len(w) == 2:
            out_w = list([0.0, 0.0])
        elif len(w) == 1:
            out_w = [0.0]
        # Newmark, explicit
        for i in range(0, len(w)):
            out_w[i] = w[i] + dt * wdot_np1[i]
        return out_w

    def integrateWDot(self, w, wdot, wddot, wddot_np1):
        """
        Not implemented
        :param w:
        :param wdot:
        :param wddot:
        :return:
        """
        raise NotImplementedError()

    def computeWddotSuchThatW(self, w, wdot, wddot, wddot_np1, target_w):
        """
        computes wddot such that targetW is obtained in next time step
        :param w:
        :param wdot:
        :param wddot:
        :param target_w:
        :return: wddotNew
        """
        raise NotImplementedError()

    def computeWdotSuchThatW(self, w, target_w):
        """
        computes wddot such that targetW is obtained in next time step
        :param w:
        :param wdot:
        :param wddot:
        :param target_w:
        :return: wddotNew
        """
        dt = self.Parameters["dth"]
        out_w_dot = [None] * len(w)
        for i in range(0, len(w)):
            out_w_dot[i] = (target_w[i] - w[i]) / dt
        return out_w_dot

import copy
import numpy as np

import SolidLBM.solver.solution.LatticePointD2Q5_abstract as LatticePointD2Q5_abstract_Module
import SolidLBM.solver.solution.LatticePointD2Q5_WaveEquation_Chopard as LatticePointD2Q5_WaveEquation_Chopard


class LatticePointD2Q5_NavierEquation_Chopard(
    LatticePointD2Q5_WaveEquation_Chopard.LatticePointD2Q5_WaveEquation_Chopard
):
    __slots__ = (
        "wddot",
        "wddot_temp",
        "rotation_dilatation",
    )

    fields_initialized = False
    rotation_dilatation_initialized = False
    initialization_finished = False
    computation_of_rotation_dilatation_at_all_lattice_points_finished = False

    @staticmethod
    def compute_dependent_parameters(cell_size, params):
        params["rhoh"] = 1.0  # ?
        params["cs"] = np.sqrt(params["mue"] / params["rho"])
        params["cd"] = np.sqrt((params["lambda"] + 2.0 * params["mue"]) / params["rho"])
        params["h"] = cell_size

        params["lambdah"] = params["lambda"] / params["mue"]
        params["mueh"] = params["mue"] / params["mue"]

        params["hh"] = params["h"] / params["L"]
        params["csh"] = params["cs"] / params["cs"]
        params["cdh"] = params["cd"] / params["cs"]
        params["tauh"] = 0.5

        # different scaling for different wave speeds
        params["a0"] = [
            None,
            0.9999,
        ]  # 0.9999 originally # first component is shear wave speed, sec component is dilat. wave speed
        params["a"] = [None, (1.0 - params["a0"][1]) / 4.0]
        params["a"][0] = params["a"][1] * (params["csh"] / params["cdh"]) ** 2  # correct
        params["a0"][0] = 1.0 - 4.0 * params["a"][0]

        # damped waves correct alpha
        if "alpha_psi" in params.keys():
            params["a"][0] = params["a"][0] * 0.5 * params["alpha_psi"]
        if "alpha_phi" in params.keys():
            params["a"][1] = params["a"][1] * 0.5 * params["alpha_phi"]

        params["ch"] = params["csh"] / (2.0 * params["a"][0]) ** 0.5  # one information wave speed
        params["dth"] = params["hh"] / params["ch"]
        params["b"] = 1.0

        return params

    # define as namedtuple
    class BoundaryDataBalanceOfMomentum:
        """
        summarizes all quantities that are necessary to evaluate boundary conditions from balance of momentum for associated lattice point
        """

        __slots__ = (
            "point",
            "_tau_n",
            "_tau_np1",
            "_phi_known",
            "_psi_known",
            "_cku",
            "_ckv",
            "_dx",
            "_dx_nonboun",
            "_dx_boun",
            "_dxx",
            "_dxx_nonboun",
            "_dxx_boun",
            "_t_ext",
            "rhs",
        )

        def __init__(self, point):
            self.point = point
            self._tau_n = []
            self._tau_np1 = []
            self._phi_known = None
            self._psi_known = None
            self._cku = None
            self._ckv = None
            self._dx = list([None, None, None, None, None])
            self._dx_nonboun = list([None, None, None, None, None])
            self._dx_boun = list([None, None, None, None, None])
            self._dxx = list([None, None, None, None])
            self._dxx_nonboun = list([None, None, None, None])
            self._dxx_boun = list([None, None, None, None])
            self._t_ext = dict()

            self.rhs = [0.0, 0.0]

        def set_dx(self, dx):
            self._dx = dx

        def get_dx(self):
            return self._dx

        def set_dx_nonboun(self, dx):
            self._dx_nonboun = dx

        def get_dx_nonboun(self):
            return self._dx_nonboun

        def set_dx_boun(self, dx):
            self._dx_boun = dx

        def get_dx_boun(self):
            return self._dx_boun

        def set_dxx(self, dxx):
            self._dxx = dxx

        def get_dxx(self):
            return self._dxx

        def set_dxx_nonboun(self, dxx):
            self._dxx_nonboun = dxx

        def get_dxx_nonboun(self):
            return self._dxx_nonboun

        def set_dxx_boun(self, dxx):
            self._dxx_boun = dxx

        def get_dxx_boun(self):
            return self._dxx_boun

        def set_t_ext(self, t_ext):
            self._t_ext = t_ext

        def get_t_ext(self):
            return self._t_ext

    def __init__(self, x=None, y=None, z=None, number=None):
        super(LatticePointD2Q5_NavierEquation_Chopard, self).__init__(x, y, z, number)
        self.w = [None] * 2  # displacement of the last n time step (if needed for time integration)
        self.wdot = [None] * 2  # velocity of the last time step
        self.wddot = [None] * 2  # velocity of the last time step
        self.wddot_temp = [None] * 2  # acceleration at current time step
        self.f = [[None] * 5, [None] * 5]
        self.f_temp = [[None] * 5, [None] * 5]
        self.f_eq = [[None] * 5, [None] * 5]
        self.rotation_dilatation = [None] * 2  # first component is rotation, second component is dilatation
        self.BoundaryDataBalanceOfMomentum = LatticePointD2Q5_NavierEquation_Chopard.BoundaryDataBalanceOfMomentum(self)

    def __str__(self):
        return "Navier Lattice Point " + str(self.ID)

    def _compute_dx(self, lattice):
        """
        computes gradient coefficients and stores them to BoundaryDataBalanceOfMomentum
        :return:
        """
        [dx_nonboun, dx_boun] = self._compute_gradient_coefficients_for_boundary_points_separately(lattice)
        dx = self.get_gradient_coefficients(h=lattice.CellSize)
        self.BoundaryDataBalanceOfMomentum.set_dx_nonboun(dx_nonboun)
        self.BoundaryDataBalanceOfMomentum.set_dx_boun(dx_boun)
        self.BoundaryDataBalanceOfMomentum.set_dx(dx)

        return dx

    def _compute_t_ext(self, computation):
        """
        computes total force on cell exerted from boundary tractions, and stores it to self.BoundaryDataBalanceOfMomentum
        :return:
        """
        boundary_names = self.VolumeSurfaceMeasure[1][0]
        boundary_surface_measure = self.VolumeSurfaceMeasure[1][1]

        total_traction_forceX = 0.0
        total_traction_forceY = 0.0

        i = 0
        for boundary_name in boundary_names:
            if computation.BoundaryConditions[boundary_name].BCType in [
                "Neumann2Order",
                "Neumann",
            ]:
                current_tractionX_at_boundary_name = (
                    computation.BoundaryConditions[boundary_name].CurrentValue[0] * boundary_surface_measure[i]
                )
                current_tractionY_at_boundary_name = (
                    computation.BoundaryConditions[boundary_name].CurrentValue[1] * boundary_surface_measure[i]
                )
                total_traction_forceX = total_traction_forceX + current_tractionX_at_boundary_name
                total_traction_forceY = total_traction_forceY + current_tractionY_at_boundary_name
            i = i + 1

        self.BoundaryDataBalanceOfMomentum.set_t_ext([total_traction_forceX, total_traction_forceY])
        return [total_traction_forceX, total_traction_forceY]

    def correct_psi_phi_f(self, computation):
        """
        corrects psi phi and distribution functions according to field w
        :param computation:
        :return:
        """

        # correct rotation and dilatation
        rot_dil_from_fields = self._compute_current_rotation_dilatation_from_fields(computation=computation)
        self.rotation_dilatation = rot_dil_from_fields

        self.f[0][0] = computation.Parameters["a0"][0] * self.rotation_dilatation[0]
        self.f[1][0] = computation.Parameters["a0"][1] * self.rotation_dilatation[1]
        #
        for j in range(0, 2):
            for i in range(1, 5):
                self.f[j][i] = computation.Parameters["a"][j] * self.rotation_dilatation[j]

    def set_all_flags_false():
        LatticePointD2Q5_NavierEquation_Chopard.fields_initialized = False
        LatticePointD2Q5_NavierEquation_Chopard.rotation_dilatation_initialized = False
        LatticePointD2Q5_NavierEquation_Chopard.initialization_finished = False
        LatticePointD2Q5_NavierEquation_Chopard.computation_of_rotation_dilatation_at_all_lattice_points_finished = (
            False
        )

    def initialise_distribution_functions_and_fields(self, computation):
        """
        initializes distribution functions from fields, fields from *.init file, PPData may be written here
        :param computation:
        :return: initial w, (wdot), f, f_temp
        """

        if (
            not LatticePointD2Q5_NavierEquation_Chopard.fields_initialized
        ):  # TODO not very nice, necessary because of gradient dependence of initial fields
            for lattice_point in computation.Lattice.Points:
                lattice_point._initialize_fields(computation=computation)  # sets fields to fields specified in BC
            LatticePointD2Q5_NavierEquation_Chopard.fields_initialized = True
        if not LatticePointD2Q5_NavierEquation_Chopard.rotation_dilatation_initialized:
            for lattice_point in computation.Lattice.Points:
                lattice_point._initialize_rotation_dilatation(
                    computation.Lattice
                )  # # initializes rotation and dilatation from fields
            LatticePointD2Q5_NavierEquation_Chopard.rotation_dilatation_initialized = True

        # initialize from fields only -> will be streamed / must be consistetn with wddot = 0?
        self.f[0][0] = (
            computation.Parameters["a0"][0] * self.rotation_dilatation[0] * 0.5
        )  # Faktor 0.5 da displacement ja nicht zur zeit n komplett aufgebracht ist
        self.f[1][0] = computation.Parameters["a0"][1] * self.rotation_dilatation[1] * 0.5

        for j in range(0, 2):
            for i in range(1, 5):
                self.f[j][i] = computation.Parameters["a"][j] * self.rotation_dilatation[j] * 0.5
                # self.f_temp[j][i] = computation.Parameters['a'][j] * self.rotation_dilatation[j]

        self.wddot = list([0.0, 0.0])  # start from equlibrium
        self.PPData["rotation_dilatation"] = self.rotation_dilatation

    def acceleration(self, computation):
        """
        writes acceleration from current value of self.rotation_dilatation
        :param computation:
        :return:
        """
        if not self.BoundaryName:
            self.wddot = self._compute_current_acceleration(computation)
            self.PPData["wddot"] = self.wddot

    def acceleration_at_boundary_point(self, computation):
        """
        computes acceleration at boundary points from balance of momentum with current sigma(current_u)
        :param computation:
        :return:
        """

        if self.BoundaryName:
            valid_boundary_name = min(self.BoundaryName.values())  # determines which boundary is to be fulfilled

            if computation.BoundaryConditions[valid_boundary_name].BCType == "Neumann2Order":
                gamma = 0.5
                gamma_k = 1.0 - gamma

                rho = computation.Parameters["rhoh"]
                sigma_k = self.compute_sigma(computation)  # computes sigma at current node
                # self.PPData['sigma'] = sigma_k

                inner_surface_measures = self.VolumeSurfaceMeasure[2]  # dict with length of inner surfaces
                volume = self.VolumeSurfaceMeasure[0]

                sqrt2 = 0.5 * 2**0.5

                normals_of_inner_surfaces = {
                    "x+": [1, 0],
                    "y+": [0, 1],
                    "x-": [-1, 0],
                    "y-": [0, -1],
                    "x+y+": [sqrt2, sqrt2],
                    "x+y-": [sqrt2, -sqrt2],
                    "x-y+": [-sqrt2, sqrt2],
                    "x-y-": [-sqrt2, -sqrt2],
                }

                t_ext = self._compute_t_ext(computation)

                # if (self.ID==287): #top left corner Shear
                #       t_ext_new = [0.0,0.0]
                #       t_ext_new[0] = t_ext[0]
                #       t_ext_new[1] = -t_ext[0] /2.0 # halbe Länge der Kante
                # #
                # if (self.ID == 418): #top right corner Shear
                #       t_ext_new = [0.0,0.0]
                #       t_ext_new[0] = t_ext[0]
                #       t_ext_new[1] = t_ext[0] /2.0 # halbe Länge der Kante

                t_int = [0.0, 0.0]

                self.find_diagonal_neighbors()
                neighbors = {**self.Neighbors, **self.NeighborsDiagonal}

                for dir in inner_surface_measures.keys():
                    normal = normals_of_inner_surfaces[dir]
                    nx = normal[0]
                    ny = normal[1]
                    l_in = inner_surface_measures[dir]

                    lattice_point_in = neighbors[dir]  # neigbor in current direction

                    sigma_lin = lattice_point_in.compute_sigma(computation)

                    sigma_avg_lin_k = [  # weighted average of sigma on edge
                        gamma_k * sigma_k[0] + gamma * sigma_lin[0],
                        gamma_k * sigma_k[1] + gamma * sigma_lin[1],
                        gamma_k * sigma_k[2] + gamma * sigma_lin[2],
                    ]

                    t_int[0] = t_int[0] + sigma_avg_lin_k[0] * nx * l_in + sigma_avg_lin_k[2] * ny * l_in
                    t_int[1] = t_int[1] + sigma_avg_lin_k[2] * nx * l_in + sigma_avg_lin_k[1] * ny * l_in

                t_total = [0.0, 0.0]
                t_total[0] = t_ext[0] + t_int[0]
                t_total[1] = t_ext[1] + t_int[1]

                wddot = [0.0, 0.0]
                wddot[0] = t_total[0] / rho / volume
                wddot[1] = t_total[1] / rho / volume

                self.wddot = wddot
                self.PPData["wddot"] = wddot
                return wddot
            elif computation.BoundaryConditions[valid_boundary_name].BCType == "Dirichlet":
                target_w = computation.BoundaryConditions[valid_boundary_name].CurrentValue

                self.wddot = computation.IntegrationRule.computeWddotSuchThatW(self.w, self.wdot, None, None, target_w)
                self.PPData["wddot"] = self.wddot

    def compute_w_explicit(self, computation):
        """
        :param computation:
        :return:
        """

        self.w = computation.IntegrationRule.integrateW(self.w, self.wdot, self.wddot, None)
        self.wdot = computation.IntegrationRule.integrateWDot(self.w, self.wdot, self.wddot, None)

    def update_rot_dil_from_distribution_functions(self, computation):
        """
        computes rotation dilation from distribution functions at non boundary points
        :param computation:
        :return:
        """
        if not self.BoundaryName:
            self._compute_current_rotation_dilatation(computation.Lattice)

    def _initialize_fields(self, computation):
        """
        initialises_fields at lattice points only, needs to be done since gradient requires initial fields at each neighbor

        :param computation:
        :return:
        """

        if (
            computation.Lattice.InitialFieldsW is not None
            and (len(computation.Lattice.InitialFieldsWdot[0]) == 2)
            and self.w[0] is None
        ):  # has not been written yet
            # compute fields from initial data
            w_dot = []
            w = []
            for index in range(0, len(computation.Lattice.InitialFieldsWdot[self.ID])):  # number of component
                w_dot.append(
                    computation.Lattice.InitialFieldsWdot[self.ID][index] / computation.Parameters["cs"]
                )  # rates scales by cs
                w.append(computation.Lattice.InitialFieldsW[self.ID][index] / computation.Parameters["wr"])
        else:
            w_dot = list([0.0, 0.0])
            w = list([0.0, 0.0])
        self.w = w
        self.wdot = w_dot

    def _initialize_rotation_dilatation(self, lattice):
        if self.rotation_dilatation[0] is None:  # only if field has not yet been written to
            gradient_of_w = self.get_gradient_of_w(lattice)
            self.rotation_dilatation = list(
                [
                    gradient_of_w[1][0] - gradient_of_w[0][1],
                    gradient_of_w[0][0] + gradient_of_w[1][1],
                ]
            )

    def compute_error_of_rotation_dilatation(self, computation):
        """
        For post-processing etc
        :param computation:
        :return:
        """
        gradient_of_w = self.get_gradient_of_w(computation.Lattice)
        rot_dil = list(
            [
                gradient_of_w[1][0] - gradient_of_w[0][1],
                gradient_of_w[0][0] + gradient_of_w[1][1],
            ]
        )
        error = list(
            [
                rot_dil[0] - self.rotation_dilatation[0],
                rot_dil[1] - self.rotation_dilatation[1],
            ]
        )

        # error = list([self.PPData['rotation_dilatation'][0] - self.PPData['rotation_dilatation_from_fields'][0],
        #             self.PPData['rotation_dilatation'][1] - self.PPData['rotation_dilatation_from_fields'][1]])
        # error = (error[0] ** 2 + error[1] ** 2) ** 0.5
        self.PPData["rot_dil_error"] = error

        return error

    def _compute_current_rotation_dilatation_from_fields(self, computation):
        lattice = computation.Lattice
        #
        self._compute_dx(lattice)
        dx, dy = self.BoundaryDataBalanceOfMomentum.get_dx()
        #
        #
        udx = 0.0
        vdy = 0.0
        udy = 0.0
        vdx = 0.0
        #
        for ptIndex in dx.keys():
            point = lattice.Points[ptIndex]
            udx = udx + point.w[0] * dx[ptIndex]
            vdx = vdx + point.w[1] * dx[ptIndex]

        for ptIndex in dy.keys():
            point = lattice.Points[ptIndex]
            udy = udy + point.w[0] * dy[ptIndex]
            vdy = vdy + point.w[1] * dy[ptIndex]
        #
        psi = vdx - udy
        phi = udx + vdy
        #
        rot_dil = [psi, phi]

        self.PPData["rotation_dilatation_from_fields"] = rot_dil
        if self.BoundaryName:
            self.PPData["rotation_dilatation"] = rot_dil
        return rot_dil

    def _compute_gradient_of_rotation_dilatation(self, lattice):
        """
        computes gradient of rotation and dilatation
        :param lattice:
        :return:
        """
        # get coefficients
        dx, dy = self.get_gradient_coefficients(h=lattice.CellSize)
        # build initial state
        tmp_array = list([0.0, 0.0])
        gradient_of_rotation_dilatation = []
        for i in range(0, len(self.w)):  # dimension of rotation_dilatation: i
            gradient_of_rotation_dilatation.append(copy.deepcopy(tmp_array))
        for i in range(0, len(self.w)):  # dimensions of rotation_dilatation: i
            for pointID in dx.keys():
                gradient_of_rotation_dilatation[i][0] = (
                    gradient_of_rotation_dilatation[i][0] + lattice.Points[pointID].rotation_dilatation[i] * dx[pointID]
                )
            for pointID in dy.keys():
                gradient_of_rotation_dilatation[i][1] = (
                    gradient_of_rotation_dilatation[i][1] + lattice.Points[pointID].rotation_dilatation[i] * dy[pointID]
                )
        # self.PPData["gradOfRotationDilatation_comp0"] = gradient_of_rotation_dilatation[0]
        # self.PPData["gradOfRotationDilatation_comp1"] = gradient_of_rotation_dilatation[1]
        return gradient_of_rotation_dilatation

    def _compute_current_acceleration(self, computation):
        """
        computes current acceleration from rotation_dilatation and wave speeds
        :param computation:
        :return:
        """

        gradient_of_rotation_dilatation = self._compute_gradient_of_rotation_dilatation(computation.Lattice)
        nablaXrotation = list(
            [
                gradient_of_rotation_dilatation[0][1],
                -gradient_of_rotation_dilatation[0][0],
            ]
        )  # first index = 0 is rotation, = 1 is dilatation
        gradDilatation = list(
            [
                gradient_of_rotation_dilatation[1][0],
                gradient_of_rotation_dilatation[1][1],
            ]
        )

        self.wddot_temp = list([0.0, 0.0])  # zero
        for i in range(0, len(self.wddot_temp)):
            self.wddot_temp[i] = (
                computation.Parameters["cdh"] ** 2 * gradDilatation[i]
                - computation.Parameters["csh"] ** 2 * nablaXrotation[i]
            )  # 'cdh?' TODO minus or plus sign?
        self.PPData["wddot"] = self.wddot_temp

        return self.wddot_temp

    def _compute_current_rotation_dilatation(self, lattice):
        """
        computes rotation_dilatation from distribution functions after streaming (and updating/overwriting the distribution functions)
        :return:
        """
        if not self.BoundaryName:
            rotation_dilatation = list([0.0, 0.0])

            rotation_dilatation[0] = sum(self.f_temp[0])  # rotation
            rotation_dilatation[1] = sum(self.f_temp[1])  # dilatation

            self.rotation_dilatation = rotation_dilatation
            self.PPData["rotation_dilatation"] = rotation_dilatation

    def compute_equilibrium_distribution(self, computation):
        """
        computes equilibrium distribution functions, Chopard style
        :param computation:
        :return:
        """
        cc = computation.Parameters["ch"]  # 2 components, TODO needs to be
        # computed depending on wavespeeds, if both are given compute two sets of scaling factors

        c = list([[0, 0], [cc, 0], [0, cc], [-cc, 0], [0, -cc]])

        for j in range(0, len(self.rotation_dilatation)):  # for tr(eps) and rot(u)_z
            J = list([0, 0])  # reset
            for i in range(0, 5):
                J[0] = J[0] + self.f[j][i] * c[i][0]
                J[1] = J[1] + self.f[j][i] * c[i][1]
            self.f_eq[j][0] = (
                computation.Parameters["a0"][j] * self.rotation_dilatation[j]
            )  # a0 should not depend on wavespeed
            for i in range(1, 5):
                self.f_eq[j][i] = computation.Parameters["a"][j] * self.rotation_dilatation[j] + computation.Parameters[
                    "b"
                ] * (c[i][0] * J[0] + c[i][1] * J[1]) / (2.0 * cc**2)  # b is not dependent on wave speed

    def stream(self):
        """
        :return: update distribution functions at neighbors after streaming
        """
        # push implementation
        for j in range(0, 2):
            self.f_temp[j][0] = self.f[j][0]
            if not self.BoundaryName:  # interior nodes
                n = 1
                for n_key in LatticePointD2Q5_abstract_Module.LatticePointD2Q5_abstract.neighbor_keys:
                    # neighbor = self.Neighbors[n_key]
                    # if not neighbor.BoundaryName:
                    #     neighbor.f_temp[j][n] = self.f[j][n]
                    # else: # DO NOT STREAM TO BOUNDARY POINTS
                    #     neighbor.f_temp[j][n] = neighbor.f[j][n]  # overwrite
                    self.Neighbors[n_key].f_temp[j][n] = self.f[j][n]  # CORRECT
                    n = n + 1
            else:  # boundary nodes only stream to existing neighbors
                n = 1
                for n_key in LatticePointD2Q5_abstract_Module.LatticePointD2Q5_abstract.neighbor_keys:
                    if n_key in self.Neighbors.keys():
                        # neighbor = self.Neighbors[n_key]
                        # if not neighbor.BoundaryName:
                        #     neighbor.f_temp[j][n] = self.f[j][n]
                        # else: # DO NOT STREAM TO BOUNDARY POINTS
                        #     neighbor.f_temp[j][n] = neighbor.f[j][n]
                        self.Neighbors[n_key].f_temp[j][n] = self.f[j][n]
                        n = n + 1
                    else:
                        n = n + 1

    def compute_sigma(self, computation):
        """
        :param self:
        :param target_value:
        :return:
        """
        la = computation.Parameters["lambdah"]
        mue = computation.Parameters["mueh"]

        gradient_of_w = self.get_gradient_of_w(computation.Lattice)

        sigma = list([0.0, 0.0, 0.0])
        sigma[0] = la * (gradient_of_w[0][0] + gradient_of_w[1][1]) + 2.0 * mue * gradient_of_w[0][0]
        sigma[1] = la * (gradient_of_w[0][0] + gradient_of_w[1][1]) + 2.0 * mue * gradient_of_w[1][1]
        sigma[2] = mue * (gradient_of_w[0][1] + gradient_of_w[1][0])
        self.PPData["sigma"] = sigma
        return sigma

    def update_distribution_function_after_streaming(self):
        """
        f_temp necessary to prevent overwriting f
        :return: updates f
        """
        for j in range(0, len(self.w)):
            for i in range(0, 5):
                self.f[j][i] = self.f_temp[j][i]
                self.f_temp[j][i] = None  # invalidate after time step

    def collide(self, computation):
        """
        :param computation: relaxation tim
        :return: update distribution
        """
        for j in range(0, 2):
            for i in range(0, 5):
                self.f[j][i] = self.f[j][i] - 1.0 / computation.Parameters["tauh"] * (self.f[j][i] - self.f_eq[j][i])

    def psi_phi_at_boundary_point_from_displacement_field(self, computation):
        """
        computes phi and psi at boundary point from definition
        phi = nabla * u, psi = nabla x u
        :return:
        """
        if self.BoundaryName:
            lattice = computation.Lattice

            self._compute_dx(lattice)
            dx, dy = self.BoundaryDataBalanceOfMomentum.get_dx()

            udx = 0.0
            vdy = 0.0
            udy = 0.0
            vdx = 0.0

            for ptIndex in dx.keys():
                point = lattice.Points[ptIndex]
                udx = udx + point.w[0] * dx[ptIndex]
                vdx = vdx + point.w[1] * dx[ptIndex]

            for ptIndex in dy.keys():
                point = lattice.Points[ptIndex]
                udy = udy + point.w[0] * dy[ptIndex]
                vdy = vdy + point.w[1] * dy[ptIndex]

            psi = vdx - udy
            phi = udx + vdy

            rot_dil = [psi, phi]
            rot_dil_avg = [0, 0]
            for i in range(0, 2):
                rot_dil_avg[i] = rot_dil[i] * 0.5 + self.rotation_dilatation[i] * 0.5
            # self.rotation_dilatation_avg = rot_dil_avg

            # self.rotation_dilatation = rot_dil
            # self.PPData['rotation_dilatation'] = rot_dil
            # self.PPData['rotation_dilatation_from_fields'] = rot_dil
            return [rot_dil, rot_dil_avg]

    def compute_missing_distribution_functions_according_to_target_value_rotation_dilatation(
        self, computation, target_psi_phi
    ):
        """
        computes missing (all not only missing) distribution functions according to a target value, uses definition of equilibrium distribution to do so
        :param computation:
        :param target_psi_phi:
        :return:
        """
        self.f_temp[0][0] = computation.Parameters["a0"][0] * target_psi_phi[0]
        self.f_temp[1][0] = computation.Parameters["a0"][1] * target_psi_phi[1]
        #
        # self.f_temp[0][0] = self.f[0][0]
        # self.f_temp[1][0] = self.f[1][0]
        #
        for j in range(0, 2):
            for i in range(1, 5):
                self.f_temp[j][i] = computation.Parameters["a"][j] * target_psi_phi[j]

import SolidLBM.solver.solution.LatticePointD2Q5_abstract as LatticePointD2Q5_abstract_Module
import SolidLBM.solver.solution.LatticePointD2Q5_WaveEquation_Guangwu as LatticePointD2Q5_WaveEquation_Guangwu_Module


class LatticePointD2Q5_HeatEquation(LatticePointD2Q5_WaveEquation_Guangwu_Module.LatticePointD2Q5_WaveEquation_Guangwu):
    @staticmethod
    def compute_dependent_parameters(cell_size, params):
        params["h"] = cell_size
        params["hh"] = params["h"] / params["L"]  # TODO Nondimensialization
        params["alphah"] = params["alpha"]
        params["tauh"] = params["tau"]
        params["dth"] = params["hh"] ** 2 / 3.0 / params["alphah"] * (params["tauh"] - 0.5)

        return params

    def __init__(self, x=0.0, y=0.0, z=0.0, number=None):
        #  super(LatticePointD2Q5, self).__init__(x, y, z)
        super(LatticePointD2Q5_HeatEquation, self).__init__(x, y, z, number)

    def initialise_distribution_functions_and_fields(self, computation):
        # TODO only displacement can be initialized, what about velocity?
        if computation.Lattice.InitialFieldsW:
            w = list()
            for index in range(0, len(computation.Lattice.InitialFieldsWdot[self.ID])):
                w.append(computation.Lattice.InitialFieldsW[self.ID][index] / computation.Parameters["thetar"])  # ...
        else:
            w = list([0.0])
        self.w = w
        self.f[0] = 2.0 / 6.0 * self.w[0]
        self.f_temp[0] = self.f[0]
        for i in range(1, 5):
            self.f[i] = 1.0 / 6.0 * self.w[0]
            self.f_temp[i] = 1.0 / 6.0 * self.w[0]

    def compute_missing_distribution_functions_according_to_target_value(self, computation, in_target_value):
        target_value = in_target_value[0]
        neighbor_keys = LatticePointD2Q5_abstract_Module.LatticePointD2Q5_abstract.neighbor_keys
        stream_keys = [3, 4, 1, 2]
        missing_f = list()
        for key in neighbor_keys:  # does not need to be done in every time step TODO
            if key not in self.Neighbors.keys():  # distribution function is missing
                missing_f.append(
                    stream_keys[neighbor_keys.index(key)]
                )  # save index of distribution function that needs to be determined by boundary conditions
        f_sum = 0.0
        if len(missing_f) == 1:
            for i in range(0, 5):
                if i != missing_f[0]:
                    f_sum = f_sum + self.f_temp[i]
        elif len(missing_f) == 2:
            for i in range(0, 5):
                if i != missing_f[0] and i != missing_f[1]:
                    f_sum = f_sum + self.f_temp[i]
        elif len(missing_f) == 3:
            for i in range(0, 5):
                if i not in (missing_f[0], missing_f[1], missing_f[2]):
                    f_sum = f_sum + self.f_temp[i]
        else:
            print(
                "Error: Apply Boundary Condition not implemented for lattice points with more than three out of boundary neighbours!"
            )
        for f_miss in missing_f:
            self.f_temp[f_miss] = 1.0 / len(missing_f) * (target_value - f_sum)

            # self.f_temp[f_miss] = 1.0 / len(missing_f) * (tmp_value - f_sum)

    def compute_equilibrium_distribution(self, computation):
        self.f_eq[0] = 2.0 / 6.0 * self.w[0]
        for i in range(1, 5):
            self.f_eq[i] = 1.0 / 6.0 * self.w[0]

    def boun(self, computation):
        if bool(self.BoundaryName):
            valid_boundary_name = min(self.BoundaryName.values())  # determines which boundary is to be fulfilled
            # valid_boundary_name =
            neighbor_keys = LatticePointD2Q5_abstract_Module.LatticePointD2Q5_abstract.neighbor_keys
            inv_keys = LatticePointD2Q5_abstract_Module.LatticePointD2Q5_abstract.inv_keys
            # neighbor_keys = ['x+', 'y+', 'x-', 'y-']
            # inv_keys = ['x-', 'y-', 'x+', 'y+']
            stream_keys = [3, 4, 1, 2]
            missing_f = list()
            for key in neighbor_keys:
                if key not in self.Neighbors.keys():  # distribution function is missing
                    missing_f.append(
                        stream_keys[neighbor_keys.index(key)]
                    )  # save index of distribution function that needs to be determined by boundary conditions
            if computation.BoundaryConditions[valid_boundary_name].BCType == "Dirichlet":
                tmp_value = computation.BoundaryConditions[valid_boundary_name].CurrentValue[0]
            elif computation.BoundaryConditions[valid_boundary_name].BCType == "Neumann":
                key_in_dir_of_valid_boundary = [
                    key for key in self.BoundaryName.keys() if self.BoundaryName[key] == valid_boundary_name
                ]
                key = inv_keys[
                    neighbor_keys.index(key_in_dir_of_valid_boundary[0])
                ]  # key of neighbor in the interior of the domain that is used to compute gradient according to bc
                if key in self.Neighbors.keys():
                    w_temp_neighbor_tpdt = 0.0
                    for f in self.Neighbors[key].f_temp:
                        if f is not None:
                            w_temp_neighbor_tpdt = w_temp_neighbor_tpdt + f
                    # w_temp_neighbor_tpdt = self.get_future_w(computation, self)[0]
                    # w_temp_neighbor_tpdt = sum(self.Neighbors[key].f_temp) # TODO wtemp
                    tmp_value = (
                        computation.BoundaryConditions[valid_boundary_name].CurrentValue[0]
                        * computation.Parameters["hh"]
                        + w_temp_neighbor_tpdt
                    )  #
                else:
                    print(
                        "Class LatticePointD2Q5Chopard Method: boun Error: Cannot Apply BC! Interior neighbour missing for Point at (x,y)!"
                    )
            elif computation.BoundaryConditions[valid_boundary_name].BCType == "Neumann2Order":
                key_in_dir_of_valid_boundary = [
                    key for key in self.BoundaryName.keys() if self.BoundaryName[key] == valid_boundary_name
                ]
                key = inv_keys[
                    neighbor_keys.index(key_in_dir_of_valid_boundary[0])
                ]  # key of neighbor in the interior of the domain that is used to compute gradient according to bc
                if key in self.Neighbors.keys() and key in self.Neighbors[key].Neighbors.keys():
                    w_temp_neighbor_tpdt = 0.0  # TODO problematisch wenn Nachbarn selbst Randpunkte sind
                    for f in self.Neighbors[key].f_temp:
                        if f is not None:
                            w_temp_neighbor_tpdt = w_temp_neighbor_tpdt + f

                    w_temp_neighbor_neighbor_tpdt = 0.0
                    for f in self.Neighbors[key].Neighbors[key].f_temp:
                        if f is not None:
                            w_temp_neighbor_neighbor_tpdt = w_temp_neighbor_neighbor_tpdt + f

                    # w_temp_neighbor_tpdt = sum(self.Neighbors[key].f_temp)
                    # w_temp_neighbor_neighbor_tpdt = sum(self.Neighbors[key].Neighbors[key].f_temp)
                    tmp_value = (
                        4.0 / 3.0 * w_temp_neighbor_tpdt
                        - 1.0 / 3.0 * w_temp_neighbor_neighbor_tpdt
                        + 2.0
                        / 3.0
                        * computation.BoundaryConditions[valid_boundary_name].CurrentValue[0]
                        * computation.Parameters["hh"]
                    )
                else:
                    print(
                        "Class LatticePointD2Q5Chopard Method: boun Error: Cannot Apply BC! Interior neighbour(s) missing for Point at ({0:16.8e},{1:16.8e})!".format(
                            self.x, self.y
                        )
                    )
            else:
                print("Class LatticePointD2Q5Chopard Method: boun Error: BCType not implemented!")
            f_sum = 0.0
            if len(missing_f) == 1:
                for i in range(0, 5):
                    if i != missing_f[0]:
                        f_sum = f_sum + self.f_temp[i]
            elif len(missing_f) == 2:
                for i in range(0, 5):
                    if i != missing_f[0] and i != missing_f[1]:
                        f_sum = f_sum + self.f_temp[i]
            elif len(missing_f) == 3:
                for i in range(0, 5):
                    if i not in (missing_f[0], missing_f[1], missing_f[2]):
                        f_sum = f_sum + self.f_temp[i]
            else:
                print(
                    "Error: Apply Boundary Condition not implemented for lattice points with more than three out of boundary neighbours!"
                )
            for f_miss in missing_f:
                self.f_temp[f_miss] = 1.0 / len(missing_f) * (tmp_value - f_sum)

    def compute_w(self, computation):
        w_dot = list([(sum(self.f_temp) - self.w[0]) / computation.Parameters["dth"]])
        self.wdot = w_dot
        w = list()
        w.append(sum(self.f_temp))
        self.w = w

    def get_future_w(self, computation=None, point=None):
        return list([sum(point.f_temp)])

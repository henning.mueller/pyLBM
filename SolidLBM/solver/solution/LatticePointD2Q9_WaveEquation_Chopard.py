import numpy as np

import SolidLBM.solver.solution.LatticePointD2Q9_abstract as LatticePointD2Q9_abstract_Module


class LatticePointD2Q9_WaveEquation_Chopard(LatticePointD2Q9_abstract_Module.LatticePointD2Q9_abstract):
    __slots__ = ()

    @staticmethod
    def compute_dependent_parameters(cell_size, params):
        # cs (mue/rho) is the only free parameter
        params["cs"] = np.sqrt(params["mue"] / params["rho"])
        params["h"] = cell_size

        params["hh"] = params["h"] / params["L"]
        params["csh"] = params["cs"] / params["cs"]
        params["tauh"] = 0.5
        params["a0"] = 0.5
        params["a"] = (1.0 - params["a0"]) / 8.0
        params["ch"] = params["csh"] / (6.0 * params["a"]) ** 0.5
        params["dth"] = params["hh"] / params["ch"]
        params["b"] = 1.0 / 3.0

        return params

    def __init__(self, x=0.0, y=0.0, z=0.0, number=None):
        super(LatticePointD2Q9_WaveEquation_Chopard, self).__init__(x, y, z, number)

    def initialise_distribution_functions_and_fields(self, computation):
        # TODO only displacement can be initialized, what about velocity?
        if computation.Lattice.InitialFieldsW:
            w_dot = list()
            w = list()
            for index in range(0, len(computation.Lattice.InitialFieldsWdot[self.ID])):
                w_dot.append(
                    computation.Lattice.InitialFieldsWdot[self.ID][index]
                    / (computation.Parameters["cs"] * computation.Parameters["w_r"])
                    * computation.Parameters["L"]
                )
                w.append(computation.Lattice.InitialFieldsW[self.ID][index] / computation.Parameters["wr"])  # ...
        else:
            w_dot = list([0.0])
            w = list([0.0])
        self.w = w
        self.wdot = w_dot
        self.f[0] = computation.Parameters["a0"] * self.w[0]
        self.f_temp[0] = self.f[0]
        for i in range(1, 9):
            self.f[i] = computation.Parameters["a"] * self.w[0]
            self.f_temp[i] = computation.Parameters["a"] * self.w[0]

    def compute_missing_distribution_functions_according_to_target_value(self, computation, in_target_value):
        target_value = in_target_value[0]
        neighbor_keys = LatticePointD2Q9_abstract_Module.LatticePointD2Q9_abstract.neighbor_keys
        stream_keys = LatticePointD2Q9_abstract_Module.LatticePointD2Q9_abstract.stream_keys
        missing_f = list()
        missing_f_set = set()
        for key in neighbor_keys:  # does not need to be done in every time step TODO
            if key not in self.Neighbors.keys():  # distribution function is missing
                missing_f.append(
                    stream_keys[neighbor_keys.index(key) + 1]
                )  # save index of distribution function that needs to be determined by boundary conditions
                missing_f_set.add(stream_keys[neighbor_keys.index(key) + 1])
        f_sum = 0.0
        if len(missing_f) <= 5:  # TODO same implementation everywhere
            for i in range(0, 9):
                if i not in missing_f_set:
                    f_sum = f_sum + self.f_temp[i]

        else:
            print(
                "Error: Apply Boundary Condition not implemented for lattice points with more than three out of boundary neighbours!"
            )
        for f_miss in missing_f:
            self.f_temp[f_miss] = 1.0 / len(missing_f) * (target_value - f_sum)

    def compute_equilibrium_distribution(self, computation):
        cc = computation.Parameters["ch"]
        c = list(
            [
                [0, 0],
                [cc, 0],
                [0, cc],
                [-cc, 0],
                [0, -cc],
                [cc, cc],
                [-cc, cc],
                [-cc, -cc],
                [cc, -cc],
            ]
        )
        J = list([0, 0])
        for i in range(0, 9):
            J[0] = J[0] + self.f[i] * c[i][0]
            J[1] = J[1] + self.f[i] * c[i][1]
        self.f_eq[0] = computation.Parameters["a0"] * self.w[0]
        for i in range(1, 9):
            self.f_eq[i] = computation.Parameters["a"] * self.w[0] + computation.Parameters["b"] * (
                c[i][0] * J[0] + c[i][1] * J[1]
            ) / (2.0 * cc**2)

    def boun(self, computation):
        if bool(self.BoundaryName):
            valid_boundary_name = min(self.BoundaryName.values())  # determines which boundary is to be fulfilled
            # valid_boundary_name =
            neighbor_keys = LatticePointD2Q9_abstract_Module.LatticePointD2Q9_abstract.neighbor_keys
            inv_keys = LatticePointD2Q9_abstract_Module.LatticePointD2Q9_abstract.inv_keys
            # neighbor_keys = ['x+', 'y+', 'x-', 'y-']
            # inv_keys = ['x-', 'y-', 'x+', 'y+']
            stream_keys = LatticePointD2Q9_abstract_Module.LatticePointD2Q9_abstract.stream_keys
            missing_f = list()
            for key in neighbor_keys:
                if key not in self.Neighbors.keys():  # distribution function is missing
                    missing_f.append(
                        stream_keys[neighbor_keys.index(key)]
                    )  # save index of distribution function that needs to be determined by boundary conditions
            if computation.BoundaryConditions[valid_boundary_name].BCType == "Dirichlet":
                tmp_value = computation.BoundaryConditions[valid_boundary_name].CurrentValue[0]
            elif computation.BoundaryConditions[valid_boundary_name].BCType == "Neumann":
                key_in_dir_of_valid_boundary = [
                    key for key in self.BoundaryName.keys() if self.BoundaryName[key] == valid_boundary_name
                ]
                key = inv_keys[
                    neighbor_keys.index(key_in_dir_of_valid_boundary[0])
                ]  # key of neighbor in the interior of the domain that is used to compute gradient according to bc
                if key in self.Neighbors.keys():
                    w_temp_neighbor_tpdt = 0.0
                    for f in self.Neighbors[key].f_temp:
                        if f is not None:
                            w_temp_neighbor_tpdt = w_temp_neighbor_tpdt + f
                    # w_temp_neighbor_tpdt = self.get_future_w(computation, self)[0]
                    # w_temp_neighbor_tpdt = sum(self.Neighbors[key].f_temp) # TODO wtemp
                    tmp_value = (
                        computation.BoundaryConditions[valid_boundary_name].CurrentValue[0]
                        * computation.Parameters["hh"]
                        + w_temp_neighbor_tpdt
                    )  #
                else:
                    print(
                        "Class LatticePointD2Q9Chopard Method: boun Error: Cannot Apply BC! Interior neighbour missing for Point at (x,y)!"
                    )
            elif computation.BoundaryConditions[valid_boundary_name].BCType == "Neumann2Order":
                key_in_dir_of_valid_boundary = [
                    key for key in self.BoundaryName.keys() if self.BoundaryName[key] == valid_boundary_name
                ]
                key = inv_keys[
                    neighbor_keys.index(key_in_dir_of_valid_boundary[0])
                ]  # key of neighbor in the interior of the domain that is used to compute gradient according to bc
                if key in self.Neighbors.keys() and key in self.Neighbors[key].Neighbors.keys():
                    w_temp_neighbor_tpdt = 0.0  # TODO problematisch wenn Nachbarn selbst Randpunkte sind
                    for f in self.Neighbors[key].f_temp:
                        if f is not None:
                            w_temp_neighbor_tpdt = w_temp_neighbor_tpdt + f

                    w_temp_neighbor_neighbor_tpdt = 0.0
                    for f in self.Neighbors[key].Neighbors[key].f_temp:
                        if f is not None:
                            w_temp_neighbor_neighbor_tpdt = w_temp_neighbor_neighbor_tpdt + f

                    # w_temp_neighbor_tpdt = sum(self.Neighbors[key].f_temp)
                    # w_temp_neighbor_neighbor_tpdt = sum(self.Neighbors[key].Neighbors[key].f_temp)
                    tmp_value = (
                        4.0 / 3.0 * w_temp_neighbor_tpdt
                        - 1.0 / 3.0 * w_temp_neighbor_neighbor_tpdt
                        + 2.0
                        / 3.0
                        * computation.BoundaryConditions[valid_boundary_name].CurrentValue[0]
                        * computation.Parameters["hh"]
                    )
                else:
                    print(
                        "Class LatticePointD2Q9Chopard Method: boun Error: Cannot Apply BC! Interior neighbour(s) missing for Point at ({0:16.8e},{1:16.8e})!".format(
                            self.x, self.y
                        )
                    )
            else:
                print("Class LatticePointD2Q9Chopard Method: boun Error: BCType not implemented!")

            self.compute_missing_distribution_functions_according_to_target_value(
                computation=computation, in_target_value=list([tmp_value])
            )

    def compute_w(self, computation):
        w = list()
        w.append(sum(self.f))
        w_dot = list([(w[0] - self.w[0]) / computation.Parameters["dth"]])
        self.w = w
        self.wdot = w_dot

    def get_future_w(self, computation=None, point=None):
        return list([sum(point.f_temp)])

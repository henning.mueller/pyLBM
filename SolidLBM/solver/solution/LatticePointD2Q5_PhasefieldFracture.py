import SolidLBM.solver.solution.LatticePointD2Q5_abstract as LatticePointD2Q5_abstract_Module
import SolidLBM.solver.solution.LatticePointD2Q5_WaveEquation_Guangwu as LatticePointD2Q5_WaveEquation_Guangwu_Module

import SolidLBM.util.geometry.Line as LineModule
import SolidLBM.util.geometry.Point as PointModule

import math
import copy
import numpy as np


class LatticePointD2Q5_PhasefieldFracture(
    LatticePointD2Q5_WaveEquation_Guangwu_Module.LatticePointD2Q5_WaveEquation_Guangwu
):
    weights = [1.0 / 3.0, 1.0 / 6.0, 1.0 / 6.0, 1.0 / 6.0, 1.0 / 6.0]

    @classmethod
    def compute_dependent_parameters(cls, cellSize, computationParameters):
        outComputationParameters = copy.deepcopy(computationParameters)
        # outComputationParameters['cs'] = np.sqrt(outComputationParameters['mue'] / outComputationParameters['rho'])
        outComputationParameters["h"] = cellSize

        outComputationParameters["hh"] = outComputationParameters["h"] / outComputationParameters["L"]

        D = outComputationParameters["M"] * outComputationParameters["Gc"] * outComputationParameters["eps"] * 2.0
        tmp = -1.0 / 6.0 * outComputationParameters["hh"] ** 2 / D
        outComputationParameters["dth"] = tmp + math.sqrt(
            tmp**2 + 1.0 / 3.0 * outComputationParameters["hh"] ** 2 / D * outComputationParameters["tauh"]
        )
        outComputationParameters["ch"] = outComputationParameters["hh"] / outComputationParameters["dth"]

        return outComputationParameters

    def __init__(self, x=0.0, y=0.0, z=0.0, number=None):
        #  super(LatticePointD2Q5, self).__init__(x, y, z)
        super(LatticePointD2Q5_PhasefieldFracture, self).__init__(x, y, z, number)

    def initialise_distribution_functions_and_fields(self, computation):
        # TODO provisory initial condition
        def decide_if_in_range(xmin: float, xmax: float, ymin: float, ymax: float, tol: float):
            return self.x >= xmin - tol and self.x <= xmax + tol and self.y >= ymin - tol and self.y <= ymax + tol

        if computation.Lattice.InitialFieldsW:
            w = list()
            for index in range(0, len(computation.Lattice.InitialFieldsWdot[self.ID])):
                w.append(computation.Lattice.InitialFieldsW[self.ID][index] / computation.Parameters["thetar"])  # ...
        else:
            w = list([1.0])

        # TODO provisory initial condition
        # if decide_if_in_range(-0.2, 0.2, 0.0, 0.0, 0.001):
        #    w[0] = 0.0

        # TODO provisory initial condition
        intialCrack = LineModule.Line(PointModule.Point(x=-0.2, y=-0.1), PointModule.Point(x=0.2, y=0.1))
        if intialCrack.compute_shortest_distance_to_point(self)[0] <= 0.02:
            w[0] = 0.0

        self.w = w
        self.f[0] = 2.0 / 6.0 * self.w[0]
        self.f_temp[0] = self.f[0]
        for i in range(1, 5):
            self.f[i] = 1.0 / 6.0 * self.w[0]
            self.f_temp[i] = 1.0 / 6.0 * self.w[0]

    # def compute_missing_distribution_functions_according_to_target_value(self, computation, target_zeroth_moment):    # COMMENTED 23.02.2023
    #     target_value = target_zeroth_moment[0]
    #     neighbor_keys = LatticePointD2Q5_abstract_Module.LatticePointD2Q5_abstract.neighbor_keys
    #     stream_keys = [3, 4, 1, 2]
    #     missing_f = list()
    #     for key in neighbor_keys:  # does not need to be done in every time step TODO
    #         if key not in self.Neighbors.keys():  # distribution function is missing
    #             missing_f.append(stream_keys[neighbor_keys.index(
    #                 key)])  # save index of distribution function that needs to be determined by boundary conditions
    #     f_sum = 0.0
    #     if len(missing_f) == 1:
    #         for i in range(0, 5):
    #             if i != missing_f[0]:
    #                 f_sum = f_sum + self.f_temp[i]
    #     elif len(missing_f) == 2:
    #         for i in range(0, 5):
    #             if i != missing_f[0] and i != missing_f[1]:
    #                 f_sum = f_sum + self.f_temp[i]
    #     elif len(missing_f) == 3:
    #         for i in range(0, 5):
    #             if i not in (missing_f[0], missing_f[1], missing_f[2]):
    #                 f_sum = f_sum + self.f_temp[i]
    #     else:
    #         raise Error('Error: Apply Boundary Condition not implemented for lattice points with more than three out of boundary neighbours!')
    #     for f_miss in missing_f:
    #         self.f_temp[f_miss] = 1.0 / len(missing_f) * (target_value - f_sum)
    #
    #         #self.f_temp[f_miss] = 1.0 / len(missing_f) * (tmp_value - f_sum)

    def compute_equilibrium_distribution(self, computation):
        c = computation.Parameters["ch"]
        cc = [[0.0, 0.0], [c, 0.0], [0.0, c], [-c, 0.0], [0.0, -c]]
        cs_squared = 1.0 / 3.0 * c**2
        gradw = [
            self.get_gradient_of_w(computation.Lattice)[0],
            self.get_gradient_of_w(computation.Lattice)[1],
        ]

        factor = (
            -2.0
            * computation.Parameters["M"]
            * computation.Parameters["eps"]
            * computation.Parameters["Gc"]
            / cs_squared
        )
        factor = 0.0
        self.f_eq[0] = LatticePointD2Q5_PhasefieldFracture.weights[0] * self.w[0]
        for i in range(1, 5):
            ccTimesGradS = cc[i][0] * gradw[0] + cc[i][1] * gradw[1]
            self.f_eq[i] = LatticePointD2Q5_PhasefieldFracture.weights[i] * (self.w[0] + factor * ccTimesGradS)

    def boun(self, computation):
        if bool(self.BoundaryName):
            valid_boundary_name = min(self.BoundaryName.values())  # determines which boundary is to be fulfilled
            neighbor_keys = LatticePointD2Q5_abstract_Module.LatticePointD2Q5_abstract.neighbor_keys
            inv_keys = LatticePointD2Q5_abstract_Module.LatticePointD2Q5_abstract.inv_keys
            stream_keys = [3, 4, 1, 2]
            missing_f = list()
            for key in neighbor_keys:
                if key not in self.Neighbors.keys():  # distribution function is missing
                    missing_f.append(
                        stream_keys[neighbor_keys.index(key)]
                    )  # save index of distribution function that needs to be determined by boundary conditions
            if computation.BoundaryConditions[valid_boundary_name].BCType == "Dirichlet":
                desired_w_at_boundary_lattice_point = computation.BoundaryConditions[valid_boundary_name].CurrentValue[
                    0
                ]
            elif computation.BoundaryConditions[valid_boundary_name].BCType == "Neumann":
                key_in_dir_of_valid_boundary = [
                    key for key in self.BoundaryName.keys() if self.BoundaryName[key] == valid_boundary_name
                ]
                key_of_neighbor_for_interpolation = inv_keys[
                    neighbor_keys.index(key_in_dir_of_valid_boundary[0])
                ]  # key of neighbor in the interior of the domain that is used to compute gradient according to bc
                if key_of_neighbor_for_interpolation in self.Neighbors.keys():
                    w_temp_neighbor_tpdt = 0.0
                    for i in range(0, len(self.Neighbors[key_of_neighbor_for_interpolation].f_temp)):
                        if self.Neighbors[key_of_neighbor_for_interpolation].f_temp[i] is not None:
                            w_temp_neighbor_tpdt = (
                                w_temp_neighbor_tpdt
                                + self.Neighbors[key_of_neighbor_for_interpolation].f_temp[i]
                                + computation.Parameters["dth"]
                                / 2.0
                                * self.Neighbors[key_of_neighbor_for_interpolation].source_Qi(computation, i)
                            )
                    desired_w_at_boundary_lattice_point = (
                        computation.BoundaryConditions[valid_boundary_name].CurrentValue[0]
                        * computation.Parameters["hh"]
                        + w_temp_neighbor_tpdt
                    )  #
                else:
                    raise NotImplementedError(
                        "Class "
                        + self.__class__.__name__
                        + "Method: boun Error: Cannot Apply BC! Interior neighbour missing for Point at (x,y)!"
                    )
            elif computation.BoundaryConditions[valid_boundary_name].BCType == "Neumann2Order":
                key_in_dir_of_valid_boundary = [
                    key for key in self.BoundaryName.keys() if self.BoundaryName[key] == valid_boundary_name
                ]
                key_of_neighbor_for_interpolation = inv_keys[
                    neighbor_keys.index(key_in_dir_of_valid_boundary[0])
                ]  # key of neighbor in the interior of the domain that is used to compute gradient according to bc
                if (
                    key_of_neighbor_for_interpolation in self.Neighbors.keys()
                    and key_of_neighbor_for_interpolation
                    in self.Neighbors[key_of_neighbor_for_interpolation].Neighbors.keys()
                ):
                    w_temp_neighbor_tpdt = 0.0  # TODO problematisch wenn Nachbarn selbst Randpunkte sind
                    for i in range(0, len(self.Neighbors[key_of_neighbor_for_interpolation].f_temp)):
                        if self.Neighbors[key_of_neighbor_for_interpolation].f_temp[i] is not None:
                            w_temp_neighbor_tpdt = (
                                w_temp_neighbor_tpdt
                                + self.Neighbors[key_of_neighbor_for_interpolation].f_temp[i]
                                + self.source_Qi(computation, i) * computation.Parameters["dth"] / 2.0
                            )

                    w_temp_neighbor_neighbor_tpdt = 0.0
                    for i in range(
                        0,
                        len(
                            self.Neighbors[key_of_neighbor_for_interpolation]
                            .Neighbors[key_of_neighbor_for_interpolation]
                            .f_temp
                        ),
                    ):
                        if (
                            self.Neighbors[key_of_neighbor_for_interpolation]
                            .Neighbors[key_of_neighbor_for_interpolation]
                            .f_temp[i]
                            is not None
                        ):
                            w_temp_neighbor_neighbor_tpdt = (
                                w_temp_neighbor_neighbor_tpdt
                                + self.Neighbors[key_of_neighbor_for_interpolation]
                                .Neighbors[key_of_neighbor_for_interpolation]
                                .f_temp[i]
                                + computation.Parameters["dth"]
                                / 2.0
                                * self.Neighbors[key_of_neighbor_for_interpolation]
                                .Neighbors[key_of_neighbor_for_interpolation]
                                .source_Qi(computation, i)
                            )

                    desired_w_at_boundary_lattice_point = (
                        4.0 / 3.0 * w_temp_neighbor_tpdt
                        - 1.0 / 3.0 * w_temp_neighbor_neighbor_tpdt
                        + 2.0
                        / 3.0
                        * computation.BoundaryConditions[valid_boundary_name].CurrentValue[0]
                        * computation.Parameters["hh"]
                    )
                else:
                    raise Error(
                        "Class "
                        + self.__class__.__name__
                        + "Method: boun Error: Cannot Apply BC! Interior neighbour(s) missing for Point at ({0:16.8e},{1:16.8e})!".format(
                            self.x, self.y
                        )
                    )
            else:
                raise Error("Class " + self.__class__.__name__ + " Method: boun Error: BCType not implemented!")
            f_sum = 0.0
            if len(missing_f) == 1:
                for i in range(0, 5):
                    if i != missing_f[0]:
                        f_sum = f_sum + self.f_temp[i]
            elif len(missing_f) == 2:
                for i in range(0, 5):
                    if i != missing_f[0] and i != missing_f[1]:
                        f_sum = f_sum + self.f_temp[i]
            elif len(missing_f) == 3:
                for i in range(0, 5):
                    if i not in (missing_f[0], missing_f[1], missing_f[2]):
                        f_sum = f_sum + self.f_temp[i]
            else:
                raise Error(
                    "Error: Apply Boundary Condition not implemented for lattice points with more than three out of boundary neighbours!"
                )
            for f_miss in missing_f:
                self.f_temp[f_miss] = (
                    1.0
                    / len(missing_f)
                    * (
                        desired_w_at_boundary_lattice_point
                        - f_sum
                        - computation.Parameters["dth"] / 2.0 * sum(self.source_Q(computation))
                    )
                )

    def compute_w(self, computation):
        wscal = self.zeroth_moment() + sum(self.source_Q(computation)) * computation.Parameters["dth"] / 2.0
        w_dot = list([(wscal - self.w[0]) / computation.Parameters["dth"]])
        self.wdot = w_dot
        w = list()
        w.append(wscal)
        self.w = w

    def zeroth_moment(self):
        return sum(self.f)

    def source_psi(self, computation):
        s = self.w[0]
        psi = (
            computation.Parameters["M"]
            * computation.Parameters["Gc"]
            * 1.0
            / (2.0 * computation.Parameters["eps"])
            * (1.0 - s)
        )  # no psi_e so far
        return psi

    def source_Q(self, computation):
        Q = (
            (1.0 - 1.0 / 2.0 / computation.Parameters["tauh"])
            * self.source_psi(computation)
            * np.array(LatticePointD2Q5_PhasefieldFracture.weights)
        )
        return Q

    def source_Qi(self, computation, i):
        Q = (
            (1.0 - 1.0 / 2.0 / computation.Parameters["tauh"])
            * self.source_psi(computation)
            * LatticePointD2Q5_PhasefieldFracture.weights[i]
        )
        return Q

    def collide(self, computation):
        """
        :param computation: relaxation time
        :return: update distribution
        """

        def set_f_so_that_s_doesnt_change(Q, i):
            return
            # self.w[0] = 0.0
            # self.f[i] = self.f[i] #self.f_eq[i]  + Q[i]

        Q = self.source_Q(computation)
        for i in range(0, 5):
            if self.isFractured() and not self.isFracturedBoundaryPoint():  # irreversibility
                set_f_so_that_s_doesnt_change(Q, i)
            else:
                self.f[i] = self.f[i] - 1.0 / computation.Parameters["tauh"] * (self.f[i] - self.f_eq[i]) + Q[i]

    def update_distribution_function_after_streaming(self):
        """
        f_temp necessary to prevent overwriting f
        :return: updates f
        """
        for i in range(0, 5):
            if not self.isFractured() or self.isFracturedBoundaryPoint():  # irreversibility
                self.f[i] = self.f_temp[i]
            self.f_temp[i] = None  # invalidate distributions functions from current time step

    def isFractured(self):
        irreversibility_s_tol = 0.001
        return self.w[0] <= irreversibility_s_tol

    def isFracturedBoundaryPoint(self):
        return self.isFractured() and self.BoundaryName

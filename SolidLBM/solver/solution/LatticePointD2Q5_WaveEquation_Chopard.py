import numpy as np

import SolidLBM.solver.solution.LatticePointD2Q5_abstract as LatticePointD2Q5_abstract_Module
import SolidLBM.solver.solution.LatticePointD2Q5_WaveEquation_Guangwu as LatticePointD2Q5_WaveEquation_Guangwu_Module


class LatticePointD2Q5_WaveEquation_Chopard(
    LatticePointD2Q5_WaveEquation_Guangwu_Module.LatticePointD2Q5_WaveEquation_Guangwu
):
    __slots__ = ()

    @staticmethod
    def compute_dependent_parameters(cell_size, params):
        # cs (mue/rho) is the only free parameter
        params["cs"] = np.sqrt(params["mue"] / params["rho"])
        params["h"] = cell_size

        params["hh"] = params["h"] / params["L"]
        params["csh"] = params["cs"] / params["cs"]
        params["tauh"] = 0.5
        params["a0"] = 0.5
        params["a"] = (1.0 - params["a0"]) / 4.0
        params["ch"] = params["csh"] / (2.0 * params["a"]) ** 0.5
        params["dth"] = params["hh"] / params["ch"]
        params["b"] = 1.0

        return params

    def __init__(self, x=0.0, y=0.0, z=0.0, number=None):
        #  super(LatticePointD2Q5, self).__init__(x, y, z)
        super(LatticePointD2Q5_WaveEquation_Chopard, self).__init__(x, y, z, number)

    def initialise_distribution_functions_and_fields(self, computation):
        # TODO only displacement can be initialized, what about velocity?

        if computation.Lattice.InitialFieldsW:
            w_dot = []
            w = []
            for index in range(0, len(computation.Lattice.InitialFieldsWdot[self.ID])):
                w_dot.append(computation.Lattice.InitialFieldsWdot[self.ID][index] / computation.Parameters["cs"])
                w.append(computation.Lattice.InitialFieldsW[self.ID][index] / computation.Parameters["wr"])  # ...
        else:
            w_dot = list([0.0])
            w = list([0.0])
        self.w = w
        self.wdot = w_dot
        self.f[0] = computation.Parameters["a0"] * self.w[0]
        self.f_temp[0] = self.f[0]
        for i in range(1, 5):
            self.f[i] = computation.Parameters["a"] * self.w[0]
            self.f_temp[i] = computation.Parameters["a"] * self.w[0]

    def compute_missing_distribution_functions_according_to_target_value(self, computation, in_target_value):
        target_value = in_target_value[0]
        neighbor_keys = LatticePointD2Q5_abstract_Module.LatticePointD2Q5_abstract.neighbor_keys
        stream_keys = [3, 4, 1, 2]
        missing_f = []
        for key in neighbor_keys:  # does not need to be done in every time step TODO
            if key not in self.Neighbors.keys():  # distribution function is missing
                missing_f.append(
                    stream_keys[neighbor_keys.index(key)]
                )  # save index of distribution function that needs to be determined by boundary conditions
        f_sum = 0.0
        if len(missing_f) == 1:
            for i in range(0, 5):
                if i != missing_f[0]:
                    f_sum = f_sum + self.f_temp[i]
        elif len(missing_f) == 2:
            for i in range(0, 5):
                if i != missing_f[0] and i != missing_f[1]:
                    f_sum = f_sum + self.f_temp[i]
        elif len(missing_f) == 3:
            for i in range(0, 5):
                if i not in (missing_f[0], missing_f[1], missing_f[2]):
                    f_sum = f_sum + self.f_temp[i]
        else:
            print(
                "Error: Apply Boundary Condition not implemented for lattice points with more than three out of boundary neighbours!"
            )
        for f_miss in missing_f:
            self.f_temp[f_miss] = 1.0 / len(missing_f) * (target_value - f_sum)

    def compute_equilibrium_distribution(self, computation):
        cc = computation.Parameters["ch"]
        c = list([[0, 0], [cc, 0], [0, cc], [-cc, 0], [0, -cc]])
        J = list([0, 0])
        for i in range(0, 5):
            J[0] = J[0] + self.f[i] * c[i][0]
            J[1] = J[1] + self.f[i] * c[i][1]
        self.f_eq[0] = computation.Parameters["a0"] * self.w[0]
        for i in range(1, 5):
            self.f_eq[i] = computation.Parameters["a"] * self.w[0] + computation.Parameters["b"] * (
                c[i][0] * J[0] + c[i][1] * J[1]
            ) / (2.0 * computation.Parameters["ch"] ** 2)

    def boun(self, computation):
        if bool(self.BoundaryName):
            valid_boundary_name = min(self.BoundaryName.values())  # determines which boundary is to be fulfilled
            neighbor_keys = LatticePointD2Q5_abstract_Module.LatticePointD2Q5_abstract.neighbor_keys
            inv_keys = LatticePointD2Q5_abstract_Module.LatticePointD2Q5_abstract.inv_keys
            stream_keys = [3, 4, 1, 2]
            missing_f = []
            for key in neighbor_keys:
                if key not in self.Neighbors.keys():  # distribution function is missing
                    missing_f.append(
                        stream_keys[neighbor_keys.index(key)]
                    )  # save index of distribution function that needs to be determined by boundary conditions
            if computation.BoundaryConditions[valid_boundary_name].BCType == "Dirichlet":
                tmp_value = computation.BoundaryConditions[valid_boundary_name].CurrentValue[0]
            elif computation.BoundaryConditions[valid_boundary_name].BCType == "Neumann":
                key_in_dir_of_valid_boundary = [
                    key for key in self.BoundaryName.keys() if self.BoundaryName[key] == valid_boundary_name
                ]
                key = inv_keys[
                    neighbor_keys.index(key_in_dir_of_valid_boundary[0])
                ]  # key of neighbor in the interior of the domain that is used to compute gradient according to bc
                if key in self.Neighbors.keys():
                    w_temp_neighbor_tpdt = 0.0
                    for f in self.Neighbors[key].f_temp:
                        if f is not None:
                            w_temp_neighbor_tpdt = w_temp_neighbor_tpdt + f
                    tmp_value = (
                        computation.BoundaryConditions[valid_boundary_name].CurrentValue[0]
                        * computation.Parameters["hh"]
                        + w_temp_neighbor_tpdt
                    )  #
                else:
                    print(
                        "Class LatticePointD2Q5Chopard Method: boun Error: Cannot Apply BC! Interior neighbour missing for Point at (x,y)!"
                    )
            elif computation.BoundaryConditions[valid_boundary_name].BCType == "Neumann2Order":
                key_in_dir_of_valid_boundary = [
                    key for key in self.BoundaryName.keys() if self.BoundaryName[key] == valid_boundary_name
                ]
                key = inv_keys[
                    neighbor_keys.index(key_in_dir_of_valid_boundary[0])
                ]  # key of neighbor in the interior of the domain that is used to compute gradient according to bc
                if key in self.Neighbors.keys() and key in self.Neighbors[key].Neighbors.keys():
                    w_temp_neighbor_tpdt = 0.0  # TODO problematisch wenn Nachbarn selbst Randpunkte sind
                    for f in self.Neighbors[key].f_temp:
                        if f is not None:
                            w_temp_neighbor_tpdt = w_temp_neighbor_tpdt + f

                    w_temp_neighbor_neighbor_tpdt = 0.0
                    for f in self.Neighbors[key].Neighbors[key].f_temp:
                        if f is not None:
                            w_temp_neighbor_neighbor_tpdt = w_temp_neighbor_neighbor_tpdt + f

                    tmp_value = (
                        4.0 / 3.0 * w_temp_neighbor_tpdt
                        - 1.0 / 3.0 * w_temp_neighbor_neighbor_tpdt
                        + 2.0
                        / 3.0
                        * computation.BoundaryConditions[valid_boundary_name].CurrentValue[0]
                        * computation.Parameters["hh"]
                    )
                else:
                    print(
                        "Class LatticePointD2Q5Chopard Method: boun Error: Cannot Apply BC! Interior neighbour(s) missing for Point at ({0:16.8e},{1:16.8e})!".format(
                            self.x, self.y
                        )
                    )
            else:
                print("Class LatticePointD2Q5Chopard Method: boun Error: BCType not implemented!")
            f_sum = 0.0
            if len(missing_f) == 1:
                for i in range(0, 5):
                    if i != missing_f[0]:
                        f_sum = f_sum + self.f_temp[i]
            elif len(missing_f) == 2:
                for i in range(0, 5):
                    if i != missing_f[0] and i != missing_f[1]:
                        f_sum = f_sum + self.f_temp[i]
            elif len(missing_f) == 3:
                for i in range(0, 5):
                    if i not in (missing_f[0], missing_f[1], missing_f[2]):
                        f_sum = f_sum + self.f_temp[i]
            else:
                print(
                    "Error: Apply Boundary Condition not implemented for lattice points with more than three out of boundary neighbours!"
                )
            for f_miss in missing_f:
                self.f_temp[f_miss] = 1.0 / len(missing_f) * (tmp_value - f_sum)

    def boun_chopard_local_non_geo_conforming(self, computation):  # Does not work correctly
        if bool(self.BoundaryName):
            neighbor_keys_string = ["x+", "y+", "x-", "y-"]
            inv_neighbor_keys_string = ["x-", "y-", "x+", "y+"]
            neighbor_keys_int = [1, 2, 3, 4]
            inv_neighbor_keys_int = [3, 4, 1, 2]
            missing_f = []
            for key in neighbor_keys_string:
                if key not in self.Neighbors.keys():  # distribution function is missing
                    missing_f.append(
                        inv_neighbor_keys_int[neighbor_keys_string.index(key)]
                    )  # save index of distribution function that needs to be determined by boundary conditions

            for key in neighbor_keys_string:
                if key not in self.Neighbors.keys():
                    missing_f_no = inv_neighbor_keys_int[neighbor_keys_string.index(key)]
                    q = self.DistanceToBoundary[key]
                    h = computation.Parameters["hh"]
                    valid_boundary_name = self.BoundaryName[key]

                    if computation.BoundaryConditions[valid_boundary_name].BCType == "Dirichlet":
                        factor = -1.0
                        f_miss_bc = (
                            0.5 * computation.BoundaryConditions[valid_boundary_name].CurrentValue[0]
                        )  # TODO Factor 0.5?
                    elif computation.BoundaryConditions[valid_boundary_name].BCType in [
                        "Neumann2Order",
                        "Neumann",
                    ]:
                        f_miss_bc = (
                            1.0
                            * (computation.Parameters["csh"] * computation.Parameters["dth"] - q)
                            * computation.BoundaryConditions[valid_boundary_name].CurrentValue[0]
                        )  # mueh = 1.0
                        factor = 1.0

                    if q <= 0.5 * h:
                        index_f_star = neighbor_keys_int[
                            neighbor_keys_string.index(key)
                        ]  # key = 'x+' --> index_f_star = 1
                        x_star = 2.0 * q
                        n_key = inv_neighbor_keys_string[neighbor_keys_string.index(key)]

                        if n_key in self.Neighbors.keys():
                            neighbor = self.Neighbors[n_key]
                            f_star = neighbor.f[index_f_star] * (h - x_star) / h + self.f[index_f_star] * x_star / h
                            self.f_temp[missing_f_no] = factor * f_star + f_miss_bc
                        else:
                            self.f_temp[missing_f_no] = 0.0  # set to zero since no streaming can be defined
                            print(
                                "Class LatticePointD2Q5Chopard method boun_chopard_local_non_geo_conforming Warning: Cannot deal with non-mesh conforming BC at point. Neighbor needed for interpolation does not exist "
                            )
                    else:  # q > 0.5
                        x_star = 2.0 * q - h
                        n_key = inv_neighbor_keys_string[neighbor_keys_string.index(key)]
                        if n_key in self.Neighbors.keys():
                            neighbor = self.Neighbors[n_key]
                            index_aux = neighbor_keys_int[neighbor_keys_string.index(key)]
                            f_star = factor * self.f[index_aux]
                            self.f_temp[missing_f_no] = (
                                h / (h + x_star) * self.f[missing_f_no] + x_star / (h + x_star) * f_star + f_miss_bc
                            )
                        else:
                            self.f_temp[missing_f_no] = 0.0  # set to zero since no streaming can be defined
                            print(
                                "Class LatticePointD2Q5Chopard method boun_chopard_local_non_geo_conforming Warning: Cannot deal with non-mesh conforming BC at point. Neighbor needed for interpolation does not exist "
                            )

    def compute_w(self, computation):
        w = []
        w.append(sum(self.f))
        w_dot = list([(w[0] - self.w[0]) / computation.Parameters["dth"]])
        self.w = w
        self.wdot = w_dot

    def get_future_w(self, computation=None, point=None):
        return list([sum(point.f_temp)])

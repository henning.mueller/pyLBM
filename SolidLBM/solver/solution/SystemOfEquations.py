import numpy as np
import copy


class SystemOfEquations:
    """
    a system of equations to which individual entries can be added and that can be solved
    """

    def __init__(self, dimension_of_quadratic_matrix):
        self.S = list()
        self.rhs = list()
        self._dim = dimension_of_quadratic_matrix

        self._allocate_zero_coefficient_matrix(dimension_of_quadratic_matrix)
        self._allocate_zero_rhs(dimension_of_quadratic_matrix)

    def _allocate_zero_coefficient_matrix(self, dimension_of_quadratic_matrix):
        tmp_array = [0.0] * int(dimension_of_quadratic_matrix)  # row
        tmp_matrix = list()
        for i in range(0, int(dimension_of_quadratic_matrix)):
            tmp_matrix.append(copy.copy(tmp_array))
        self.S = tmp_matrix

    def _allocate_zero_rhs(self, dimension_of_quadratic_matrix):
        self.rhs = list([0.0] * int(dimension_of_quadratic_matrix))

    def write_S(self, row, col, entry):
        self.S[row][col] = self.S[row][col] + entry

    def get_S(self):
        return self.S

    def multiply_left(self, A):
        Snp = np.array(self.S)
        Anp = np.array(A)
        Res = Anp.dot(Snp)
        return Res.tolist()

    def multiply_right(self, A):
        Snp = np.array(self.S)
        Anp = np.array(A)
        Res = Snp.dot(A)
        return Res.tolist()

    def inverse_of_S(self):
        Snp = np.array(self.S)
        Res = np.linalg.inv(Snp)
        return Res.tolist()

    def write_rhs(self, row, entry):
        self.rhs[row] = self.rhs[row] + entry

    def zero_S(self):
        for row in range(0, len(self.S)):
            for col in range(0, len(self.S[row])):
                self.S[row][col] = 0.0

    def zero_rhs(self):
        for row in range(0, len(self.rhs)):
            self.rhs[row] = 0.0

    def solve(self):
        S = np.array(self.S)
        rhs = np.array(self.rhs)
        x = np.linalg.solve(S, rhs)
        return x.tolist()

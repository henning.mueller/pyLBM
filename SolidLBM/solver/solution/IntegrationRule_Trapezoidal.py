import SolidLBM.solver.solution.IntegrationRule_abstract as IntegrationRule_abstract_Module


class IntegrationRule_Trapezoid(IntegrationRule_abstract_Module.IntegrationRule_abstract):
    def __init__(self, parameters=dict()):
        self.Parameters = parameters

    def integrateW(self, w, wdot_np1, wdot_n):
        """
        computes current w from old w, wdot, old wdot
        :param w:
        :param wdot:
        :param wddot:
        :return:
        """
        dt = self.Parameters["dth"]
        if len(w) == 2:
            out_w = list([0.0, 0.0])
        elif len(w) == 1:
            out_w = [0.0]
        for i in range(len(w)):
            out_w[i] = w[i] + dt * 0.5 * (wdot_n[i] + wdot_np1[i])
        return out_w

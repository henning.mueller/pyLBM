from collections import namedtuple
from typing import Any
import tomli

from SolidLBM.solver.solution.Link import Link


crackfaces = namedtuple('CrackFaces', 'R L')


def zero(*args) -> int:
    return 0

def stretch(*args) -> float:
    return args[0].rel_stretch()

__CRITERION_SWITCH__ = {
    'none': zero,
    'link_stretch': stretch,
}


class CrackFree:
    def __init__(self, computation) -> None:
        """
        Initializes the CrackFree class.

        Args:
            computation: An instance of the Computation class.

        Returns:
            None
        """
        def _read_config() -> tuple[dict[Any]]:
            with open(str(in_file_path) + ".toml", mode="rb") as f:
                config_all = tomli.load(f)
                config = config_all['DynCrack']
            return config
        
        in_file_path = computation.WorkingDir / computation.Name

        self.failure = []

        config = _read_config()
        self.crit_val = config.get('crit_val', 1.1)
        self.criterion = config.get('criterion', 'none')
        self.crit_method = __CRITERION_SWITCH__[self.criterion]
        bc_r, bc_l = config.get('bc', [0, 0])
        self.bc_names = crackfaces(bc_r, bc_l)
        
        pts = computation.Lattice.Points
        if 'box' in config:
            box = config['box']
            pts = self.point_in_box_generator(pts, box)
        
        self.links: list = self._define_links(pts)

        self.verbose = computation.Flags.get('verbose', False)

        if self.verbose:
            print(f"CrackFree initialized with criterion: {self.criterion}")

    def point_in_box_generator(self, pts, box):
        xmin, xmax, ymin, ymax = box
        for pt in pts:
            if xmin < pt.x < xmax and ymin < pt.y < ymax:
                yield pt

    def __repr__(self) -> str:
        return f"CrackFree ({self.criterion})"

    def _handle_crack_propagation(self, link) -> bool:
        """
        Handles crack propagation based on a criterion.

        Args:
            *args: Variable number of arguments.

        Returns:
            bool: True if crack propagation occurs, False otherwise.
        """
        val = self.crit_method(link)
        return val > self.crit_val

    def evaluate_crack_propagation(self, *args) -> tuple[list, list, bool]:
        """
        Evaluates crack propagation based on a criterion set from input and
        processes the links accordingly.

        Args:
            *args: Variable number of arguments.

        Returns:
            Tuple: A tuple containing three elements:
                - aux_pts: Always returns None.
                - boun_pts: A list of boundary points.
                - propagation: A boolean indicating whether crack propagation occurred.
        """
        propagation = False
        boun_pts = []
        broken_links = []
        failure = []

        for link in self.links:
            fail: bool = self._handle_crack_propagation(link)

            if fail:
                boun_pts.append(link.points)
                broken_links.append(link)
                failure.append((link.center, link.center))
                
                if self.verbose:
                    print(f"Link {link.ID} at ({link.center.x:.3f}, {link.center.y:.3f}) failed with {link.rel_stretch():.4f}")

            propagation |= fail

        self.failure.extend(failure)

        for link in broken_links:
            for p, k in zip(link.points, link.keys):
                # set quantities for boundary points from link
                # without resorting to building a crack
                assert k in p.Neighbors

                # get sign of normal
                sign = link.get_sign_of_direction(k)
                p.IsLeftOrRightFaceOfCrack = 'R' if sign < 0 else 'L'
                p.BoundaryNormals[k] = link.direction() * sign

                # remove link from neighbors and set bc
                bc = self.bc_names.R if p.IsLeftOrRightFaceOfCrack == 'R' else self.bc_names.L
                p.BoundaryName[k] = bc
                p.BoundaryClosestPoints[k] = link.center
                p.Neighbors.pop(k)

            # finally, remove link from list
            self.links.remove(link)

        # 'failure' could be extended to provide AuxPoints
        return None, boun_pts, propagation

    @staticmethod
    def _define_links(pts) -> list:
        """
        Defines the links between points.

        Args:
            pts: A set of points.

        Returns:
            list: A list of links.
        """
        # pts = set(pts)
        links = []
        visited = set()
        idx = 0
        for pt in pts:
            for n_pt in set(pt.Neighbors.values()) - visited:
                link = Link(pt, n_pt, number=idx)
                links.append(link)
                idx += 1
            visited.add(pt)
        return links

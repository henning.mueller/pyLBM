from abc import ABC, abstractmethod

import numpy as np

from SolidLBM.util.geometry.Point import Point
import SolidLBM.solver.interpolation.CellTriangleD2 as CellTriangleD2_Module


class LatticePointD2_abstract(Point, ABC):
    __slots__ = (
        "x",
        "y",
        "z",
        "ID",
        "f",
        "f_temp",
        "f_eq",
        "w",
        "wdot",
        "u",
        "u_dot",
        "imp",
        "Neighbors",
        "NeighborsDiagonal",
        "VolumeSurfaceMeasure",
        "BoundaryName",
        "DistanceToBoundary",
        "BoundaryClosestPoints",
        "BoundaryNormals",
        "BCImplementationSwitch",
        "BoundaryPointListID",
        "BoundaryCoefficients",
        "PPData",
        "gradient_coefficients",
    )

    # static attributes
    neighbor_keys = []  # neighbor keys for D2Q*
    inv_keys = []  # inverse to neighbor keys
    stream_keys = []
    no_f = None
    FD_ACC_DEFAULT = 2  # default finite difference accuracy, must be 2 for now
    LOW_ORDER_BOUNDARY = True  # reduce order at boundary to 1st order

    @staticmethod
    @abstractmethod
    def compute_dependent_parameters(cell_size, params):
        pass

    @classmethod
    def set_fd_acc_default(cls, acc):
        cls.FD_ACC_DEFAULT = acc

    def __init__(self, x=0.0, y=0.0, z=0.0, number=None):
        super(LatticePointD2_abstract, self).__init__(x, y, z)
        self.Neighbors = dict()
        self.NeighborsDiagonal = dict()
        self.BoundaryName = dict()  # valid boundary name for each lattice link without a corresponding neighbor
        self.DistanceToBoundary = dict()
        self.BoundaryClosestPoints = dict()  # the closest points for each boundary lattice point
        self.BoundaryNormals = dict()  # normals at this point
        self.SourceIdent = False
        self.ID = number
        self.f = []
        self.f_temp = []
        self.f_eq = []

        self.w = []
        self.wdot = []  # rate of macroscopic field
        self.u = None  # displacement as 3D-Vector
        self.u_dot = None  # rate of field as 3D-Vector
        self.u_dot_prev = None  # previous rate of field as 3D-Vector

        # data for postprocessing
        self.PPData = dict()  # dictionary for output, handled by Output_Interface

        self.BCImplementationSwitch = -1  # for multiple BC implementations in same computation

        # non-mesh conforming BC macroscopic matrix (TODO which type?)
        self.BoundaryPointListID = -1
        self.BoundaryCoefficients = []

        # non-mesh conforming BC local
        self.VolumeSurfaceMeasure = []

        # gradient coefficients
        self.gradient_coefficients = []

    def __repr__(self):
        return "{} (ID: {}; x: {}, y: {})".format(self.__class__.__name__, self.ID, self.x, self.y)

    def __hash__(self):
        # hash from attributes; makes LatticePoints hashable, e.g. for sets
        return hash((self.ID, self.x, self.y, self.z, self.__class__))

    def __lt__(self, other):
        # compare x, then y, then ID (lexicographical order)
        if not isinstance(other, LatticePointD2_abstract):
            raise TypeError(f"Can't compare {self.__class__.__name__}-object and {type(other)}")

        return (
            (self.x < other.x)
            or (self.x == other.x and self.y < other.y)
            or (self.x == other.x and self.y == other.y and self.ID < other.ID)
        )

    def __le__(self, other):
        # compare x, then y, then ID (lexicographical order)
        if not isinstance(other, LatticePointD2_abstract):
            raise TypeError(f"Can't compare {self.__class__.__name__}-object and {type(other)}")

        return (
            (self.x <= other.x)
            or (self.x == other.x and self.y <= other.y)
            or (self.x == other.x and self.y == other.y and self.ID <= other.ID)
        )
    
    def __gt__(self, other):
        # compare x, then y, then ID (lexicographical order)
        if not isinstance(other, LatticePointD2_abstract):
            raise TypeError(f"Can't compare {self.__class__.__name__}-object and {type(other)}")

        return (
            (self.x > other.x)
            or (self.x == other.x and self.y > other.y)
            or (self.x == other.x and self.y == other.y and self.ID > other.ID)
        )
    
    def __ge__(self, other):
        # compare x, then y, then ID (lexicographical order)
        if not isinstance(other, LatticePointD2_abstract):
            raise TypeError(f"Can't compare {self.__class__.__name__}-object and {type(other)}")

        return (
            (self.x >= other.x)
            or (self.x == other.x and self.y >= other.y)
            or (self.x == other.x and self.y == other.y and self.ID >= other.ID)
        )

    @abstractmethod
    def compute_equilibrium_distribution(self, computation):
        """
        :param computation: fields at lattice point, general data of computation, dt
        :return: f_eq
        """
        pass

    @abstractmethod
    def compute_w(self, computation):
        """
        computes macroscopic field from distribution functions (depends on interpretation of distribution functions)
        :param computation:
        :return: self.w
        """
        pass

    def _getNextNeighbors(self, key=""):
        """
        computes whether neighbor key exists and returns [boolean pointExists, int ID of neighbor]
        :param key:
        :return:
        """

        if key == "x+x+":
            if "x+" in self.Neighbors.keys():
                if "x+" in self.Neighbors["x+"].Neighbors.keys():
                    return [True, self.Neighbors["x+"].Neighbors["x+"]]
        elif key == "y+y+":
            if "y+" in self.Neighbors.keys():
                if "y+" in self.Neighbors["y+"].Neighbors.keys():
                    return [True, self.Neighbors["y+"].Neighbors["y+"]]
        elif key == "x-x-":
            if "x-" in self.Neighbors.keys():
                if "x-" in self.Neighbors["x-"].Neighbors.keys():
                    return [True, self.Neighbors["x-"].Neighbors["x-"]]
        elif key == "y-y-":
            if "y-" in self.Neighbors.keys():
                if "y-" in self.Neighbors["y-"].Neighbors.keys():
                    return [True, self.Neighbors["y-"].Neighbors["y-"]]
        elif key == "x+y+":
            if "x+" in self.Neighbors.keys():
                if "y+" in self.Neighbors["x+"].Neighbors.keys():
                    return [True, self.Neighbors["x+"].Neighbors["y+"]]
            elif "y+" in self.Neighbors.keys():  # second path to neighbor
                if "x+" in self.Neighbors["y+"].Neighbors.keys():
                    return [True, self.Neighbors["y+"].Neighbors["x+"]]
        elif key == "x+y-":
            if "x+" in self.Neighbors.keys():
                if "y-" in self.Neighbors["x+"].Neighbors.keys():
                    return [True, self.Neighbors["x+"].Neighbors["y-"]]
            elif "y-" in self.Neighbors.keys():  # second path to neighbor
                if "x+" in self.Neighbors["y-"].Neighbors.keys():
                    return [True, self.Neighbors["y-"].Neighbors["x+"]]
        elif key == "x-y+":
            if "x-" in self.Neighbors.keys():
                if "y+" in self.Neighbors["x-"].Neighbors.keys():
                    return [True, self.Neighbors["x-"].Neighbors["y+"]]
            elif "y+" in self.Neighbors.keys():  # second path to neighbor
                if "x-" in self.Neighbors["y+"].Neighbors.keys():
                    return [True, self.Neighbors["y+"].Neighbors["x-"]]
        elif key == "x-y-":
            if "x-" in self.Neighbors.keys():
                if "y-" in self.Neighbors["x-"].Neighbors.keys():
                    return [True, self.Neighbors["x-"].Neighbors["y-"]]
            elif "y-" in self.Neighbors.keys():  # second path to neighbor
                if "x-" in self.Neighbors["y-"].Neighbors.keys():
                    return [True, self.Neighbors["y-"].Neighbors["x-"]]
        elif key == "x+x+y+":
            if "x+" in self.Neighbors.keys():
                if "x+" in self.Neighbors["x+"].Neighbors.keys():
                    if "y+" in self.Neighbors["x+"].Neighbors["x+"].Neighbors.keys():
                        return [
                            True,
                            self.Neighbors["x+"].Neighbors["x+"].Neighbors["y+"],
                        ]
        elif key == "x+x+y-":
            if "x+" in self.Neighbors.keys():
                if "x+" in self.Neighbors["x+"].Neighbors.keys():
                    if "y-" in self.Neighbors["x+"].Neighbors["x+"].Neighbors.keys():
                        return [
                            True,
                            self.Neighbors["x+"].Neighbors["x+"].Neighbors["y-"],
                        ]
        elif key == "x+y+y+":
            if "x+" in self.Neighbors.keys():
                if "y+" in self.Neighbors["x+"].Neighbors.keys():
                    if "y+" in self.Neighbors["x+"].Neighbors["y+"].Neighbors.keys():
                        return [
                            True,
                            self.Neighbors["x+"].Neighbors["y+"].Neighbors["y+"],
                        ]
        elif key == "x-y+y+":
            if "x-" in self.Neighbors.keys():
                if "y+" in self.Neighbors["x-"].Neighbors.keys():
                    if "y+" in self.Neighbors["x-"].Neighbors["y+"].Neighbors.keys():
                        return [
                            True,
                            self.Neighbors["x-"].Neighbors["y+"].Neighbors["y+"],
                        ]
        elif key == "x-x-y+":
            if "x-" in self.Neighbors.keys():
                if "x-" in self.Neighbors["x-"].Neighbors.keys():
                    if "y+" in self.Neighbors["x-"].Neighbors["x-"].Neighbors.keys():
                        return [
                            True,
                            self.Neighbors["x-"].Neighbors["x-"].Neighbors["y+"],
                        ]
        elif key == "x-x-y-":
            if "x-" in self.Neighbors.keys():
                if "x-" in self.Neighbors["x-"].Neighbors.keys():
                    if "y-" in self.Neighbors["x-"].Neighbors["x-"].Neighbors.keys():
                        return [
                            True,
                            self.Neighbors["x-"].Neighbors["x-"].Neighbors["y-"],
                        ]
        elif key == "x+y-y-":
            if "x+" in self.Neighbors.keys():
                if "y-" in self.Neighbors["x+"].Neighbors.keys():
                    if "y-" in self.Neighbors["x+"].Neighbors["y-"].Neighbors.keys():
                        return [
                            True,
                            self.Neighbors["x+"].Neighbors["y-"].Neighbors["y-"],
                        ]
        elif key == "x-y-y-":
            if "x-" in self.Neighbors.keys():
                if "y-" in self.Neighbors["x-"].Neighbors.keys():
                    if "y-" in self.Neighbors["x-"].Neighbors["y-"].Neighbors.keys():
                        return [
                            True,
                            self.Neighbors["x-"].Neighbors["y-"].Neighbors["y-"],
                        ]
        elif key == "x+x+y+y+":
            if "x+" in self.Neighbors.keys():
                if "x+" in self.Neighbors["x+"].Neighbors.keys():
                    if "y+" in self.Neighbors["x+"].Neighbors["x+"].Neighbors.keys():
                        if "y+" in self.Neighbors["x+"].Neighbors["x+"].Neighbors["y+"].Neighbors.keys():
                            return [
                                True,
                                self.Neighbors["x+"].Neighbors["x+"].Neighbors["y+"].Neighbors["y+"],
                            ]
        elif key == "x+x+y-y-":
            if "x+" in self.Neighbors.keys():
                if "x+" in self.Neighbors["x+"].Neighbors.keys():
                    if "y-" in self.Neighbors["x+"].Neighbors["x+"].Neighbors.keys():
                        if "y-" in self.Neighbors["x+"].Neighbors["x+"].Neighbors["y-"].Neighbors.keys():
                            return [
                                True,
                                self.Neighbors["x+"].Neighbors["x+"].Neighbors["y-"].Neighbors["y-"],
                            ]
        elif key == "x-x-y+y+":
            if "x-" in self.Neighbors.keys():
                if "x-" in self.Neighbors["x-"].Neighbors.keys():
                    if "y+" in self.Neighbors["x-"].Neighbors["x-"].Neighbors.keys():
                        if "y+" in self.Neighbors["x-"].Neighbors["x-"].Neighbors["y+"].Neighbors.keys():
                            return [
                                True,
                                self.Neighbors["x-"].Neighbors["x-"].Neighbors["y+"].Neighbors["y+"],
                            ]
        elif key == "x-x-y-y-":
            if "x-" in self.Neighbors.keys():
                if "x-" in self.Neighbors["x-"].Neighbors.keys():
                    if "y-" in self.Neighbors["x-"].Neighbors["x-"].Neighbors.keys():
                        if "y-" in self.Neighbors["x-"].Neighbors["x-"].Neighbors["y-"].Neighbors.keys():
                            return [
                                True,
                                self.Neighbors["x-"].Neighbors["x-"].Neighbors["y-"].Neighbors["y-"],
                            ]
        else:
            raise NotImplementedError("Illegal key")

        return [False, None]

    def find_diagonal_neighbors(self):
        """
        finds the neighboring points in NE-, NW-, SE-, SW-direction
        :return:
        """

        [xpypExists, xpyp] = self._getNextNeighbors("x+y+")
        if xpypExists:
            self.NeighborsDiagonal["x+y+"] = xpyp

        [xpymExists, xpym] = self._getNextNeighbors("x+y-")
        if xpymExists:
            self.NeighborsDiagonal["x+y-"] = xpym

        [xmypExists, xmyp] = self._getNextNeighbors("x-y+")
        if xmypExists:
            self.NeighborsDiagonal["x-y+"] = xmyp

        [xmymExists, xmym] = self._getNextNeighbors("x-y-")
        if xmymExists:
            self.NeighborsDiagonal["x-y-"] = xmym

    def _compute_gradient_coefficients(self, lattice=None, h=None, order=None,
                                       *, low_order_boundary=None):
        """Computes lattice points and corresponding weights for computation of gradients.

        - deteremines the position of the point (interior / boundary)
        - sets the corresponding coefficients
        - supports orders of 4 and 2
        - the highest possible order is used, reduced order (e.g. 3rd order) could occur for cracks

        :param lattice:
        :return: a tuple of dictionaries (dx, dy) where the entries are dictionaries.
                 the keys are the node numbers and the values are the corresponding weights for gradient computation
        """

        if order is None:
            order = self.FD_ACC_DEFAULT
        if low_order_boundary is None:
            low_order_boundary = self.LOW_ORDER_BOUNDARY

        if not h:
            h = lattice.CellSize

        grad_coeffs = {"x": dict(), "y": dict()}

        def set_coeffs(n_pts, dkeys, cvals):
            """link coefficients to point IDs"""
            coeffs = dict()
            for k, c in zip(dkeys, cvals):
                pt = n_pts[k]
                coeffs[pt.ID] = c / h
            return coeffs

        # determine pattern of neighbouring points first
        for axis in ("x", "y"):
            n_pts = {"p": self}
            pattern = "p"

            # determine bulk case
            d = axis + "-"
            if d in self.Neighbors.keys():
                pattern = "o" + pattern
                n_pts[d] = self.Neighbors[d]

                dmmExists, pt_mm = self._getNextNeighbors(2 * d)
                if dmmExists:
                    pattern = "o" + pattern
                    n_pts[2 * d] = pt_mm

            d = axis + "+"
            if d in self.Neighbors.keys():
                pattern = pattern + "o"
                n_pts[d] = self.Neighbors[d]

                dppExists, pt_pp = self._getNextNeighbors(2 * d)
                if dppExists:
                    pattern = pattern + "o"
                    n_pts[2 * d] = pt_pp

            if order > 2:
                if pattern == "oop" or pattern == "oopo":
                    # d-d-d-
                    d = axis + "-"
                    ptm = n_pts[d]
                    dmmmExists, pt_mmm = ptm._getNextNeighbors(2 * d)
                    if dmmmExists:
                        pattern = "o" + pattern
                        n_pts[3 * d] = pt_mmm
                if pattern == "ooop":
                    # d-d-d-d-
                    d = axis + "-"
                    ptmm = n_pts[2 * d]
                    dmmmmExists, pt_mmmm = ptmm._getNextNeighbors(2 * d)
                    if dmmmmExists:
                        pattern = "o" + pattern
                        n_pts[4 * d] = pt_mmmm

                if pattern == "poo" or pattern == "opoo":
                    # d+d+d+
                    d = axis + "+"
                    ptp = n_pts[d]
                    dpppExists, pt_ppp = ptp._getNextNeighbors(2 * d)
                    if dpppExists:
                        pattern = pattern + "o"
                        n_pts[3 * d] = pt_ppp
                if pattern == "pooo":
                    # d+d+d+d+
                    d = axis + "+"
                    ptpp = n_pts[2 * d]
                    dppppExists, pt_pppp = ptpp._getNextNeighbors(2 * d)
                    if dppppExists:
                        pattern = pattern + "o"
                        n_pts[4 * d] = pt_pppp
            else:
                # reduce to 2nd order
                if "opo" in pattern:
                    pattern = "opo"
                elif "poo" in pattern:
                    pattern = "poo"
                elif "oop" in pattern:
                    pattern = "oop"

            if low_order_boundary:
                # higher order at boundary leads to instabilities, needs to be reduced to 1st order
                if pattern.startswith("p") and len(pattern) > 1:
                    pattern = "po"
                if pattern.endswith("p") and len(pattern) > 1:
                    pattern = "op"

            # set coefficients according to pattern of neighbouring points
            dp = axis + "+"
            dm = axis + "-"
            match pattern:
                # 4th order
                case "oopoo":
                    dkeys = (2 * dm, dm, dp, 2 * dp)
                    cvals = (1 / 12, -2 / 3, 2 / 3, -1 / 12)
                case "opooo":
                    dkeys = (dm, "p", dp, 2 * dp, 3 * dp)
                    cvals = (-1 / 4, -5 / 6, 3 / 2, -1 / 2, 1 / 12)
                case "poooo":
                    dkeys = ("p", dp, 2 * dp, 3 * dp, 4 * dp)
                    cvals = (-25 / 12, 4, -3, 4 / 3, -1 / 4)
                case "ooopo":
                    dkeys = (3 * dm, 2 * dm, dm, "p", dp)
                    cvals = (-1 / 12, 1 / 2, -3 / 2, 5 / 6, 1 / 4)
                case "oooop":
                    dkeys = (4 * dm, 3 * dm, 2 * dm, dm, "p")
                    cvals = (1 / 4, -4 / 3, 3, -4, 25 / 12)

                # 3rd order
                case "opoo":
                    dkeys = (dm, "p", dp, 2 * dp)
                    cvals = (-1 / 3, -1 / 2, 1, -1 / 6)
                case "pooo":
                    dkeys = ("p", dp, 2 * dp, 3 * dp)
                    cvals = (-11 / 6, 3, -3 / 2, 1 / 3)
                case "oopo":
                    dkeys = (2 * dm, dm, "p", dp)
                    cvals = (1 / 6, -1, 1 / 2, 1 / 3)
                case "ooop":
                    dkeys = (3 * dm, 2 * dm, dm, "p")
                    cvals = (-1 / 3, 3 / 2, -3, 11 / 6)

                # 2nd order
                case "opo":
                    dkeys = (dm, dp)
                    cvals = (-1 / 2, 1 / 2)
                case "poo":
                    dkeys = ("p", dp, 2 * dp)
                    cvals = (-3 / 2, 2, -1 / 2)
                case "oop":
                    dkeys = (2 * dm, dm, "p")
                    cvals = (1 / 2, -2, 3 / 2)

                # 1st order
                case "po":
                    dkeys = ("p", dp)
                    cvals = (-1, 1)
                case "op":
                    dkeys = (dm, "p")
                    cvals = (-1, 1)
                # oth order
                case "p":
                    dkeys = ("p",)
                    cvals = (0,)
                case _:
                    raise ValueError(f"Pattern '{pattern}' could not be matched.")

            grad_coeffs[axis] = set_coeffs(n_pts, dkeys, cvals)

        return grad_coeffs["x"], grad_coeffs["y"]

    def _compute_second_order_derivative_coefficients(self, lattice):
        """computes second order derivative coefficients of a field
        :param field:
        :param lattice:
        :return:
        """
        dxk, dyk = self.get_gradient_coefficients(h=lattice.CellSize)

        dxx = dict()
        dyy = dict()
        dxy = dict()
        dyx = dict()

        for rID in dxk.keys():
            dxr, dyr = lattice.Points[rID].get_gradient_coefficients(lattice=lattice)
            for lID in dxr.keys():
                if lID in dxx.keys():  # field already written -> add
                    dxx[lID] = dxx[lID] + dxk[rID] * dxr[lID]
                else:
                    dxx[lID] = dxk[rID] * dxr[lID]
            for lID in dyr.keys():
                if lID in dxy.keys():
                    dxy[lID] = dxy[lID] + dxk[rID] * dyr[lID]
                else:
                    dxy[lID] = dxk[rID] * dyr[lID]
        for rID in dyk.keys():
            dxr, dyr = lattice.Points[rID].get_gradient_coefficients(lattice=lattice)
            for lID in dyr.keys():
                if lID in dyy.keys():
                    dyy[lID] = dyy[lID] + dyk[rID] * dyr[lID]
                else:
                    dyy[lID] = dyk[rID] * dyr[lID]
            for lID in dxr.keys():
                if lID in dyx.keys():
                    dyx[lID] = dyx[lID] + dyk[rID] * dxr[lID]
                else:
                    dyx[lID] = dyk[rID] * dxr[lID]

        return [dxx, dyy, dxy, dyx]

    def _compute_second_order_coefficients_for_boundary_points_separately(self, lattice):
        """computes second order coefficients separately for boundary and non-boundary nodes
        :param lattice:
        :return:
        """
        dxx, dyy, dxy, dyx = self._compute_second_order_derivative_coefficients(lattice)

        dxx_nonboundary = dict()
        dxx_boundary = dict()

        dyy_nonboundary = dict()
        dyy_boundary = dict()

        dxy_nonboundary = dict()
        dxy_boundary = dict()

        dyx_nonboundary = dict()
        dyx_boundary = dict()

        for lp_number in dxx.keys():
            if bool(lattice.Points[lp_number].BoundaryName):  # boundary Lattice point
                dxx_boundary[lp_number] = dxx[lp_number]
            else:
                dxx_nonboundary[lp_number] = dxx[lp_number]

        for lp_number in dyy.keys():
            if bool(lattice.Points[lp_number].BoundaryName):  # boundary Lattice point
                dyy_boundary[lp_number] = dyy[lp_number]
            else:
                dyy_nonboundary[lp_number] = dyy[lp_number]

        for lp_number in dxy.keys():
            if bool(lattice.Points[lp_number].BoundaryName):  # boundary Lattice point
                dxy_boundary[lp_number] = dxy[lp_number]
            else:
                dxy_nonboundary[lp_number] = dxy[lp_number]

        for lp_number in dyx.keys():
            if bool(lattice.Points[lp_number].BoundaryName):  # boundary Lattice point
                dyx_boundary[lp_number] = dyx[lp_number]
            else:
                dyx_nonboundary[lp_number] = dyx[lp_number]

        return [
            [dxx_nonboundary, dyy_nonboundary, dxy_nonboundary, dyx_nonboundary],
            [dxx_boundary, dyy_boundary, dxy_boundary, dyx_boundary],
        ]

    def _compute_gradient_coefficients_for_boundary_points_separately(self, lattice):
        """
        :param lattice:
        :return: to dictionaries of gradient coeefficients see _compute_gradient_coefficients
        """

        dx, dy = self.get_gradient_coefficients(h=lattice.CellSize)
        dxx, dyy, dxy, _ = self._compute_second_order_derivative_coefficients(lattice)

        dx_nonboundary = dict()
        dx_boundary = dict()
        dy_nonboundary = dict()
        dy_boundary = dict()
        dxx_nonboundary = dict()
        dxx_boundary = dict()
        dyy_nonboundary = dict()
        dyy_boundary = dict()
        dxy_nonboundary = dict()
        dxy_boundary = dict()

        for lp_number in dx.keys():
            if bool(lattice.Points[lp_number].BoundaryName):  # boundary Lattice point
                dx_boundary[lp_number] = dx[lp_number]
            else:
                dx_nonboundary[lp_number] = dx[lp_number]

        for lp_number in dy.keys():
            if bool(lattice.Points[lp_number].BoundaryName):  # boundary Lattice point
                dy_boundary[lp_number] = dy[lp_number]
            else:
                dy_nonboundary[lp_number] = dy[lp_number]

        for lp_number in dxx.keys():
            if bool(lattice.Points[lp_number].BoundaryName):  # boundary Lattice point
                dxx_boundary[lp_number] = dxx[lp_number]
            else:
                dxx_nonboundary[lp_number] = dxx[lp_number]

        for lp_number in dyy.keys():
            if bool(lattice.Points[lp_number].BoundaryName):  # boundary Lattice point
                dyy_boundary[lp_number] = dyy[lp_number]
            else:
                dyy_nonboundary[lp_number] = dyy[lp_number]

        for lp_number in dxy.keys():
            if bool(lattice.Points[lp_number].BoundaryName):  # boundary Lattice point
                dxy_boundary[lp_number] = dxy[lp_number]
            else:
                dxy_nonboundary[lp_number] = dxy[lp_number]

        return [
            [
                dx_nonboundary,
                dy_nonboundary,
                dxx_nonboundary,
                dyy_nonboundary,
                dxy_nonboundary,
            ],
            [dx_boundary, dy_boundary, dxx_boundary, dyy_boundary, dxy_boundary],
        ]

    def get_point_set_from_gradient_coefficients(self, lattice, *, recompute: bool = False):
        """returns set of points, at which gradient coefficients are defined"""
        dx, dy = self.get_gradient_coefficients(lattice=lattice, recompute=recompute)

        pt_list = [self]
        for c in (dx, dy):
            # for c in coeff:
            pt_list.extend([lattice.Points[idx] for idx in c.keys()])

        # set ensures no multiples
        return set(pt_list)

    def get_gradient_coefficients(self, lattice=None, h=None, recompute=False):
        """
        returns gradient coefficients as a list [dx, dy] and computes them if necessary

         use as:

        dx, dy = self.get_gradient_coefficients(lattice)
        gradient_of_w = [[0, 0], [0, 0]]    # 2x2 matrix
        for i in range(0, len(self.w)):     # dimensions of w i
            for pointID in dx.keys():
                gradient_of_w[i][0] = gradient_of_w[i][0] + lattice.Points[pointID].w[i] * dx[pointID]
            for pointID in dy.keys():
                gradient_of_w[i][1] = gradient_of_w[i][1] + lattice.Points[pointID].w[i] * dy[pointID]

        gradient_of_w[i] contains the derivative of the i-th component of w in x (gradient_of_w[i][0]) and y direction (gradient_of_w[i][1])

        force is used to force a recomputation, e.g. if the lattice topology changes
        :param lattice:
        :return:
        """
        if self.gradient_coefficients and not recompute:
            # coefficients already set
            coefficients = self.gradient_coefficients
        else:
            # coefficients not set or recomputation forced
            coefficients = self._compute_gradient_coefficients(lattice=lattice, h=h)
            self.gradient_coefficients = coefficients
        return coefficients

    def get_gradient_of_w(self, lattice):
        """
        computes the gradient of the primary field
        delegate to get_gradient_of_attribute with field w as attribute
        :param lattice:
        :return:
        """
        return self.get_gradient_of_attribute(lattice, attr="w")

    def get_gradient_of_attribute(self, lattice, attr="w", dim=3):
        """
        computes the gradient of any compatible attribute (as tensor of rank 0, 1 or 2)
        dim sets the shape of the output as ... x dim
        :param lattice, attr, dims:
        :return:
        """

        def parse_values(val):
            """ensure arrays have ndim congruent with rank of tensor"""
            if hasattr(val, "__len__") and len(val) == 1:
                # iterable with 1 item
                return np.asarray(val[0])
            else:
                # float or longer iterable
                return np.asarray(val)

        # get coefficients
        dx, dy = self.get_gradient_coefficients(h=lattice.CellSize)

        field_val_dx = field_val_dy = dict()
        for pt_x in dx:
            field_val_dx[pt_x] = parse_values(getattr(lattice.Points[pt_x], attr))
        for pt_y in dy:
            field_val_dy[pt_y] = parse_values(getattr(lattice.Points[pt_y], attr))

        field = field_val_dx[next(iter(dx))]
        rank = field.ndim
        shape_ = field.shape

        if rank == 0:
            gradient = np.zeros(dim)
            for pointID in dx:
                gradient[0] += field_val_dx[pointID] * dx[pointID]
            for pointID in dy:
                gradient[1] += field_val_dy[pointID] * dy[pointID]

        elif rank == 1:
            (n,) = shape_
            gradient = np.zeros((n, dim))
            for i in range(n):
                for pointID in dx:
                    gradient[i][0] += field_val_dx[pointID][i] * dx[pointID]
                for pointID in dy:
                    gradient[i][1] += field_val_dy[pointID][i] * dy[pointID]

        elif rank == 2:
            n, m = shape_
            gradient = np.zeros((n, m, dim))
            for i in range(n):
                for j in range(m):
                    for pointID in dx:
                        gradient[i][j][0] += field_val_dx[pointID][i][j] * dx[pointID]
                    for pointID in dy:
                        gradient[i][j][1] += field_val_dy[pointID][i][j] * dy[pointID]

        return gradient.T

    def get_divergence_of_attribute(self, lattice, attr="w"):
        """compute divergence of an attribute of at the point
        by taking the double contraction of gradient and identity
        """
        grad = self.get_gradient_of_attribute(lattice, attr=attr)
        n = grad.shape[0]
        div = np.tensordot(grad, np.eye(n))
        return div

    # @abstractmethod
    # def get_future_w(self, computation, point):
    #     """
    #     computes current value of macroscopic field from distribution functions ( depends on interpretation of distribution functions)
    #     :param computation:
    #     :param point:
    #     :return:
    #     """
    #     pass

    def collide(self, computation):
        """
        :param computation: relaxation tim
        :return: update distribution
        """
        for i in range(0, 5):
            self.f[i] = self.f[i] - 1.0 / computation.Parameters["tauh"] * (self.f[i] - self.f_eq[i])

    @abstractmethod
    def initialise_distribution_functions_and_fields(self, computation):
        """
        initializes distribution functions from fields, fields from *.init file, PPData may be written here
        :param computation:
        :return: initial w, (wdot), f, f_temp
        """
        pass

    def stream(self):
        """
        :return: update distribution functions at neighbors after streaming
        """
        pass

    def update_distribution_function_after_streaming(self):
        """
        f_temp necessary to prevent overwriting f
        :return: updates f
        """
        # for j in range(0, len(self.w)):
        for i in range(0, 9):
            self.f[i] = self.f_temp[i]
            self.f_temp[i] = None  # invalidate distributions functions from current time step

    ### boundary conditions

    # @abstractmethod
    # def compute_missing_distribution_functions_according_to_target_value(self, computation, target_value):
    #     """
    #     computes the missing f_temp at boundary sites depending on physical problem and current boundary value
    #     :param computation:
    #     :param target_value:
    #     :return:
    #     """
    #     pass

    @abstractmethod
    def boun(self, computation):
        """
        applies macroscopic bc. use if mesh is regarded as geometry conforming
        computes missing distribution functions for boundary lattice points f_temp
        :param computation:
        :return: computes unknown f_temp at boundary locations from macroscopic bc
        """
        pass

    # @abstractmethod
    # def boun_alt(self, computation):
    #     """
    #     applies macroscopic bc in alternative fashion (momentum balance at cell around point). mesh is considered as non-geometry conforming
    #     :param computation:
    #     :return: computes unknown f_temp at boundary locations from macroscopic bc
    #     """

    #     pass

    ### matrix implementation of boundary conditions

    def set_w_at_aux_points_for_bc(self, computation, dim_of_w=1):
        """
        computes macroscopic field at auxiliary (postprocessing point on boundary) points by polynomial (quadratic or linear) extrapolation Neumann BC, specified value Dirichlet BC
        :param computation:
        :return:
        """

        def compute_w_at_aux_points_for_neumann_bc_precise(point=None, computation=None, key="", component_of_w=0):
            bc_number_at_aux_point = self.BoundaryName[key]

            delta = point.BoundaryClosestPoints[key] - point
            q = (delta.x**2 + delta.y**2) ** 0.5

            FactorA = 4.0 / 3.0 - (q**2) / 3.0 / computation.Parameters["hh"] ** 2
            FactorB = (q**2) / (3.0 * computation.Parameters["hh"] ** 2) - 1.0 / 3.0

            point_i1 = point.BoundaryClosestPoints[key] - computation.Parameters["hh"] * point.BoundaryNormals[key]
            point_i2 = (
                point.BoundaryClosestPoints[key] - 2.0 * computation.Parameters["hh"] * point.BoundaryNormals[key]
            )

            weights_bp_i1 = []
            weights_p_i1 = []
            weights_bp_i2 = []
            weights_p_i2 = []
            if FactorA != 0.0:
                interpolation_factors_i1 = point.BoundaryCoefficients[0]
                for i in range(0, len(interpolation_factors_i1[0])):
                    weights_bp_i1.append(interpolation_factors_i1[1][i] / FactorA)
                for i in range(0, len(interpolation_factors_i1[2])):
                    weights_p_i1.append(interpolation_factors_i1[3][i] / FactorA)

            else:  # interpolation factors might still be different from 0.0  -> need to compute interpolation factors again
                cells = self.find_interpolation_cells_for_neumann2order_bc(
                    boundary_lattice_point=self,
                    computation=computation,
                    point_i1=point_i1,
                    point_i2=point_i2,
                )
                interpolation_factors_i1 = cells[0].interpolate_displacements_alt(search_point=point_i1)
                weights_bp_i1 = interpolation_factors_i1[1]
                weights_p_i1 = interpolation_factors_i1[3]

            if FactorB != 0.0:
                interpolation_factors_i2 = point.BoundaryCoefficients[1]
                for i in range(0, len(interpolation_factors_i2[0])):
                    weights_bp_i2.append(interpolation_factors_i2[1][i] / FactorB)
                for i in range(0, len(interpolation_factors_i2[2])):
                    weights_p_i2.append(interpolation_factors_i2[3][i] / FactorB)
            else:  # interpolation factors might still be different from 0.0  -> need to compute interpolation factors again
                cells = self.find_interpolation_cells_for_neumann2order_bc(
                    boundary_lattice_point=self,
                    computation=computation,
                    point_i1=point_i1,
                    point_i2=point_i2,
                )
                interpolation_factors_i2 = cells[1].interpolate_displacements_alt(search_point=point_i2)
                weights_bp_i2 = interpolation_factors_i2[1]
                weights_p_i2 = interpolation_factors_i2[3]

            wi1 = 0.0
            for i in range(0, len(interpolation_factors_i1[0])):  # interpolation for boundary_points
                tmp_point = computation.Lattice.BoundaryPointsMatrixBC[interpolation_factors_i1[0][i]]
                wi1 = wi1 + tmp_point.w[component_of_w] * weights_bp_i1[i]  # interpolation_factors_i1[1][i]

            for i in range(0, len(interpolation_factors_i1[2])):  # interpolation for boundary_points
                tmp_point = computation.Lattice.Points[interpolation_factors_i1[2][i]]
                wi1 = wi1 + tmp_point.w[component_of_w] * weights_p_i1[i]  # interpolation_factors_i1[3][i]

            wi2 = 0.0
            for i in range(0, len(interpolation_factors_i2[0])):  # interpolation for boundary_points
                tmp_point = computation.Lattice.BoundaryPointsMatrixBC[interpolation_factors_i2[0][i]]
                wi2 = wi2 + tmp_point.w[component_of_w] * weights_bp_i2[i]  # interpolation_factors_i2[1][i]

            for i in range(0, len(interpolation_factors_i2[2])):  # interpolation for boundary_points
                tmp_point = computation.Lattice.Points[interpolation_factors_i2[2][i]]
                wi2 = wi2 + tmp_point.w[component_of_w] * weights_p_i2[i]  # interpolation_factors_i2[3][i]

            current_dwdn = computation.BoundaryConditions[bc_number_at_aux_point].CurrentValue[component_of_w]

            return 4.0 / 3.0 * wi1 - 1.0 / 3.0 * wi2 + 2.0 / 3.0 * computation.Parameters["hh"] * current_dwdn

        if self.BoundaryName:
            for tmp_key in self.BoundaryClosestPoints.keys():
                bc_number_at_aux_point = self.BoundaryName[tmp_key]
                if computation.BoundaryConditions[bc_number_at_aux_point].BCType in ["Neumann2Order"]:
                    if computation.Flags["precise_key"]:
                        for k in range(0, dim_of_w):
                            self.BoundaryClosestPoints[tmp_key].w[k] = compute_w_at_aux_points_for_neumann_bc_precise(
                                point=self,
                                computation=computation,
                                key=tmp_key,
                                component_of_w=k,
                            )
                    else:
                        d = self.BoundaryClosestPoints[tmp_key] - self
                        abs_d = (d.x**2.0 + d.y**2.0) ** 0.5
                        for k in range(0, dim_of_w):
                            self.BoundaryClosestPoints[tmp_key].w[k] = (
                                self.w[k]
                                + abs_d * computation.BoundaryConditions[bc_number_at_aux_point].CurrentValue[k]
                            )
                elif computation.BoundaryConditions[bc_number_at_aux_point].BCType in ["Dirichlet"]:
                    for k in range(0, dim_of_w):  # TODO
                        self.BoundaryClosestPoints[tmp_key].w[k] = computation.BoundaryConditions[
                            self.BoundaryName[tmp_key]
                        ].CurrentValue[k]

    def setPPDataAtAuxpoint(self, name_of_field=" "):
        """
        Sets the PPData field with name "name_of_field" at auxpoints

        - currently the value is just set to the value at the associated lattice point (i.e. the field is assumed to be constant)
        - TODO: Use some kind of extrapolation
        :param computation:
        :param name:
        :return:
        """
        if self.BoundaryName:  # boundary point
            for key in self.BoundaryClosestPoints.keys():
                self.BoundaryClosestPoints[key].PPData[name_of_field] = self.PPData[name_of_field]

    def compute_matrix_coefficients_rhs_coefficients_for_boundary_conditions_beta(self, computation):
        """
        returns coefficients for all boundary points and rhs scaling factor in boundary equation
        :param computation:
        :return:
        """

        if bool(self.BoundaryName):
            valid_boundary_name = min(self.BoundaryName.values())  # determines which boundary is to be fulfilled

            key_in_dir_of_valid_boundary = [
                key for key in self.BoundaryName.keys() if self.BoundaryName[key] == valid_boundary_name
            ]

            key = key_in_dir_of_valid_boundary[0]
            delta = self.BoundaryClosestPoints[key] - self
            q = (delta.x**2 + delta.y**2) ** 0.5
            h = computation.Parameters["hh"]

            # if computation.BoundaryConditions[valid_boundary_name].BCType == 'Neumann2Order':
            if "Neumann" in computation.BoundaryConditions[valid_boundary_name].BCType:
                FactorA = 4.0 / 3.0 - (q**2) / 3.0 / computation.Parameters["hh"] ** 2
                FactorB = (q**2) / (3.0 * computation.Parameters["hh"] ** 2) - 1.0 / 3.0
                FactorC = (
                    1.0 / (3.0 * computation.Parameters["hh"]) * q**2 - q + 2.0 * computation.Parameters["hh"] / 3.0
                )

            elif "Dirichlet" in computation.BoundaryConditions[valid_boundary_name].BCType:
                FactorA = -(q**2) / h**2 + 2.0 * q / h
                FactorB = 0.5 * q**2 / h**2 - 0.5 * q / h
                FactorC = 0.5 * q**2 / h**2 - 3.0 / 2.0 * q / h + 1.0
            else:
                print(
                    "Class LatticePointD2 Method compute_matrix_coefficients_rhs_coefficients Error: Boundary type not implemented"
                )

            key = key_in_dir_of_valid_boundary[0]
            point_i1 = self.BoundaryClosestPoints[key] - computation.Parameters["hh"] * self.BoundaryNormals[key]
            point_i2 = self.BoundaryClosestPoints[key] - 2.0 * computation.Parameters["hh"] * self.BoundaryNormals[key]

            cells = self.find_interpolation_cells_for_neumann2order_bc(
                boundary_lattice_point=self,
                computation=computation,
                point_i1=point_i1,
                point_i2=point_i2,
            )

            cell = cells[0]

            if cell.Points[0].x is not None:  # cell created
                # if cell.test_if_point_is_in_box(point_i1, tol=computation.Parameters['hh'] / 1.0e15):
                coefficent_array_i1 = cell.interpolate_displacements_alt(
                    search_point=point_i1
                )  # interpolate displacement at point1
                for i in range(0, len(coefficent_array_i1[1])):
                    coefficent_array_i1[1][i] = coefficent_array_i1[1][i] * FactorA
                for i in range(0, len(coefficent_array_i1[3])):
                    coefficent_array_i1[3][i] = coefficent_array_i1[3][i] * FactorA
            else:
                print("Class LatticePointD2 Method boun_non_geo_conforming Error! Could not create cell")

            cell = cells[1]

            if cell.Points[0].x is not None:  # cell created
                coefficent_array_i2 = cell.interpolate_displacements_alt(search_point=point_i2)  # at point_2
                for i in range(0, len(coefficent_array_i2[1])):
                    coefficent_array_i2[1][i] = coefficent_array_i2[1][i] * FactorB
                for i in range(0, len(coefficent_array_i2[3])):
                    coefficent_array_i2[3][i] = coefficent_array_i2[3][i] * FactorB
            else:
                print("Class LatticePointD2 Method boun_non_geo_conforming Error! Could not create cell")
            self.BoundaryCoefficients = list([coefficent_array_i1, coefficent_array_i2, FactorC])
        else:
            print("Class LatticePointD2 Method: boun Error: BCType not implemented!")

    def assemble_boundary_matrix_entries_at_point(self, computation, dim_of_w=1):
        """
        computes matrix coefficients and writes them to computation.Lattice.BCMatrix[ID][...]
        :param computation:
        :return:
        """
        if self.BoundaryName:
            interpolation_factors_i1 = self.BoundaryCoefficients[0]
            interpolation_factors_i2 = self.BoundaryCoefficients[1]

            for k in range(0, dim_of_w):
                computation.Lattice.BCMatrix[k][self.BoundaryPointListID][self.BoundaryPointListID] = 1.0

            for i in range(0, len(interpolation_factors_i1[0])):
                entry_tmp = (
                    computation.Lattice.BCMatrix[k][self.BoundaryPointListID][interpolation_factors_i1[0][i]]
                    - interpolation_factors_i1[1][i]
                )
                for k in range(0, dim_of_w):
                    computation.Lattice.BCMatrix[k][self.BoundaryPointListID][interpolation_factors_i1[0][i]] = (
                        entry_tmp
                    )

            if len(
                interpolation_factors_i2
            ):  # values for a second interpolated point inside the domain exist (Neumann boundary)
                for i in range(0, len(interpolation_factors_i2[0])):
                    entry_tmp = (
                        computation.Lattice.BCMatrix[k][self.BoundaryPointListID][interpolation_factors_i2[0][i]]
                        - interpolation_factors_i2[1][i]
                    )
                    for k in range(0, dim_of_w):
                        computation.Lattice.BCMatrix[k][self.BoundaryPointListID][interpolation_factors_i2[0][i]] = (
                            entry_tmp
                        )

    def assemble_boundary_rhs_entries_at_point(self, computation, dim_of_w=1):
        """
         assembles rhs entries in bc lgs for current bc and fields (needs to be done in every time step)
        :param computation:
        :return:
        """
        if bool(self.BoundaryName):
            valid_boundary_name = min(self.BoundaryName.values())  # determines at which boundary BC is to be fulfilled

            interpolation_factors_i1 = self.BoundaryCoefficients[0]
            interpolation_factors_i2 = self.BoundaryCoefficients[1]
            FactorC = self.BoundaryCoefficients[2]

            for i in range(0, len(interpolation_factors_i1[2])):
                tmp_point = computation.Lattice.Points[interpolation_factors_i1[2][i]]

                tmp_future_w = tmp_point.get_future_w(computation=computation, point=tmp_point)

                for k in range(0, dim_of_w):
                    computation.Lattice.BCRHS[k][self.BoundaryPointListID] = (
                        computation.Lattice.BCRHS[k][self.BoundaryPointListID]
                        + interpolation_factors_i1[3][i] * tmp_future_w[k]
                    )  # FactorA * tmp_future_w

            if len(
                interpolation_factors_i2
            ):  # values for a second interpolated point inside the domain exist (Neumann boundary)
                for i in range(0, len(interpolation_factors_i2[2])):
                    tmp_point = computation.Lattice.Points[interpolation_factors_i2[2][i]]
                    tmp_point_future_w = tmp_point.get_future_w(computation=computation, point=tmp_point)
                    for k in range(0, dim_of_w):
                        computation.Lattice.BCRHS[k][self.BoundaryPointListID] = (
                            computation.Lattice.BCRHS[k][self.BoundaryPointListID]
                            + interpolation_factors_i2[3][i] * tmp_point_future_w[k]
                        )  # FactorB * tmp_future_w

            for k in range(0, dim_of_w):
                computation.Lattice.BCRHS[k][self.BoundaryPointListID] = (
                    computation.Lattice.BCRHS[k][self.BoundaryPointListID]
                    + FactorC * computation.BoundaryConditions[valid_boundary_name].CurrentValue[k]
                )

    def find_interpolation_cells_for_neumann2order_bc(
        self, computation, boundary_lattice_point=None, point_i1=None, point_i2=None
    ):
        """
        returns to TriangleCell objects for interpolation of macroscopic fields for neumann bc
        :param computation:
        :param boundary_lattice_point:
        :param point_i1:
        :param point_i2:
        :return:
        """

        # returns two cell objects for interpolation of the internal points for neumann bc (point_i1, point_i2)
        def find_starting_point_for_cell_creation_for_interpolation(point=None, computation=None, point_i2=None):
            xs_x_index = (point_i2.x - point.x) / computation.Parameters["hh"]
            xs_y_index = (point_i2.y - point.y) / computation.Parameters["hh"]
            if xs_x_index >= 1.0:
                direction_xs_x = "east"
            elif xs_x_index <= -1.0:
                direction_xs_x = "west"
            else:
                direction_xs_x = "same"

            if xs_y_index >= 1.0:
                direction_xs_y = "north"
            elif xs_y_index <= -1.0:
                direction_xs_y = "south"
            else:
                direction_xs_y = "same"

            xs = None
            if direction_xs_y == "north" and direction_xs_x == "east":
                if "x+" in point.Neighbors.keys():
                    if "y+" in point.Neighbors["x+"].Neighbors.keys():
                        xs = point.Neighbors["x+"].Neighbors["y+"]
                elif "y+" in point.Neighbors.keys():
                    if "x+" in point.Neighbors["y+"].Neighbors.keys():
                        xs = point.Neighbors["y+"].Neighbors["x+"]
            elif direction_xs_y == "north" and direction_xs_x == "west":
                if "x-" in point.Neighbors.keys():
                    if "y+" in point.Neighbors["x-"].Neighbors.keys():
                        xs = point.Neighbors["x-"].Neighbors["y+"]
                elif "y+" in point.Neighbors.keys():
                    if "x-" in point.Neighbors["y+"].Neighbors.keys():
                        xs = point.Neighbors["y+"].Neighbors["x-"]
            elif direction_xs_y == "north" and direction_xs_x == "same":
                if "y+" in point.Neighbors.keys():
                    xs = point.Neighbors["y+"]

            elif direction_xs_y == "south" and direction_xs_x == "east":
                if "x+" in point.Neighbors.keys():
                    if "y-" in point.Neighbors["x+"].Neighbors.keys():
                        xs = point.Neighbors["x+"].Neighbors["y-"]
                elif "y-" in point.Neighbors.keys():
                    if "x+" in point.Neighbors["y-"].Neighbors.keys():
                        xs = point.Neighbors["y-"].Neighbors["x+"]
            elif direction_xs_y == "south" and direction_xs_x == "west":
                if "x-" in point.Neighbors.keys():
                    if "y-" in point.Neighbors["x-"].Neighbors.keys():
                        xs = point.Neighbors["x-"].Neighbors["y-"]
                elif "y-" in point.Neighbors.keys():
                    if "x-" in point.Neighbors["y-"].Neighbors.keys():
                        xs = point.Neighbors["y-"].Neighbors["x-"]
            elif direction_xs_y == "south" and direction_xs_x == "same":
                if "y-" in point.Neighbors.keys():
                    xs = point.Neighbors["y-"]

            elif direction_xs_y == "same" and direction_xs_x == "east":
                if "x+" in point.Neighbors.keys():
                    xs = point.Neighbors["x+"]
            elif direction_xs_y == "same" and direction_xs_x == "west":
                if "x-" in point.Neighbors.keys():
                    xs = point.Neighbors["x-"]
            elif direction_xs_y == "same" and direction_xs_x == "same":
                xs = point

            return xs

        # point_i1
        xs = boundary_lattice_point
        dx = point_i1 - xs

        if dx.x > 0.0:
            direction_part2 = list(["east"])
        elif dx.x < 0.0:
            direction_part2 = list(["west"])
        else:
            direction_part2 = list(["east", "west"])  # point can be in both cells
        if dx.y > 0.0:
            direction_part1 = list(["north"])  # point can be in both cells
        elif dx.y < 0.0:
            direction_part1 = list(["south"])
        else:
            direction_part1 = list(["north", "south"])
        direction = direction_part1[0] + direction_part2[0]

        cell = None
        if True:
            if (len(direction_part1) == 1) & (len(direction_part2) == 1):  # cell is unambigous
                possible_directions = list(
                    [
                        direction_part1[0] + direction_part2[0] + direction_part2[0],
                        direction_part1[0] + direction_part1[0] + direction_part2[0],
                    ]
                )
            elif len(direction_part1) > 1:
                possible_directions = list(
                    [
                        direction_part1[0] + direction_part2[0] + direction_part2[0],
                        direction_part1[1] + direction_part2[0] + direction_part2[0],
                    ]
                )
            elif len(direction_part2) > 1:
                possible_directions = list(
                    [
                        direction_part1[0] + direction_part1[0] + direction_part2[0],
                        direction_part1[0] + direction_part1[0] + direction_part2[1],
                    ]
                )

            # Modification for D2Q9 - If only a diagonal neighbor is missing (i_1 might be closer to boundary than corresponding lattice point)
            # a valid interpolation cell must be found
            if computation.Lattice.LatticeType in [
                "D2Q9_WaveEquation_Chopard",
                "D2Q9_NavierEquation_Chopard",
            ]:
                if "southsouthwest" in possible_directions or "southwestwest" in possible_directions:
                    possible_directions.append("southwest")
                elif "southsoutheast" in possible_directions or "southeasteast" in possible_directions:
                    possible_directions.append("southeast")
                elif "northnorthwest" in possible_directions or "northwestwest" in possible_directions:
                    possible_directions.append("northwest")
                elif "northnortheast" in possible_directions or "northeasteast" in possible_directions:
                    possible_directions.append("northeast")

            for direction in possible_directions:
                cell = CellTriangleD2_Module.CellTriangleD2()
                cell.create_cell(start_point=xs, direction=direction)
                if cell.Points[0].x is not None and cell.test_if_point_is_in_box(
                    test_point=point_i1, tol=computation.Parameters["hh"] / 1.0e8
                ):
                    break
        if cell.Points[0].x is None:
            print(
                "Class LatticePointD2/ Method find_interpolation_cells_for neumann2order_bc Error! Cell for point_i1 could not be created!"
            )
        if cell.test_if_point_is_in_box(point_i1, tol=computation.Parameters["hh"] / 1.0e8):
            cell1 = cell
        else:
            print(
                "Class LatticePointD2 Method find_interpolation_cells_for_neumann2order_bc Error! point_i1 is not in cell"
            )

        # point_i2
        xs = find_starting_point_for_cell_creation_for_interpolation(
            point=self, computation=computation, point_i2=point_i2
        )

        if xs is None:
            print(
                "Class {0} Method find_interpolation_cells_for_neumann2order_bc Error: Cannot find lattice point for interpolation at point2".format(
                    type(self).__name__
                )
            )

        dx = point_i2 - xs
        if dx.x > 0.0:
            direction_part2 = list(["east"])
        elif dx.x < 0.0:
            direction_part2 = list(["west"])
        else:
            direction_part2 = list(["east", "west"])  # can either create west or east cell
        if dx.y > 0.0:
            direction_part1 = list(["north"])
        elif dx.y < 0.0:
            direction_part1 = list(["south"])
        else:
            direction_part1 = list(["north", "south"])  # can either create north or south cell
        direction = direction_part1[0] + direction_part2[0]

        cell = None
        if True:  # if not successful try to create triangle cell
            if (len(direction_part1) == 1) & (len(direction_part2) == 1):  # cell is unambigous
                possible_directions = list(
                    [
                        direction_part1[0] + direction_part2[0] + direction_part2[0],
                        direction_part1[0] + direction_part1[0] + direction_part2[0],
                    ]
                )
            elif len(direction_part1) > 1:
                possible_directions = list(
                    [
                        direction_part1[0] + direction_part2[0] + direction_part2[0],
                        direction_part1[1] + direction_part2[0] + direction_part2[0],
                    ]
                )
            elif len(direction_part2) > 1:
                possible_directions = list(
                    [
                        direction_part1[0] + direction_part1[0] + direction_part2[0],
                        direction_part1[0] + direction_part1[0] + direction_part2[1],
                    ]
                )
            for direction in possible_directions:
                cell = CellTriangleD2_Module.CellTriangleD2()
                cell.create_cell(start_point=xs, direction=direction)
                if cell.Points[0].x is not None and cell.test_if_point_is_in_box(
                    point_i2, tol=computation.Parameters["hh"] / 1.0e8
                ):
                    break
        if cell.Points[0].x is None:
            print(
                "Class LatticePointD2/ Method find_interpolation_cells_for neumann2order_bc Error! Cell for point_i2 could not be created!"
            )
        if cell.test_if_point_is_in_box(point_i2, tol=computation.Parameters["hh"] / 1.0e8):
            cell2 = cell
        else:
            print(
                "Class LatticePointD2 Method find_interpolation_cells_for_neumann2order_bc Error! point_i2 is not in cell"
            )
        return [cell1, cell2]

    ### Output/Postprocessing
    def write_position_2_vtk(self, computation):
        return (
            "{0:16.8e} ".format(self.x * computation.Parameters["L"])
            + "{0:16.8e} ".format(self.y * computation.Parameters["L"])
            + "{0:16.8e} \n".format(self.z * computation.Parameters["L"])
        )

    def write_position_2_vtu(self, computation):
        return self.write_position_2_vtk(computation)

    def write_data_2_vtk(self, computation):
        s = ""
        for wcomp in self.w:
            s = s + "{0:16.8e} ".format(wcomp * computation.Parameters["wr"])
        return s

    def write_data_2_vtu(self, computation):
        return self.write_data_2_vtk(computation)

    def write_PPData_2_vtk(self, key):
        if hasattr(self.PPData[key], "__len__"):  # if PPData[key] is a list
            tmp_string = ""
            for i in range(0, len(self.PPData[key])):
                tmp_string2 = "{0" + ":18.8e} "
                value = self.PPData[key][i]
                if value is None:
                    value = -999
                tmp_string = tmp_string + tmp_string2.format(value)
        else:
            value = self.PPData[key]
            if value is None:
                value = -999
            tmp_string = "{0:18.8e} ".format(value)
        return tmp_string + "\n"

    def write_cell_connectivity_2_vtk(self):
        return "1 {0:6d} \n".format(self.ID)

    def write_cell_type_2_vtk(self):
        return "1\n"

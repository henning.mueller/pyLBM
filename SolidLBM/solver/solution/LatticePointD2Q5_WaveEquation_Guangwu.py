import copy
import numpy as np

import SolidLBM.solver.solution.LatticePointD2Q5_abstract as LatticePointD2Q5_abstract_Module
from SolidLBM.solver.solution.LatticePointD2_mechanics import (
    LatticePointD2_mechanics_antiplaneshear,
)


class LatticePointD2Q5_WaveEquation_Guangwu(
    LatticePointD2Q5_abstract_Module.LatticePointD2Q5_abstract,
    LatticePointD2_mechanics_antiplaneshear,
):
    @classmethod
    def compute_dependent_parameters(cls, cellSize, computationParameters):
        outComputationParameters = copy.deepcopy(computationParameters)
        outComputationParameters["cs"] = np.sqrt(outComputationParameters["mue"] / outComputationParameters["rho"])
        outComputationParameters["h"] = cellSize

        outComputationParameters["hh"] = outComputationParameters["h"] / outComputationParameters["L"]
        outComputationParameters["csh"] = outComputationParameters["cs"] / outComputationParameters["cs"]
        outComputationParameters["ch"] = 7.5 * outComputationParameters["csh"]
        outComputationParameters["dth"] = outComputationParameters["hh"] / outComputationParameters["ch"]

        outComputationParameters["la"] = (
            outComputationParameters["csh"] ** 2.0
            / outComputationParameters["dth"]
            / (outComputationParameters["tauh"] - 0.5)
        )

        return outComputationParameters

    def __init__(self, x=0.0, y=0.0, z=0.0, number=None):
        super(LatticePointD2Q5_WaveEquation_Guangwu, self).__init__(x, y, z, number=number)
        self.w = [None] * 1  # displacement of the last n time steps (if needed for time integration)
        self.wdot = [None] * 1  # velocity of the last n time steps
        self.wdot_prev = [0] * 1  # velocity of the last n time steps

        self.u = self.u_dot = self.u_dot_prev = None  # np.empty(3)

        self.imp = None  # eshelby tensor (or impenergy)

    def _set_tensors(self):
        """Sets u, u_dot, u_dot_prev as vectors (NumPy-Arrays) with 3 components"""
        self.u = np.array((0, 0, self.w[0]))
        self.u_dot = np.array((0, 0, self.wdot[0]))
        self.u_dot_prev = np.array((0, 0, self.wdot_prev[0]))

    def disp_to_array(self, *args, **kwargs):
        return np.array((0, 0, self.w[0]))

    def compute_equilibrium_distribution(self, computation):
        w_dot = sum(self.f)
        self.f_eq[0] = w_dot - (computation.Parameters["D"] * computation.Parameters["la"] * self.w[0]) / (
            computation.Parameters["ch"] ** 2
        )
        tmp_value = (computation.Parameters["la"] * computation.Parameters["D"] * self.w[0]) / (
            computation.Parameters["ch"] ** 2 * computation.Parameters["b"]
        )
        for i in range(1, 5):
            self.f_eq[i] = tmp_value

    def stream_alt_bc(self, computation):
        # only used with alternative (cell) boundary conditions
        self.f_temp[0] = self.f[0]
        if self.VolumeSurfaceMeasure:
            v_self = self.VolumeSurfaceMeasure[0]
        else:
            v_self = computation.Lattice.CellSize * computation.Lattice.CellSize

        n = 1
        if not self.BoundaryName:  # interior nodes
            for n_key in self.neighbor_keys:
                if self.Neighbors[n_key].VolumeSurfaceMeasure:
                    v_neighbor = self.Neighbors[n_key].VolumeSurfaceMeasure[0]
                else:
                    v_neighbor = computation.Lattice.CellSize * computation.Lattice.CellSize
                self.Neighbors[n_key].f_temp[n] = self.f[n] * v_self / v_neighbor
                n += 1
        else:  # boundary nodes only stream to existing neighbors
            for n_key in self.neighbor_keys:
                if n_key in self.Neighbors.keys():
                    if self.Neighbors[n_key].VolumeSurfaceMeasure:
                        v_neighbor = self.Neighbors[n_key].VolumeSurfaceMeasure[0]
                    else:
                        v_neighbor = computation.Lattice.CellSize * computation.Lattice.CellSize

                    self.Neighbors[n_key].f_temp[n] = self.f[n] * v_self / v_neighbor  # v_self/v_neighbor
                n += 1

    def boun(self, computation):
        # macroscopic mesh conforming
        if bool(self.BoundaryName):
            valid_boundary_name = min(
                self.BoundaryName.values()
            )  # determines which boundary condition is to be fulfilled

            neighbor_keys = LatticePointD2Q5_abstract_Module.LatticePointD2Q5_abstract.neighbor_keys
            missing_f = []
            for key in self.neighbor_keys:
                if key not in self.Neighbors.keys():  # distribution function is missing
                    missing_f.append(
                        self.stream_keys[self.neighbor_keys.index(key)]
                    )  # save index of distribution function that needs to be determined by boundary conditions
            if computation.BoundaryConditions[valid_boundary_name].BCType == "Dirichlet":
                tmp_value = computation.BoundaryConditions[valid_boundary_name].CurrentValue[0]
            elif computation.BoundaryConditions[valid_boundary_name].BCType == "Neumann":
                key_in_dir_of_valid_boundary = [
                    key for key in self.BoundaryName.keys() if self.BoundaryName[key] == valid_boundary_name
                ]  # keys of all links that intersect valid_boundary_name
                key = self.inv_keys[
                    neighbor_keys.index(key_in_dir_of_valid_boundary[0])
                ]  # key of neighbor in the interior of the domain that is used to compute gradient according to bc
                if key in self.Neighbors.keys():
                    w_temp_neighbor_tpdt = 0.0
                    for f in self.Neighbors[key].f_temp:
                        if f is not None:  # TODO boundary values are used
                            w_temp_neighbor_tpdt = w_temp_neighbor_tpdt + f * computation.Parameters["dth"]
                    w_temp_neighbor_tpdt = w_temp_neighbor_tpdt + self.Neighbors[key].w[0]

                    tmp_value = (
                        computation.BoundaryConditions[valid_boundary_name].CurrentValue[0]
                        * computation.Parameters["hh"]
                        + w_temp_neighbor_tpdt
                    )  #
                else:
                    print(
                        "Class LatticePointD2Q5 Method: boun Error: Cannot Apply BC! Interior neighbour(s) missing for Point at ({0:16.8e},{1:16.8e})!".format(
                            self.x, self.y
                        )
                    )
            elif computation.BoundaryConditions[valid_boundary_name].BCType == "Neumann2Order":
                key_in_dir_of_valid_boundary = [
                    key for key in self.BoundaryName.keys() if self.BoundaryName[key] == valid_boundary_name
                ]
                key = self.inv_keys[
                    neighbor_keys.index(key_in_dir_of_valid_boundary[0])
                ]  # key of neighbor in the interior of the domain that is used to compute gradient according to bc
                if key in self.Neighbors.keys() and key in self.Neighbors[key].Neighbors.keys():
                    w_temp_neighbor_tpdt = self.Neighbors[key].w[0]
                    for f in self.Neighbors[key].f_temp:
                        if f is not None:  # TODO Boundary values are used
                            w_temp_neighbor_tpdt = w_temp_neighbor_tpdt + f * computation.Parameters["dth"]

                    w_temp_neighbor_neighbor_tpdt = self.Neighbors[key].Neighbors[key].w[0]
                    for f in self.Neighbors[key].Neighbors[key].f_temp:
                        if f is not None:
                            w_temp_neighbor_neighbor_tpdt = (
                                w_temp_neighbor_neighbor_tpdt + f * computation.Parameters["dth"]
                            )

                    # w_temp_neighbor_tpdt = sum(self.Neighbors[key].f_temp) * computation.Parameters['dth'] + \
                    #                       self.Neighbors[key].w[0]
                    # w_temp_neighbor_neighbor_tpdt = sum(self.Neighbors[key].Neighbors[key].f_temp) * \
                    #                                computation.Parameters['dth'] + \
                    #                                self.Neighbors[key].Neighbors[key].w[0]
                    tmp_value = (
                        4.0 / 3.0 * w_temp_neighbor_tpdt
                        - 1.0 / 3.0 * w_temp_neighbor_neighbor_tpdt
                        + 2.0
                        / 3.0
                        * computation.BoundaryConditions[valid_boundary_name].CurrentValue[0]
                        * computation.Parameters["hh"]
                    )
                else:
                    print(
                        "Class LatticePointD2Q5 Method: boun Error: Cannot Apply BC! Interior neighbour(s) missing for Point at ({0:16.8e},{1:16.8e})!".format(
                            self.x, self.y
                        )
                    )
            else:
                print("Class LatticePointD2Q5 Method: boun Error: BCType not implemented!")
            f_sum = 0.0
            if len(missing_f) == 1:
                for i in range(0, 5):
                    if i != missing_f[0]:
                        f_sum = f_sum + self.f_temp[i]
            elif len(missing_f) == 2:
                for i in range(0, 5):
                    if i != missing_f[0] and i != missing_f[1]:
                        f_sum = f_sum + self.f_temp[i]
            elif len(missing_f) == 3:
                for i in range(0, 5):
                    if i not in (missing_f[0], missing_f[1], missing_f[2]):
                        f_sum = f_sum + self.f_temp[i]
            else:
                raise NotImplementedError(
                    "Apply Boundary Condition not implemented for lattice points with more than three out of boundary neighbours!"
                )
            for f_miss in missing_f:
                self.f_temp[f_miss] = (
                    1.0 / len(missing_f) * ((tmp_value - self.w[0]) / computation.Parameters["dth"] - f_sum)
                )

    def boun_alt(self, computation):
        # only used with alternative (cell) boundary conditions
        # local Guangwu implementation
        def compute_missing_distribution_functions_according_to_target_value(point, target_value):
            missing_f = []
            for key in self.neighbor_keys:  # TODO does not need to be done in every time step
                if key not in point.Neighbors.keys():  # distribution function is missing
                    missing_f.append(
                        self.stream_keys[self.neighbor_keys.index(key)]
                    )  # save index of distribution function that needs to be determined by boundary conditions
            no_missing_f = len(missing_f)

            for f_miss in missing_f:
                point.f_temp[f_miss] = 1.0 / no_missing_f * (target_value)

        # TODO check for boundary point should appear before calling
        if self.BoundaryName:
            boundary_names = self.VolumeSurfaceMeasure[1][0]
            boundary_surface_measure = self.VolumeSurfaceMeasure[1][1]
            volume = self.VolumeSurfaceMeasure[0]
            current_traction_at_boundaries = []

            BCType = "Neumann2Order"  # so far implementation is only valid for Neumann BC
            for boundary_name in boundary_names:
                if (
                    computation.BoundaryConditions[boundary_name].BCType != "Neumann2Order"
                    and computation.BoundaryConditions[boundary_name].BCType != "Neumann"
                ):
                    BCType = ""
                    break

            if BCType == "Neumann2Order":
                for boundary_name in boundary_names:
                    current_traction_at_boundary_name = computation.BoundaryConditions[boundary_name].CurrentValue[0]
                    current_traction_at_boundaries.append(current_traction_at_boundary_name)

                total_traction_force = 0.0
                for i in range(0, len(boundary_surface_measure)):
                    total_traction_force = (
                        total_traction_force + current_traction_at_boundaries[i] * boundary_surface_measure[i]
                    )

                w_dot_old = sum(self.f)
                # w_dot_old = 0.0
                # dot_w_old = sum(self.f)

                sum_fi_t_known = 0.0  # self.f[0]
                i = 0
                for key in self.neighbor_keys:
                    i = i + 1
                    if key in self.Neighbors.keys():
                        sum_fi_t_known = sum_fi_t_known + self.f[i]

                target = (
                    -self.f_temp[0]
                    + w_dot_old
                    - sum_fi_t_known
                    + total_traction_force / volume * computation.Parameters["dth"]
                )

                compute_missing_distribution_functions_according_to_target_value(point=self, target_value=target)
            else:
                print("Class LatticePointD2Q5 Method boun_alt error: Boundary condition type is not implemented")

    def compute_missing_distribution_functions_according_to_target_value(self, computation, in_target_value):
        target_value = in_target_value[0]  # target value is potentially multi dimensional
        missing_f = []
        for key in self.neighbor_keys:  # does not need to be done in every time step TODO
            if key not in self.Neighbors.keys():  # distribution function is missing
                missing_f.append(
                    self.stream_keys[self.neighbor_keys.index(key)]
                )  # save index of distribution function that needs to be determined by boundary conditions
        no_missing_f = len(missing_f)
        f_sum = 0.0
        if len(missing_f) == 1:
            for i in range(0, 5):
                if i != missing_f[0]:
                    f_sum = f_sum + self.f_temp[i]
        elif len(missing_f) == 2:
            for i in range(0, 5):
                if i != missing_f[0] and i != missing_f[1]:
                    f_sum = f_sum + self.f_temp[i]
        elif len(missing_f) == 3:
            for i in range(0, 5):
                if i not in (missing_f[0], missing_f[1], missing_f[2]):
                    f_sum = f_sum + self.f_temp[i]
                # no_missing_f = no_missing_f/2.0
        else:
            print(
                "Error: Apply Boundary Condition not implemented for lattice points with more than three out of boundary neighbours!"
            )
        for f_miss in missing_f:
            self.f_temp[f_miss] = (
                1.0 / no_missing_f * ((target_value - self.w[0]) / computation.Parameters["dth"] - f_sum)
            )

    def initialise_distribution_functions_and_fields(self, computation):
        if computation.Lattice.InitialFieldsW:
            w_dot = []
            w = []
            for index in range(0, len(computation.Lattice.InitialFieldsWdot[self.ID])):
                # w_dot = []
                # w = []
                w_dot.append(
                    computation.Lattice.InitialFieldsWdot[self.ID][index]
                    / (computation.Parameters["cs"] * computation.Parameters["wr"])
                    * computation.Parameters["L"]
                )
                w.append(computation.Lattice.InitialFieldsW[self.ID][index] / computation.Parameters["wr"])  # ...
        else:
            w_dot = list([0.0])
            w = list([0.0])
        self.w = w
        self.wdot = w_dot
        self.wdot_prev = self.wdot
        # self.PPData['w'] = self.w
        self.f[0] = w_dot[0] - 2.0 * computation.Parameters["la"] * w[0] / (computation.Parameters["ch"] ** 2)
        self.f_temp[0] = self.f[0]
        tmp_value = computation.Parameters["la"] * w[0] / (2.0 * computation.Parameters["ch"] ** 2)
        for i in range(1, 5):
            self.f[i] = tmp_value
            self.f_temp[i] = tmp_value

    def compute_w(self, computation):
        #   if len(self.BoundaryName) == 0:
        w_dot = []
        w_dot.append(sum(self.f))
        #      self.PPData['w'] = self.w
        self.w[0] = self.w[0] + w_dot[0] * computation.Parameters["dth"]

        self.wdot_prev = self.wdot
        self.wdot = w_dot

    def compute_eshelby(self, lattice, rho=1.0, mu=1.0):
        coeff = self.get_gradient_coefficients(h=lattice.CellSize)
        pt_list = []
        for c in coeff:
            pt_list.extend([lattice.Points[idx] for idx in c.keys()])
        for pt in set(pt_list):  # TODO : needs refinement
            pt._set_tensors()

        epsilon, sigma = self.compute_strain_and_stress_tensor(lattice, mu=mu)
        psi_e = np.tensordot(sigma, epsilon) / 2
        kin_e = rho * np.dot(self.u_dot, self.u_dot) / 2
        grad_u = self.get_gradient_of_attribute(lattice, attr="u")
        self.imp = (psi_e + kin_e) * np.eye(3) - grad_u.T * sigma

    def compute_strain_and_stress_tensor(self, lattice, mu=1.0):
        """Computes the strain tensor from u and
        the stress tensor for antiplane shear
        """
        grad_u = self.get_gradient_of_attribute(lattice, attr="u")
        epsilon = (grad_u + grad_u.T) / 2

        c = np.zeros((3, 3))
        c[0, 2] = c[1, 2] = c[2, 0] = c[2, 1] = 2 * mu
        sigma = c * epsilon

        return epsilon, sigma

    def get_future_w(self, computation, point):
        # TODO
        w_dot = []
        w_dot.append(sum(point.f_temp))

        w_tmp = []
        w_tmp.append(point.w[0] + w_dot[0] * computation.Parameters["dth"])
        return w_tmp

    def write_position_2_vtk(self, computation):
        return (
            "{0:16.8e} ".format(self.x * computation.Parameters["L"])
            + "{0:16.8e} ".format(self.y * computation.Parameters["L"])
            + "{0:16.8e} \n".format(self.z * computation.Parameters["L"])
        )

    def write_position_2_vtu(self, computation):
        return (
            "{:16.8f} ".format(self.x * computation.Parameters["L"])
            + "{:16.8f} ".format(self.y * computation.Parameters["L"])
            + "{:16.8f} \n".format(self.z * computation.Parameters["L"])
        )

    def write_wdot_2_vtu(self, computation):
        s = ""
        for wdotcomp in self.wdot:
            s = s + "{:16.8f} ".format(wdotcomp * computation.Parameters["cs"])
        return s + "\n"

import numpy as np
from enum import Enum


class BoundaryCondition(object):
    """
    Represents a boundary condition.
    """

    #
    class BCImplementation(Enum):
        MacroscopicNonLatticeConforming = 1  # valid for wave equation only?
        MesoscopicNonLatticeConforming = 3
        LocalLatticeConforming = 0

    class BCTypeEnum(Enum):
        NEUMANN = "Neumann"
        NEUMANN2ORder = "Neumann2Order"
        DIRICHLET = "Dirichlet"

    macroscopicNonLatticeConforming = 1  # valid for wave equation only?
    mesoscopicNonLatticeConforming = 3
    localLatticeConforming = 0

    __slots__ = (
        "Value",
        "PropValue",
        "CurrentValue",
        "BCType",
        "Parameters",
        "ID",
        "ImplementationSwitch",
        "point_ids",
        "nb_coord",
        "norm_coord",
        "An",
        "Amissingdisti",
        "double_ids",
        "double_distri_ids",
    )

    def __init__(
        self,
        value=0.0,
        bc_type=BCTypeEnum.NEUMANN,
        parameters=None,
        identification_number=None,
    ):
        self.Value = value  # a scalar or vector quantity indicating the value of the boundary condition
        self.PropValue = 0.0  # a scalar that indicates the time dependence of the applied boundary condition
        self.CurrentValue = 0.0  # the current value of the boundary condition for the current time computed as the PropValue(current_time) * Value
        self.BCType = bc_type  # the type of the boundary condition, i.e. Neumann, Dirichlet etc.
        self.Parameters = parameters
        self.ID = identification_number
        self.point_ids = list()
        self.nb_coord = list()
        self.norm_coord = list()
        self.An = list()
        self.Amissingdisti = list()
        self.double_ids = list()
        self.double_distri_ids = list()

    def compute_current_value(self, computation):
        """
        boundary conditions might be time dependent. this method computes the current value of the bc
        :param computation:
        :return:
        """
        # like Feap proportional
        time = computation.current_time  # BC are evaluated at absolute time
        if self.Parameters[0] == 1:
            if time < self.Parameters[3]:
                if self.Parameters[7] == 0.0:
                    raise ZeroDivisionError(
                        "Class BoundaryCondition/ Method compute_current_value: Parameter a_4 cannot be zero for BC of type 1!"
                    )
                self.PropValue = (
                    self.Parameters[4]
                    + self.Parameters[5] * (time - self.Parameters[2])
                    + self.Parameters[6]
                    * np.sin(2 * np.pi / self.Parameters[7] * (time - self.Parameters[2]) + self.Parameters[8])
                    ** self.Parameters[1]
                )
            else:  # time exceeds max_time
                self.PropValue = 0.0
        elif self.Parameters[0] == 2:
            if time < self.Parameters[3]:
                if self.Parameters[7] == 0.0:
                    raise ZeroDivisionError(
                        "Class BoundaryCondition/ Method compute_current_value: Parameter a_4 cannot be zero for BC of type 1!"
                    )
                self.PropValue = (
                    self.Parameters[4]
                    + self.Parameters[5] * (time - self.Parameters[2])
                    + self.Parameters[6]
                    * np.sin(2.0 * np.pi / self.Parameters[7] * (time - self.Parameters[2]) + self.Parameters[8])
                    ** self.Parameters[1]
                )
            else:  # time exceeds max_time
                self.PropValue = 1.0
        elif self.Parameters[0] == 3:  # Ricker-Wavelet
            if time < self.Parameters[3]:
                if self.Parameters[1] == 0.0:
                    raise ZeroDivisionError(
                        "Class BoundaryCondition/ Method compute_current_value: Parameter k cannot be zero for BC of type 3!"
                    )
                prefactor = 2 / (3 * self.Parameters[1]) ** 0.5 / np.pi**0.25
                tau = ((time - self.Parameters[2]) / self.Parameters[1]) ** 2
                self.PropValue = self.Parameters[4] * prefactor * (1 - tau) * np.exp(-0.5 * tau)
            else:
                self.PropValue = 0.0
        elif self.Parameters[0] == 99:
            if time < self.Parameters[3]:
                if self.Parameters[7] == 0.0:
                    raise ZeroDivisionError(
                        "Class BoundaryCondition/ Method compute_current_value: Parameter a_3 cannot be zero for BC of type 99!"
                    )
                self.PropValue = self.Parameters[4] * (
                    self.Parameters[7]
                    * np.sin(2.0 * np.pi * (time - self.Parameters[2]) / self.Parameters[5] - self.Parameters[6])
                    + self.Parameters[7]
                )
            else:
                self.PropValue = 0.0
        elif self.Parameters[0] == 98:
            if time < 50.0 * computation.Parameters["dth"]:
                self.PropValue = 0.1 * (
                    0.5 * np.sin(2.0 * np.pi / (50.0 * computation.Parameters["dth"]) * (time - 0.0) - np.pi / 2.0)
                    + 0.5
                )
            else:
                self.PropValue = 0.0
        elif self.Parameters[0] == 97:
            if time < 0.5:
                self.PropValue = 0.1 * (0.5 * np.sin(2.0 * np.pi / (0.5) * (time - 0.0) - np.pi / 2.0) + 0.5)
            else:
                self.PropValue = 0.0
        elif self.Parameters[0] == 100:  # like BC 1 but PropValue remains at 1.0 after the load has been applied
            if time < self.Parameters[3]:
                if self.Parameters[7] == 0.0:
                    raise ZeroDivisionError(
                        "Class BoundaryCondition/ Method compute_current_value: Parameter a_4 cannot be zero for BC of type 1!"
                    )
                self.PropValue = (
                    self.Parameters[4]
                    + self.Parameters[5] * (time - self.Parameters[2])
                    + self.Parameters[6]
                    * np.sin(2.0 * np.pi / self.Parameters[7] * (time - self.Parameters[2]) + self.Parameters[8])
                    ** self.Parameters[1]
                )
            else:  # time exceeds max_time
                self.PropValue = 1.0
        else:
            print("Proportional key " + str(self.Parameters[0]) + " not implemented")

        if self.BCType == "Neumann" or self.BCType == "Neumann2Order":
            for k in range(0, len(self.CurrentValue)):
                if computation.Lattice.LatticeType in [
                    "D2Q5_HeatEquation",
                    "D2Q5_PhasefieldFracture",
                ]:
                    self.CurrentValue[k] = (
                        self.Value[k] / (computation.Parameters["wr"] / computation.Parameters["L"]) * self.PropValue
                    )
                else:
                    self.CurrentValue[k] = (
                        self.Value[k]
                        / (computation.Parameters["mue"] * computation.Parameters["wr"] / computation.Parameters["L"])
                        * self.PropValue
                    )  # non-dimensional Parameters
        elif self.BCType == "Dirichlet":
            for k in range(0, len(self.CurrentValue)):
                self.CurrentValue[k] = (
                    self.Value[k] / (computation.Parameters["wr"]) * self.PropValue
                )  # non-dimensional Parameters
        ################################################################################################################
        elif self.BCType == "Source":
            for k in range(0, len(self.CurrentValue)):
                self.CurrentValue[k] = (
                    self.Value[k]
                    * self.PropValue
                    / (
                        computation.Parameters["mue"]
                        * computation.Parameters["wr"]
                        / (computation.Parameters["L"] ** 3)
                    )
                )
        elif self.BCType == "Dirichlet_firstorder":
            for k in range(0, len(self.Value)):
                self.CurrentValue[k] = self.Value[k] * self.PropValue / computation.Parameters["L"]
        elif self.BCType == "Dirichlet_firstorder_loc":
            for k in range(0, len(self.Value)):
                self.CurrentValue[k] = self.Value[k] * self.PropValue / computation.Parameters["L"]
        elif self.BCType == "Neumann_firstorder":
            for k in range(0, len(self.Value)):
                self.CurrentValue[k] = (
                    self.Value[k]
                    * self.PropValue
                    / (computation.Parameters["mue"] * computation.Parameters["wr"] / (computation.Parameters["L"]))
                )
        elif self.BCType == "Neumann_firstorder_loc":
            for k in range(0, len(self.Value)):
                self.CurrentValue[k] = (
                    self.Value[k]
                    * self.PropValue
                    / (computation.Parameters["mue"] * computation.Parameters["wr"] / (computation.Parameters["L"]))
                )
        elif self.BCType == "Dirichlet_Con":
            for k in range(0, len(self.Value)):
                self.CurrentValue[k] = self.Value[k] * self.PropValue / computation.Parameters["L"]
        elif self.BCType == "Neumann_Con":
            for k in range(0, len(self.Value)):
                self.CurrentValue[k] = (
                    self.Value[k]
                    * self.PropValue
                    / (computation.Parameters["mue"] * computation.Parameters["wr"] / (computation.Parameters["L"]))
                )
        elif self.BCType == "Dirichlet_NonCon":
            for k in range(0, len(self.Value)):
                self.CurrentValue[k] = self.Value[k] * self.PropValue / computation.Parameters["L"]
        elif self.BCType == "Neumann_NonCon":
            for k in range(0, len(self.Value)):
                self.CurrentValue[k] = (
                    self.Value[k]
                    * self.PropValue
                    / (computation.Parameters["mue"] * computation.Parameters["wr"] / (computation.Parameters["L"]))
                )
        elif self.BCType == "Dirichlet_NonCon_Loc":
            for k in range(0, len(self.Value)):
                self.CurrentValue[k] = self.Value[k] * self.PropValue / computation.Parameters["L"]
        elif self.BCType == "Neumann_NonCon_Loc":
            for k in range(0, len(self.Value)):
                self.CurrentValue[k] = (
                    self.Value[k]
                    * self.PropValue
                    / (computation.Parameters["mue"] * computation.Parameters["wr"] / (computation.Parameters["L"]))
                )
        else:
            print(
                "Class boundary Method compute_current_value Error: Cannot compute self.CurrentValue! Comp. not defined for self.BCType"
            )

    def compute_current_value_par(self, time, par):
        """
        boundary conditions might be time dependent. this method computes the current value of the bc
        :param computation:
        :return:
        TODO: This should be temporary
        """
        # like Feap proportioral
        if self.Parameters[0] == 1:
            if time < self.Parameters[3]:
                if self.Parameters[7] == 0.0:
                    raise ZeroDivisionError(
                        "Class BoundaryCondition/ Method compute_current_value: Parameter a_4 cannot be zero for BC of type 1!"
                    )
                self.PropValue = (
                    self.Parameters[4]
                    + self.Parameters[5] * (time - self.Parameters[2])
                    + self.Parameters[6]
                    * np.sin(2.0 * np.pi / self.Parameters[7] * (time - self.Parameters[2]) + self.Parameters[8])
                    ** self.Parameters[1]
                )
            else:  # time exceeds max_time
                self.PropValue = 0.0
        elif self.Parameters[0] == 2:
            if time < self.Parameters[3]:
                if self.Parameters[7] == 0.0:
                    raise ZeroDivisionError(
                        "Class BoundaryCondition/ Method compute_current_value: Parameter a_4 cannot be zero for BC of type 1!"
                    )
                self.PropValue = (
                    self.Parameters[4]
                    + self.Parameters[5] * (time - self.Parameters[2])
                    + self.Parameters[6]
                    * np.sin(2.0 * np.pi / self.Parameters[7] * (time - self.Parameters[2]) + self.Parameters[8])
                    ** self.Parameters[1]
                )
            else:  # time exceeds max_time
                self.PropValue = 1.0
        elif self.Parameters[0] == 3:  # Ricker-Wavelet
            if time < self.Parameters[3]:
                if self.Parameters[1] == 0.0:
                    raise ZeroDivisionError(
                        "Class BoundaryCondition/ Method compute_current_value: Parameter k cannot be zero for BC of type 3!"
                    )
                prefactor = 2 / (3 * self.Parameters[1]) ** 0.5 / np.pi**0.25
                tau = ((time - self.Parameters[2]) / self.Parameters[1]) ** 2
                self.PropValue = self.Parameters[4] * prefactor * (1 - tau) * np.exp(-0.5 * tau)
            else:
                self.PropValue = 0.0
        elif self.Parameters[0] == 99:
            if time < self.Parameters[3]:
                if self.Parameters[7] == 0.0:
                    raise ZeroDivisionError(
                        "Class BoundaryCondition/ Method compute_current_value: Parameter a_3 cannot be zero for BC of type 99!"
                    )
                self.PropValue = self.Parameters[4] * (
                    self.Parameters[7]
                    * np.sin(2.0 * np.pi * (time - self.Parameters[2]) / self.Parameters[5] - self.Parameters[6])
                    + self.Parameters[7]
                )
            else:
                self.PropValue = 0.0
        elif self.Parameters[0] == 98:
            if time < 50.0 * par["dth"]:
                self.PropValue = 0.1 * (
                    0.5 * np.sin(2.0 * np.pi / (50.0 * par["dth"]) * (time - 0.0) - np.pi / 2.0) + 0.5
                )
            else:
                self.PropValue = 0.0
        elif self.Parameters[0] == 97:
            if time < 0.5:
                self.PropValue = 0.1 * (0.5 * np.sin(2.0 * np.pi / (0.5) * (time - 0.0) - np.pi / 2.0) + 0.5)
            else:
                self.PropValue = 0.0
        elif self.Parameters[0] == 100:  # like BC 1 but PropValue remains at 1.0 after the load has been applied
            if time < self.Parameters[3]:
                if self.Parameters[7] == 0.0:
                    raise ZeroDivisionError(
                        "Class BoundaryCondition/ Method compute_current_value: Parameter a_4 cannot be zero for BC of type 1!"
                    )
                self.PropValue = (
                    self.Parameters[4]
                    + self.Parameters[5] * (time - self.Parameters[2])
                    + self.Parameters[6]
                    * np.sin(2.0 * np.pi / self.Parameters[7] * (time - self.Parameters[2]) + self.Parameters[8])
                    ** self.Parameters[1]
                )
            else:  # time exceeds max_time
                self.PropValue = 1.0
        else:
            print("Proportional key " + str(self.Parameters[0]) + " not implemented")

        if self.BCType == "Neumann" or self.BCType == "Neumann2Order":
            for k in range(0, len(self.CurrentValue)):
                self.CurrentValue[k] = (
                    self.Value[k] / (par["mue"] * par["wr"] / par["L"]) * self.PropValue
                )  # non-dimensional Parameters
        elif self.BCType == "Dirichlet":
            for k in range(0, len(self.CurrentValue)):
                self.CurrentValue[k] = self.Value[k] / (par["wr"]) * self.PropValue  # non-dimensional Parameters
        ################################################################################################################
        elif self.BCType == "Source":
            for k in range(0, len(self.CurrentValue)):
                self.CurrentValue[k] = self.Value[k] * self.PropValue / (par["mue"] * par["wr"] / (par["L"] ** 3))
        elif self.BCType == "Dirichlet_Con":
            for k in range(0, len(self.Value)):
                self.CurrentValue[k] = self.Value[k] * self.PropValue / par["L"]
        elif self.BCType == "Neumann_Con":
            for k in range(0, len(self.Value)):
                self.CurrentValue[k] = self.Value[k] * self.PropValue / (par["mue"] * par["wr"] / (par["L"]))

        elif self.BCType == "Dirichlet_firstorder":
            for k in range(0, len(self.Value)):
                self.CurrentValue[k] = self.Value[k] * self.PropValue / par["L"]
        elif self.BCType == "Dirichlet_firstorder_loc":
            for k in range(0, len(self.Value)):
                self.CurrentValue[k] = self.Value[k] * self.PropValue / par["L"]
        elif self.BCType == "Neumann_firstorder":
            for k in range(0, len(self.Value)):
                self.CurrentValue[k] = self.Value[k] * self.PropValue / (par["mue"] * par["wr"] / (par["L"]))
        elif self.BCType == "Neumann_firstorder_loc":
            for k in range(0, len(self.Value)):
                self.CurrentValue[k] = self.Value[k] * self.PropValue / (par["mue"] * par["wr"] / (par["L"]))

        elif self.BCType == "Dirichlet_NonCon":
            for k in range(0, len(self.Value)):
                self.CurrentValue[k] = self.Value[k] * self.PropValue / par["L"]
        elif self.BCType == "Neumann_NonCon":
            for k in range(0, len(self.Value)):
                self.CurrentValue[k] = self.Value[k] * self.PropValue / (par["mue"] * par["wr"] / (par["L"]))
        elif self.BCType == "Dirichlet_NonCon_Loc":
            for k in range(0, len(self.Value)):
                self.CurrentValue[k] = self.Value[k] * self.PropValue / par["L"]
        elif self.BCType == "Neumann_NonCon_Loc":
            for k in range(0, len(self.Value)):
                self.CurrentValue[k] = self.Value[k] * self.PropValue / (par["mue"] * par["wr"] / (par["L"]))
        else:
            print(
                "Class boundary Method compute_current_value Error: Cannot compute self.CurrentValue! Comp. not defined for self.BCType"
            )

    def read_boundary_condition_from_string(self, in_string):
        assert isinstance(
            in_string, str
        ), "Class Boundary Condition / method read_boundary_condition_from_string Error! Input is not a string"

        bc_types_implemented = {
            "Neumann2Order",
            "Dirichlet",
            "Neumann",
            "Dirichlet_firstorder",
            "Dirichlet_firstorder_loc",
            "Neumann_firstorder",
            "Neumann_firstorder_loc",
            "Dirichlet_Con",
            "Neumann_Con",
            "Dirichlet_NonCon",
            "Neumann_NonCon",
            "Dirichlet_NonCon_Loc",
            "Neumann_NonCon_Loc",
            "Source",
            "periodic",
        }

        bc_params = in_string.split()

        if in_string.strip().startswith("#") or not bc_params:
            return

        self.ID = int(bc_params[0])
        if bc_params[2] in bc_types_implemented:
            dim_of_w = 1
            self.Value = [float(bc_params[1])]
            self.CurrentValue = [0.0]
            self.BCType = bc_params[2]
            self.Parameters = bc_params[3 : len(bc_params)]
            self.Parameters[0] = int(self.Parameters[0])
            self.Parameters[1 : len(self.Parameters)] = [
                float(parameter) for parameter in self.Parameters[1 : len(self.Parameters)]
            ]
        elif bc_params[3] in bc_types_implemented:
            dim_of_w = 2
            self.Value = [float(bc_params[1]), float(bc_params[2])]
            self.CurrentValue = [0.0, 0.0]
            self.BCType = bc_params[3]
            self.Parameters = bc_params[4 : len(bc_params)]
            self.Parameters[0] = int(self.Parameters[0])
            self.Parameters[1 : len(self.Parameters)] = [
                float(parameter) for parameter in self.Parameters[1 : len(self.Parameters)]
            ]
        elif bc_params[4] in bc_types_implemented:
            dim_of_w = 3
            self.Value = [float(bc_params[1]), float(bc_params[2]), float(bc_params[3])]
            self.CurrentValue = [0.0, 0.0, 0.0]
            self.BCType = bc_params[4]
            self.Parameters = bc_params[5 : len(bc_params)]
            self.Parameters[0] = int(self.Parameters[0])
            self.Parameters[1 : len(self.Parameters)] = [
                float(parameter) for parameter in self.Parameters[1 : len(self.Parameters)]
            ]
        # choose boundary condition implementation
        if len(self.Parameters) > 9:
            # choose boundary condition implementation
            self.ImplementationSwitch = int(self.Parameters[9])
        else:
            self.ImplementationSwitch = 0

import numpy as np
from typing import TypeVar

from SolidLBM.util.geometry.Vector import Vector
from SolidLBM.util.geometry.Line import Line
from SolidLBM.util.geometry.Crack import link_directions
# import SolidLBM.util.geometry.Crack as Crack_Module
from SolidLBM.util.tools import find_link_keys


VectorType = TypeVar("VectorType", bound=Vector)
LineType = TypeVar("LineType", bound=Line)
# link_directions = Crack_Module.link_directions


class Link(Line):
    """Link between lattice points"""

    __slots__ = ["points", "keys", "center", "ID", "_length"]

    def __init__(self, p1: VectorType = None, p2: VectorType = None, number: float = None) -> None:
        """
        Initializes a Link object with two points.

        Args:
            p1 (Vector, optional): The first point of the link. Defaults to None.
            p2 (Vector, optional): The second point of the link. Defaults to None.
        """
        super().__init__(p1, p2)
        self.points = p1, p2

        # set keys for points
        self.keys = find_link_keys(p1, p2, p1.neighbor_keys, p1.inv_keys)

        # set center of link
        self.center = p1 + 0.5 * (p2 - p1)

        self.ID = number
        self._length = p1.dist(p2)
    
    def direction(self) -> VectorType:
        """Compute the direction vector of the link.

        Returns:
            Vector: The direction vector of the link.
        """
        dir = self.P2 - self.P1
        dir = dir.normalized()
        return dir

    def get_sign_of_direction(self, key: str) -> float:
        """Get the sign of the direction of the link in a specific lattice direction
        
        Args:
            key (str): The key representing the lattice direction
        
        Returns:
            float: The sign of the direction of the link in the specified lattice direction
        """
        # get direction of link in lattice directions
        dir: VectorType = self.direction()
        ldir = Vector(*link_directions[key]).normalized()
        return dir * ldir
    
    def distance(self, point: VectorType) -> float:
        """Compute the distance between the link and a given point.

        Args:
            point (Vector): The point to compute the distance from.

        Returns:
            float: The distance between the link and the given point.
        """
        assert isinstance(point, Vector)
        return point.dist(self.center)

    def stretch(self) -> float:
        """Compute the stretch, i.e. length in current configuration
        
        Returns:
            float: The computed stretch value.
        """
        p1, p2 = self.points
        xi1, xi2 = p1.current_r(), p2.current_r()
        s = (xi1[0] - xi2[0])**2 + (xi1[1] - xi2[1])**2
        return np.sqrt(s)

    def rel_stretch(self) -> float:
        """Compute the relative stretch
        
        Returns:
            float: The computed relative stretch value.
        """
        return self.stretch() / self._length

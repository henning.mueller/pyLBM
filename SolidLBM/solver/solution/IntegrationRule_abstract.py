class IntegrationRule_abstract:
    def __init__(self, parameters=dict()):
        self.Parameters = parameters

    def integrateW(self, w, wdot, wddot, wddot_np1):
        """
        computes current w from old w, wdot, wddot
        :param w:
        :param wdot:
        :param wddot:
        :return: out_
        """
        pass

    def integrateWDot(self, w, wdot, wddot, wddot_np1):
        """
        computes current wdot from old w, wdot, wddot
        :param w:
        :param wdot:
        :param wddot:
        :return: out_wdot"""
        pass

    def computeWddotSuchThatW(self, w, wdot, wddot, wddot_np1, target_w):
        """
        computes wddot such that targetW is obtained in next time step
        :param w:
        :param wdot:
        :param wddot:
        :param target_w:
        :return:
        """

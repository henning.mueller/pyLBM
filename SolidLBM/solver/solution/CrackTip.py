import logging
from abc import ABC, abstractmethod
from collections import namedtuple
from collections.abc import Iterable
from typing import Optional, TypeVar
import numpy as np

import SolidLBM.util.geometry.Crack as Crack_Module
import SolidLBM.util.geometry.Vector as Vector_Module
import SolidLBM.util.geometry.Point as Point_Module
import SolidLBM.util.geometry.Polygon as Polygon_Module
import SolidLBM.util.tools as tools
import SolidLBM.util.functions.crack_velocity as velocity_func


Crack = Crack_Module.Crack
Point = Point_Module.Point
Vector = Vector_Module.Vector
Lattice = TypeVar("Lattice")
CrackBoundary = TypeVar("CrackBoundary")

crackfaces = namedtuple("CrackFaces", "R L")


class CrackTip_abstract(ABC):
    """abstract base class for CrackTips,
    which implement the evaluation and computations related to fracture mechanics.

    The class handles:
    - framework of crack propagation algorithm with delegation to methods in subclasses
    - creation of crack segments
    - seed points for the search of severed lattice links

    handling of output data and the evaluation of criteria is implemented in subclasses
    """

    __slots__ = (
        "id",
        "crack_tip",
        "prev_crack_tip",
        "seed_pts",
        "bc_names",
        "bc_switch",
        "tip_output",
        "criterion",
        "crit_val",
        "direction",
        "velocity",
    )

    def __init__(
        self,
        number: int,
        cr_tips: tuple[Point],
        velocity,
        crit_val,
        criterion,
        direction,
        bc_names: tuple[int] = (-1, -1),
        bc_switch: tuple[int] = (-1, -1),
        **kwargs,
    ) -> None:
        self.id = number
        self.crack_tip, self.prev_crack_tip = cr_tips
        self.velocity = velocity
        self.crit_val = crit_val
        self.criterion = criterion
        self.direction = direction
        self.bc_names = bc_names  # boundary names; L & R of crack face
        self.bc_switch = bc_switch  # bc implementation switch; L & R of crack face

        self.tip_output = dict()
        self._set_output_nan()  # set fields to NaN for check when writing output

        if self.criterion not in ("steady", "K_crit"):
            # treat velocity as maximum allowed, velocity gets overwritten
            self.tip_output["speed"] = 0

    def __repr__(self):
        return f"{self.__class__.__name__} instance #{self.id}"

    def handle_crack_propagation(
        self,
        cr_boundary: CrackBoundary,
        parameters: dict[float],
        time: float = -1.0,
        lattice: Optional[Lattice] = None,
    ) -> tuple[Iterable | bool]:
        """handles dynamic cracks
        1st, the method to evaluate the criterion is called
        2nd, for a propagating crack, the next crack segment is constructed
        3rd, new boundary points are found and processed, setting auxiliary points on the crack

        returns the auxiliary points, the new boundary points,
        and if the crack did propagate
        """

        aux_pts = []
        boun_pts = []
        intersected_pts = []

        propagation: bool = self._evaluate_criterion(
            parameters,
            boun_pts=cr_boundary.BounPoints,
            time=time,
            lattice=lattice,
        )

        if propagation:
            segment: Crack = self._get_crack_segment(parameters["dth"], cr_boundary)

            intersected_pts: list[tuple[Point]] = Crack_Module.find_boun_points_at_crack(segment, self.seed_pts)
            if intersected_pts:
                cr_boundary.join_crack_segments()
                cr_boundary.BounPoints += intersected_pts
                aux_pts: list[Vector] = Crack_Module.process_links_of_boun_points(
                    intersected_pts,
                    segment,
                    cr_boundary,
                    bc_imp=self.bc_switch,
                )
                self._set_seed_points(parameters["hh"], points=intersected_pts)
                boun_pts = tools.make_list(intersected_pts)

        # set current position of crack tip as output
        self.tip_output["position"] = self.crack_tip.to_array()
        return aux_pts, boun_pts, propagation

    def _get_crack_segment(self, delta_t, cr_boundary: CrackBoundary) -> Crack:
        """update crack tip position, then call _build_crack to create a
        Crack-object with the right boundary conditions
        """

        direction = self.direction
        velocity = self.velocity

        self.prev_crack_tip = self.crack_tip
        self.crack_tip += direction * velocity * delta_t

        crack_seg = self._build_crack()

        cr_boundary.BoundingGeometry.append(crack_seg)
        cr_boundary.total_length += crack_seg.compute_length()

        return crack_seg

    def _build_crack(self) -> Crack:
        """create a Crack-object with the right boundary conditions"""
        crack = Crack(p1=self.crack_tip, p2=self.prev_crack_tip)
        crack.BoundaryName, crack.BoundaryName2Crack = self.bc_names
        return crack

    def _set_seed_points(
        self,
        delta_x,
        points: list[Point] = [],
        cr_boundary: Optional[CrackBoundary] = None,
    ) -> None:
        """set seed point for queue in finding new boundary points;
        at least on neighbor is new boundary point when crack grows further
        """
        if not points:
            points = cr_boundary.BounPoints

        points = tools.make_list(points)
        assert points, "No points to find seed points from."

        if len(points) <= 2:
            # crack grows slow enough that last intersected points should suffice as seed
            seeds = points
        else:
            # find all points within circle around crack tip
            seeds = []
            for pt in points:
                from_tip = self.crack_tip - pt
                if from_tip.absolute_value() < 1.5 * delta_x:
                    seeds.append(pt)
        self.seed_pts = seeds

    @abstractmethod
    def _evaluate_criterion(self):
        pass

    @abstractmethod
    def get_output(self, *args, **kwargs):
        pass

    @abstractmethod
    def _set_output_nan(self):
        pass


class CrackTip_domain_abstract(CrackTip_abstract):
    """abstract class for domain based CrackTips

    Expands CrackTip_abstract with a domain around the crack tip and the
    handling ythereof, but does not implement the abstract methods for
    criteria or output.
    """

    __slots__ = ("tip_domain",)

    def __init__(
        self,
        number: int,
        cr_tips: tuple[Point],
        velocity,
        crit_val,
        criterion,
        direction,
        bc_names: tuple[int] = (-1, -1),
        bc_switch: tuple[int] = (-1, -1),
        init_points: list[Point] = [],
        parameters: dict[float] = {},
    ) -> None:
        super().__init__(
            number,
            cr_tips,
            velocity,
            crit_val,
            criterion,
            direction,
            bc_names,
            bc_switch,
        )

        # initialize domain for j-integral from lattice
        self.tip_domain = [[], []]
        self._update_tip_domain(points=init_points, parameters=parameters)

    def handle_crack_propagation(
        self,
        cr_boundary: CrackBoundary,
        parameters: dict,
        time: float = -1.0,
        lattice: Optional[Lattice] = None,
    ) -> tuple[Iterable | bool]:
        """handles dynamic cracks

        calls the method in the abstract base class, then updates the tip_domain (if needed)
        """

        aux_pts, boun_pts, propagation = super().handle_crack_propagation(cr_boundary, parameters, time, lattice)

        self._update_tip_domain(parameters=parameters)

        return aux_pts, boun_pts, propagation

    def _update_tip_domain(self, points: Optional[Iterable[Point]] = None, parameters: dict[float] = {}) -> None:
        """get list of points around crack tip
        If no list of points is passed, the neighbors are added
        """

        if not points:
            # expand domain with neighbors
            pts = [pt.Neighbors.values() for pt in self.tip_domain[1]]
            points = set(tools.make_list(pts))

        if not self.tip_domain[0]:
            self._set_tip_domain_points(parameters)

        # corners of domain in absolute coordinate system
        poly_pts = [pt + self.crack_tip for pt in self.tip_domain[0]]
        domain_poly = Polygon_Module.Polygon(points=poly_pts)
        self.tip_domain[1] = [pt for pt in points if domain_poly.decide_if_point_is_in_polygon(pt)]

    def _set_tip_domain_points(self, parameters) -> None:
        """set points of rectangle for J-Integral domain
        Corners are relative to crack tip in lateral directions.
        """
        a, b = parameters["domain_extend"]
        x, y = a * parameters["hh"], b * parameters["hh"]
        self.tip_domain[0] = [  # 4 points of a rectangle
            Point_Module.Point(-x, -y),
            Point_Module.Point(+x, -y),
            Point_Module.Point(+x, +y),
            Point_Module.Point(-x, +y),
        ]


class CrackTip_sif(CrackTip_abstract):
    """CrackTip with definitions for stress intensity factors

    Criteria are based on stress intensity factors from the crack opening displacement
    """

    def __init__(
        self,
        number: int,
        cr_tips: tuple[Point],
        velocity,
        crit_val,
        criterion,
        direction,
        bc_names: tuple[int] = (-1, -1),
        bc_switch: tuple[int] = (-1, -1),
        **kwargs,
    ) -> None:
        super().__init__(
            number,
            cr_tips,
            velocity,
            crit_val,
            criterion,
            direction,
            bc_names,
            bc_switch,
        )

    def get_output(self, parameters: dict[float], lattice: Lattice, boun_pts: Iterable[Point]) -> dict:
        """decide which data of crack set is requested, if it is current and compute if needed;
        return tip_output, e.g. for writing the output in Lattice-class
        """
        if np.isnan(self.tip_output["sif"]).any():
            self._compute_sif(parameters, boun_pts)
        return self.tip_output

    def _evaluate_criterion(
        self,
        parameters: dict,
        boun_pts: Optional[Iterable[Point]] = None,
        time=-1.0,
        **kwargs,
    ) -> bool:
        """evaluates the fracture criterion, returns True if crack grows
        time an paramater max_time can be used to set a hard limit

        implemented criteria:
            > steady :  unconditional growth at preset rate
            > K_crit :  K as condition with preset growth rate
            > SIF:      K as condition, growth rate from function
            > J-Proj:   as SIF, with abs(J) = G as condition
            > MERR:     maximum energy release rate,
                        with condition and direction from J, growth rate from function
        """

        def steady():
            """steady growth with constant speed; not an actual criterion"""
            return True

        def k_crit():
            """evaluate K-factor only, growth at constant speed"""
            # compute K-factor
            sif = self._compute_sif(parameters, boun_pts)
            k_eff = np.linalg.norm(sif)
            return k_eff > self.crit_val

        def sif():
            """evaluate K-factor (2-norm of K1, K2, K3) and determine variable speed
            represents K-criterion for mode III or mixed mode criterion for mode I + II
            """
            sif = self._compute_sif(parameters, boun_pts)
            k_eff = np.linalg.norm(sif)
            if k_eff > self.crit_val:
                v_max = parameters.get("v_max", 0.85)
                v = v_max * velocity_func.tanh_order4(k_eff, self.crit_val)

                # FIXME : needs to set parameter entry somwhere else
                parameters["velocity"] = v
                return True
            else:
                self.velocity = 0
                return False

        criterion_switch = {
            "steady": steady,
            "K_crit": k_crit,
            "SIF": sif,
        }

        crit_eval = False

        # check current time against max time for propagation, w/o input: t not in [0, inf]
        t_min = parameters.get("min_time", 0)
        t_max = parameters.get("max_time", np.inf)
        if t_min < time < t_max:
            try:
                crit_evaluator = criterion_switch[self.criterion]
                crit_eval = crit_evaluator()
            except KeyError:
                logging.error("Criterion {} not implemented, defaulting to no propagation".format(self.criterion))

            if self.criterion not in ("steady", "K_crit"):
                self.tip_output["speed"] = self.velocity

        return crit_eval

    def _compute_sif(self, parameters: dict[float], boun_pts: Iterable[Point]):
        """compute the SIFs at the current crack tip from the crack opening displacement (COD)

        looks for points next to the crack (in CrackBoundary.BounPoints)
        within a certain window from the tip, evaluates delta of displacement
        window: from 0.9 d_min to d_min + 1.1;
                d_min is an input parameter or set to 2.5
                can be adapted by crack speed
        d_min is measured in multiples of h, wheras r is the absolute (dimensionless) distance
        """

        h = parameters["hh"]
        d_min = parameters.get("d_min", 2.5)

        for pts in boun_pts:
            pt1, pt2 = pts

            # TODO : works for straight cracks between cells right now, extend to arbitrary geometries
            # might not work for D2Q9, needs check for both points
            r1 = self.crack_tip.dist(pt1)
            if 0.9 * d_min < r1 / h < d_min + 1.1:
                break

        r = r1 + (self.crack_tip.dist(pt2) - r1) / 2

        delta = abs(pt2.w[0] - pt1.w[0])
        alpha_s = np.sqrt(1 - self.velocity**2)
        k = delta / 2 * np.sqrt(np.pi / (2 * r)) * parameters["mu"] * alpha_s
        sif = np.array([0, 0, k])

        self.tip_output["sif"] = sif * parameters["rho0"] * np.sqrt(parameters["L"])
        return sif

    def _set_output_nan(self):
        """reset arrays in tip_output to NaN"""
        self.tip_output["sif"] = np.array([np.nan, np.nan, np.nan])


class CrackTip_sif_domain(CrackTip_domain_abstract, CrackTip_sif):
    """computation of SIF pointwise in domain,
    otherwise delegates to implementation of methods in superclasses

    Currently not working properly!
    """

    def _evaluate_criterion(self, *args, **kwargs):
        return super()._evaluate_criterion(*args, **kwargs)

    def _compute_sif(self):
        """compute the SIF from points in the near-tip domain
        Get a value for K at ervery point in the domain around the crack tip
        and evaluate the weighted mean value from this.
        """

        def weight(r, a=2.0, n=2.0):
            """weibull probablity density function"""
            # a: scaling parameter
            # n: form parameter
            # return (a / n) * ((r-d_min) / n)**(a - 1) * np.exp(-((r-d_min) / n)**a)
            return 1

        # constant or instantaneous parameters
        h = self.parameters["hh"]
        vol_x, vol_y = self.parameters["vol_extend"]
        d_min = self.parameters.get("d_min", 1.0) * h
        d_max = min(vol_x, vol_y) * h
        v_s = self.parameters["velocity"]  # relative crack speed
        c_rel = 1 / self.parameters["cdh"]  # relation c_s / c_d

        beta = np.arctan2(self.direction.y, self.direction.x)  # propagation direction to x-axis

        # initialize output
        domain_size = int(4 * np.ceil(vol_x * vol_y))
        r_vals = np.array([np.nan] * domain_size)
        p_vals = np.array([np.nan] * domain_size)
        k_vals = np.array([np.nan] * domain_size * 3).reshape((domain_size, 3))
        u_vals = np.array([np.nan] * domain_size * 3).reshape((domain_size, 3))

        w_total = 0
        sif = np.zeros(3)
        """TODO : vectorize using Arrays"""
        for i, pt in enumerate(self.tip_domain[1]):
            xi, eta = pt.transform_to_local(self.crack_tip.coords(), beta)
            r, theta = pt.transform_to_polar_coordinates((xi, eta))

            if d_min > r or r > d_max:  # restrict to circular band
                continue

            k = pt.sif_pointwise((xi, eta), v_s, self.parameters["mu"], c_rel)
            w = weight(r)
            w_total += w
            sif += w * k

            # write output
            r_vals[i] = r / h
            p_vals[i] = theta
            k_vals[i] = k
            u_vals[i] = pt.disp_to_array()

        sif /= w_total

        self.tip_output["domain"] = {
            "r": r_vals,
            "phi": p_vals,
            "k": k_vals,
            "disp": u_vals,
        }

        return sif

    def get_output(self, *args, **kwargs):
        return super().get_output(*args, **kwargs)


class CrackTip_configurational(CrackTip_domain_abstract):
    """Implements a criterion based on configurational forces,
    computed from a domain around the crack tip.

    Written for usage with NavierEquation_Ansumali-type lattice points,
    other lattice point classes need the the implementation of
    """

    __slots__ = ("data",)

    def __init__(
        self,
        number: int,
        cr_tips: tuple[Point],
        velocity,
        crit_val,
        criterion,
        direction,
        bc_names: tuple[int] = (-1, -1),
        bc_switch: tuple[int] = (-1, -1),
        init_points: list[Point] = [],
        parameters: dict[float] = {},
    ) -> None:
        super().__init__(
            number,
            cr_tips,
            velocity,
            crit_val,
            criterion,
            direction,
            bc_names,
            bc_switch,
            init_points,
            parameters,
        )

        self.data = {"energy": 0}

    def get_output(self, parameters: dict[float], lattice: Lattice, *args, **kwargs) -> dict:
        """decide which data of crack set is requested, if it is current and compute if needed;
        return tip_output, e.g. for writing the output in Lattice-class
        """
        if np.isnan(self.tip_output["force_j"]).any():
            self._compute_j_integral(parameters, lattice)
        return self.tip_output

    def _set_output_nan(self):
        """reset arrays in tip_output to NaN"""
        self.tip_output["force_j"] = np.array([np.nan, np.nan, np.nan])

    def _evaluate_criterion(
        self,
        parameters: dict,
        time: float = -1.0,
        lattice: Optional[Lattice] = None,
        **kwargs,
    ) -> bool:
        """evaluates the fracture criterion, returns True if crack grows
        time an paramater max_time can be used to set a hard limit

        implemented criteria:
            > steady :  unconditional growth at preset rate
            > J-Proj:   abs(J) = G as condition, but prescribed speed and direction
            > MERR:     maximum energy release rate,
                        with condition and direction from J, growth rate from function
            > MERR_reg: MERR with regularized velocity
        """

        def steady():
            """steady growth with constant speed; not an actual criterion"""
            return True

        def j_projection():
            """project J-Integral onto prescribed direction of growth"""
            j = self._compute_j_integral(parameters, lattice)
            g = abs(np.dot(j, self.direction.to_array()))

            propagation = g > self.crit_val
            if propagation:
                c_rs = parameters["c_r"] / parameters["cs"]
                v = velocity_func.from_g_static_regular(g, self.crit_val, c_rs)
            else:
                v = 0

            self.velocity = v
            return propagation

        def j_vec_integral():
            """evaluate J-Integral and determine direction and speed"""

            j = self._compute_j_integral(parameters, lattice)
            g = np.sqrt(j[0] ** 2 + j[1] ** 2)
            propagation = g > self.crit_val

            if propagation:
                direction = -j / g
                self.direction = Vector_Module.Vector(*direction)

                c_rs = parameters["c_r"] / parameters["cs"]
                if "reg" in self.criterion:
                    v = velocity_func.from_g_static_regular(g, self.crit_val, c_rs)
                else:
                    v = velocity_func.from_g_static(g, self.crit_val, c_rs)
            else:
                v = 0

            self.velocity = v
            return propagation

        criterion_switch = {
            "steady": steady,
            "J-Proj": j_projection,
            "MERR": j_vec_integral,
            "MERR_reg": j_vec_integral,
        }

        crit_eval = False

        # check current time against max time for propagation, w/o input: t not in [0, inf]
        t_min = parameters.get("min_time", 0)
        t_max = parameters.get("max_time", np.inf)
        if t_min < time < t_max:
            try:
                crit_eval = criterion_switch[self.criterion]()
            except KeyError:
                logging.error("Criterion {} not implemented, defaulting to no propagation".format(self.criterion))

            if self.criterion not in ("steady",):
                self.tip_output["speed"] = self.velocity

        return crit_eval

    def _compute_j_integral(self, parameters: dict[float], lattice):
        """Computes the J-vector-integral as a configurational force
        from a volume integral over a domain around the crack tip.
        """

        def get_cell_volume(pt):
            """determine the volume of the cell around a lattice point"""

            """FIXME :
                has gone into infinite loop in compute_cell_volumes_and_areas_at_boundary_points;
                assuming square cells in accordance to staircase-b.c. for now
            """
            # for boundary points: get volume of cell,
            # compute if needed from Boundary-class, e.g. for boundary points at crack
            # otherwise use volume of cube (or square)
            # if pt.BoundaryName:
            # if not pt.VolumeSurfaceMeasure:
            # # passing CrackBoundary should be enough,
            # # tip_domain should not reach egdes of lattice
            # """problem, if crack reaches boundary of domain
            # -> cell volume not updated and incorrect
            # Boundary_Module.compute_cell_volumes_and_areas_at_boundary_points(
            # cellsize = parameters['hh'],
            # points = [pt],
            # seedpoint = self.mesh_seed,
            # boundary = CrackSet.boundary,
            # )
            # vol = pt.VolumeSurfaceMeasure[0]
            # else:
            # vol = parameters['hh']**2

            vol = parameters["hh"] ** 2
            return vol

        rho = parameters["rho0"]
        dt = parameters["dth"]

        tip_domain = set(self.tip_domain[1])

        j_integral = np.zeros(3)

        # get points corresponding to gradient coefficients and extend domain accordingly;
        # recompute for BounPts, as neighbors might have changed
        # use experimental implementation, as it uses fewer checks and points
        # -> no computation of gradient across crack
        dom_extension = set()
        # for pt in tip_domain:
        #     pt.gradient_coefficients = pt.get_gradient_coefficients(lattice, force=True)
        for pt in tip_domain:
            coeff_pts = pt.get_point_set_from_gradient_coefficients(lattice, recompute=True)
            # coeff_pts = pt.get_point_set_from_gradient_coefficients(lattice, force=True)
            dom_extension.update(coeff_pts)

        # set tensors (grad_u, momentum, cauchy): quantities as NumPy-Arrays in attributes
        # pass on points already updated for evaluation of displacement gradient
        # then compute in extended domain:
        #   - hamiltonian (for eshelby)
        #   - eshelby stress (needed for grad & div)
        # needs cauchy, momentum, grad_u defined
        visited = set()  # points that have been processed
        for pt in tip_domain | dom_extension:
            # rho = sum(pt.f_eq)
            pt._set_tensors(lattice, parameters, pts_visited=visited)
            pt.compute_hamiltonian(rho=rho, grad_u=pt.grad_u)
            pt.compute_eshelby(lattice)
            # pt.compute_poynting()
            visited.add(pt)

        # in domain around crack tip (w/o immediate vicinity):
        #   - force densities (configurational g, dynamical p)
        #   - integrate over volume of domain
        for pt in tip_domain:
            grad_u = pt.grad_u  # F
            # grad_j = pt.grad_j
            grad_j = pt.get_gradient_of_attribute(lattice, attr="momentum")  # L
            j_dot = (pt.momentum - pt.momentum_prev) / dt

            force_g = pt.get_divergence_of_attribute(lattice, attr="eshelby")
            force_p = -np.einsum("ji,j->i", grad_j, pt.momentum) / rho + np.einsum("ji,j->i", grad_u, j_dot)

            distance = (pt - self.crack_tip).absolute_value()
            if distance < parameters["d_min"]:
                # small circle around crack tip
                continue
            volume = get_cell_volume(pt)
            j_integral += volume * (force_g + force_p)

        # TODO : consistent re-dimensionalisation of output (also critical value etc.)
        # self.tip_output['force_j'] = j_integral * parameters['rho'] \
        # * parameters['cs']**2 * parameters['L']
        self.tip_output["force_j"] = j_integral

        return j_integral


# expose 'CrackTip' as general type for all classes in this module
CrackTip = type(CrackTip_abstract)

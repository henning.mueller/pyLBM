import SolidLBM.solver.solution.IntegrationRule_abstract as IntegrationRule_abstract_Module


class IntegrationRule_NewmarkExplicit(IntegrationRule_abstract_Module.IntegrationRule_abstract):
    def __init__(self, parameters=dict()):
        self.Parameters = parameters

    def integrateW(self, w, wdot, wddot, wddot_np1):
        """
        computes current w from old w, wdot, wddot
        :param w:
        :param wdot:
        :param wddot:
        :return:
        """
        # Newmark parameters
        # beta = self.Parameters['beta_Newmark']
        # gamma = self.Parameters['gamma_Newmark']
        dt = self.Parameters["dth"]
        if len(w) == 2:
            out_w = list([0.0, 0.0])
        elif len(w) == 1:
            out_w = [0.0]
        # Newmark, explicit
        for i in range(0, len(w)):
            out_w[i] = w[i] + dt * wdot[i] + (dt**2) / 2.0 * (1.0 * wddot[i])  # update displacement
        return out_w

    def integrateWDot(self, w, wdot, wddot, wddot_np1):
        """
        computes current wdot from old w, wdot, wddot
        :param w:
        :param wdot:
        :param wddot:
        :return:
        """
        # Newmark parameters
        # beta = self.Parameters['beta_Newmark']
        # gamma = self.Parameters['gamma_Newmark']
        dt = self.Parameters["dth"]
        if len(w) == 2:
            out_wdot = list([0.0, 0.0])
        elif len(w) == 1:
            out_wdot = [0.0]
        # Newmark, explicit
        for i in range(0, len(w)):
            out_wdot[i] = wdot[i] + dt * (wddot[i])  # update velocity
        return out_wdot

    def computeWddotSuchThatW(self, w, wdot, wddot, wddot_np1, target_w):
        """
        computes wddot such that targetW is obtained in next time step
        :param w:
        :param wdot:
        :param wddot:
        :param target_w:
        :return: wddotNew
        """
        dt = self.Parameters["dth"]

        if len(w) == 2:
            wddot_out = list([0.0, 0.0])
        elif len(w) == 1:
            wddot_out = [0.0]

        for i in range(0, len(w)):
            wddot_out[i] = (target_w[i] - w[i]) * 2.0 / dt**2 - 2.0 * wdot[i] / dt

        return wddot_out

import logging
from collections import namedtuple, deque
from typing import Any, TypeVar
# from pathlib import Path

import tables as tb
import numpy as np
import tomli

import SolidLBM.solver.solution.CrackTip as CrackTip_Module
import SolidLBM.util.geometry.Crack as Crack_Module
import SolidLBM.util.geometry.Vector as Vector_Module
import SolidLBM.util.geometry.Point as Point_Module
import SolidLBM.mesher.mesh.Boundary as Boundary_Module


Crack = Crack_Module.Crack
Point = Point_Module.Point
Vector = Vector_Module.Vector
Lattice = TypeVar("Lattice")

crackfaces = namedtuple("CrackFaces", "R L")


__CRACKTIP_SWITCH__ = {
    "sif": CrackTip_Module.CrackTip_sif,
    "sif_domain": CrackTip_Module.CrackTip_sif_domain,
    "configurational": CrackTip_Module.CrackTip_configurational,
}


def _get_cracktip_class(flags: dict[bool]):
    crtip_class = "sif"
    if flags["configurational"]:
        crtip_class = "configurational"
    elif flags["k_domain"]:
        crtip_class = "sif_domain"
    return __CRACKTIP_SWITCH__[crtip_class]


class _CrackBoundary(Boundary_Module.Boundary):
    """
    boundary-type object needed for finding closest point on boundary;
    also keeps record of all crack segments and length
    """

    def __init__(self, crack_segments=[], input_file_path=""):
        self.InputFilePath = input_file_path
        self.Dimensions = "2D"

        self.total_length = sum([crack.compute_length() for crack in crack_segments])

        self.BounPoints = []
        self.ExteriorBoundary = []  # initial w/o cracks
        self.BoundingGeometry = crack_segments  # only cracks

        self.read_input_file()
        self._set_boundary_lists()

    def _set_boundary_lists(self):
        """Set the lists of Boundaries:
            BoundingGeometry is needed for computations, and keeps only the cracks to reduce overhead
            ExteriorBoundary keeps any initial boundary, except cracks (thus the actual bounding geometry)
            CrackBoundaries is unchanged, thus contains only initial cracks

            Exterior- and CrackBoundaries combined rebuilt the initial boundaries

        This method is meant to be called right after building the inital boundaries.
        """
        exterior = [boun for boun in self.BoundingGeometry if type(boun) is not Crack]
        self.ExteriorBoundary = exterior
        self.BoundingGeometry = self.CrackBoundaries

    def _initial_boun_points(self, points, crack=None, ctips=None, h=None):
        # distance between 1st and 2nd lattice point is approx. lattice spacing
        if not h:
            h = points[0].dist(points[1])

        if ctips:
            ct_1, ct_2 = ctips
        elif crack:
            ct_1, ct_2 = Point_Module.aspoint(crack)
        else:
            logging.error(
                "_CrackBoundary needs crack tips or crack for initial points."
            )
            raise RuntimeError

        # find all boundary points in environments around crack tips;
        # sort to group geometrically close points together in list,
        # then build list of 2-tuples of points, which should be on opposite crack faces
        pt_list = []
        for pt in (p for p in points if p.BoundaryName):
            if pt.dist(ct_1) <= 4 * h or pt.dist(ct_2) <= 4 * h:
                pt_list.append(pt)
        pt_list.sort(key=lambda p: p.dist(ct_2))

        if len(pt_list) % 2 == 1:
            pt_list.pop()
        n = int(len(pt_list) / 2)

        pt_list = np.array(pt_list).reshape(n, 2).tolist()
        self.BounPoints += pt_list

    def join_crack_segments(self):
        """Join crack_segments to longer segments, if they are parallel and continuous.
        Works best for straight cracks, propagating in small increments.
        """
        segments = []
        try:
            # sort by coordinates of 1st point
            # should give better results with more than one crack tip
            q = sorted(self.BoundingGeometry, key=lambda c: c.P1.coords())
        except AttributeError:
            logging.error(
                "Cannot join crack segments, there might be non-Cracks-objects in CrackBoundary."
            )
            return

        q = deque(q)
        while len(q) > 1:
            # try to join the 2 leftmost segments
            crack1 = q.popleft()
            crack2 = q.popleft()

            assert type(crack1) is Crack
            assert type(crack2) is Crack

            # get list with either joined crack, or both cracks
            joined_cracks = crack1.add(crack2)
            if len(joined_cracks) == 2:  # not joined
                segments.append(joined_cracks[0])  # discard crack1
            # put element back in queue for next iteration
            q.appendleft(joined_cracks[-1])
        segments += q

        logging.debug(
            f"Reduced list of crack segments from {len(self.BoundingGeometry)} to {len(segments)}"
        )

        self.BoundingGeometry = segments
        # update length, since double entries might have been joined
        self.total_length = sum([crack.compute_length() for crack in segments])


class CrackSet:
    """Handling of the dynamics of fractures.

    The class acts as an interface and controls the CrackTip instances.
    - Reading input files
    - Initialization of CrackTip instances

    Attributes:
        Flags (dict): Allowed flags for controlling the flow of the program.
        boundary (CrackBoundary): The CrackBoundary instance.
        mesh_seed (int): The initial seed of the mesh.
        cracktips (list): List of CrackTip instances.
        parameters (dict): Parameters needed for crack computation.
        output_file (Path): The output file path.

    Methods:
        __init__(self, computation): Initializes the CrackSet instance.
        __repr__(self): Returns a string representation of the CrackSet instance.
        _update_flags(cls, flags): Set certain flags depending on the class of CrackTip.
        evaluate_crack_propagation(self, time, lattice): Evaluates crack propagation.
        write_crack_tip_data(self, lattice, time): Writes crack tip data to an HDF5 file.
        _setup_output_structure(self, attrs, verbose): Sets up the output structure.
    """

    # allowed Flags for controlling the flow of the program
    Flags = {
        "adaptive_d": False,  # for sif_stat: adapt eval. dist. to velocity
        "k_domain": False,  # compute SIF in domain
        "configurational": False,  # compute configigurational force (J-Integral)
        "regular_v": False,  # use function with regularization of speed
        "_tip_domain": False,  # use domain around tip; for G, J-Int or K
    }

    def __init__(self, computation) -> None:
        def _read_config() -> tuple[dict[Any]]:
            with open(
                computation.WorkingDir / f"{computation.Name}.toml", mode="rb"
            ) as f:
                tmp_config = tomli.load(f)
                config = tmp_config["DynCrack"]
                flags = tmp_config.get("Flags", dict())
            return config, flags

        def process_parameters(
            config: dict[Any] = dict(), comp_par: dict[Any] = dict()
        ) -> dict[Any]:
            """get parameters from config and computation"""

            params = config

            # explicitly defined parameters needed for crack from computation
            # TODO: take de-dimensionalised values
            params["L"] = comp_par["L"]
            params["dth"] = comp_par["dth"]
            params["hh"] = comp_par["hh"]
            params["mu"] = comp_par["mueh"]
            params["cs"] = comp_par["cs"]
            params["csh"] = comp_par["csh"]
            params["ch"] = comp_par["ch"]
            params["cdh"] = comp_par.get("cdh", 1.0)  # c^_d = c_d / c_s
            params["lambda"] = comp_par.get("lambda", np.nan)
            try:
                """TODO : bad practice
                    -> unify declaration of parameters: in config files & Computation"""
                params["rho0"] = comp_par["rho"]
            except KeyError:
                params["rho0"] = comp_par["rho0"]

            params["c_r"] = compute_rayleigh_velocity(
                params["mu"],
                params["lambda"],
            )

            # normalize crack velocity to fractions of wave speed
            params["velocity"] = velocity / params["cs"]
            if "v_max" not in params:
                params["v_max"] = max(params["velocity"], 1.0)

            if "d_min" not in params:
                params["d_min"] = 0

            if "domain_size" in params:
                # domain size relative to L is given: set or override 'domain_extend'
                a, b = params["domain_size"]
                refL = comp_par["L"]
                h = comp_par["hh"]

                params["domain_extend"] = a * refL / h, b * refL / h
            return params

        def parse_flags(flags: dict[bool]) -> dict[bool]:
            flags["configurational"] = flags.get(
                "configurational", False
            ) or criterion in {
                "J-Proj",
                "MERR",
            }  # compute config forces
            flags["_tip_domain"] = flags.get("k_domain", False) or flags.get(
                "configurational", False
            )  # need domain around tip
            self._update_flags(flags)

        input_file_path = computation.WorkingDir / f"{computation.Name}.geo"
        self.boundary = _CrackBoundary(input_file_path=input_file_path)

        self.mesh_seed = computation.Lattice.Points[0]

        # set flags and parameters from config
        config, flags = _read_config()
        crit_val = config.get("crit_val", np.nan)
        criterion = config.get("criterion", "none")
        velocity = config.get("velocity", 0.0)
        parse_flags(flags)
        params = process_parameters(config, computation.Parameters)

        # load cracks and create CrackSet objects
        CrackTip = _get_cracktip_class(self.Flags)

        cracktips = []
        cr_id = 1
        crackset_conf = config.pop("Cracksets")
        for tip_conf in crackset_conf:
            # get crack, then points and BounNames from it
            cr_tag = tip_conf["tag"]
            crack = self.boundary.CrackBoundaries[cr_tag]
            other_tip = crack.P1
            crack_tip = crack.P2
            bc_r = crack.BoundaryName
            bc_l = crack.BoundaryName2Crack

            if tip_conf.get("invert", False):
                # inversion: swap points and boundary names
                other_tip, crack_tip = crack_tip, other_tip
                bc_r, bc_l = bc_l, bc_r

            direction = crack_tip - other_tip

            bc_imp_r = computation.BoundaryConditions[bc_r].ImplementationSwitch
            bc_imp_l = computation.BoundaryConditions[bc_l].ImplementationSwitch

            # instantiate CrackSets with attributes from config
            cr_tip = CrackTip(
                cr_id,
                (crack_tip, other_tip),
                velocity,
                crit_val,
                criterion,
                direction / direction.absolute_value(),
                bc_names=crackfaces(bc_r, bc_l),
                bc_switch=crackfaces(bc_imp_r, bc_imp_l),
                init_points=computation.Lattice.Points,
                parameters=params,
            )

            # find initial boun point tuples & seeds from entire Lattice
            self.boundary._initial_boun_points(
                points=computation.Lattice.Points, ctips=(other_tip, crack_tip)
            )
            cr_tip._set_seed_points(params["hh"], points=self.boundary.BounPoints)

            cracktips.append(cr_tip)
            cr_id += 1

        self.cracktips = cracktips
        self.parameters = params
        self.boundary.join_crack_segments()  # join cracks (to remove double entries)
        self.output_file = computation.output_path / f"{computation.Name}_cracktip_output.h5"
        self._setup_output_structure(params, verbose=computation.Flags["verbose"])

    def __repr__(self):
        return f"{self.__class__.__name__} interface, {len(self.cracktips)} crack tips"

    @classmethod
    def _update_flags(cls, flags: dict[bool]) -> None:
        """update Flags of class, if key was initially defined"""
        for key, flag in flags.items():
            if key in cls.Flags.keys():
                cls.Flags[key] = flag

    def evaluate_crack_propagation(self, time, lattice) -> tuple[list | bool]:
        aux_pts = []
        boun_pts = []
        for ctip in self.cracktips:
            aux, boun, propagation = ctip.handle_crack_propagation(
                self.boundary,
                self.parameters,
                time=time,
                lattice=lattice,
            )
            aux_pts += aux
            boun_pts += boun

        return aux_pts, boun_pts, propagation

    def write_crack_tip_data(self, lattice: Lattice, time: float) -> None:
        """write the data at crack tips to an HDF5-file by appending rows of tables

        append rows to tables:
        time and length to general data in roots,
        at each crack tip, columns and data are directly taken from keys and values
        in tip_output, therefore the tables have to be prepared accordingly;
        for domain data, a different table is used, has no column in crack tip table;
        if requested data has not been computed, the respective methods are called and
        tip_output is then reset after writing (-> delegated to CrackSet)

        """

        def append_row(table, data):
            """write data from dictionary as row of table"""
            row = table.row
            for k, v in data.items():
                row[k] = v
            row.append()
            table.flush()

        with tb.open_file(self.output_file, "a") as output_file:
            append_row(
                output_file.root.cracks,
                {"time": time, "length": self.boundary.total_length},
            )

            for cr_tip in self.cracktips:
                tip_output = cr_tip.get_output(
                    self.parameters, lattice, self.boundary.BounPoints
                )
                domain_output = tip_output.pop("domain", dict())
                assert "domain" not in cr_tip.tip_output.keys()

                tip_group = output_file.root.__getattr__("tip_" + str(cr_tip.id))
                append_row(
                    tip_group.crack_tip,
                    cr_tip.tip_output,
                )
                if self.Flags["k_domain"]:
                    append_row(
                        tip_group.tip_domain,
                        domain_output,
                    )
                    tip_group.tip_domain
                cr_tip._set_output_nan()

    def _setup_output_structure(self, attrs: dict = {}, verbose=False):
        """create HDF5 file; create groups and tables;
        set attributes with parameters;

        Each entry in tip_output needs a corresponding column, which has to be
        allocated properly. If more data should be written, this setup must be
        expanded accordingly with the dictionaries defining columns and dtypes.
        """

        # prepare columns of tables with dictionaries:
        # tables for general data and for each crack tip
        # additional table per crack tip for data in domain, if requested for K
        # J-integral only needs column in tip-table for now
        _CrackSetData = {
            "time": tb.Float64Col(),
            "length": tb.Float64Col(),
        }

        _CrackTipData = {
            "position": tb.Float64Col(shape=(1, 3)),
            "speed": tb.Float64Col(),
            # 'sif': tb.Float64Col(shape=(1,3)),
        }

        if CrackSet.Flags["_tip_domain"]:
            vol_x, vol_y = attrs["domain_extend"]
            domain_size = int(4 * np.ceil(vol_x * vol_y))
        if CrackSet.Flags["k_domain"]:
            _CrackTipDomainData = {
                "k": tb.Float64Col(shape=(domain_size, 3)),
                "disp": tb.Float64Col(shape=(domain_size, 3)),
                "r": tb.Float64Col(shape=(domain_size,)),
                "phi": tb.Float64Col(shape=(domain_size,)),
            }
        if CrackSet.Flags["configurational"]:
            _CrackTipData["force_j"] = tb.Float64Col(shape=(1, 3))
        else:
            _CrackTipData["sif"] = tb.Float64Col(shape=(1, 3))

        # create the groups and tables for output as needed and set attributes
        # general data in root group, tip data in distinct groups
        # crack tip groups are consecutively named, stating at 1 (tip_1, ...)
        with tb.open_file(self.output_file, "w") as datafile:
            root = datafile.root
            crackset_table = datafile.create_table(
                root,
                "cracks",
                _CrackSetData,
                "general data",
            )

            crackset_table.attrs.criterion = attrs["criterion"]
            crackset_table.attrs.critical_value = attrs.get("crit_val", np.nan)
            crackset_table.attrs.delta_t = attrs["dth"]
            crackset_table.attrs.delta_h = attrs["hh"]
            crackset_table.attrs.v_max = attrs["v_max"]
            crackset_table.attrs.d_min = attrs.get("d_min", np.nan)
            crackset_table.attrs.c_s = attrs["csh"]
            crackset_table.attrs.c_d = attrs.get("cdh", np.nan)
            crackset_table.attrs.c_r = attrs.get("c_r", np.nan)

            if "domain_extend" in attrs:
                crackset_table.attrs.domain_shape = attrs["domain_extend"]

            crackset_table.flush()

            for idx in range(len(self.cracktips)):
                tip_str = f"tip_{idx+1}"
                tip_group = datafile.create_group(root, tip_str)

                crack_tip_data = datafile.create_table(
                    tip_group,
                    "crack_tip",
                    _CrackTipData,
                    "data at " + tip_str,
                )
                crack_tip_data.flush()

                if CrackSet.Flags["k_domain"]:
                    crack_dom_data = datafile.create_table(
                        tip_group,
                        "tip_domain",
                        _CrackTipDomainData,
                        "data of SIF in domain around " + tip_str,
                    )
                    crack_dom_data.flush()

        # print some information about the crack and its modelling to the terminal
        if verbose:
            crack_string = (
                "criterion: {} -- speed: {} -- d_min: {} -- domain: {}".format(
                    attrs["criterion"],
                    attrs["velocity"],
                    attrs["d_min"],
                    CrackSet.Flags["_tip_domain"],
                )
            )
            if CrackSet.Flags["_tip_domain"]:
                crack_string += " [{}h x {}h]".format(*attrs["domain_extend"])

            print("Dynamic Crack Propagation")
            print(crack_string)


def compute_rayleigh_velocity(lame_mu: float, lame_lambda: float) -> float:
    nu = lame_lambda / (2 * (lame_lambda + lame_mu))
    c_inv = 1.14418 - 0.25771 * nu + 0.12661 * nu**2

    return 1 / c_inv

import numpy as np

from collections.abc import Iterable
from typing import Optional, Any

from SolidLBM.solver.solution.LatticePointD2_abstract import LatticePointD2_abstract


Array = Iterable[float]


class LatticePointD2_mechanics_abstract(LatticePointD2_abstract):
    def transform_to_local(self, shift: Array = (0, 0), angle: float = 0) -> Array:
        """Euklidean transform with shift of origin and rotation"""
        rotation = np.array(
            [[np.cos(angle), np.sin(angle)], [-np.sin(angle), np.cos(angle)]]
        )
        coords = np.array([self.x - shift[0], self.y - shift[1]])
        return rotation @ coords

    @staticmethod
    def transform_to_polar_coordinates(coords: Array) -> Array:
        x, y = coords[:2]
        r = np.sqrt(x**2 + y**2)
        phi = np.arctan2(y, x)
        return r, phi

    def sif_from_cod(self, *args: Any, **kwargs: Any) -> Array:
        pass

    def sif_pointwise(self, *args: Any, **kwargs: Any) -> Array:
        pass

    @staticmethod
    def elastic_moduli(lame_lambda, lame_mu):
        e = lame_mu * (3 * lame_lambda + 2 * lame_mu) / (lame_lambda + lame_mu)
        nu = lame_lambda / (2 * lame_lambda + lame_mu)
        return e, nu


class LatticePointD2_mechanics_antiplaneshear(LatticePointD2_mechanics_abstract):
    def sif_pointwise(
        self,
        coords: Array,
        speed: float = 0,
        mu: float = 1,
        speed_rel: Optional[Any] = None,
    ) -> Array:
        """K_III at point in comoving system

        speed is relative to c_s (shear wave speed)
        """

        alpha_s = np.sqrt(1 - speed**2)
        xi_s = coords[0]
        eta_s = alpha_s * coords[1]
        r_s, phi_s = self.transform_to_polar_coordinates((xi_s, eta_s))
        disp_w = self.w[-1]
        k3 = (
            np.sqrt(np.pi / 2)
            * mu
            * alpha_s
            / np.sqrt(r_s)
            * disp_w
            / np.sin(phi_s / 2)
        )

        return abs(np.array([0, 0, k3]))


class LatticePointD2_mechanics_planestrain(LatticePointD2_mechanics_abstract):
    def current_r(self) -> tuple[float, float]:
        x, y = self.x, self.y
        w1, w2 = self.w
        return x + w1, y + w2

    def sif_pointwise(
        self, coords: Array, speed: float = 0, mu: float = 1, speed_rel: float = 1
    ) -> Array:
        """K_I and K_II at point in comoving system

        speed is relative to c_s (shear wave speed)
        speed_rel is relation of wave speeds c_s/c_d
        """

        # get local coordinates for wave speed c_s
        alpha_s = np.sqrt(1 - speed**2)
        xi_s = coords[0]
        eta_s = alpha_s * coords[1]
        r_s, phi_s = self.transform_to_polar_coordinates((xi_s, eta_s))

        # get local coordinates for wave speed c_d
        alpha_d = np.sqrt(1 - speed**2 * speed_rel**2)
        xi_d = coords[0]
        eta_d = alpha_d * coords[1]
        r_d, phi_d = self.transform_to_polar_coordinates((xi_d, eta_d))

        # some constant factors
        alpha_2 = alpha_s**2
        alpha_sd = 2 * alpha_s * alpha_d
        d_rayleigh = 1 / np.sqrt(2 * np.pi) * alpha_2 / (2 * alpha_sd - alpha_s**2)
        sqrt_r_s, sqrt_r_d = np.sqrt((r_s, r_d))

        # matrix entries
        f1i = sqrt_r_d * np.cos(phi_d / 2) - alpha_sd / alpha_2 * sqrt_r_s * np.cos(
            phi_s / 2
        )
        f2i = alpha_d * (
            -sqrt_r_d * np.sin(phi_d / 2) + sqrt_r_s * 2 / alpha_2 * np.sin(phi_s / 2)
        )
        f1ii = (
            2
            / mu
            * alpha_s
            * (
                sqrt_r_d * 2 / alpha_2 * np.sin(phi_d / 2)
                - sqrt_r_s * np.sin(phi_s / 2)
            )
        )
        f2ii = (
            2
            / mu
            * (
                -alpha_sd / alpha_2 * sqrt_r_d * np.cos(phi_d / 2)
                + sqrt_r_s * np.cos(phi_s / 2)
            )
        )

        # linear system for K_I, K_II from displacement
        det_f = f1i * f2ii - f2i * f1ii
        factor = 1 / (det_f * d_rayleigh)
        u1, u2 = self.w[:2]
        k1 = factor * (f2ii * u1 - f1ii * u2)
        k2 = factor * (-f2i * u1 + f1i * u2)

        return abs(np.array([k1, k2, 0]))
from functools import cache

import numpy as np

import SolidLBM.util.tensorcalc as tensor

import SolidLBM.solver.solution.LatticePointD2Q9_abstract as LatticePointD2Q9_abstract_Module
from SolidLBM.solver.solution.LatticePointD2_mechanics import (
    LatticePointD2_mechanics_planestrain,
)


_indices_plus_two = [1, 2, 5, 6]
_indices_minus_two = [3, 4, 7, 8]
_direction_keys = ["0", "x+", "y+", "x-", "y-", "x+y+", "x-y+", "x-y-", "x+y-"]


class LatticePointD2Q9_NavierEquation_Ansumali(LatticePointD2Q9_abstract_Module.LatticePointD2Q9_abstract):
    weights = np.array([16, 4, 4, 4, 4, 1, 1, 1, 1]) / 36

    @staticmethod
    def compute_dependent_parameters(cell_size, params):
        params["cs"] = np.sqrt(params["mue"] / params["rho0"])
        params["csh"] = params["cs"] / params["cs"]
        params["cd"] = np.sqrt((params["lambda"] + 2.0 * params["mue"]) / params["rho0"])
        params["cdh"] = params["cd"] / params["cs"]
        params["lambdah"] = params["lambda"] / params["mue"]
        params["mueh"] = params["mue"] / params["mue"]
        params["rho0h"] = params["rho0"] / params["rho0"]

        params["h"] = cell_size
        params["hh"] = params["h"] / params["L"]
        params["dth"] = 1.0 / np.sqrt(3.0) * params["hh"] / params["csh"]
        params["ch"] = params["hh"] / params["dth"]
        if "tau" not in params:
            params["tau"] = 0.55

        params["tauh"] = params["tau"] * params["dth"]

        return params

    def __init__(self, x=None, y=None, z=None, number=None):
        super(LatticePointD2Q9_NavierEquation_Ansumali, self).__init__(x, y, z, number)
        self.w = [0.0] * 2  # displacement of the last n time step (if needed for time integration)
        self.wdot = [0.0] * 2  # velocity of the last time step
        self.f = [0.0] * 9
        self.f_temp = [0.0] * 9
        self.f_eq = [0.0] * 9
        self.f_coll = [0.0] * 9

        self.rho = 1.0
        self.divu = [0.0] * 9  # for boundary, to calculate P from sigma

    @cache
    @staticmethod
    def _compute_c(cc):
        c = [
            [0, 0],
            [cc, 0],
            [0, cc],
            [-cc, 0],
            [0, -cc],
            [cc, cc],
            [-cc, cc],
            [-cc, -cc],
            [cc, -cc],
        ]
        return c

    @cache
    @staticmethod
    def _compute_c_tensors(cc):
        c_tensors = [
            [
                [0.0, 0.0],  # c0c0
                [0.0, 0.0],
            ],
            [
                [cc**2, 0.0],  # c1c1
                [0.0, 0.0],
            ],
            [
                [0.0, 0.0],  # c2c2
                [0.0, cc**2],
            ],
            [
                [cc**2, 0.0],  # c3c3
                [0.0, 0.0],
            ],
            [
                [0.0, 0.0],  # c4c4
                [0.0, cc**2],
            ],
            [
                [cc**2, cc**2],  # c5c5
                [cc**2, cc**2],
            ],
            [
                [cc**2, -(cc**2)],  # c6c6
                [-(cc**2), cc**2],
            ],
            [
                [cc**2, cc**2],  # c7c7
                [cc**2, cc**2],
            ],
            [
                [cc**2, -(cc**2)],  # c8c8
                [-(cc**2), cc**2],
            ],
        ]
        return c_tensors

    def _compute_zeroth_moment(self):
        return sum(self.f)

    def _compute_first_moment(self, computation=None, cc=None):
        if not cc:
            cc = computation.Parameters["ch"]
        c = LatticePointD2Q9_NavierEquation_Ansumali._compute_c(cc)

        first_moment = np.einsum("i,ij", self.f, c)

        return first_moment

    def _compute_second_moment(self, computation=None, cc=None):
        if not cc:
            cc = computation.Parameters["ch"]
        c_tensors = LatticePointD2Q9_NavierEquation_Ansumali._compute_c_tensors(cc)

        second_moment = np.einsum("i,ijk", self.f, c_tensors)

        return second_moment

    def _compute_second_moment_eq(self, computation=None, cc=None):
        if not cc:
            cc = computation.Parameters["ch"]
        c_tensors = LatticePointD2Q9_NavierEquation_Ansumali._compute_c_tensors(cc)

        second_moment_eq = np.einsum("i,ijk", self.f_eq, c_tensors)

        return second_moment_eq

    def _compute_source_s(self, computation=None, lattice=None, mueh=None, lambdah=None, rho0h=None):
        if computation:
            mueh = computation.Parameters["mueh"]
            lambdah = computation.Parameters["lambdah"]
            rho0h = computation.Parameters["rho0h"]

        if not lattice:
            lattice = computation.Lattice

        grad_rho_t = self._compute_grad_rho(lattice=lattice)
        source = (mueh - lambdah) / rho0h * grad_rho_t

        return source

    def _compute_grad_rho(self, computation=None, lattice=None):
        if not lattice:
            lattice = computation.Lattice
        dx, dy = self.get_gradient_coefficients(h=lattice.CellSize)

        rhodx = rhody = 0.0
        for ptIndex in dx:
            point = lattice.Points[ptIndex]
            rhodx += sum(point.f) * dx[ptIndex]

        for ptIndex in dy:
            point = lattice.Points[ptIndex]
            rhody += sum(point.f) * dy[ptIndex]

        return np.array([rhodx, rhody])

    def _compute_rho_tpdt(self):
        rho_temp = 0.0
        if self.BoundaryName:
            indices_of_missing_distribution_functions = self._compute_indices_of_missing_distribution_functions()
            for i in range(0, 9):
                if i not in indices_of_missing_distribution_functions:
                    rho_temp = rho_temp + self.f_temp[i]
            rho_temp = rho_temp + self._compute_sum_of_missing_distribution_functions()
        else:
            rho_temp = sum(self.f_temp)
        return rho_temp

    def _compute_missing_part_of_first_order_moment_at_boundary_from_t_ext(
        self, t_ext, computation
    ):
        c = LatticePointD2Q9_NavierEquation_Ansumali._compute_c(computation)
        dt = computation.Parameters["dth"]

        indices_of_missing_distribution_funcions = self._compute_indices_of_missing_distribution_functions()

        first_order_moment_to_be_streamed_across_boundary = [0.0, 0.0]
        for i in range(1, 9):
            if self.stream_keys[i] in indices_of_missing_distribution_funcions:
                first_order_moment_to_be_streamed_across_boundary[0] = (
                    first_order_moment_to_be_streamed_across_boundary[0] + c[i][0] * self.f[i]
                )
                first_order_moment_to_be_streamed_across_boundary[1] = (
                    first_order_moment_to_be_streamed_across_boundary[1] + c[i][1] * self.f[i]
                )

        missing_part_of_first_order_moment = [0, 0]
        missing_part_of_first_order_moment[0] = t_ext[0] * dt + first_order_moment_to_be_streamed_across_boundary[0]
        missing_part_of_first_order_moment[1] = t_ext[1] * dt + first_order_moment_to_be_streamed_across_boundary[1]

        return missing_part_of_first_order_moment

    def boun(self, computation):
        # TODO : should be broken up into several shorter methods,
        #        each covering only one implementation of boundary conditions

        __bc_implementations__ = {
            "Dirichlet_firstorder",
            "Dirichlet_firstorder_loc",
            "Dirichlet_Con",
            "Dirichlet_NonCon",
            "Dirichlet_NonCon_Loc",
            "Neumann_firstorder",
            "Neumann_firstorder_loc",
            "Neumann_Con",
            "Neumann_NonCon",
            "Neumann_NonCon_Loc",
        }

        @cache
        def _derive_transform_tensor(idx_bar):
            normal = self.BoundaryNormals[_direction_keys[idx_bar]]
            return np.array([[normal.x, normal.y], [-normal.y, normal.x]])

        params = computation.Parameters
        b = params["csh"]
        ctimesc = params["ch"]
        delta_x = params["hh"]
        c = LatticePointD2Q9_NavierEquation_Ansumali._compute_c(cc=ctimesc)
        weights = self.weights

        valid_boun_name = min(self.BoundaryName.values())
        valid_bc_type = computation.BoundaryConditions[valid_boun_name].BCType
        if valid_bc_type not in __bc_implementations__:
            raise NotImplementedError(f"BC {valid_bc_type} not implemented.")
        local_bc = "loc" in valid_bc_type

        # determine missing distribution functions
        idx_f_miss, idx_f_miss_bar = self._compute_indices_of_missing_distribution_functions()
        n_miss = len(idx_f_miss)
        
        ########################################################################################################
        # first order accurate Dirichlet and Neumann BCs, as described in BC submission
        # via bounce-back and anti-bounce-back boundary rules
        ########################################################################################################

        if valid_bc_type in ["Dirichlet_firstorder", "Dirichlet_firstorder_loc"]:
            # loop through missing distribution functions
            for idx, idx_bar in zip(idx_f_miss, idx_f_miss_bar):
                # key for current link out of bulk
                c_current = np.asarray(c[idx_bar])

                # linear momentum density (for each link individually)
                j_bd = computation.BoundaryConditions[valid_boun_name].CurrentValue

                # in nonlocal COS, just overwrite
                j_bd = np.asarray(j_bd)
                if local_bc:
                    # if local version, BC provided in local COS - transform into global COS
                    mat_loc = _derive_transform_tensor(idx_bar)
                    j_bd = mat_loc @ j_bd

                # missing_part_of_first_order_moment = self._compute_missing_part_of_first_order_moment_at_boundary_from_wdot(
                factor = 2.0 * weights[idx_bar] / (b**2)
                f_temp = self.f_coll[idx_bar] - factor * j_bd @ c_current
                self.f_temp[idx] = f_temp

        elif valid_bc_type in ["Neumann_firstorder", "Neumann_firstorder_loc"]:
            """Neumann boundary conditions on Cauchy stress vector
            works by getting Cauchy stress from Poisson stress and density, and rotating Cauchy's law into a normal COS
            """

            eye = np.eye(2)
            rho0 = params["rho0h"]
            mue_ = params["mueh"]
            lambda_ = params["lambdah"]

            b_2 = b**2
            b_4 = b**4


            # quantities on boundary node
            rho_self = self._compute_zeroth_moment()
            P_self = self._compute_second_moment(computation)  # cannot pass cc for some reason -> wrong results
            sigma_self = -np.asarray(P_self) + eye * (mue_ - lambda_) * (rho_self / rho0 - 1)
            
            # bounce back with phase change and added term
            for idx, idx_bar in zip(idx_f_miss, idx_f_miss_bar):
                # velcotities of current lattice link
                current_link = _direction_keys[idx_bar]
                c_current = c[idx_bar]
                ctimesc = np.outer(c_current, c_current)

                mat_loc = _derive_transform_tensor(idx_bar)

                # if nonlocal, transform into local form (because stress can be formulated more easily there)
                # t_new = computation.BoundaryConditions[self.BoundaryName[current_link]].CurrentValue
                traction = computation.BoundaryConditions[self.BoundaryName[current_link]].CurrentValue
                traction = np.asarray(traction)
                if not local_bc:
                    traction = mat_loc @ traction

                sigma_bd = mat_loc @ sigma_self @ mat_loc.T
                sigma_bd = np.array(
                    [traction,
                     [traction[1], sigma_bd[1][1]]]
                )
                sigma_bd = mat_loc.T @ sigma_bd @ mat_loc
                P_bd = -sigma_bd + eye * (mue_ - lambda_) * (rho_self / rho0 - 1)
                P_n_bd = P_bd - rho_self * b_2 * eye
                P_times_cc = np.tensordot(P_n_bd, (ctimesc - eye * b_2))
                f_temp = -self.f_coll[idx_bar] + 2 * weights[idx_bar] * (rho_self + P_times_cc / (2 * b_4))
                self.f_temp[idx] = f_temp

        elif valid_bc_type in ["Dirichlet_NonCon", "Dirichlet_NonCon_Loc"]:
            # FIXME: not working, throws errors
            weights = self.weights

            # loop through missing distribution functions
            for idx in range(n_miss):
                # key for current link out of bulk
                current_link = _direction_keys[idx_f_miss_bar[idx]]

                # mass flux (for each link individually)
                j_bd = computation.BoundaryConditions[self.BoundaryName[current_link]].CurrentValue

                # distance to bound along lattice link
                dist_to_bound = self.DistanceToBoundary[current_link]

                # initialise new bc vector
                j_bd_new = [0.0, 0.0]
                # if local version, transform bc into local coordinate system using normal vector
                if valid_bc_type == "Dirichlet_NonCon_Loc":
                    # normal vector
                    normal = self.BoundaryNormals[current_link]
                    normal_vec = [normal.x, normal.y]

                    # tangent vector
                    tangent_vec = [-normal_vec[1], normal_vec[0]]

                    # reinterpret load: x-normal, y-tangent
                    j_bd_new[0] = j_bd[0] * normal_vec[0] + j_bd[1] * tangent_vec[0]
                    j_bd_new[1] = j_bd[0] * normal_vec[1] + j_bd[1] * tangent_vec[1]

                else:
                    # in nonlocal, just overwrite
                    j_bd_new[0] = j_bd[0]
                    j_bd_new[1] = j_bd[1]

                # length of current lattice link
                if idx_f_miss[idx] in [1, 2, 3, 4]:
                    l_current = delta_x
                else:
                    l_current = np.sqrt(2) * delta_x

                # missing_part_of_first_order_moment = self._compute_missing_part_of_first_order_moment_at_boundary_from_t_ext(
                neighbour_bulk = self.Neighbors[_direction_keys[idx_f_miss[idx]]]  # FIXME: KeyError

                # current grid velocity
                c_current = c[idx_f_miss_bar[idx]]

                # bounce back and nonconform correction
                self.f_temp[idx_f_miss[idx]] = self.f_coll[idx_f_miss_bar[idx]] + (l_current - 2.0 * dist_to_bound) * (
                    neighbour_bulk.f_coll[idx_f_miss_bar[idx]] - self.f_coll[idx_f_miss[idx]]
                ) / (l_current + 2.0 * dist_to_bound)

                for aa in range(2):
                    self.f_temp[idx_f_miss[idx]] = self.f_temp[idx_f_miss[idx]] - 2.0 * l_current / (
                        l_current + 2.0 * dist_to_bound
                    ) * 2.0 * weights[idx_f_miss_bar[idx]] * j_bd_new[aa] * c_current[aa] / (b**2)

        elif valid_bc_type in ["Neumann_NonCon", "Neumann_NonCon_Loc"]:
            # some parameters
            weights = self.weights
            ident_mat = [[1.0, 0.0], [0.0, 1.0]]
            rho0 = params["rho0h"]
            mue_ = params["mueh"]
            lambda_ = params["lambdah"]

            # get own density
            rho_self = self._compute_zeroth_moment()

            # initialise
            P_n_bd = [[0.0, 0.0], [0.0, 0.0]]
            P_bd = [[0.0, 0.0], [0.0, 0.0]]

            # bounce back with phase change and added term
            for idx in range(n_miss):
                # initialise (need to be reset between lattice links)
                sigma_bd = [[0.0, 0.0], [0.0, 0.0]]
                sigma_bd = [[0.0, 0.0], [0.0, 0.0]]
                mat_loc = [[0.0, 0.0], [0.0, 0.0]]
                traction = [0.0, 0.0]

                # key for current link out of bulk
                current_link = _direction_keys[idx_f_miss_bar[idx]]

                # traction vector (for each link individually)
                traction = computation.BoundaryConditions[self.BoundaryName[current_link]].CurrentValue

                ########################################################################################################
                dist_to_bound = self.DistanceToBoundary[current_link]
                # switch for arbitrarily-valued Dirichlet and Neumann BCs (conforming and nonconforming)
                normal = self.BoundaryNormals[current_link]
                normal_vec = [normal.x, normal.y]

                # tangent vector
                tangent_vec = [-normal_vec[1], normal_vec[0]]

                # transformation matrix (transpose instead of inverse later)
                for aa in range(2):
                    mat_loc[0][aa] = normal_vec[aa]
                    mat_loc[1][aa] = tangent_vec[aa]

                # length of current lattice link
                if idx_f_miss[idx] in [1, 2, 3, 4]:
                    l_current = delta_x
                else:
                    l_current = np.sqrt(2) * delta_x

                # relevant neighbour
                neighbour_bulk = self.Neighbors[_direction_keys[idx_f_miss[idx]]]

                # density of relevant neighbour
                rho_neighbour = neighbour_bulk._compute_zeroth_moment()

                rho_self = (
                    l_current + dist_to_bound
                ) * rho_self / l_current - dist_to_bound * rho_neighbour / l_current

                # interpolate free stress tensor entry
                P_self = self._compute_second_moment(cc=ctimesc)
                P_neighbour = neighbour_bulk._compute_second_moment(cc=ctimesc)

                sigma_self = [[0.0, 0.0], [0.0, 0.0]]
                sigma_neighbour = [[0.0, 0.0], [0.0, 0.0]]

                for aa in range(2):
                    for bb in range(2):
                        sigma_self[aa][bb] = (
                            -P_self[aa][bb] + ident_mat[aa][bb] * (mue_ - lambda_) * (rho_self - rho0) / rho0
                        )
                        sigma_neighbour[aa][bb] = (
                            -P_neighbour[aa][bb] + ident_mat[aa][bb] * (mue_ - lambda_) * (rho_self - rho0) / rho0
                        )

                sigma_self_loc = [[0.0, 0.0], [0.0, 0.0]]
                sigma_neighbour_loc = [[0.0, 0.0], [0.0, 0.0]]
                # transform into local coordinate system
                for aa in range(2):
                    for bb in range(2):
                        for ctimesc in range(2):
                            for dd in range(2):
                                sigma_self_loc[aa][bb] = (
                                    sigma_self_loc[aa][bb]
                                    + mat_loc[aa][ctimesc] * sigma_self[ctimesc][dd] * mat_loc[bb][dd]
                                )
                                sigma_neighbour_loc[aa][bb] = (
                                    sigma_neighbour_loc[aa][bb]
                                    + mat_loc[aa][ctimesc] * sigma_neighbour[ctimesc][dd] * mat_loc[bb][dd]
                                )

                # interpolate new entry
                sigma_t = (l_current + dist_to_bound) * sigma_self_loc[1][
                    1
                ] / l_current - dist_to_bound * sigma_neighbour_loc[1][1] / l_current

                # f_miss[0] = (sum_of_missing_distribution_functions - sum_f_miss_g1_g2[1]) / len(group1)
                if valid_bc_type == "Neumann_NC":
                    # transform from x,y into n,t system
                    for aa in range(2):
                        for bb in range(2):
                            traction[aa] = traction[aa] + mat_loc[aa][bb] * traction[bb]

                else:
                    # already in n,t system, no transform necessary
                    for aa in range(2):
                        traction[aa] = traction[aa]

                ########################################################################################################
                for aa in range(2):
                    sigma_bd[aa][0] = traction[aa]
                    # make it symmetrical (makes no difference but let's be precise)
                    sigma_bd[0][aa] = traction[aa]
                sigma_bd[1][1] = sigma_t

                # determine missing distribution functions
                for aa in range(2):
                    for bb in range(2):
                        for ctimesc in range(2):
                            for dd in range(2):
                                sigma_bd[aa][bb] = (
                                    sigma_bd[aa][bb] + mat_loc[ctimesc][aa] * sigma_bd[ctimesc][dd] * mat_loc[dd][bb]
                                )

                # preallocate (overwrite later)
                rho_help = ((l_current + dist_to_bound) * rho_self - dist_to_bound * rho_neighbour) / l_current

                # find opposite-direction lattice vectors and distribution function indices
                for aa in range(2):
                    for bb in range(2):
                        P_bd[aa][bb] = -sigma_bd[aa][bb] + ident_mat[aa][bb] * (mue_ - lambda_) * (rho_help - rho0) / rho0
                # P^n
                for aa in range(2):
                    for bb in range(2):
                        P_n_bd[aa][bb] = P_bd[aa][bb] - rho_self * (b**2) * ident_mat[aa][bb]

                # keys for neighbours in order (get from lattice?)
                c_current = c[idx_f_miss_bar[idx]]

                ########################################################################################################
                self.f_temp[idx_f_miss[idx]] = (
                    0.0
                    - self.f_coll[idx_f_miss_bar[idx]]
                    - (l_current - 2.0 * dist_to_bound)
                    * (neighbour_bulk.f_coll[idx_f_miss_bar[idx]] + self.f_coll[idx_f_miss[idx]])
                    / (l_current + 2.0 * dist_to_bound)
                    + 2.0
                    * l_current
                    * 2.0
                    * weights[idx_f_miss_bar[idx]]
                    * rho_self
                    / (l_current + 2.0 * dist_to_bound)
                )

                # add density- and stress-dependent term (quasi-equilibrium-distribution-function)
                for oo in range(2):
                    for pp in range(2):
                        self.f_temp[idx_f_miss[idx]] = self.f_temp[idx_f_miss[idx]] + 2.0 * (
                            l_current / (l_current + 2.0 * dist_to_bound)
                        ) * 2.0 * weights[idx_f_miss_bar[idx]] * P_n_bd[oo][pp] * (
                            c_current[oo] * c_current[pp] - ident_mat[oo][pp] * (b**2)
                        ) / (2 * b**4)

        # compute missing distribution functions from opposite distribution functions
        elif valid_bc_type == "Dirichlet_Con":
            weights = self.weights

            for idx in range(n_miss):
                # key for current link out of bulk
                current_link = _direction_keys[idx_f_miss_bar[idx]]

                # mass flux
                j_bd = computation.BoundaryConditions[valid_boun_name].CurrentValue

                # current grid velocity
                c_current = c[idx_f_miss_bar[idx]]

                # bounce back
                self.f_temp[idx_f_miss[idx]] = self.f_coll[idx_f_miss_bar[idx]]

                for aa in range(2):
                    self.f_temp[idx_f_miss[idx]] = self.f_temp[idx_f_miss[idx]] - 2 * weights[
                        idx_f_miss_bar[idx]
                    ] * j_bd[aa] * c_current[aa] / (b**2)

        elif valid_bc_type == "Neumann_Con":
            weights = self.weights
            ident_mat = [[1.0, 0.0], [0.0, 1.0]]
            rho0 = params["rho0h"]
            mue_ = params["mueh"]
            lambda_ = params["lambdah"]

            # get own density
            rho_self = self._compute_zeroth_moment()

            # initialise
            P_n_bd = [[0.0, 0.0], [0.0, 0.0]]
            P_bd = [[0.0, 0.0], [0.0, 0.0]]

            # edge case (identify edges to eliminate 'devil's horns')
            if 6 in idx_f_miss_bar and 8 in idx_f_miss_bar:
                double_bd = True
                if 7 in idx_f_miss_bar:
                    # edge in which lattice direction
                    edge_index = 7
                    # indices pointing towards first and second boundary respectively
                    indices_1 = [3, 6]
                    indices_2 = [4, 8]
                elif 5 in idx_f_miss_bar:
                    edge_index = 5
                    indices_1 = [1, 8]
                    indices_2 = [2, 6]
            elif 5 in idx_f_miss_bar and 7 in idx_f_miss_bar:
                double_bd = True
                if 8 in idx_f_miss_bar:
                    edge_index = 8
                    indices_1 = [1, 5]
                    indices_2 = [4, 7]
                elif 6 in idx_f_miss_bar:
                    edge_index = 6
                    indices_2 = [2, 5]
                    indices_1 = [3, 7]
            # end of crack (identify ends to eliminate doubling effect)
            # vertical crack cases
            if (
                (1 in idx_f_miss_bar and 8 in idx_f_miss_bar and 5 not in idx_f_miss_bar)
                or (3 in idx_f_miss_bar and 7 in idx_f_miss_bar and 6 not in idx_f_miss_bar)
                or (1 in idx_f_miss_bar and 5 in idx_f_miss_bar and 8 not in idx_f_miss_bar)
                or (3 in idx_f_miss_bar and 6 in idx_f_miss_bar and 7 not in idx_f_miss_bar)
                or (
                    # horizontal crack cases
                    2 in idx_f_miss_bar and 5 in idx_f_miss_bar and 6 not in idx_f_miss_bar
                )
                or (4 in idx_f_miss_bar and 8 in idx_f_miss_bar and 7 not in idx_f_miss_bar)
                or (2 in idx_f_miss_bar and 6 in idx_f_miss_bar and 5 not in idx_f_miss_bar)
                or (4 in idx_f_miss_bar and 7 in idx_f_miss_bar and 8 not in idx_f_miss_bar)
            ):
                double_bd = False

            # calculate necessary tensors once, in case two bounds calculate both
            # this implementation is wordy but clear and relatively efficient
            if not double_bd:
                # single bound - i.e. no edges anywhere

                # initialise
                sigma_bd = [[0.0, 0.0], [0.0, 0.0]]
                sigma_bd = [[0.0, 0.0], [0.0, 0.0]]
                mat_loc = [[0.0, 0.0], [0.0, 0.0]]
                traction = [0.0, 0.0]

                # dummy link (not important because BC values for all links are the same)
                normal_index = min(idx_f_miss_bar)
                current_link = _direction_keys[normal_index]
                if normal_index in _indices_plus_two:
                    opp_link = _direction_keys[normal_index + 2]
                elif normal_index in _indices_minus_two:
                    opp_link = _direction_keys[normal_index - 2]

                # traction vector
                traction = computation.BoundaryConditions[self.BoundaryName[current_link]].CurrentValue

                # normal vector
                normal = self.BoundaryNormals[current_link]
                normal_vec = [normal.x, normal.y]

                # tangent vector
                tangent_vec = [-normal_vec[1], normal_vec[0]]

                # transformation matrix (transpose instead of inverse later)
                for aa in range(2):
                    mat_loc[0][aa] = normal_vec[aa]
                    mat_loc[1][aa] = tangent_vec[aa]

                # transform from x,y into n,t system
                for aa in range(2):
                    for bb in range(2):
                        traction[aa] = traction[aa] + mat_loc[aa][bb] * traction[bb]

                # insert into local sigma (remaining entry chosen as zero)
                for aa in range(2):
                    sigma_bd[aa][0] = traction[aa]

                    # some parameters
                    sigma_bd[0][aa] = traction[aa]

                # interpolate free stress tensor entry (normal, for efficiency)
                # relevant neighbour
                neighbour_bulk = self.Neighbors[opp_link]

                P_self = self._compute_second_moment(cc=ctimesc)
                P_neighbour = neighbour_bulk._compute_second_moment(cc=ctimesc)

                sigma_self = [[0.0, 0.0], [0.0, 0.0]]
                sigma_neighbour = [[0.0, 0.0], [0.0, 0.0]]

                for aa in range(2):
                    for bb in range(2):
                        sigma_self[aa][bb] = (
                            -P_self[aa][bb] + ident_mat[aa][bb] * (mue_ - lambda_) * (rho_self - rho0) / rho0
                        )
                        sigma_neighbour[aa][bb] = (
                            -P_neighbour[aa][bb] + ident_mat[aa][bb] * (mue_ - lambda_) * (rho_self - rho0) / rho0
                        )

                sigma_self_loc = [[0.0, 0.0], [0.0, 0.0]]
                sigma_neighbour_loc = [[0.0, 0.0], [0.0, 0.0]]
                # transform into local coordinate system
                for aa in range(2):
                    for bb in range(2):
                        for ctimesc in range(2):
                            for dd in range(2):
                                sigma_self_loc[aa][bb] = (
                                    sigma_self_loc[aa][bb]
                                    + mat_loc[aa][ctimesc] * sigma_self[ctimesc][dd] * mat_loc[bb][dd]
                                )
                                sigma_neighbour_loc[aa][bb] = (
                                    sigma_neighbour_loc[aa][bb]
                                    + mat_loc[aa][ctimesc] * sigma_neighbour[ctimesc][dd] * mat_loc[bb][dd]
                                )

                # interpolate new entry
                sigma_t = 3.0 * sigma_self_loc[1][1] / 2 - sigma_neighbour_loc[1][1] / 2.0

                # insert into stress tensor
                sigma_bd[1][1] = sigma_t

                # transform (back) into global coordinate system
                for aa in range(2):
                    for bb in range(2):
                        for ctimesc in range(2):
                            for dd in range(2):
                                sigma_bd[aa][bb] = (
                                    sigma_bd[aa][bb] + mat_loc[ctimesc][aa] * sigma_bd[ctimesc][dd] * mat_loc[dd][bb]
                                )

            else:
                # double bound - at edge
                # do everything twice for the two boundaries

                # empirical edge load factor
                edge_factor = 1.0

                # opposite of edge
                edge_opp_index = 0
                if edge_index in _indices_plus_two:
                    edge_opp_index = edge_index + 2
                else:
                    edge_opp_index = edge_index - 2

                # initialise additional
                sigma_1 = [[0.0, 0.0], [0.0, 0.0]]
                sigma_lok_1 = [[0.0, 0.0], [0.0, 0.0]]
                m_bwd_1 = [[0.0, 0.0], [0.0, 0.0]]
                t_new_1 = [0.0, 0.0]

                sigma_2 = [[0.0, 0.0], [0.0, 0.0]]
                sigma_lok_2 = [[0.0, 0.0], [0.0, 0.0]]
                m_bwd_2 = [[0.0, 0.0], [0.0, 0.0]]
                t_new_2 = [0.0, 0.0]

                normal_index_1 = min(indices_1)
                current_link_1 = _direction_keys[normal_index_1]
                if normal_index_1 in _indices_plus_two:
                    opp_link_1 = _direction_keys[normal_index_1 + 2]
                elif normal_index_1 in _indices_minus_two:
                    opp_link_1 = _direction_keys[normal_index_1 - 2]
                normal_index_2 = min(indices_2)
                current_link_2 = _direction_keys[normal_index_2]
                if normal_index_2 in _indices_plus_two:
                    opp_link_2 = _direction_keys[normal_index_2 + 2]
                elif normal_index_2 in _indices_minus_two:
                    opp_link_2 = _direction_keys[normal_index_2 - 2]

                normal_index_1 = min(indices_1)
                current_link_1 = _direction_keys[normal_index_1]
                if normal_index_1 in _indices_plus_two:
                    opp_link_1 = _direction_keys[normal_index_1 + 2]
                elif normal_index_1 in _indices_minus_two:
                    opp_link_1 = _direction_keys[normal_index_1 - 2]
                normal_index_2 = min(indices_2)
                current_link_2 = _direction_keys[normal_index_2]
                if normal_index_2 in _indices_plus_two:
                    opp_link_2 = _direction_keys[normal_index_2 + 2]
                elif normal_index_2 in _indices_minus_two:
                    opp_link_2 = _direction_keys[normal_index_2 - 2]

                # traction vector
                t_1 = computation.BoundaryConditions[self.BoundaryName[current_link_1]].CurrentValue
                t_2 = computation.BoundaryConditions[self.BoundaryName[current_link_2]].CurrentValue

                # normal vector
                n_temp_1 = self.BoundaryNormals[current_link_1]
                n_vect_1 = [n_temp_1.x, n_temp_1.y]
                n_temp_2 = self.BoundaryNormals[current_link_2]
                n_vect_2 = [n_temp_2.x, n_temp_2.y]

                # tangent vector
                t_vect_1 = [-n_vect_1[1], n_vect_1[0]]
                t_vect_2 = [-n_vect_2[1], n_vect_2[0]]

                # transformation matrix (transpose instead of inverse later)
                for aa in range(2):
                    m_bwd_1[0][aa] = n_vect_1[aa]
                    m_bwd_1[1][aa] = t_vect_1[aa]
                    m_bwd_2[0][aa] = n_vect_2[aa]
                    m_bwd_2[1][aa] = t_vect_2[aa]

                # transform from x,y into n,t system
                for aa in range(2):
                    for bb in range(2):
                        t_new_1[aa] = t_new_1[aa] + m_bwd_1[aa][bb] * t_1[bb]
                        t_new_2[aa] = t_new_2[aa] + m_bwd_2[aa][bb] * t_2[bb]

                # insert into local sigma (remaining entry chosen as zero)
                for aa in range(2):
                    sigma_lok_1[aa][0] = t_new_1[aa]
                    # make it symmetrical (makes no difference but let's be precise)
                    sigma_lok_1[0][aa] = t_new_1[aa]
                    sigma_lok_2[aa][0] = t_new_2[aa]
                    sigma_lok_2[0][aa] = t_new_2[aa]

                # interpolate free stress tensor entry (normal, for efficiency)
                # relevant neighbour
                neighbour_bulk_1 = self.Neighbors[opp_link_1]
                neighbour_bulk_2 = self.Neighbors[opp_link_2]

                P_self = self._compute_second_moment(cc=ctimesc)
                P_neighbour_1 = neighbour_bulk_1._compute_second_moment(cc=ctimesc)
                P_neighbour_2 = neighbour_bulk_2._compute_second_moment(cc=ctimesc)

                sigma_self = [[0.0, 0.0], [0.0, 0.0]]
                sigma_neighbour_1 = [[0.0, 0.0], [0.0, 0.0]]
                sigma_neighbour_2 = [[0.0, 0.0], [0.0, 0.0]]

                for aa in range(2):
                    for bb in range(2):
                        sigma_self[aa][bb] = (
                            -P_self[aa][bb] + ident_mat[aa][bb] * (mue_ - lambda_) * (rho_self - rho0) / rho0
                        )
                        sigma_neighbour_1[aa][bb] = (
                            -P_neighbour_1[aa][bb] + ident_mat[aa][bb] * (mue_ - lambda_) * (rho_self - rho0) / rho0
                        )
                        sigma_neighbour_2[aa][bb] = (
                            -P_neighbour_2[aa][bb] + ident_mat[aa][bb] * (mue_ - lambda_) * (rho_self - rho0) / rho0
                        )

                sigma_self_loc_1 = [[0.0, 0.0], [0.0, 0.0]]
                sigma_neighbour_loc_1 = [[0.0, 0.0], [0.0, 0.0]]
                sigma_self_loc_2 = [[0.0, 0.0], [0.0, 0.0]]
                sigma_neighbour_loc_2 = [[0.0, 0.0], [0.0, 0.0]]
                # transform into local coordinate system
                for aa in range(2):
                    for bb in range(2):
                        for ctimesc in range(2):
                            for dd in range(2):
                                sigma_self_loc_1[aa][bb] = (
                                    sigma_self_loc_1[aa][bb]
                                    + m_bwd_1[aa][ctimesc] * sigma_self[ctimesc][dd] * m_bwd_1[bb][dd]
                                )
                                sigma_self_loc_2[aa][bb] = (
                                    sigma_self_loc_2[aa][bb]
                                    + m_bwd_2[aa][ctimesc] * sigma_self[ctimesc][dd] * m_bwd_2[bb][dd]
                                )
                                sigma_neighbour_loc_1[aa][bb] = (
                                    sigma_neighbour_loc_1[aa][bb]
                                    + m_bwd_1[aa][ctimesc] * sigma_neighbour_1[ctimesc][dd] * m_bwd_1[bb][dd]
                                )
                                sigma_neighbour_loc_2[aa][bb] = (
                                    sigma_neighbour_loc_2[aa][bb]
                                    + m_bwd_2[aa][ctimesc] * sigma_neighbour_2[ctimesc][dd] * m_bwd_2[bb][dd]
                                )

                # interpolate new entry
                sigma_t_1 = 3.0 * sigma_self_loc_1[1][1] / 2 - sigma_neighbour_loc_1[1][1] / 2.0
                sigma_t_2 = 3.0 * sigma_self_loc_2[1][1] / 2 - sigma_neighbour_loc_2[1][1] / 2.0

                # lattice-conforming version
                sigma_lok_1[1][1] = sigma_t_1
                sigma_lok_2[1][1] = sigma_t_2

                # compute missing distribution functions from opposite distribution functions
                for aa in range(2):
                    for bb in range(2):
                        for ctimesc in range(2):
                            for dd in range(2):
                                sigma_1[aa][bb] = (
                                    sigma_1[aa][bb] + m_bwd_1[ctimesc][aa] * sigma_lok_1[ctimesc][dd] * m_bwd_1[dd][bb]
                                )
                                sigma_2[aa][bb] = (
                                    sigma_2[aa][bb] + m_bwd_2[ctimesc][aa] * sigma_lok_2[ctimesc][dd] * m_bwd_2[dd][bb]
                                )

            # bounce back with phase change and added term
            for idx in range(n_miss):
                if not double_bd:
                    # some parameters

                    # get boundary mass flux
                    neighbour_bulk = self.Neighbors[_direction_keys[idx_f_miss[idx]]]
                    # j_bd = computation.BoundaryConditions[valid_boundary_name].CurrentValue
                    rho_neighbour = neighbour_bulk._compute_zeroth_moment()

                    # density at boundary for stress calculation
                    rho_help = 1.5 * rho_self - 0.5 * rho_neighbour
                    rho_self = 1.5 * rho_self - 0.5 * rho_neighbour

                    rho_help_old = self.rhobd[idx_f_miss[idx]]
                    divu_old = self.divu[idx_f_miss[idx]]
                    divu_new = divu_old - (rho_help - rho_help_old) / rho_help
                    self.divu[idx_f_miss[idx]] = divu_new
                    self.rhobd[idx_f_miss[idx]] = rho_help
                    for aa in range(2):
                        for bb in range(2):
                            P_bd[aa][bb] = -sigma_bd[aa][bb] + ident_mat[aa][bb] * (lambda_ - mue_) * divu_new

                    # P^n
                    for aa in range(2):
                        for bb in range(2):
                            P_n_bd[aa][bb] = P_bd[aa][bb] - rho_self * (b**2) * ident_mat[aa][bb]

                    # some parameters
                    c_current = c[idx_f_miss_bar[idx]]

                    # get own density
                    self.f_temp[idx_f_miss[idx]] = (
                        0.0 - self.f_coll[idx_f_miss_bar[idx]] + 2 * weights[idx_f_miss_bar[idx]] * rho_self
                    )

                    # initialise
                    for oo in range(2):
                        for pp in range(2):
                            self.f_temp[idx_f_miss[idx]] = self.f_temp[idx_f_miss[idx]] + 2 * weights[
                                idx_f_miss_bar[idx]
                            ] * P_n_bd[oo][pp] * (c_current[oo] * c_current[pp] - ident_mat[oo][pp] * (b**2)) / (
                                2 * b**4
                            )

                elif idx_f_miss_bar[idx] == edge_index:
                    # nearest-neighbour interpolation for rho cancels term in CEE --> Neumann BC
                    P_n_bd_1 = [[0.0, 0.0], [0.0, 0.0]]
                    P_bd_1 = [[0.0, 0.0], [0.0, 0.0]]
                    P_n_bd_2 = [[0.0, 0.0], [0.0, 0.0]]
                    P_bd_2 = [[0.0, 0.0], [0.0, 0.0]]

                    # edge case (identify edges to eliminate 'devil's horns')
                    neighbour_bulk = self.Neighbors[_direction_keys[idx_f_miss[idx]]]

                    # end of crack (identify ends to eliminate doubling effect)
                    rho_neighbour = neighbour_bulk._compute_zeroth_moment()

                    # vertical crack cases
                    rho_help = 1.5 * rho_self - 0.5 * rho_neighbour
                    rho_self = 1.5 * rho_self - 0.5 * rho_neighbour  # doesnt this make more sense? originally rho_bd

                    # horizontal crack cases
                    rho_help_old = self.rhobd[idx_f_miss[idx]]
                    divu_old = self.divu[idx_f_miss[idx]]
                    divu_new = divu_old - (rho_help - rho_help_old) / rho_help
                    self.divu[idx_f_miss[idx]] = divu_new
                    self.rhobd[idx_f_miss[idx]] = rho_help
                    for aa in range(2):
                        for bb in range(2):
                            P_bd_1[aa][bb] = (
                                -1.0 * edge_factor * sigma_1[aa][bb] + ident_mat[aa][bb] * (lambda_ - mue_) * divu_new
                            )
                            P_bd_2[aa][bb] = (
                                -1.0 * edge_factor * sigma_2[aa][bb] + ident_mat[aa][bb] * (lambda_ - mue_) * divu_new
                            )

                    # calculate necessary tensors once, in case two bounds calculate both
                    for aa in range(2):
                        for bb in range(2):
                            P_n_bd_1[aa][bb] = P_bd_1[aa][bb] - rho_self * (b**2) * ident_mat[aa][bb]
                            P_n_bd_2[aa][bb] = P_bd_2[aa][bb] - rho_self * (b**2) * ident_mat[aa][bb]

                    # current grid velocity
                    c_current = c[idx_f_miss_bar[idx]]

                    # phase change bounce back
                    self.f_temp[idx_f_miss[idx]] = (
                        0.0 - self.f_coll[idx_f_miss_bar[idx]] + 2 * weights[idx_f_miss_bar[idx]] * rho_self
                    )

                    # add density- and stress-dependent term (quasi-equilibrium-distribution-function)
                    st_1 = 0.0
                    st_2 = 0.0
                    for oo in range(2):
                        for pp in range(2):
                            st_1 = st_1 + 2 * weights[idx_f_miss_bar[idx]] * P_n_bd_1[oo][pp] * (
                                c_current[oo] * c_current[pp] - ident_mat[oo][pp] * (b**2)
                            ) / (2 * b**4)
                            st_2 = st_2 + 2 * weights[idx_f_miss_bar[idx]] * P_n_bd_2[oo][pp] * (
                                c_current[oo] * c_current[pp] - ident_mat[oo][pp] * (b**2)
                            ) / (2 * b**4)

                    self.f_temp[idx_f_miss[idx]] = self.f_temp[idx_f_miss[idx]] + (st_1 + st_2) / 2

                elif idx_f_miss_bar[idx] in indices_1:
                    # double bound but not on the edge and on boundary 1

                    # initialise
                    P_n_bd_1 = [[0.0, 0.0], [0.0, 0.0]]
                    P_bd_1 = [[0.0, 0.0], [0.0, 0.0]]

                    # catch case in which two opposite distribution functions are undefined
                    if _direction_keys[idx_f_miss[idx]] in self.Neighbors:
                        # transform from x,y into n,t system
                        neighbour_bulk = self.Neighbors[_direction_keys[idx_f_miss[idx]]]

                        # insert into local sigma (remaining entry chosen as zero)
                        rho_neighbour = neighbour_bulk._compute_zeroth_moment()

                        # transform (back) into global coordinate system
                        rho_help = 1.5 * rho_self - 0.5 * rho_neighbour
                        rho_self = rho_help  # this would be rho_self in the original case

                        mult = 1.0
                    else:
                        # double bound - at edge
                        # do everything twice for the two boundaries

                        # initialise additional
                        indices_n = [[1, 2, 3, 4], [2, 3, 4, 1]]
                        indices_nc = [2, 3, 4, 1]
                        index_n1 = indices_n[0][edge_opp_index - 5]
                        index_n2 = indices_n[1][edge_opp_index - 5]
                        min_oeom = min([edge_opp_index, idx_f_miss_bar[idx]])
                        if min_oeom == 5:
                            if max([edge_opp_index, idx_f_miss_bar[idx]]) == 8:
                                min_oeom = 8
                        index_nc = indices_nc[min_oeom - 5]
                        if index_nc == index_n1:
                            index_nf = index_n2
                        else:
                            index_nf = index_n1

                        # traction vector
                        neighbour_c = self.Neighbors[_direction_keys[index_nc]]
                        neighbour_f = self.Neighbors[_direction_keys[index_nf]]
                        neighbour_oe = self.Neighbors[_direction_keys[edge_opp_index]]

                        # normal vector
                        rho_c = neighbour_c._compute_zeroth_moment()
                        rho_f = neighbour_f._compute_zeroth_moment()
                        rho_oe = neighbour_oe._compute_zeroth_moment()

                        # tangent vector
                        rho_help = (3.0 * (rho_self + rho_c) - rho_f - rho_oe) / 4.0
                        rho_help = (
                            rho_self  # hard coded by FS since I was too lazy to interpolate 3 points in par version
                        )
                        rho_self = rho_help

                        mult = 1.0

                    # compute P from sigma
                    rho_help_old = self.rhobd[idx_f_miss[idx]]
                    divu_old = self.divu[idx_f_miss[idx]]
                    divu_new = divu_old - (rho_help - rho_help_old) / rho_help
                    self.divu[idx_f_miss[idx]] = divu_new
                    self.rhobd[idx_f_miss[idx]] = rho_help
                    for aa in range(2):
                        for bb in range(2):
                            P_bd_1[aa][bb] = (
                                -mult * edge_factor * sigma_1[aa][bb] + ident_mat[aa][bb] * (lambda_ - mue_) * divu_new
                            )

                    # P^n
                    for aa in range(2):
                        for bb in range(2):
                            P_n_bd_1[aa][bb] = P_bd_1[aa][bb] - rho_self * (b**2) * ident_mat[aa][bb]

                    # current grid velocity
                    c_current = c[idx_f_miss_bar[idx]]

                    # bounce back with phase change and added term
                    self.f_temp[idx_f_miss[idx]] = (
                        0.0 - self.f_coll[idx_f_miss_bar[idx]] + 2 * weights[idx_f_miss_bar[idx]] * rho_self
                    )

                    # add density- and stress-dependent term (quasi-equilibrium-distribution-function)
                    for oo in range(2):
                        for pp in range(2):
                            self.f_temp[idx_f_miss[idx]] = self.f_temp[idx_f_miss[idx]] + 2 * weights[
                                idx_f_miss_bar[idx]
                            ] * P_n_bd_1[oo][pp] * (c_current[oo] * c_current[pp] - ident_mat[oo][pp] * (b**2)) / (
                                2 * b**4
                            )

                elif idx_f_miss_bar[idx] in indices_2:
                    # double bound but not on the edge and on boundary 2

                    # initialise
                    P_n_bd_2 = [[0.0, 0.0], [0.0, 0.0]]
                    P_bd_2 = [[0.0, 0.0], [0.0, 0.0]]

                    # catch case in which two opposite distribution functions are undefined
                    if _direction_keys[idx_f_miss[idx]] in self.Neighbors:
                        # relevant neighbour
                        neighbour_bulk = self.Neighbors[_direction_keys[idx_f_miss[idx]]]

                        # density of relevant neighbour
                        rho_neighbour = neighbour_bulk._compute_zeroth_moment()

                        # density at boundary for stress calculation
                        rho_help = 1.5 * rho_self - 0.5 * rho_neighbour
                        rho_self = rho_help  # this would be rho_self in the original case

                        mult = 1.0

                    else:
                        ########################################################################################
                        # some hard-coded lookup stuff to test interpolation
                        # get indices of lattice directions pointing to neighbours close to boundary point
                        indices_n = [[1, 2, 3, 4], [2, 3, 4, 1]]
                        indices_nc = [2, 3, 4, 1]
                        index_n1 = indices_n[0][edge_opp_index - 5]
                        index_n2 = indices_n[1][edge_opp_index - 5]
                        min_oeom = min([edge_opp_index, idx_f_miss_bar[idx]])
                        if min_oeom == 5:
                            if max([edge_opp_index, idx_f_miss_bar[idx]]) == 8:
                                min_oeom = 8
                        index_nc = indices_nc[min_oeom - 5]
                        if index_nc == index_n1:
                            index_nf = index_n2
                        else:
                            index_nf = index_n1

                        # get neighbours close to boundary point
                        neighbour_c = self.Neighbors[_direction_keys[index_nc]]
                        neighbour_f = self.Neighbors[_direction_keys[index_nf]]
                        neighbour_oe = self.Neighbors[_direction_keys[edge_opp_index]]

                        # get density at neighbour points
                        rho_c = neighbour_c._compute_zeroth_moment()
                        rho_f = neighbour_f._compute_zeroth_moment()
                        rho_oe = neighbour_oe._compute_zeroth_moment()

                        # interpolate
                        rho_help = (3.0 * (rho_self + rho_c) - rho_f - rho_oe) / 4.0
                        rho_help = rho_self  # hard coded by FS, too lazy to interpolte 3 points in par version
                        rho_self = rho_help

                        mult = 1.0

                    # compute P from sigma
                    rho_help_old = self.rhobd[idx_f_miss[idx]]
                    divu_old = self.divu[idx_f_miss[idx]]
                    divu_new = divu_old - (rho_help - rho_help_old) / rho_help
                    self.divu[idx_f_miss[idx]] = divu_new
                    self.rhobd[idx_f_miss[idx]] = rho_help
                    for aa in range(2):
                        for bb in range(2):
                            P_bd_2[aa][bb] = (
                                -mult * edge_factor * sigma_2[aa][bb] + ident_mat[aa][bb] * (lambda_ - mue_) * divu_new
                            )

                    # P^n
                    for aa in range(2):
                        for bb in range(2):
                            P_n_bd_2[aa][bb] = P_bd_2[aa][bb] - rho_self * (b**2) * ident_mat[aa][bb]

                    # current grid velocity
                    c_current = c[idx_f_miss_bar[idx]]

                    # phase change bounce back
                    self.f_temp[idx_f_miss[idx]] = (
                        0.0 - self.f_coll[idx_f_miss_bar[idx]] + 2 * weights[idx_f_miss_bar[idx]] * rho_self
                    )

                    # add density- and stress-dependent term (quasi-equilibrium-distribution-function)
                    for oo in range(2):
                        for pp in range(2):
                            self.f_temp[idx_f_miss[idx]] = self.f_temp[idx_f_miss[idx]] + 2 * weights[
                                idx_f_miss_bar[idx]
                            ] * P_n_bd_2[oo][pp] * (c_current[oo] * c_current[pp] - ident_mat[oo][pp] * (b**2)) / (
                                2 * b**4
                            )
        else:
            pass

    def _compute_t_ext(self, computation):
        """
        computes total force on cell exerted from boundary tractions, and stores it to self.BoundaryDataBalanceOfMomentum
        :return:
        """
        boundary_names = self.VolumeSurfaceMeasure[1][0]
        boundary_surface_measure = self.VolumeSurfaceMeasure[1][1]

        total_traction_forceX = 0.0
        total_traction_forceY = 0.0

        i = 0
        for boundary_name in boundary_names:
            if computation.BoundaryConditions[boundary_name].BCType in [
                "Neumann2Order",
                "Neumann",
            ]:
                current_tractionX_at_boundary_name = (
                    computation.BoundaryConditions[boundary_name].CurrentValue[0] * boundary_surface_measure[i]
                )
                current_tractionY_at_boundary_name = (
                    computation.BoundaryConditions[boundary_name].CurrentValue[1] * boundary_surface_measure[i]
                )
                total_traction_forceX = total_traction_forceX + current_tractionX_at_boundary_name
                total_traction_forceY = total_traction_forceY + current_tractionY_at_boundary_name
            i += 1

        return [total_traction_forceX, total_traction_forceY]

    def _compute_sum_of_missing_distribution_functions(self):
        """
        apply after streaming and collision
        :return:
        """
        # indices_of_missing_distribution_functions = self._compute_indices_of_missing_distribution_functions()
        f_sum = 0
        for i in range(1, 9):
            if self.stream_keys[i] in self.idx_f_miss:
                f_sum += self.f[i]
        return f_sum

    def _compute_indices_of_missing_distribution_functions(self):
        @cache
        def opposite_idx(idx):
            if idx in _indices_plus_two:
                return idx + 2
            elif idx in _indices_minus_two:
                return idx - 2

        idx_f_miss = []
        idx_f_miss_bar = []
        for i, key in enumerate(self.neighbor_keys):
            if key not in self.Neighbors.keys():
                missing_index = self.stream_keys[i + 1]
                idx_f_miss.append(missing_index)
                idx_f_miss_bar.append(opposite_idx(missing_index))
        return idx_f_miss, idx_f_miss_bar

    def initialise_distribution_functions_and_fields(self, computation):
        """
        initializes distribution functions from fields, fields from *.init file, PPData may be written here
        :param computation:
        :return: initial w, (wdot), f, f_temp
        """

        params = computation.Parameters
        rho0 = params["rho0h"]
        b = params["csh"]
        cc = params["ch"]
        c = LatticePointD2Q9_NavierEquation_Ansumali._compute_c(cc)
        c_tensors = LatticePointD2Q9_NavierEquation_Ansumali._compute_c_tensors(cc)

        self._initialize_fields(computation)  # so wdot is available
        j0 = rho0 * self.wdot[0]
        j1 = rho0 * self.wdot[1]

        for i in range(9):  # from equilibrium distribution
            Ci = c[i]
            j0TimesCi = j0 * Ci[0] + j1 * Ci[1]
            CiCi = c_tensors[i]
            OneTimesOne = 2.0
            OneTimesCiCi = CiCi[0][0] + CiCi[1][1]

            self.f[i] = self.weights[i] * (
                rho0 + j0TimesCi / b**2 + 1 / (2 * b**4) * (-rho0 * b**2 * OneTimesCiCi + rho0 * b**4 * OneTimesOne)
            )

    def compute_w(self, computation):
        """Intregrate the displacement and set lots of other output data,
        :param computation:
        :return:
        """
        # TODO: re-dimensionalize consistently

        # lattice = computation.Lattice
        params = computation.Parameters
        cc = params["ch"]
        rho_ = params["rho0h"]
        mu_ = params["mue"]
        lambda_ = params["lambda"]
        nu_ = 0.5 * lambda_ / (lambda_ + mu_)

        # density
        rho = self._compute_zeroth_moment()
        self.rho = rho
        if "rho" in self.PPData:
            # TODO: re-dimensionalize
            self.PPData["rho"] = rho

        # momentum
        source = self._compute_source_s(computation)
        first_moment = self._compute_first_moment(cc=cc)
        j_tpdt = self._compute_j(first_moment, source, params["dth"])
        if "j" in self.PPData:
            # TODO: re-dimensionalize
            self.PPData["j"] = [*j_tpdt, 0.0]

        # compute the Cauchy stress from second moment
        if "sigma" in self.PPData:
            second_moment = self._compute_second_moment(computation=computation)
            sigma = self.compute_sigma(la=lambda_, mue=mu_, rho0=rho_, mom_flux=second_moment)
            self.PPData["sigma"] = sigma
            # self.PPData["sigma"] = sigma * mu_ * params["wr"] / params["L"]

        if "sigma_vm" in self.PPData:
            # write von Mises Stress
            second_moment = self._compute_second_moment(computation=computation)
            sigma = self.compute_sigma(la=lambda_, mue=mu_, rho0=rho_, mom_flux=second_moment)
            sigma_vm = np.sqrt(
                (1.0 - nu_ + nu_**2) * (sigma[0] ** 2 + sigma[1] ** 2)
                - (1.0 + 2.0 * nu_ - 2.0 * nu_**2) * sigma[0] * sigma[1]
                + 3.0 * sigma[2] ** 2
            )
            self.PPData["sigma_vm"] = sigma_vm
            # self.PPData["sigma_vm"] = sigma_vm * mu_ * params["wr"] / params["L"]

        # velocity
        wdot_prev = self.wdot
        self.wdot[0] = j_tpdt[0] / self.rho
        self.wdot[1] = j_tpdt[1] / self.rho
        if "wdot" in self.PPData:
            # TODO: re-dimensionalize
            self.PPData["wdot"] = [*self.wdot, 0.0]

        # displacement
        self.w = computation.IntegrationRule.integrateW(self.w, self.wdot, wdot_prev)
        if "w" in self.PPData:
            # TODO: re-dimensionalize
            self.PPData["w"] = [*self.w, 0.0]

        # internal LB quantities
        if "f" in self.PPData:
            self.PPData["f"] = self.f
        if "feq" in self.PPData:
            self.PPData["feq"] = self.f_eq
        if "kinStress" in self.PPData:
            kinStress = self._compute_second_moment(cc=cc)
            self.PPData["kinStress"] = np.asarray(
                [kinStress[0, 0], kinStress[1, 1], kinStress[0, 1]]
            )

    def _initialize_fields(self, computation):
        """initialises_fields at lattice points only, needs to be done since gradient requires initial fields at each neighbor
        :param computation:
        :return:
        """
        params = computation.Parameters
        if (
            computation.Lattice.InitialFieldsW is not None
            and len(computation.Lattice.InitialFieldsWdot[0]) == 2
            and self.wdot[0] is None
        ):  # has not been written yet
            w = computation.Lattice.InitialFieldsW[self.ID] / (params["cs"] * params["wr"]) * params["L"]
            wdot = computation.Lattice.InitialFieldsWdot[self.ID] / params["wr"]
        else:
            wdot = [0.0, 0.0]
            w = [0.0, 0.0]
        self.w = w
        self.wdot = wdot
        self.rho = params["rho0h"]

    def compute_sigma(
        self,
        # computation=None,
        # lattice=None,
        la=None,
        mue=None,
        rho0=None,
        mom_flux=None,
    ):
        """
        :param self:
        :param target_value:
        :return:
        """

        # if not (la and mue and rho0):
        #     rho0 = computation.Parameters["rho0h"]
        #     la = computation.Parameters["lambdah"]
        #     mue = computation.Parameters["mueh"]

        sigma = np.empty(3)
        sigma[0] = -mom_flux[0, 0] + (mue - la) * (self.rho - rho0) / rho0
        sigma[1] = -mom_flux[1, 1] + (mue - la) * (self.rho - rho0) / rho0
        sigma[2] = -mom_flux[0, 1]

        return sigma

    @staticmethod
    def _compute_j(first_moment, source_s, dt):
        j0 = first_moment[0] + dt / 2.0 * source_s[0]
        j1 = first_moment[1] + dt / 2.0 * source_s[1]
        return j0, j1

    def compute_equilibrium_distribution(self, computation):
        """computes equilibrium distribution functions, Chopard style
        :param computation:
        :return:
        """

        params = computation.Parameters

        b = params["csh"]
        cc = params["ch"]
        dt = params["dth"]

        b_2 = b**2
        b_4 = b**4

        c = LatticePointD2Q9_NavierEquation_Ansumali._compute_c(cc=cc)
        c_tensors = LatticePointD2Q9_NavierEquation_Ansumali._compute_c_tensors(cc)
        source = self._compute_source_s(computation)
        first_moment = self._compute_first_moment(cc=cc)
        j = self._compute_j(first_moment, source, dt)

        P = self._compute_second_moment(cc=cc)
        P_trace = P[0][0] + P[1][1]

        for i in range(0, 9):
            jTimesCi = j[0] * c[i][0] + j[1] * c[i][1]
            CiCi = c_tensors[i]
            P_CiCi = P[0][0] * CiCi[0][0] + P[0][1] * CiCi[0][1] + P[1][0] * CiCi[1][0] + P[1][1] * CiCi[1][1]
            CiCi_trace = CiCi[0][0] + CiCi[1][1]
            OneTimesOne = 2.0

            self.f_eq[i] = self.weights[i] * (
                self.rho
                + jTimesCi / (b_2)
                + (0.5 * b_4) * (P_CiCi - b_2 * P_trace - self.rho * b_2 * CiCi_trace + self.rho * b_4 * OneTimesOne)
            )

    def collide(self, computation):
        """
        :param computation: relaxation time
        :return: update distribution
        """
        params = computation.Parameters

        b_sq = 1 / params["csh"]**2
        cc = params["ch"]
        dth = params["dth"]
        tauh = params["tauh"]

        c = LatticePointD2Q9_NavierEquation_Ansumali._compute_c(cc=cc)
        source = self._compute_source_s(computation)

        dth_tauh = dth / tauh
        dth_factor = dth * (1 - dth / (2 * tauh))

        for i in range(9):
            CiTimesS = c[i][0] * source[0] + c[i][1] * source[1]
            Si = self.weights[i] * b_sq * CiTimesS
            self.f_coll[i] = (
                self.f[i]
                - dth_tauh * (self.f[i] - self.f_eq[i])
                + dth_factor * Si
            )

        # prototype for vectorized computation; seems slower than loop
        # cis = c @ source * self.weights * b_sq
        # self.f_coll = self.f - dth / tauh * (self.f - self.f_eq) + dth * (1 - dth / (2 * tauh)) * cis

    def stream(self):
        """
        :return: update distribution functions at neighbors after streaming
        """
        self.f_temp[0] = self.f_coll[0]
        for idx, n_key in enumerate(self.neighbor_keys, start=1):
            if n_key in self.Neighbors:
                self.Neighbors[n_key].f_temp[idx] = self.f_coll[idx]

    def update_distribution_function_after_streaming(self):
        """
        f_temp necessary to prevent overwriting f
        :return: updates f
        """
        for i in range(self.no_f):
            self.f[i] = self.f_temp[i]
            self.f_temp[i] = None
            self.f_coll[i] = None

    def regularize_precollision(self, params: dict[str, float]) -> None:
        """
        :return: regularize precollision distribution functions
        """
        cc = params["ch"]

        mom_flux = self._compute_second_moment(cc=cc)
        mom_flux_eq = self._compute_second_moment_eq(cc=cc)
        mom_flux_neq = mom_flux - mom_flux_eq

        ctimesc = LatticePointD2Q9_NavierEquation_Ansumali._compute_c_tensors(cc)
        q_tensor = ctimesc - cc**2 * np.eye(2)

        w = self.weights

        for i in range(0, 9):
            reg = 0.5 * w[i] / cc**4 * tensor.tensordot(q_tensor[i], mom_flux_neq)
            self.f_eq[i] += reg


class LatticePointD2Q9_TRT_NavierEquation_Ansumali(LatticePointD2Q9_NavierEquation_Ansumali):
    @staticmethod
    def compute_dependent_parameters(params: dict, cell_size: float, *, tau_sym: bool = True):
        # get params from SRT
        # super needs type arguments relating to this class explicitly, because this is a staticmethod
        params = super(__class__, __class__).compute_dependent_parameters(params, cell_size)

        def parse_omega(par: dict, tau_sym: bool) -> tuple[float]:
            if "tau+" in par and "tau-" in par:
                tau_p = par["tau+"] * par["dth"]
                tau_m = par["tau-"] * par["dth"]
                return 1 / tau_p, 1 / tau_m

            tau_p = par.get("tauh", 0.55)
            omega_plus = 1 / tau_p
            magic_par = par.get("magic_parameter", 0.18)

            omega_minus = (1 - omega_plus * params["dth"]) / (
                omega_plus * params["dth"] ** 2 * (magic_par - 1) + params["dth"]
            )

            if not tau_sym:
                # swap: get symmetric tau (omega +) from Lambda instead
                omega_plus, omega_minus = omega_minus, omega_plus
            return omega_plus, omega_minus

        omega_plus, omega_minus = parse_omega(params, tau_sym=True)

        params["omega_symh"] = omega_plus
        params["omega_asymh"] = omega_minus

        return params

    def __init__(self, x=None, y=None, z=None, number=None):
        super().__init__(x, y, z, number)

    def collide(self, computation) -> None:
        params = computation.Parameters

        b = params["csh"]
        cc = params["ch"]
        dth = params["dth"]
        dt2 = dth * 0.5
        o_symm = params["omega_symh"]
        o_asym = params["omega_asymh"]

        c = LatticePointD2Q9_NavierEquation_Ansumali._compute_c(cc=cc)
        source = self._compute_source_s(computation)

        def forcing(i):
            CiTimesS = c[i][0] * source[0] + c[i][1] * source[1]
            Si = self.weights[i] / b**2 * CiTimesS
            return Si

        def collide_trt(i, j):
            self.f_coll[i] = (
                self.f[i]
                - o_symm * dt2 * (self.f[i] + self.f[j] - self.f_eq[i] - self.f_eq[j])
                - o_asym * dt2 * (self.f[i] - self.f[j] - self.f_eq[i] + self.f_eq[j])
                + 2 * dt2 * (1 - o_asym * dt2) * forcing(i)
            )

        self.f_coll[0] = (
            self.f[0] - o_symm * (self.f[0] - self.f_eq[0]) * dth + (1 + o_asym * dth / 2) * forcing(0) * dth
        )

        ## with submethod
        # for q in (1, 2, 5, 6):
        #     o = q + 2  # opposite direction
        #     self.f_coll[q] = collide_trt(q, o)
        #     self.f_coll[o] = collide_trt(o, q)

        ## explicit
        # for q in (1, 2, 5, 6):
        #     o = q + 2       # opposite direction
        #     self.f_coll[q] = self.f[q] \
        #         - o_symm * dt2 * (self.f[q] + self.f[o] - self.f_eq[q] - self.f_eq[o]) \
        #         - o_asym * dt2 * (self.f[q] - self.f[o] - self.f_eq[q] + self.f_eq[o]) \
        #         + 2 * dt2 * (1 - o_asym * dt2) * forcing(q)

        #     self.f_coll[o] = self.f[o] \
        #         - o_symm * dt2 * (self.f[o] + self.f[q] - self.f_eq[o] - self.f_eq[q]) \
        #         - o_asym * dt2 * (self.f[o] - self.f[q] - self.f_eq[o] + self.f_eq[q]) \
        #         + 2 * dt2 * (1 - o_asym * dt2) * forcing(o)

        ## initial version
        for i in (1, 2, 5, 6):
            self.f_coll[i] = (
                self.f[i]
                - o_symm / 2 * (self.f[i] - self.f_eq[i] + self.f[i + 2] - self.f_eq[i + 2]) * dth
                - o_asym / 2 * (self.f[i] - self.f_eq[i] - self.f[i + 2] + self.f_eq[i + 2]) * dth
                + (1 - o_asym * dth / 2) * forcing(i) * dth
            )

            # opposite direction
            self.f_coll[i + 2] = (
                self.f[i + 2]
                - o_symm / 2 * (self.f[i + 2] - self.f_eq[i + 2] + self.f[i] - self.f_eq[i]) * dth
                - o_asym / 2 * (self.f[i + 2] - self.f_eq[i + 2] - self.f[i] + self.f_eq[i]) * dth
                + (1 - o_asym * dth / 2) * forcing(i + 2) * dth
            )


class LatticePointD2Q9_NavierEquation_Ansumali_Fracture(
    LatticePointD2Q9_NavierEquation_Ansumali, LatticePointD2_mechanics_planestrain
):
    # FD_ACC_DEFAULT = 4
    # LOW_ORDER_BOUNDARY = False

    def __init__(self, x=None, y=None, z=None, number=None):
        super().__init__(x, y, z, number)

        self.u = np.zeros(3)
        self.grad_u = np.zeros((3, 3))

        self.momentum = np.zeros(3)
        self.momentum_dot = np.zeros(3)
        self.grad_j = np.zeros((3, 3))

        self.cauchy = np.zeros((3, 3))
        self.eshelby = np.zeros((3, 3))

    def compute_eq_momentum(self, cc=1.0, **kwargs):
        """compute the momentum j from eqquilibrium distributions"""
        c = LatticePointD2Q9_NavierEquation_Ansumali._compute_c(cc)
        j_eq = np.einsum("i,ij", self.f_eq, c)
        return j_eq

    def compute_momentum(self, dt=1.0, c=1.0, lame_mu=1.0, lame_lambda=1.0, rho=1.0, lattice=None):
        """compute the momentum j"""
        source = self._compute_source_s(mueh=lame_mu, lambdah=lame_lambda, rho0h=rho, lattice=lattice)
        first_moment = self._compute_first_moment(cc=c)
        j = self._compute_j(first_moment, source, dt)
        return j

    def compute_hamiltonian(self, rho=1.0, lame_mu=1.0, lame_lambda=1.0, grad_u=None):
        """compute the Hamiltonian, i.e. the energy density, at a lattice point
        and assign as attribute
        """
        # linear stain-stress relationship
        stress = self.cauchy
        if grad_u is None:
            e_mod, nu = self.elastic_moduli(lame_lambda, lame_mu)
            strain = (1 + nu) / e_mod * stress - nu / e_mod * stress.trace() * np.eye(3)
        else:
            strain = 0.5 * (grad_u + grad_u.T)

        # compute energy densities
        potential = 0.5 * np.tensordot(stress, strain)
        kinetic = 0.5 * np.inner(self.momentum, self.momentum) / rho

        self.hamiltonian = kinetic + potential

    def compute_eshelby(self, lattice, *args, **kwargs):
        """compute the Eshelby stress tensor at a lattice point
        and assign as attribute
        """
        eshelby = self.hamiltonian * np.eye(3) - np.einsum("ji,jk->ik", self.grad_u, self.cauchy)
        self.eshelby = eshelby

    def compute_poynting(self):
        """compute the energy flux (Poynting vector) at a lattice point
        and assign as attribute
        """
        density = self._compute_zeroth_moment()
        poynting = -1 / density * self.momentum.T @ self.cauchy
        self.poynting = poynting

    def disp_to_array(self):
        return np.array((self.w[0], self.w[1], 0))

    def compute_grad_disp(self, lattice):
        """compute displacement gradient"""

        grad_u = self.get_gradient_of_attribute(lattice, attr="u", dim=3)
        grad_u_3d = np.zeros((3, 3))
        grad_u_3d[:2, :2] = grad_u[:2, :2]
        return grad_u_3d

    def compute_grad_disp_from_momentum(self, lattice, dt=1.0):
        grad_u_prev = np.copy(self.grad_u)
        grad_j_prev = np.copy(self.grad_j)
        grad_j_2d = self.get_gradient_of_attribute(lattice, attr="momentum")  # L

        # ensure 3x3 tensor
        grad_j = np.zeros((3, 3))
        grad_j[:2, :2] = grad_j_2d[:2, :2]

        # predict: 1st order Euler
        grad_u = grad_u_prev + dt * grad_j @ grad_u_prev

        # correct: 2nd order implicit
        grad_u = grad_u_prev + 0.5 * dt * (3 * grad_j @ grad_u - grad_j_prev @ grad_u_prev)

        return grad_u

    def _set_tensors(
        self,
        lattice,
        params,
        pts_visited=set(),
        *,
        recompute=False,
    ):
        """set attributes as numpy Arrays

        u; grad_u; momentum; momentum_prev; cauchy
        """

        # get points from keys of coefficients; do not recompute,
        # neighbors in extended domain have been reprocessed before or are far away from crack
        coeff_pts = self.get_point_set_from_gradient_coefficients(lattice, recompute=recompute)
        # displacement needed for gradient at coefficient-points
        for pt in coeff_pts - pts_visited:  # includes self
            pt.u = pt.disp_to_array()

        # grad_u = self.compute_grad_disp_from_momentum(lattice, dt=dt)
        grad_u = self.compute_grad_disp(lattice)
        self.grad_u = grad_u

        # setting the gradient here leads to unexplainable (indeterministic) results
        # grad_j = self.get_gradient_of_attribute(lattice, attr='momentum')
        # self.grad_j = grad_j

        # get momentum, keep previous value
        self.momentum_prev = np.copy(self.momentum)
        self._set_momentum(lattice, params)

        # set Cauchy stress as symmetric 3x3-Array
        self._set_cauchy(params)
        # self.set_cauchy(lattice, lame_mu, lame_lambda, grad_u)

    def _set_momentum(self, lattice, params):
        dt = params["dth"]
        c = params["ch"]
        mu = params["mu"]
        la = params["lambda"]
        rho = params["rho0"]

        j = self.compute_momentum(dt, c, mu, la, rho, lattice=lattice)
        # j = self.compute_eq_momentum(cc=c)
        self.momentum[0] = j[0]
        self.momentum[1] = j[1]

    def _set_cauchy(self, params):
        mom_flux = self._compute_second_moment(cc=params["ch"])

        la = params["lambda"]
        mu = params["mu"]
        rho0 = params["rho0"]

        s11, s22, s12 = self.compute_sigma(
            la=la,
            mue=mu,
            rho0=rho0,
            mom_flux=mom_flux,
        )
        # s11, s22, s12 = self.compute_sigma(
        #     lattice=lattice, la=lame_lambda, mue=lame_mu, grad_w=grad_u
        # )
        sigma = np.zeros((3, 3))
        sigma[0, 0] = s11
        sigma[1, 1] = s22
        sigma[0, 1] = sigma[1, 0] = s12

        self.cauchy = sigma

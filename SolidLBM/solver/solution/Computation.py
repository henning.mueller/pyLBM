import os
import shutil
import logging
from pathlib import Path
from typing import Optional
import numpy as np

import SolidLBM.solver.solution.Lattice as Lattice_Module
import SolidLBM.solver.solution.BoundaryCondition as BoundaryCondition_Module
import SolidLBM.solver.solution.CrackSet as CrackSet_Module
import SolidLBM.solver.solution.CrackFree as CrackFree_Module
from SolidLBM.util.Output import Output_Interface

import SolidLBM.util.geometry.Line as Line_Module

import SolidLBM.solver.solution.LatticePointD2Q5_NavierEquation_Chopard as LatticePointD2Q5_NavierEquation_Chopard_Module
import SolidLBM.solver.solution.IntegrationRule_NewmarkExplicit as IntegrationRule_NewmarkExplicit_Module
import SolidLBM.solver.solution.IntegrationRule_Trapezoidal as IntegrationRule_Trapezoidal_Module
# import SolidLBM.solver.solution.IntegrationRule_EulerBackward as IntegrationRule_EulerBackward_Module

from SolidLBM.util.tools import coord_array_from_points, data_array_from_points


class Computation(object):
    """
    represents a computation run, i.e. lattice with parameters and commands
    """

    # define flags with boolean values (and ensure existence with common case)
    Flags = {
        "dynamic_crack": False,
        "link_based_crack": False,
        "aux_point_switch": False,
        "output_w_dot": False,
        "precise_key": False,  # determines whether displacement is extrapolated at auxiliary points with second order accuracy (true), just for post-processing
        "verbose": False,
    }

    def __init__(
        self, working_directory: Path | str, name="", output_path: Optional[Path | str] = "output/", *, verbose=False
    ):
        """
        constructor
        :param name: the name of the computation
        :param working_directory:
        """
        self.Name = name  # the name of the computation
        self.WorkingDir: Path = Path(working_directory)

        self.Parameters = dict()  # global parameters of this computation
        self.read_parameter_file()  # reads global parameters from text file
        self._process_flags(verbose=verbose)

        self.BoundaryConditions = []  # the boundary conditions of this computation
        self.read_boundary_condition_file()  # reads boundary conditions from text file

        # the discretized domain
        self.Lattice = Lattice_Module.Lattice(
            AuxPointsSwitch=self.Flags["aux_point_switch"],
            DynCrackSwitch=self.Flags["dynamic_crack"],
        )
        self.Lattice.LatticeType = self.Parameters["point_type"]  # has to be done here to be able to create lattice
        self.Lattice.read_lattice_from_file(self, in_file_path=self.WorkingDir / f"{self.Name}.msh")
        self.compute_dependent_parameters()  # certain parameters can be derived from global parameters

        self.IntegrationRule = self.set_integration_rule()  # usually only the rate of the primary fields is determined and needs to be integrated by an integration rule that is used in command_integrate
        self.integralswitch = False  # ?

        self.command_sequence = []  # the command sequence required to run the computation
        self.read_batch_file()

        # OUTPUT, more detailed settings through setup_output
        output_path = Path(output_path)
        self.vtk_path = self.WorkingDir / output_path
        self.output_path = self.WorkingDir / output_path
        self.Output = Output_Interface(
            output_dir=self.vtk_path,
        )

        self.current_time_h = 0.0  # normalized time, i.e. time in lattice units
        self.current_time = 0.0  # time in input dimensions

        # Prepare handling of boundary conditions
        if BoundaryCondition_Module.BoundaryCondition.macroscopicNonLatticeConforming in self.Lattice.BCTypes:
            self.Lattice.allocate_bc_matrix(self)
        if (
            BoundaryCondition_Module.BoundaryCondition.mesoscopicNonLatticeConforming in self.Lattice.BCTypes
            or self.Flags["dynamic_crack"]
        ):
            # read areas etc for BC implementation; also needed for conf. forces at crack tip
            self.Lattice.read_alt_bc_from_file(self, in_file_path=self.WorkingDir / f"{self.Name}.bc_alt")

        # TODO : replace by Flag['precise_key']
        if BoundaryCondition_Module.BoundaryCondition.localLatticeConforming not in self.Lattice.BCTypes:
            # self.PreciseKey = Computation.Flags['precise_key'] # 2nd order approximation of displacement field at boundary aux points
            if self.Flags["precise_key"]:  # 2nd order approximation of displacement field at boundary aux points
                self.Lattice.compute_interpolation_quantities_for_precise_aux_point_field(self)

        # TODO Flag all lattice points in a circle domain for computation of configurational forces?
        if "circ_x" in self.Parameters:
            for pt in self.Lattice.Points:
                if (pt.x - self.Parameters["circ_x"]) ** 2 + (pt.y - self.Parameters["circ_y"]) ** 2 <= self.Parameters[
                    "circ_r"
                ] ** 2:
                    pt.SourceIdent = 1

        # set all flags to false TODO
        LatticePointD2Q5_NavierEquation_Chopard_Module.LatticePointD2Q5_NavierEquation_Chopard.set_all_flags_false()

        # initialize crack sets if required
        def _init_crack_set():
            import tomli

            conf_file = self.WorkingDir / f"{self.Name}.toml"
            with open(conf_file, mode="rb") as f:
                config_all = tomli.load(f)
                crack_criterion = config_all["DynCrack"].get("criterion", "none")
            self.Flags["link_based_crack"] = "link" in crack_criterion

            if self.Flags["link_based_crack"]:
                return CrackFree_Module.CrackFree(self)
            else:
                return CrackSet_Module.CrackSet(self)

        if self.Flags["dynamic_crack"]:
            self.Crackset = _init_crack_set()

    def _init_lattice(self):
        self.Lattice.read_initial_field_data(self.WorkingDir / f"{self.Name}.init")
        if self.integralswitch:
            if 3 not in self.Lattice.BCTypes:
                self.Lattice.read_alt_bc_from_file(self, in_file_path=self.WorkingDir / f"{self.Name}.bc_alt")
            self.Lattice.initialize_default_volumes()
        self.Lattice.initialize_distribution_functions_all_points(self)

    def create_directory(self, attr):
        dir_path: Path = self.WorkingDir / getattr(self, attr + "_path")
        if os.path.isdir(dir_path):
            shutil.rmtree(dir_path, ignore_errors=True)
            if not os.path.isdir(dir_path):
                os.mkdir(dir_path)
        else:
            os.mkdir(dir_path)

    def setup_output(
        self,
        quantities: list[str] = ["w", "sigma_vm"],
        time_interval: Optional[float] = None,
        format: str = "vtu",
        # output_path: Path | str = "output",
    ) -> None:
        """Create the vtk object for the output and set the requested quantities.
        Also initialises the output_dict in Lattice with the respective keys.

        Can also set the intervals to write output at via 'output_at_interval'.
        """

        if not self.Output:
            self.Output = Output_Interface(
                output_dir=self.vtk_path,
                format=format,
            )
        else:
            self.Output.fileformat = format

        Acoord = coord_array_from_points(self.Lattice.Points)
        self.Output._create_vertices(Acoord)

        self.Output.request_quantities(quantities)
        for pt in self.Lattice.Points:
            for key in quantities:
                # set keys of requested quantities in dict
                pt.PPData[key] = np.nan
        if time_interval is not None:
            self.Output.request_intervals(time_interval, self.Parameters["max_time"])

    def set_integration_rule(self):
        # TODO: set IntegrationRule from input or in LatticePoint-Classes
        lattice_type = self.Lattice.LatticeType

        if "Chopard" in lattice_type:
            return IntegrationRule_NewmarkExplicit_Module.IntegrationRule_NewmarkExplicit(self.Parameters)
        elif "Ansumali" in lattice_type:
            return IntegrationRule_Trapezoidal_Module.IntegrationRule_Trapezoid(self.Parameters)
            # return IntegrationRule_EulerBackward_Module.IntegrationRule_EulerBackward(self.Parameters)

    def read_boundary_condition_file(self):
        with open(self.WorkingDir / f"{self.Name}.bc", "r") as my_file:
            my_line = my_file.readline()  # read header info
            while "#" in my_line:
                # my_strings = my_line.split()
                # do sth. with header info
                my_current_pos = my_file.tell()
                my_line = my_file.readline()

            my_file.seek(my_current_pos)
            for my_line in my_file:
                if "#" not in my_line:
                    tmp_bc = BoundaryCondition_Module.BoundaryCondition()
                    tmp_bc.read_boundary_condition_from_string(my_line)
                    self.BoundaryConditions.append(tmp_bc)

    def read_parameter_file(self):
        string_arguments = ["point_type"]
        flag_arguments = list(Computation.Flags.keys())

        with open(self.WorkingDir / f"{self.Name}.par", "r") as my_file:
            for my_line in my_file:
                my_strings = my_line.split()
                key_string = my_strings[0]

                if key_string not in string_arguments + flag_arguments:
                    self.Parameters[key_string] = float(my_strings[2])
                elif key_string in string_arguments:
                    self.Parameters[my_strings[0]] = my_strings[2]
                elif key_string in flag_arguments:
                    Computation.Flags[key_string] = eval(my_strings[2])
                else:
                    pass

    def _process_flags(self, *, verbose: Optional[bool] = None) -> None:
        if verbose is None:
            verbose = Computation.Flags["verbose"]
        Computation.Flags["verbose"] = verbose

        # w_dot only for Guangwu, but does not work with AuxPoints
        # TODO : make it work with Auxpoints (-> extrapolation needed)
        # TODO : not needed anymore, since output handled differently
        if self.Flags["output_w_dot"] and "Guangwu" not in self.Parameters["point_type"]:
            self.Flags["output_w_dot"] = False
            logging.info("Not Guangwu-type Points, Flag 'output_w_dot' ignored")

        if self.Flags["output_w_dot"] and self.Flags["aux_point_switch"]:
            self.Flags["aux_point_switch"] = False
            logging.warning("Cannot output w_dot for AuxPoints; AuxPoints deactivated")

    def compute_dependent_parameters(self):
        point_class = Lattice_Module.Lattice._point_classes_mapping[self.Parameters["point_type"]]
        self.Parameters = point_class.compute_dependent_parameters(self.Lattice.CellSize, self.Parameters)

    def read_batch_file(self):
        def append_output_local(in_strings="", computation=None):
            # special treatment OUTPUT_LOCAL. allocate Lattice.RequestedOutPutData, can only be at level 1
            tmp_dict = dict()
            tmp_list = []

            # determine at which index the node numbers start for input
            node_numbers_start_index = -1
            i = 0
            for string in in_strings:  # find first index with numerical input -> start of node numbers
                if not string.isalpha() and (string != "OUTPUT_LOCAL"):
                    node_numbers_start_index = i
                    break
                i += 1

            for i in range(
                2, node_numbers_start_index
            ):  # allocates a dictionary with name of the field to be written to output
                tmp_dict[in_strings[i]] = None

            # NODE NUMBER MODE
            if in_strings[1] == "Number":  # all folowing numbers are node numbers
                for i in range(node_numbers_start_index, len(in_strings)):
                    tmp_list.append(computation.Lattice.Points[int(in_strings[i])])
            # COORDINATE MODE
            elif in_strings[1] == "Coordinate":  # all following numbers are x/y coordinates of nodes
                # translate coordinates into node numbers
                for i in range(node_numbers_start_index, len(in_strings), 2):
                    tmp_x = float(in_strings[i])
                    tmp_y = float(in_strings[i + 1])
                    matching_points = computation.Lattice.find_matching_point(
                        computation=computation, x=tmp_x, y=tmp_y
                    )  # can be more than one point (points might be identical)

                    # try:
                    #     matching_points = computation.Lattice.find_matching_point(
                    #         computation=computation, x=tmp_x, y=tmp_y
                    #     )  # can be more than one point (points might be identical)
                    # except:
                    #     matching_points = []
                    if matching_points:
                        for point in matching_points:
                            tmp_list.append(point)

            # LINE MODE
            elif in_strings[1] == "Line":  # x / y coordinates of two points
                coords = in_strings[node_numbers_start_index:]
                tmp_list = []
                i = 0
                while i < len(coords):
                    x1, y1, x2, y2 = coords[i : i + 4]
                    tmp_line = Line_Module.Line(p1=(x1, y1), p2=(x2, y2))
                    tmp_list += [
                        pt
                        for pt in computation.Lattice.Points
                        if tmp_line.compute_shortest_distance_to_point(pt)[0] <= computation.Parameters["hh"] / 4
                    ]
                    i += 4
            for key in tmp_dict.keys():  # all nodes are included for all field data
                tmp_dict[key] = tmp_list

            return tmp_dict  # a dictonary that contains all nodes for which output should be stored for "field" tmp_dict["field"] = list of nodes

        valid_batch_commands = (
            "APPLY_BOUNDARY_CONDITION",
            "ACCELERATION",
            "UPDATE_BOUNDARY_CONDITION",
            "UPDATE_DISTRIBUTION_FUNCTIONS",
            "UPDATE_ROTATION_DILATATION_INTERIOR",
            "COLLISION",
            "APPLY_BOUNDARY_CONDITION",
            "CORRECT",
            "EQUILIBRIUM",
            "INITIAL",
            "INTERACTIVE",
            "INTEGRATE",
            "INTEGRATE_EXPLICIT",
            "OUTPUT_LOCAL",
            "OUTPUT_CRACK_TO_CSV",
            "STREAM",
            "TIME",
            "VTU",
            "VTK",
            "DO",
            "END",
            "POSTPROCESSING_DATA",
            "INTEGRAL_DATA_TO_CSV",
            "DYNAMIC_CRACK",
        )
        level = 0
        tmp_command_sequence_lvl1 = []
        tmp_command_sequence_lvl2 = []
        tmp_command_sequence_lvl3 = []
        repeats = [0, 0, 0]  # dimension = maximal level

        filepath = self.WorkingDir / f"{self.Name}.batch"
        if not filepath.is_file():
            # do nothing if batch file does not exist -> call commands directly
            return

        with open(filepath, "r") as my_file:
            for my_line in my_file:
                my_strings = my_line.split()

                if my_strings and "#" not in my_strings:  # skip empty lines and comments
                    command = my_strings[0]
                    if command in valid_batch_commands:
                        if command == "DO":  # next lines should be written in a loop
                            level = level + 1
                            repeats[level - 1] = int(my_strings[1])
                        elif command == "END":
                            if level == 1:
                                for i in range(0, repeats[level - 1]):
                                    self.command_sequence.append(tmp_command_sequence_lvl1)
                                tmp_command_sequence_lvl1 = []
                            elif level == 2:
                                for i in range(0, repeats[level - 1]):
                                    tmp_command_sequence_lvl1.append(tmp_command_sequence_lvl2)
                                tmp_command_sequence_lvl2 = []
                            elif level == 3:
                                for i in range(0, repeats[level - 1]):
                                    tmp_command_sequence_lvl2.append(
                                        tmp_command_sequence_lvl3
                                    )  # append to next higher list
                                tmp_command_sequence_lvl3 = []  # reset

                            level = level - 1
                        elif command == "OUTPUT_LOCAL":
                            self.Lattice.RequestedLocalOutPutData = append_output_local(
                                in_strings=my_strings, computation=self
                            )  # write RequestedOutPutData dict()
                            if level == 0:
                                self.command_sequence.append(command)
                            elif level == 1:
                                tmp_command_sequence_lvl1.append(command)
                            elif level == 2:
                                tmp_command_sequence_lvl2.append(command)
                            elif level == 3:
                                tmp_command_sequence_lvl3.append(command)
                        else:
                            if command == "INTEGRAL_DATA_TO_CSV":
                                self.integralswitch = True
                            if level == 0:
                                self.command_sequence.append(command)
                            elif level == 1:
                                tmp_command_sequence_lvl1.append(command)
                            elif level == 2:
                                tmp_command_sequence_lvl2.append(command)
                            elif level == 3:
                                tmp_command_sequence_lvl3.append(command)

    def execute_command_sequence(self):
        # hotfix for legacy code
        self.setup_output(["w", "j"], format="vtk")
        
        for command in self.command_sequence:
            if type(command) is list:
                for command_lvl1 in command:
                    if type(command_lvl1) is list:
                        for command_lvl2 in command_lvl1:
                            if type(command_lvl2) is list:
                                for command_lvl3 in command_lvl2:
                                    self.execute_command(command_lvl3)
                            else:
                                self.execute_command(command_lvl2)
                    else:
                        self.execute_command(command_lvl1)
            else:
                self.execute_command(command)
            if self.current_time > self.Parameters["max_time"]:  # end loop at lowest level
                print("")
                break

    def execute_command(self, argument):
        switcher = {
            "ACCELERATION": self.command_acceleration,
            "APPLY_BOUNDARY_CONDITION": self.command_boun,
            "COLLISION": self.command_colli,
            "CORRECT": self.command_correct,
            "EQUILIBRIUM": self.command_equi,
            "INITIAL": self.command_init,
            "INTERACTIVE": self.command_interactive,
            "INTEGRATE": self.command_integrate,
            "INTEGRATE_EXPLICIT": self.command_integrate_explicit,
            "OUTPUT_LOCAL": self.command_output_local,
            "OUTPUT_CRACK_TO_CSV": self.command_output_crack,
            "STREAM": self.command_stre,
            "TIME": self.command_time,
            "UPDATE_BOUNDARY_CONDITION": self.command_update_bc,
            "UPDATE_DISTRIBUTION_FUNCTIONS": self.command_update_distribution_functions,
            "UPDATE_ROTATION_DILATATION_INTERIOR": self.command_update_rotation_dilatation_interior,
            "VTK": self.command_vtk,
            "VTU": self.command_vtu,
            "POSTPROCESSING_DATA": self.command_PPData,
            "DYNAMIC_CRACK": self.command_dyn_crack,
        }

        func = switcher.get(argument, lambda: print("Invalid Batch Command!"))
        func()

    def command_acceleration(self):
        self.Lattice.acceleration_all_points(self)

    def command_boun(self):
        self.Lattice.boun_all_points(self)

    def command_correct(self):
        self.Lattice.correct_psi_phi_f(self)

    def command_update_bc(self):
        for bc in self.BoundaryConditions:
            bc.compute_current_value(self)

    def command_update_distribution_functions(self):
        self.Lattice.update_distribution_function_after_streaming_all_points()

    def command_update_rotation_dilatation_interior(self):
        self.Lattice.update_rotation_dilatation_after_collision_and_streaming_at_interior_points(self)

    def command_colli(self):
        self.Lattice.collide_all_points(self)

    def command_reg_precoll(self):
        self.Lattice.regularize_all_points(self.Parameters)

    def command_recoll_corner(self):
        self.Lattice.recollide_corners(self)

    def command_equi(self):
        self.Lattice.compute_equilibrium_distribution_functions_all_points(self)

    def command_init(self):
        self._init_lattice()

    def command_interactive(self):
        print("to do")

    def command_integrate(self):
        self.Lattice.integrate_w_all_points(self)

    def command_integrate_explicit(self):
        self.Lattice.integrate_w_all_points_explicit(self)

    def command_output_local(self):
        self.Lattice.write_output_local(self)

    def command_output_crack(self):
        if self.Flags["dynamic_crack"]:
            self.Crackset.write_crack_tip_data(self.Lattice, self.current_time)
        else:
            pass

    def command_stre(self):
        self.Lattice.stream_all_points(self)

    def command_dyn_crack(self):
        # self.Lattice.handle_dynamic_cracks(self)
        aux, boun, propagation = self.Crackset.evaluate_crack_propagation(self.current_time, self.Lattice)
        self.Lattice.process_boun_points(self, aux, boun, propagation=propagation)

    def command_time(self):
        self.current_time_h = self.current_time_h + self.Parameters["dth"]
        if self.Lattice.LatticeType in [
            "D2Q5_HeatEquation",
            "D2Q5_PhasefieldFracture",
        ]:  # TODO keine Abfrage -> Ducktyping, berechne physikalische Zeit für Lattice Type
            self.current_time = self.current_time_h  # TODO provisorisch
        else:
            self.current_time = (
                self.current_time + self.Parameters["dth"] * self.Parameters["L"] / self.Parameters["cs"]
            )

        if self.Flags["verbose"]:
            print(f"\r  time: {self.current_time: > 4.3f} / {self.Parameters['max_time']: > 4.3f}", end="")

    def command_vtk(self):
        # self.Lattice.write_vtk(self)
        if not self.Output:
            self.setup_output()
        self.write_output()

    def command_vtu(self):
        """Starts the write_vtu method of the current Lattice"""
        # self.Lattice.write_vtu(self)
        if not self.Output:
            self.setup_output()
        self.write_output()

    def _write_output_info(self) -> None:
        """Write current vtk-number to terminal."""
        if self.Flags["verbose"]:
            print(f"  --  last output: {self.Output.output_nr: > 4d}", end="\r")

    def write_output(self) -> None:
        self._write_output_info()
        output_dict = data_array_from_points(self.Lattice.Points)
        self.Output.write_data(output_dict, name=self.Name)

    def write_output_at_interval(self) -> None:
        self.output_at_interval()

    def output_at_interval(self) -> None:
        """Write the requested output to .vtu- / .vtk-files.

        Only writes at requested intervals of simulation time and is meant to
        be called in every time iteration.
        """

        self._write_output_info()
        output_dict = data_array_from_points(self.Lattice.Points)
        self.Output.write_at_interval(self.current_time, output_dict, name=self.Name)

    def command_PPData(self):
        """Starts the compute_stressNstrain_all_point & compute_energydensities_all_points methods of the current Lattice"""
        self.Lattice.compute_stressNstrain_all_points(self)
import copy
import os
import sys
import logging
from collections.abc import Iterable
from typing import TypeVar

import numpy as np

import SolidLBM.util.geometry.Vector as Vector_Module
import SolidLBM.solver.solution.SystemOfEquations as SystemOfEquations_Module
import SolidLBM.solver.solution.LatticePointD2Q5_abstract as LatticePointD2Q5_abstract_Module
import SolidLBM.solver.solution.LatticePointD2Q9_abstract as LatticePointD2Q9_abstract_Module
import SolidLBM.solver.solution.LatticePointD2Q5_WaveEquation_Guangwu as LatticePointD2Q5_WaveEquation_Guangwu_Module
import SolidLBM.solver.solution.LatticePointD2Q5_WaveEquation_Chopard as LatticePointD2Q5_WaveEquation_Chopard_Module
import SolidLBM.solver.solution.LatticePointD2Q5_NavierEquation_Chopard as LatticePointD2Q5_NavierEquation_Chopard_Module

import SolidLBM.solver.solution.LatticePointD2Q5_HeatEquation as LatticePointD2Q5_HeatEquation_Module
import SolidLBM.solver.solution.LatticePointD2Q9_WaveEquation_Chopard as LatticePointD2Q9_WaveEquation_Chopard_Module
import SolidLBM.solver.solution.LatticePointD2Q9_NavierEquation_Ansumali as LatticePointD2Q9_NavierEquation_Ansumali_Module
import SolidLBM.solver.solution.LatticePointD2Q5_PhasefieldFracture as LatticePointD2Q5_PhasefieldFracture_Module

import SolidLBM.solver.solution.BoundaryCondition as BoundaryCondition_Module

from SolidLBM.mesher.mesh.Boundary import (
    compute_cell_volumes_and_areas_at_boundary_points,
)


Point = TypeVar("Point", bound=Vector_Module.Vector)


class Lattice(object):
    dx_computed = False

    # mapping of input strings to LatticePoint classes
    _point_classes_mapping = {
        "D2Q5_WaveEquation_Guangwu": LatticePointD2Q5_WaveEquation_Guangwu_Module.LatticePointD2Q5_WaveEquation_Guangwu,
        "D2Q5_WaveEquation_Chopard": LatticePointD2Q5_WaveEquation_Chopard_Module.LatticePointD2Q5_WaveEquation_Chopard,
        "D2Q9_WaveEquation_Chopard": LatticePointD2Q9_WaveEquation_Chopard_Module.LatticePointD2Q9_WaveEquation_Chopard,
        "D2Q5_NavierEquation_Chopard": LatticePointD2Q5_NavierEquation_Chopard_Module.LatticePointD2Q5_NavierEquation_Chopard,
        "D2Q9_NavierEquation_Ansumali": LatticePointD2Q9_NavierEquation_Ansumali_Module.LatticePointD2Q9_NavierEquation_Ansumali,
        "D2Q9_NavierEquation_Ansumali_Fracture": LatticePointD2Q9_NavierEquation_Ansumali_Module.LatticePointD2Q9_NavierEquation_Ansumali_Fracture,
        "D2Q9_TRT_NavierEquation_Ansumali": LatticePointD2Q9_NavierEquation_Ansumali_Module.LatticePointD2Q9_TRT_NavierEquation_Ansumali,
        # 'TRT_MomentChain': LatticePointD2Q9_NavierEquation_Ansumali_Module.LatticePointD2Q9_TRT_NavierEquation_Ansumali,
        "D2Q5_HeatEquation": LatticePointD2Q5_HeatEquation_Module.LatticePointD2Q5_HeatEquation,
        "D2Q5_PhasefieldFracture": LatticePointD2Q5_PhasefieldFracture_Module.LatticePointD2Q5_PhasefieldFracture,
    }

    _all_lattice_types = _point_classes_mapping.keys()

    def __init__(self, AuxPointsSwitch=False, DynCrackSwitch=False):
        self.Points = []
        self.LatticeType = ""
        self.DimOfW = None  # dimension of displacement field
        self.CellSize = 0.0
        self.InitialFieldsW = []
        self.InitialFieldsWdot = []
        self.tmpvalue_vs_w = []

        # for non-mesh conforming BC
        self.BoundaryPointsMatrixBC = []  # list of boundary points for implementation of non-mesh conforming BC
        self.InverseOfBoundaryPointsMatrix = None
        self.AuxPointsSwitch = AuxPointsSwitch
        if self.AuxPointsSwitch:
            # auxiliary points on non-mesh conforming boundaries. only for post-processing
            self.AuxPoints = []
        if DynCrackSwitch:
            self.meso_points_update = []
        self.BCMatrix = []
        self.BCRHS = []
        self.vtuCells = []
        self.BCTypes = []  # BCTypes specified in Lattice

        # dict with key = field_name, value = list of point numbers for which to ouput specified
        self.RequestedLocalOutPutData = dict()
        self.RequestedGlobalOutPutData = dict()

        self.CorrectionCounter = 0  # Only Navier

    def _boun_point_generator_(self):
        for pt in self.Points:
            if pt.BoundaryName:
                yield pt

    def acceleration_all_points(self, computation):
        # interior
        for lattice_point in self.Points:
            lattice_point.acceleration(computation=computation)

        # boundary
        for lattice_point in self.Points:
            lattice_point.acceleration_at_boundary_point(computation=computation)

    def find_matching_point(self, computation=None, x=0, y=0):
        """
        computes lattice points that are identical (or very close) to (x,y)
        :param computation:
        :param x:
        :param y:
        :return:
        """
        matching_points = []
        matching_points_ids = []
        matching_point = None
        for lattice_point in self.Points:
            tmp_vec = lattice_point - Vector_Module.Vector(x=x, y=y)
            if (
                np.sqrt(tmp_vec.x**2 + tmp_vec.y**2)
                < computation.Parameters["hh"] / 4.0
                and lattice_point.ID not in matching_points_ids
            ):
                matching_points.append(lattice_point)
                matching_points_ids.append(lattice_point.ID)
        if self.AuxPointsSwitch and matching_point is None:
            for aux_point in self.AuxPoints:
                tmp_vec = aux_point - Vector_Module.Vector(x=x, y=y)
                if (
                    np.sqrt(tmp_vec.x**2 + tmp_vec.y**2)
                    < computation.Parameters["hh"] / 4.0
                    and aux_point.ID not in matching_points_ids
                ):
                    matching_points.append(aux_point)
                    matching_points_ids.append(aux_point.ID)
        return matching_points

    def read_lattice_from_file(self, computation, in_file_path=""):
        """
        reads *.msh file an builds lattice data structure
        :param computation:
        :param in_file_path:
        :return:
        """

        def parse_point_type(lattice_type):
            if lattice_type in self._all_lattice_types:
                point_type = self._point_classes_mapping[lattice_type]
                if computation.Flags["dynamic_crack"] and "Ansumali" in lattice_type:
                    logging.info(
                        f"Dynamic fracture: changed lattice_type {lattice_type} to 'D2Q9_NavierEquation_Ansumali_Fracture'"
                    )
                    point_type = self._point_classes_mapping[
                        "D2Q9_NavierEquation_Ansumali_Fracture"
                    ]
            else:
                logging.error(
                    "Class Lattice/ Method create Lattice Error: Lattice Type not defined! Cannot create lattice!"
                )
                raise ValueError(f"LatticePoint-Class {lattice_type} not defined")

            self.DimOfW = 2 if "NavierEquation" in lattice_type else 1
            return point_type

        param_L = computation.Parameters["L"]

        point_type = parse_point_type(self.LatticeType)
        velocity_set = "D2Q9"

        with open(in_file_path, "r") as mesh_file:
            lines = mesh_file.readline()  # read header info
            while "#" in lines:
                str_entries = lines.split()
                if "CellSize" in str_entries:
                    self.CellSize = (
                        float(str_entries[3]) / computation.Parameters["L"]
                    )  # cell size relative to domain size
                if "D2Q5" in str_entries:
                    velocity_set = "D2Q5"
                my_current_pos = mesh_file.tell()
                lines = mesh_file.readline()

            mesh_file.seek(my_current_pos)

            for lines in mesh_file:  # create point objects
                str_entries = lines.split()
                tmp_point = point_type(
                    number=int(str_entries[0]),
                    x=float(str_entries[1]) / param_L,
                    y=float(str_entries[2]) / param_L,
                    z=float(str_entries[3]) / param_L,
                )
                self.Points.append(tmp_point)

            mesh_file.seek(my_current_pos)  # go back to start of file
            for lines in (
                mesh_file
            ):  # read neighbor information/ distances to boundary for boundary points
                if velocity_set == "D2Q9":  # is D2Q9
                    offset_for_D2Q9 = 4
                    neighbor_keys = LatticePointD2Q9_abstract_Module.LatticePointD2Q9_abstract.neighbor_keys
                else:  # is D2Q5
                    offset_for_D2Q9 = 0
                    neighbor_keys = LatticePointD2Q5_abstract_Module.LatticePointD2Q5_abstract.neighbor_keys
                str_entries = lines.split()
                tmp_distance_to_boundary = dict()
                tmp_boundary_name = dict()
                tmp_boundary_closest_points = dict()
                tmp_boundary_normals = dict()

                pt_id = int(str_entries[0])
                for i in range(0, 4 + offset_for_D2Q9):
                    if int(str_entries[4 + i]) != -1:
                        self.Points[pt_id].Neighbors[neighbor_keys[i]] = self.Points[
                            int(str_entries[4 + i])
                        ]
                    else:
                        tmp_distance_to_boundary[neighbor_keys[i]] = -1

                # boundary points require additional information
                i = 0
                for key in neighbor_keys:
                    if key in tmp_distance_to_boundary.keys():
                        tmp_boundary_name[key] = int(
                            str_entries[8 + offset_for_D2Q9 + i]
                        )
                        tmp_distance_to_boundary[key] = (
                            float(
                                str_entries[
                                    8 + offset_for_D2Q9 + str_entries.count("-1") + i
                                ]
                            )
                            / param_L
                        )  # normalisiert fuer Gradientenvorgabe
                        tmp_boundary_closest_points[key] = (
                            Vector_Module.Vector_With_Displacement(
                                x=float(
                                    str_entries[
                                        8
                                        + offset_for_D2Q9
                                        + 2 * str_entries.count("-1")
                                        + 2 * i
                                    ]
                                )
                                / param_L,
                                y=float(
                                    str_entries[
                                        8
                                        + offset_for_D2Q9
                                        + 2 * str_entries.count("-1")
                                        + 2 * i
                                        + 1
                                    ]
                                )
                                / param_L,
                                z=0.0 / param_L,
                                dim_of_w=self.DimOfW,
                            )
                        )
                        if self.AuxPointsSwitch:
                            if not (
                                tmp_boundary_closest_points[key] == self.Points[pt_id]
                            ):  # points are identical
                                self.AuxPoints.append(tmp_boundary_closest_points[key])
                        # tmp_boundary_closest_points[key].w = 0.0
                        tmp_boundary_normals[key] = Vector_Module.Vector(
                            x=float(
                                str_entries[
                                    8
                                    + offset_for_D2Q9
                                    + 4 * str_entries.count("-1")
                                    + 2 * i
                                ]
                            ),
                            y=float(
                                str_entries[
                                    8
                                    + offset_for_D2Q9
                                    + 4 * str_entries.count("-1")
                                    + 2 * i
                                    + 1
                                ]
                            ),
                            z=0.0,
                        )
                        i += 1
                self.Points[pt_id].BoundaryName = tmp_boundary_name
                self.Points[pt_id].DistanceToBoundary = tmp_distance_to_boundary
                self.Points[pt_id].BoundaryClosestPoints = tmp_boundary_closest_points
                self.Points[pt_id].BoundaryNormals = tmp_boundary_normals

                # for multiple bc implementation in same computation
                if tmp_boundary_name:
                    valid_boundary_name = min(tmp_boundary_name.values())
                    bc_type_switch = computation.BoundaryConditions[
                        valid_boundary_name
                    ].ImplementationSwitch
                    self.Points[pt_id].BCImplementationSwitch = bc_type_switch
                    if not self.BCTypes or bc_type_switch not in self.BCTypes:
                        self.BCTypes.append(
                            bc_type_switch
                        )  # add current BCType to BCTypes in Lattice

                # append boundary points for matrix implementation to self.BoundaryPointsMatrixBC
                if "-1" in str_entries and (
                    self.Points[pt_id].BCImplementationSwitch == 1
                ):  # or self.Points[pt_id].BCImplementationSwitch == 3):
                    self.BoundaryPointsMatrixBC.append(self.Points[pt_id])
                    self.BoundaryPointsMatrixBC[-1].BoundaryPointListID = (
                        len(self.BoundaryPointsMatrixBC) - 1
                    )

                # assign ID to AuxPoint for use in vtk, vtu files
                if self.AuxPointsSwitch:
                    id_max = self.Points[-1].ID
                    for aux_point in self.AuxPoints:
                        aux_point.ID = id_max + 1
                        id_max = id_max + 1

    def read_alt_bc_from_file(self, computation, in_file_path=""):
        """
        For alternative formulation of BC: reads *.bc_alt and initializes point.VolumeSurfaceMeasure (volume of cell and, associated surface measure of cell) for each point in lattice
        :param computation:
        :param in_file_path:
        :return:
        """
        try:
            my_file = open(in_file_path, "r")
        except IOError:
            logging.warning("The file cannot be opened!")

        param_L = computation.Parameters["L"]

        my_line = my_file.readline()  # read header info
        while "#" in my_line:
            my_current_pos = my_file.tell()
            my_line = my_file.readline()

        # format of *.bc_alt for reference:
        # ID | surface measure to interior x+ y+ x- y- x+y+ x+y- x-y+ x-y- |cell volume | n x [Bnd IDs, surface measure]
        my_file.seek(my_current_pos)
        for my_line in my_file:  # create point objects
            my_strings = my_line.split()  # TODO add separators to file for readability

            tmp_id = int(my_strings[0])
            tmp_volume = float(my_strings[9]) / param_L / param_L
            tmp_surface_measure_inner_boundary_at_point = dict()

            # surface measures of edges
            for idx, key in enumerate(
                ("x+", "y+", "x-", "y-", "x+y+", "x+y-", "x-y+", "x-y-")
            ):
                tmp_surface_measure_inner_boundary_at_point[key] = (
                    float(my_strings[idx + 1]) / param_L
                )
                if (
                    tmp_surface_measure_inner_boundary_at_point[key] == -1
                ):  # invalid, no inner surface
                    tmp_surface_measure_inner_boundary_at_point.pop(key, None)  # delete

            number_of_boundary_elements = int((len(my_strings) - 10) / 2)
            tmp_boundary_ids_at_point = []
            tmp_boundary_surface_measure_at_point = []
            for i in range(0, number_of_boundary_elements):
                tmp_boundary_id = int(my_strings[10 + 2 * i])
                tmp_boundary_ids_at_point.append(tmp_boundary_id)
                tmp_surface_measure = float(my_strings[10 + 2 * i + 1]) / param_L
                tmp_boundary_surface_measure_at_point.append(tmp_surface_measure)
            tmp_volume_boundary_surface_measure_at_point = list(
                [
                    tmp_volume,
                    [tmp_boundary_ids_at_point, tmp_boundary_surface_measure_at_point],
                    tmp_surface_measure_inner_boundary_at_point,
                ]
            )
            self.Points[
                tmp_id
            ].VolumeSurfaceMeasure = tmp_volume_boundary_surface_measure_at_point

        my_file.close()

    def compute_interpolation_quantities_for_precise_aux_point_field(self, computation):
        for lattice_point in self._boun_point_generator_():
            lattice_point.compute_matrix_coefficients_rhs_coefficients_for_boundary_conditions_beta(
                computation=computation
            )

    def allocate_bc_matrix(self, computation):
        tmp_array = [0.0] * len(self.BoundaryPointsMatrixBC)

        self.BCMatrix = [
            None
        ] * self.DimOfW  # a bc matrix for component in each direction
        for k in range(0, self.DimOfW):
            tmp_matrix = []
            for point in self.BoundaryPointsMatrixBC:
                tmp_matrix.append(copy.copy(tmp_array))
            self.BCMatrix[k] = tmp_matrix

        for lattice_point in self.BoundaryPointsMatrixBC:
            lattice_point.compute_matrix_coefficients_rhs_coefficients_for_boundary_conditions_beta(
                computation
            )
            lattice_point.assemble_boundary_matrix_entries_at_point(
                computation, dim_of_w=self.DimOfW
            )

    def allocate_zero_bc_rhs(self):
        self.BCRHS = [None] * self.DimOfW
        for k in range(0, self.DimOfW):
            self.BCRHS[k] = list([0.0] * len(self.BoundaryPointsMatrixBC))  #

    def regularize_all_points(self, params):
        for lattice_point in self.Points:
            lattice_point.regularize_precollision(params)

    def compute_equilibrium_distribution_functions_all_points(self, computation):
        for lattice_point in self.Points:
            lattice_point.compute_equilibrium_distribution(computation)

    def collide_all_points(self, computation):
        for lattice_point in self.Points:
            lattice_point.collide(computation)

    def initialize_default_volumes(self):
        default_volume = self.CellSize**2
        for lattice_point in self.Points:
            if len(lattice_point.VolumeSurfaceMeasure) < 1:
                lattice_point.VolumeSurfaceMeasure = [default_volume, [[], []]]

    def compute_gradient_coeffs_all_points(self, *, order: float = None, low_order_boundary: bool = None):
        for pt in self.Points:
            pt.gradient_coefficients = pt._compute_gradient_coefficients(
                h=self.CellSize, order=order, low_order_boundary=low_order_boundary
            )

    def compute_gradient_all_points(self, computation):
        """Computes gradients for all lattice points and saves them to PPData"""
        for lattice_point in self.Points:
            gradient_of_w = lattice_point.get_gradient_of_w(self)
            # write output
            string0 = "gradient_of_w_comp_"
            for i in range(0, len(lattice_point.w)):
                string = string0 + str(i)
                lattice_point.PPData[string] = gradient_of_w[i]

        for lattice_point in self.Points:
            [uxx, uyy, uxy, uyx] = lattice_point.get_second_order_derivatives_of_w(
                computation.Lattice
            )[0]
            [vxx, vyy, vxy, vyx] = lattice_point.get_second_order_derivatives_of_w(
                computation.Lattice
            )[1]
            lattice_point.PPData["uxx"] = uxx
            lattice_point.PPData["uyy"] = uyy
            lattice_point.PPData["uxy"] = uxy
            lattice_point.PPData["uyx"] = uyx
            lattice_point.PPData["vxx"] = vxx
            lattice_point.PPData["vyy"] = vyy
            lattice_point.PPData["vxy"] = vxy
            lattice_point.PPData["vyx"] = vyx

        if self.AuxPointsSwitch and len(self.AuxPoints) > 0:
            for key in self.Points[0].PPData.keys():
                if "gradient" in key:
                    for lattice_point in self.Points:
                        lattice_point.setPPDataAtAuxpoint(key)

    def compute_grad_and_div_all_points(self, attr="w"):
        """
        computes gradient and divergance of attribute 'attr' for all points
        and saves them to PPData
        """
        # TODO : specify dictionary for output ?
        for lattice_point in self.Points:
            gradient = lattice_point.get_gradient_of_attribute(self, attr=attr)

            # FIXME : not usable with matrices, cannot output to vtk-file
            key = "grad_" + attr
            lattice_point.PPData[key] = gradient

            key = "div_" + attr
            lattice_point.PPData[key] = np.trace(gradient * np.eye(2))

    def compute_sigma_all_points(self, computation):
        for lattice_point in self.Points:
            lattice_point.compute_sigma(computation)

    def compute_stressNstrain_all_points(self, computation):
        """Loops over all points to calculate stress and strain"""
        for lattice_point in self.Points:
            lattice_point.compute_stressNstrain(computation)
        if self.AuxPointsSwitch and len(self.AuxPoints) > 0:
            print("Missing implementation of stress and strain for AuxPoints")

    def correct_psi_phi_f(self, computation):
        """
        corrects rotation dilatation and f to be consistent with fields
        :param computation:
        :return:
        """
        for lattice_point in self.Points:
            lattice_point.correct_psi_phi_f(computation=computation)

        for lattice_point in self.Points:
            lattice_point.compute_error_of_rotation_dilatation(computation=computation)

    def integrate_w_all_points(self, computation):
        if (
            type(computation.Lattice.Points[0])
            is LatticePointD2Q5_NavierEquation_Chopard_Module.LatticePointD2Q5_NavierEquation_Chopard
        ):
            LatticePointD2Q5_NavierEquation_Chopard_Module.LatticePointD2Q5_NavierEquation_Chopard.computation_of_rotation_dilatation_at_all_lattice_points_finished = False

        for pt in self.Points:
            pt.compute_w(computation)

        if (
            type(computation.Lattice.Points[0])
            is LatticePointD2Q5_NavierEquation_Chopard_Module.LatticePointD2Q5_NavierEquation_Chopard
        ):
            for pt in self.Points:  # DEBUG TODO ONLY NAvier FOR POSTPROCESSING
                pt._compute_current_rotation_dilatation_from_fields(
                    computation
                )
                pt.compute_sigma(computation=computation)

        if self.AuxPointsSwitch:  # and not self.AltBCSwitch: # TODO BC key
            for pt in self.Points:
                pt.set_w_at_aux_points_for_bc(
                    computation, dim_of_w=self.DimOfW
                )

    def integrate_w_all_points_explicit(self, computation):
        for lattice_point in self.Points:
            lattice_point.compute_w_explicit(computation)

        # postprocessing only
        self.compute_sigma_all_points(computation=computation)

        for lattice_point in self.Points:
            lattice_point._compute_current_rotation_dilatation_from_fields(computation)

    def stream_all_points(
        self, computation
    ):  # computation argument is only required for local non-mesh conforming bc
        for lattice_point in self.Points:
            lattice_point.stream()

    def compute_w_at_boundary_points(self, computation):
        """
        Computes displacement field for navier equation from macroscopic boundary conditions at boundary points
        once the displacement field is known in the interior
        :param computation:
        :return:
        """
        S = SystemOfEquations_Module.SystemOfEquations(
            2 * len(computation.Lattice.BoundaryPointsMatrixBC)
        )

        for lattice_point in self.BoundaryPointsMatrixBC:
            lattice_point.write_matrix_boundary_problem(S=S, computation=computation)

        for lattice_point in self.BoundaryPointsMatrixBC:
            lattice_point.write_rhs_boundary_problem(S=S, computation=computation)

        w_at_boundary_points = S.solve()

        # set w at boundary points
        for lattice_point in self.BoundaryPointsMatrixBC:
            id_in_boundary_points = lattice_point.BoundaryPointListID
            lattice_point.w[0] = w_at_boundary_points[2 * id_in_boundary_points + 0]
            lattice_point.w[1] = w_at_boundary_points[2 * id_in_boundary_points + 1]

    def boun_all_points(self, computation):
        """
        Applies Boundary conditions to all lattice points.
        :param computation:
        :return:
        """

        def compute_inverse_of_matrix(matrix):
            """computes inverse of a matrix and returns it as a numpy array"""
            return np.linalg.inv(matrix)

        def boun_all_points_matrix(lattice, computation):
            """updates distribution functions to match specified boundary conditions - macroscopic matrix implementation"""

            # compute inverse only once and save it
            if lattice.InverseOfBoundaryPointsMatrix is None:
                lattice.InverseOfBoundaryPointsMatrix = [None] * lattice.DimOfW
                for k in range(0, lattice.DimOfW):
                    lattice.InverseOfBoundaryPointsMatrix[k] = (
                        compute_inverse_of_matrix(lattice.BCMatrix[k])
                    )

            # compute current right-hand-side
            lattice.allocate_zero_bc_rhs()
            for lattice_point in lattice.BoundaryPointsMatrixBC:
                lattice_point.assemble_boundary_rhs_entries_at_point(computation)

            # solve system of equation
            w_vector = []
            for k in range(0, lattice.DimOfW):
                w_vector.append(
                    np.dot(
                        lattice.InverseOfBoundaryPointsMatrix[k],
                        np.array(lattice.BCRHS[k]),
                    )
                )

            # compute missing distribution functions
            w_at_lattice_point = [
                None
            ] * lattice.DimOfW  # all components of future w at lattice point
            for i in range(0, len(lattice.BoundaryPointsMatrixBC)):
                for k in range(0, lattice.DimOfW):
                    w_at_lattice_point[k] = w_vector[k][i]

                lattice_point = lattice.BoundaryPointsMatrixBC[i]

                lattice_point.compute_missing_distribution_functions_according_to_target_value(
                    computation, w_at_lattice_point
                )

        #### Start here
        lattice_type = self.LatticeType
        bc_matrix_available = bool(self.BoundaryPointsMatrixBC)

        if lattice_type in [
            "D2Q5_NavierEquation_Chopard",
            "D2Q9_NavierEquation_Chopard",
        ]:
            for lattice_point in computation.Lattice.Points:
                if lattice_point.BoundaryName:
                    [rot_dil, rot_dil_avg] = (
                        lattice_point.psi_phi_at_boundary_point_from_displacement_field(
                            computation
                        )
                    )
                    lattice_point.rotation_dilatation_avg = rot_dil_avg
                    lattice_point.rotation_dilatation = rot_dil
                    lattice_point.PPData["rotation_dilatation"] = rot_dil
                    lattice_point.PPData["rotation_dilatation_from_fields"] = rot_dil
                    lattice_point.compute_missing_distribution_functions_according_to_target_value_rotation_dilatation(
                        computation, rot_dil_avg
                    )
        elif lattice_type in ["D2Q5_WaveEquation_Chopard", "D2Q9_WaveEquation_Chopard"]:
            for lattice_point in self.Points:
                if (
                    lattice_point.BCImplementationSwitch
                    == BoundaryCondition_Module.BoundaryCondition.BCImplementation.LocalLatticeConforming.value
                ):  # mesh conforming
                    lattice_point.boun(computation)
            if bc_matrix_available:
                boun_all_points_matrix(
                    self, computation
                )  # macroscopic non-mesh conforming
        elif lattice_type in ["D2Q5_WaveEquation_Guangwu"]:
            for lattice_point in self.Points:
                if (
                    lattice_point.BCImplementationSwitch
                    == BoundaryCondition_Module.BoundaryCondition.BCImplementation.LocalLatticeConforming.value
                ):  # mesh conforming
                    lattice_point.boun(computation)
                elif (
                    lattice_point.BCImplementationSwitch
                    == BoundaryCondition_Module.BoundaryCondition.BCImplementation.MesoscopicNonLatticeConforming.value
                ):  # non-mesh conforming local
                    lattice_point.boun_alt(computation)
            if bc_matrix_available:
                boun_all_points_matrix(
                    self, computation
                )  # macroscopic non-mesh conforming
        elif lattice_type in ["D2Q5_HeatEquation", "D2Q5_PhasefieldFracture"]:
            for lattice_point in self.Points:
                if (
                    lattice_point.BCImplementationSwitch
                    == BoundaryCondition_Module.BoundaryCondition.BCImplementation.LocalLatticeConforming.value
                ):  # mesh conforming
                    lattice_point.boun(computation)
        elif "Ansumali" in lattice_type or "MomentChain" in lattice_type:
            # elif lattice_type in ["D2Q9_NavierEquation_Ansumali"]:
            for lattice_point in self._boun_point_generator_():
                lattice_point.boun(computation)
        else:
            raise NotImplementedError("Boundary conditions not implemented!")

    def process_boun_points(
        self,
        computation,
        aux_pts: Iterable[Point],
        boun_pts: Iterable[Point],
        *,
        propagation: bool = True,
    ) -> None:
        """Handles postprocessing of points (from dynamic crack propagation) on the Lattice level.
        This returns lists of new AuxPoints and boundary LatticePoints; the lists can be empty.
        Boundary Points are treated according to their respective BC Implementation.
        """

        boun_pts = set(boun_pts)

        if computation.Flags["precise_key"]:
            for point in boun_pts:
                point.compute_matrix_coefficients_rhs_coefficients_for_boundary_conditions_beta(
                    computation=computation
                )

        if self.AuxPointsSwitch and aux_pts:
            # add auxiliary points along crack and set IDs
            if self.AuxPoints:
                aux_id = self.AuxPoints[-1].ID
            else:
                aux_id = 0
            id_max = max(self.Points[-1].ID, aux_id)
            for pt in aux_pts:
                id_max += 1
                pt.ID = id_max
            self.AuxPoints += aux_pts

        # MESOSCOPIC: select points with mesoscopic implementation
        if "Guangwu" in self.LatticeType:
            cr_boundary = computation.Crackset.boundary
            # TODO : no check for types should be necessary (-> ducktyping)

            # compute relevant cell info for bc
            if propagation:
                """TODO : might not work properly for multiple crack tips:
                    would need handling of meso points for each crack tip seperately
                """

                # update meso boundary measures for previous points
                for pt in self.meso_points_update:
                    compute_cell_volumes_and_areas_at_boundary_points(
                        cellsize=self.CellSize,
                        points=[pt],
                        seedpoint=self.Points[0],
                        boundary=cr_boundary,
                    )
                    assert pt.VolumeSurfaceMeasure[1]

            # set IDs for boundary points with matrix
            meso_boun_pts = [pt for pt in boun_pts if pt.BCImplementationSwitch == 3]
            if meso_boun_pts:
                boun_pts.difference_update(
                    meso_boun_pts
                )  # remove from set of all boun points
                self.meso_points_update = meso_boun_pts  # add meso points to be updated

                # compute relevant cell info for bc
                """ FIXME : length of boundary (i.e. VolumeSurfaceMeasure[1])
                    should be computed here, but is not
                    at least for very first pair of points
                """
                compute_cell_volumes_and_areas_at_boundary_points(
                    cellsize=self.CellSize,
                    points=meso_boun_pts,
                    seedpoint=self.Points[0],
                    boundary=cr_boundary,
                )
            # remove Inverse of Matrix from heap to enforce recomputing
            macro_boun_pts = [pt for pt in boun_pts if pt.BCImplementationSwitch == 1]
            if macro_boun_pts:
                boun_pts.difference_update(macro_boun_pts)

                # set IDs for boundary points with matrix
                if self.BoundaryPointsMatrixBC:
                    bc_pt_id = self.BoundaryPointsMatrixBC[-1].BoundaryPointListID
                else:
                    bc_pt_id = -1
                for pt in macro_boun_pts:
                    bc_pt_id += 1
                    pt.BoundaryPointListID = bc_pt_id

                self.BoundaryPointsMatrixBC += macro_boun_pts
                # remove Inverse of Matrix from heap to enforce recomputing
                self.InverseOfBoundaryPointsMatrix = None
                # get all necessary data (as in Computation initialization)
                self.allocate_bc_matrix(computation)

            # all points should have been removed from list by now
            assert not boun_pts, "Some new Boundary Points have not been treated."

    def read_initial_field_data(self, in_file_path=""):
        if not in_file_path.exists():
            logging.info("No initial conditions file found.")
            return
        
        with open(in_file_path, "r") as my_file:
            my_line = my_file.readline()  # read header info
            while "#" in my_line:
                # do sth. with header info
                my_current_pos = my_file.tell()
                my_line = my_file.readline()

            my_file.seek(my_current_pos)

            # determine number of displacement components
            my_strings = my_line.split()
            no_of_displacement_components = int((len(my_strings) - 1) / 2)

            tmp_array = [None] * no_of_displacement_components

            self.InitialFieldsWdot = []
            self.InitialFieldsW = []
            for point in self.Points:
                self.InitialFieldsWdot.append(copy.copy(tmp_array))
                self.InitialFieldsW.append(copy.copy(tmp_array))

            for my_line in my_file:
                my_strings = my_line.split()
                for k in range(0, no_of_displacement_components):
                    self.InitialFieldsW[int(my_strings[0])][k] = float(my_strings[1 + k])
                    self.InitialFieldsWdot[int(my_strings[0])][k] = float(
                        my_strings[1 + no_of_displacement_components + k]
                    )

    def initialize_distribution_functions_all_points(self, computation):
        for lattice_point in self.Points:
            lattice_point.initialise_distribution_functions_and_fields(computation)

    def update_rotation_dilatation_after_collision_and_streaming_at_interior_points(
        self, computation
    ):
        for lattice_point in self.Points:
            lattice_point.update_rot_dil_from_distribution_functions(computation)

    def update_distribution_function_after_streaming_all_points(self):
        for lattice_point in self.Points:
            lattice_point.update_distribution_function_after_streaming()

    def write_output_local(
        self,
        computation=None,
    ):
        for f_name in self.RequestedLocalOutPutData.keys():
            self.write_output_local_field_name(
                computation=computation, field_name=f_name
            )

    def write_output_local_field_name(self, computation=None, field_name=""):
        file_name = computation.output_path / f"{computation.Name}_{field_name}.outloc"
        file_exists = os.path.isfile(file_name)
        point_list = self.RequestedLocalOutPutData[field_name]

        if (
            computation.current_time_h < 0.1 * computation.Parameters["dth"]
            or not file_exists
        ):
            # create new *.outloc file
            try:
                my_file = open(file_name, "w")
            except IOError:
                logging.error("*.outloc file cannot be opened")

            # write header for new *.outloc file
            my_file.write("# This file contains local output data \n")
            my_file.write(
                "# time (N x field_value) where N is the number of nodes for which output data of type field_value (field_name) was requested\n"
            )

            my_file.write("# Point IDs:\n")
            for point in point_list:
                my_file.write("# " + str(point.ID) + "\n")
            my_file.write("# Fieldname: " + field_name)
            my_file.write("\n")

            my_file.write("# Point positions:\n")
            for point in point_list:
                my_file.write("# ")
                my_file.write(point.write_position_2_vtk(computation))

        elif file_exists and computation.current_time > 0.0:
            # only append data to existing *.outloc file
            try:
                my_file = open(file_name, "a")
            except IOError:
                logging.error("*.outloc file cannot be opened")
        else:
            logging.error("Something unexpected happened.")
            return

        # write data to file
        my_file.write("{0:16.8e} ".format(computation.current_time))
        if field_name == "w":
            # call method to print w at point
            for point in point_list:
                my_file.write(point.write_data_2_vtk(computation))
            my_file.write("\n")
        else:
            for point in point_list:
                my_file.write(point.write_PPData_2_vtk(key=field_name))
            my_file.write("\n")

        my_file.close()

    def write_vtk(self, computation):
        # output of w_dot needs flag, because it is not overall compatible
        if computation.Flags["output_w_dot"]:
            for point in self.Points:
                point.PPData["w_dot"] = point.wdot

        if computation.Flags["verbose"]:
            print(
                "Writing VTK file number: {0:4d} -- current simulation time: {1:7.4f} of {2:4.2f}".format(
                    computation.vtk_counter,
                    computation.current_time,
                    computation.Parameters["max_time"],
                ),
                end="\r",
            )
        computation.vtk_counter += 1

        file_name = computation.vtk_path / f"{computation.Name}.vtk.{computation.vtk_counter:06d}"
        with open(file_name, "w") as vtk_file:
            vtk_file.write("# vtk DataFile Version 2.0 \n")
            vtk_file.write(
                "generated by Lattice Boltzmann Method time_h = {0:16.8e} time = {1:16.8e}\n".format(
                    computation.current_time_h, computation.current_time
                )
            )
            vtk_file.write("ASCII \n")
            vtk_file.write("DATASET UNSTRUCTURED_GRID\n")
            vtk_file.write("\n")

            point_number = len(self.Points)
            if self.AuxPointsSwitch:
                point_number += len(self.AuxPoints)

            tmp_point = self.Points[0]
            fields_in_PPData_number = len(tmp_point.PPData.keys())

            vtk_file.write("POINTS {0:6d} FLOAT \n".format(point_number))
            for lattice_point in self.Points:
                vtk_file.write(lattice_point.write_position_2_vtk(computation))
            if self.AuxPointsSwitch:
                for aux_point in self.AuxPoints:
                    vtk_file.write(aux_point.write_position_2_vtk(computation))
                vtk_file.write("\n")

            vtk_file.write(
                "CELLS {0:6d} {1:6d}\n".format(point_number, 2 * point_number)
            )
            for lattice_point in self.Points:
                vtk_file.write(lattice_point.write_cell_connectivity_2_vtk())
            if self.AuxPointsSwitch:
                for aux_point in self.AuxPoints:
                    vtk_file.write(aux_point.write_cell_connectivity_2_vtk())
                vtk_file.write("\n")

            vtk_file.write("CELL_TYPES {0:6d}\n".format(point_number))
            for lattice_point in self.Points:
                vtk_file.write(lattice_point.write_cell_type_2_vtk())
            if self.AuxPointsSwitch:
                for aux_point in self.AuxPoints:
                    vtk_file.write(aux_point.write_cell_type_2_vtk())
                vtk_file.write("\n")

            vtk_file.write("POINT_DATA {0:6d}\n".format(point_number))
            vtk_file.write(
                "FIELD solution {0:1d}\n".format(fields_in_PPData_number + 1)
            )
            vtk_file.write(
                "dispw {0:1d} {1:6d} DOUBLE\n".format(self.DimOfW, point_number)
            )
            for lattice_point in self.Points:
                vtk_file.write(lattice_point.write_data_2_vtk(computation))
                vtk_file.write("\n")
            if self.AuxPointsSwitch:
                for aux_point in self.AuxPoints:
                    vtk_file.write(aux_point.write_data_2_vtk(computation))
                    vtk_file.write("\n")
                vtk_file.write("\n")

            for key in tmp_point.PPData.keys():
                if hasattr(tmp_point.PPData[key], "__len__"):
                    tmp_len = str(int(len(tmp_point.PPData[key])))
                else:
                    tmp_len = str(1)

                vtk_file.write(
                    key + " " + tmp_len + " {0:6d} DOUBLE\n".format(point_number)
                )
                for lattice_point in self.Points:
                    vtk_file.write(lattice_point.write_PPData_2_vtk(key))
                if self.AuxPointsSwitch:
                    for aux_point in self.AuxPoints:
                        vtk_file.write(aux_point.write_PPData_2_vtk(key))
                    vtk_file.write("\n")

    def write_vtu(self, computation):
        """writes the current data to a vtu file and added an entery in the corresponding pvd file/generates a corresponding pvd file"""
        k = int(round(computation.current_time_h / computation.Parameters["dth"]))
        file_name = computation.vtk_path / f"{computation.Name}_{k:06d}.vtu"
        if k == 0:
            with open(
                computation.vtk_path / f"{computation.Name}.pvd", "w",
            ) as pvdfile:
                pvdfile.writelines(
                    [
                        '<?xml version="1.0"?>\n',
                        '<VTKFile type="Collection" version="0.1">\n',
                        "<Collection>\n",
                    ]
                )
                pvdfile.write(
                    '<DataSet timestep="{:16.8f}" group="" part="0" file="'.format(
                        computation.current_time
                    )
                    + computation.Name
                    + "_{:06d}".format(k)
                    + ".vtu"
                    + '"/>\n'
                )
                pvdfile.writelines(["</Collection>\n", "</VTKFile>\n"])
                pvdfile.close()
        else:
            with open(
                computation.vtk_path / f"{computation.Name}.pvd", "r+",
            ) as pvdfile:
                if sys.version_info[0] < 3:
                    pvdfile.seek(-25, 2)
                else:
                    pvdfile.seek(0, os.SEEK_END)
                    pvdfile.seek(pvdfile.tell() - 25, os.SEEK_SET)
                pvdfile.write(
                    '<DataSet timestep="{:16.8f}" group="" part="0" file="'.format(
                        computation.current_time
                    )
                    + computation.Name
                    + "_{:06d}".format(k)
                    + ".vtu"
                    + '"/>\n'
                )
                pvdfile.writelines(["</Collection>\n", "</VTKFile>\n"])
                pvdfile.close()

        with open(file_name, "w") as my_file:
            my_file.write('<?xml version="1.0"?>\n')
            my_file.write('<VTKFile type="UnstructuredGrid" version="0.1">\n')
            my_file.write("<UnstructuredGrid>\n")
            my_file.write(
                '<Piece NumberOfPoints="'
                + str(len(self.Points))
                + '" NumberOfCells="'
                + str(len(self.vtuCells))
                + '">\n'
            )
            my_file.write("<Points>\n")
            my_file.write(
                '<DataArray type="Float64" Name="coordinates" NumberOfComponents="3" format="ascii">\n'
            )
            for pkt in self.Points:
                my_file.write(pkt.write_position_2_vtu(computation))
            my_file.write("</DataArray>\n")
            my_file.write("</Points>\n")
            my_file.write("<Cells>\n")
            my_file.write(
                '<DataArray type="Int32" Name="connectivity" NumberOfComponents="1" format="ascii">\n'
            )
            for cell in self.vtuCells:
                my_file.write("{c[0]} {c[1]} {c[2]} {c[3]} ".format(c=cell))
            my_file.write("\n</DataArray>\n")
            my_file.write(
                '<DataArray type="Int32" Name="offsets" NumberOfComponents="1" format="ascii">\n'
            )
            if sys.version_info[0] < 3:
                for i in range(1, len(self.vtuCells) + 1):
                    my_file.write(str(i * 4) + " ")
            else:
                for i in range(1, len(self.vtuCells) + 1):
                    my_file.write(str(i * 4) + " ")
            my_file.write("\n</DataArray>\n")
            my_file.write(
                '<DataArray type="UInt8" Name="types" NumberOfComponents="1" format="ascii">\n'
            )
            for cell in self.vtuCells:
                my_file.write("9 ")
            my_file.write("\n</DataArray>\n")
            my_file.write("</Cells>\n")
            my_file.write("<PointData>\n")
            my_file.write(
                '<DataArray type="Float64" Name="dispw" NumberOfComponents="1" format="ascii">\n'
            )
            for pkt in self.Points:
                my_file.write(pkt.write_data_2_vtu(computation))
            my_file.write("</DataArray>\n")
            my_file.write(
                '<DataArray type="Float64" Name="dispwdot" NumberOfComponents="1" format="ascii">\n'
            )
            for pkt in self.Points:
                my_file.write(pkt.write_wdot_2_vtu(computation))
            my_file.write("</DataArray>\n")
            for k in self.Points[0].PPData.keys():
                if hasattr(self.Points[0].PPData[k], "__len__"):
                    tmp_len = str(len(self.Points[0].PPData[k]))  # can deal with arrays
                else:
                    tmp_len = str(1)
                my_file.write(
                    '<DataArray type="Float64" Name="'
                    + k
                    + '" NumberOfComponents="'
                    + tmp_len
                    + '" format="ascii">\n'
                )
                for pkt in self.Points:
                    my_file.write(pkt.write_PPData_2_vtk(k))
                my_file.write("</DataArray>")
            my_file.write("</PointData>\n")
            my_file.write("</Piece>")
            my_file.write("</UnstructuredGrid>\n")
            my_file.write("</VTKFile>\n")
            my_file.close()

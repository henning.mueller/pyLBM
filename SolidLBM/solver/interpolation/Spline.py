import numpy as np
import SolidLBM.solver.solution.SystemOfEquations as SystemOfEquations_Module
import SolidLBM.solver.solution.Matrix as Matrix_Module
from bisect import bisect_right
from bisect import bisect_left


class Spline:
    """ """

    def __init__(self, x):
        """
        :param x:
        """
        self.x = x  # dimension 0 ....  n
        self.h = list()  # dimensions 0 .... n-1
        self.n = len(x) - 1  # n+1 = len(x)

        self.H = None
        self.Cg = None
        self.Cb1 = None
        self.Cb2 = None
        self.Cd = None

        self.g = None
        self.a = None
        self.c = None
        self.b = None

        self.CoefficientsOfFirstOrderDerivatives = None

        self._compute_h()
        self._compute_H()
        self._compute_Cb1()
        self._compute_Cb2()
        self._compute_Cg()
        self._compute_Cd()
        self.CoefficientsOfFirstOrderDerivatives = self._compute_coefficient_matrix_for_first_order_derivatives()
        self.CoefficientsOfSecondOrderDerivatives = self._compute_coefficient_matrix_for_second_order_derivatives()  #

    def _compute_H(self):
        x = self.x
        H = Matrix_Module.Matrix(self.n + 1, self.n + 1)
        for i in range(2, self.n + 1 - 1 - 1):
            H.write_A(i, i - 1, self.h[i - 1])
            H.write_A(i, i, 2.0 * (self.h[i - 1] + self.h[i]))
            H.write_A(i, i + 1, self.h[i])
        H.write_A(0, 0, 1.0)
        H.write_A(self.n, self.n, 1.0)
        H.write_A(1, 1, 2.0 * (self.h[0] + self.h[1]))
        H.write_A(1, 2, self.h[1])
        H.write_A(self.n - 1, self.n - 2, self.h[self.n - 2])
        H.write_A(self.n - 1, self.n - 1, 2.0 * (self.h[self.n - 1] + self.h[self.n - 2]))
        self.H = H

    def _compute_Cg(self):
        Cg = Matrix_Module.Matrix(self.n + 1, self.n + 1)
        for i in range(1, self.n):
            Cg.write_A(i, i - 1, 3.0 / self.h[i - 1])
            Cg.write_A(i, i, -3.0 * (1.0 / self.h[i] + 1.0 / self.h[i - 1]))
            Cg.write_A(i, i + 1, 3.0 * 1.0 / self.h[i])
        self.Cg = Cg

    def _compute_Cb1(self):
        Cb1 = Matrix_Module.Matrix(self.n, self.n + 1)
        for i in range(0, self.n):
            Cb1.write_A(i, i, -1.0 / self.h[i])
            Cb1.write_A(i, i + 1, 1.0 / self.h[i])
        self.Cb1 = Cb1

    def _compute_Cb2(self):
        Cb2 = Matrix_Module.Matrix(self.n, self.n + 1)
        for i in range(0, self.n):
            Cb2.write_A(i, i, -self.h[i] * 2.0 / 3.0)
            Cb2.write_A(i, i + 1, -self.h[i] * 1.0 / 3.0)
        self.Cb2 = Cb2

    def _compute_Cd(self):
        Cd = Matrix_Module.Matrix(self.n, self.n + 1)
        for i in range(0, self.n):
            Cd.write_A(i, i, -1.0 / self.h[i] / 3.0)
            Cd.write_A(i, i + 1, 1.0 / self.h[i] / 3.0)
        self.Cd = Cd

    def _compute_h(self):
        self.h = list()
        for i in range(1, len(self.x)):
            self.h.append(self.x[i] - self.x[i - 1])

    def _compute_g(self, y):
        res = self.Cg.multiply_right(y)
        self.g = res
        return res

    def compute_spline_coefficients(self, y):
        self._compute_g(y)
        self._compute_a(y)
        self._compute_c()
        self._compute_b(y)
        self._compute_d()

    def _compute_d(self):
        res = self.Cd.multiply_right(self.c)
        self.d = res
        return res

    def _compute_c(self):
        Hm1 = self.H.inverse_of_A()
        Hm1 = Matrix_Module.Matrix._list_to_Matrix(Hm1)
        res = Hm1.multiply_right(self.g)
        self.c = res
        return res

    def _compute_b(self, y):
        res1 = self.Cb1.multiply_right(y)
        res2 = self.Cb2.multiply_right(self.c)

        res = list()
        for i in range(0, len(res1)):
            res.append(res1[i] + res2[i])
        self.b = res
        return res

    def _compute_a(self, y):
        self.a = y

    def _compute_coefficient_matrix_for_first_order_derivatives(self):
        Hm1 = self.H.inverse_of_A()
        Hm1 = Matrix_Module.Matrix._list_to_Matrix(Hm1)
        Res = Hm1.multiply_right(self.Cg.get_A())
        Res = Matrix_Module.Matrix._list_to_Matrix(Res)
        Res = self.Cb2.multiply_right(Res.get_A())
        Res = Matrix_Module.Matrix._list_to_Matrix(Res)
        Res = Res.add(self.Cb1.get_A())  # returns list
        # self.CoefficientsOfDerivatives = Res

        h = self.x[self.n] - self.x[self.n - 1]
        Cb1np = np.array(self.Cb1.get_A())
        Cb2np = np.array(self.Cb2.get_A())
        Hnp = np.array(self.H.get_A())
        Hnpm1 = np.linalg.inv(Hnp)
        Cgnp = np.array(self.Cg.get_A())
        Cdnp = np.array(self.Cd.get_A())

        # last entry
        tmpMatrix = Hnpm1.dot(Cgnp)
        tmpMatrix = Cb2np.dot(tmpMatrix)
        bMatrix = Cb1np + tmpMatrix

        tmpMatrix = Hnpm1.dot(Cgnp)
        cMatrix = tmpMatrix.dot(2.0 * h)
        cMatrix = np.delete(cMatrix, self.n, 0)

        tmpMatrix = Hnpm1.dot(Cgnp)
        tmpMatrix = Cdnp.dot(tmpMatrix)
        dMatrix = tmpMatrix.dot(3.0 * h**2)

        resMatrix = bMatrix + cMatrix + dMatrix
        resMatrix = resMatrix.tolist()

        Res.append(resMatrix[self.n - 1])
        self.CoefficientsOfFirstOrderDerivatives = Res

        return Res

    def _compute_coefficient_matrix_for_second_order_derivatives(self):
        Hm1 = self.H.inverse_of_A()
        Hm1 = Matrix_Module.Matrix._list_to_Matrix(Hm1)
        Res = Hm1.multiply_right(self.Cg.get_A())
        Res = Matrix_Module.Matrix._list_to_Matrix(Res)
        Res = Res.multiply_left(2.0)
        Res = np.array(Res)
        Res = np.delete(Res, self.n, 0)
        Res = Res.tolist()

        h = self.x[self.n] - self.x[self.n - 1]
        Cdnp = np.array(self.Cd.get_A())
        Cgnp = np.array(self.Cg.get_A())
        Hnp = np.array(self.H.get_A())
        Hnpm1 = np.linalg.inv(Hnp)

        tmpMatrix = Hnpm1.dot(Cgnp)
        cMatrix = tmpMatrix.dot(2.0)
        cMatrix = np.delete(cMatrix, self.n, 0)

        tmpMatrix = Hnpm1.dot(Cgnp)
        tmpMatrix = Cdnp.dot(tmpMatrix)
        dMatrix = tmpMatrix.dot(6.0 * h)

        resMatrix = cMatrix + dMatrix
        resMatrix = resMatrix.tolist()

        Res.append(resMatrix[self.n - 1])

        self.CoefficientsOfSecondOrderDerivatives = Res

        return Res

    def compute_b_from_coefficients_of_derivatives(self, y):
        Coeff = Matrix_Module.Matrix._list_to_Matrix(self.CoefficientsOfFirstOrderDerivatives)

        res = Coeff.multiply_right(y)

        return res

    def get_coefficients_of_first_order_derivatives(self):
        return self.CoefficientsOfFirstOrderDerivatives

    def get_coefficients_of_second_order_derivatives(self):
        return self.CoefficientsOfSecondOrderDerivatives

    def compute_value_of_spline(self, x):
        if self.x[0] > x or self.x[len(self.x) - 1] < x:
            raise Exception("Cannot find matching function!")
        i = self._find_matching_index_of_spline(x)
        value = (
            self.a[i]
            + self.b[i] * (x - self.x[i])
            + self.c[i] * (x - self.x[i]) ** 2
            + self.d[i] * (x - self.x[i]) ** 3
        )
        return value

    def _find_matching_index_of_spline(self, x):
        i = self.binary_search(x) - 1
        if i < 0:
            return 0
        if i > len(self.x) - 1:
            return len(self.x) - 1
        # i = self.binary_search_driver(x)
        return i

    def binary_search(self, x):
        # https: // www.geeksforgeeks.org / binary - search - bisect - in -python /
        # i1 = bisect_right(self.x, x)
        i2 = bisect_left(self.x, x)
        return i2

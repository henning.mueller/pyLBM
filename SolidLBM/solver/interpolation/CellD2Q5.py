import SolidLBM.util.geometry.Vector as Vector_Module

import SolidLBM.solver.interpolation.Box as Box_Module


class CellD2Q5(Box_Module.Box):
    def __init__(self, points=[Vector_Module.Vector(x=None, y=None, z=None)] * 4):
        super(CellD2Q5, self).__init__(points)

    # def interpolate_displacements(self, computation, search_point=None):
    #     if isinstance(self.Points[0], LatticePointD2Q5) or isinstance(self.Points[0], LatticePointD2Q5Chopard):
    #         try:
    #             x_h = (search_point.x - self.Points[0].x) / (self.Points[1].x - self.Points[0].x)
    #             y_h = (search_point.y - self.Points[0].y) / (self.Points[2].y - self.Points[0].y)
    #         except ZeroDivisionError:
    #             x_h = 0.0
    #             y_h = 0.0
    #             print('Class {0} Method interpolate_displacements Error: Points of Cell must be different'.format(
    #                 type(self).__name__))
    #         wi = list()
    #         if type(self.Points[0]) == LatticePointD2Q5:
    #             for i in range(0, 4):
    #                 wi.append(sum(self.Points[i].f_temp) * computation.Parameters['dth'] + self.Points[i].w)
    #         # w0 = sum(self.Points[0].f_temp)*computation.Parameters['dth'] + self.Points[0].w
    #         # w1 = sum(self.Points[1].f_temp)*computation.Parameters['dth'] + self.Points[1].w
    #         # w2 = sum(self.Points[2].f_temp)*computation.Parameters['dth'] + self.Points[2].w
    #         # w3 = sum(self.Points[3].f_temp)*computation.Parameters['dth'] + self.Points[3].w
    #         # wi = list([w0, w1, w2, w3])
    #         elif type(self.Points[0]) == LatticePointD2Q5Chopard:
    #             for i in range(0, 4):
    #                 wi.append(sum(self.Points[0].f_temp))
    #             # w0 = sum(self.Points[0].f_temp)
    #             # w1 = sum(self.Points[1].f_temp)
    #             # w2 = sum(self.Points[2].f_temp)
    #             # w3 = sum(self.Points[3].f_temp)
    #         else:
    #             print('something!')
    #
    #         interpolation_coefficients = [1.0 - x_h - y_h + x_h * y_h, x_h - x_h * y_h, x_h * y_h, y_h - x_h * y_h]
    #         w_out = 0.0
    #         for i in range(0, 4):
    #             w_out = w_out + wi[i] * interpolation_coefficients[i]
    #
    #         ids_boundary_points_in_cell = list()  # IDs of boundary points in cell in Lattice.BoundaryPointList
    #         coeffcients_boundary_points_in_cell = list()  # Interpolation coefficients of boundary points in cell
    #         ids_points_in_cell = list()  # IDs of points in cell in Lattice.Points which are not boundary points
    #         coeffcients_points_in_cell = list()  # Interpolation coefficients of  points in cell which are not boundary points
    #         n = 0
    #         for lattice_point in self.Points:
    #             if lattice_point.BoundaryPointListID != -1:
    #                 ids_boundary_points_in_cell.append(self.Points[n].BoundaryPointListID)
    #                 coeffcients_boundary_points_in_cell.append(interpolation_coefficients[n])
    #             else:
    #                 ids_points_in_cell.append(self.Points[n].ID)
    #                 coeffcients_points_in_cell.append(interpolation_coefficients[n])
    #             n = n + 1
    #
    #         return list([w_out, ids_boundary_points_in_cell, coeffcients_boundary_points_in_cell, ids_points_in_cell, coeffcients_points_in_cell])
    #
    #         # w_out = w0 * (1.0-x_h-y_h+x_h*y_h) + w1 * (x_h-x_h*y_h) + w2 * (x_h*y_h) + w3 * (y_h-x_h*y_h)
    #
    #         # w_out = w0 + (w1-w0) * x_h \
    #         #        + (w3-w0) * y_h \
    #         #       + (w2-(w1 + w3 - w0)) * x_h * y_h
    #     else:
    #         print(
    #         'Class {0} Method interpolate_displacements Error: Interpolation not implemented for Lattice point type!'.format(
    #             type(self).__name__))
    #         return None

    def interpolate_displacements_alt(self, search_point=None):
        # if isinstance(self.Points[0], LatticePointD2Q5) or isinstance(self.Points[0], LatticePointD2Q5Chopard):
        try:
            x_h = (search_point.x - self.Points[0].x) / (self.Points[1].x - self.Points[0].x)
            y_h = (search_point.y - self.Points[0].y) / (self.Points[2].y - self.Points[0].y)
        except ZeroDivisionError:
            x_h = 0.0
            y_h = 0.0
            print(
                "Class {0} Method interpolate_displacements Error: Points of Cell must be different".format(
                    type(self).__name__
                )
            )
        # wi = list()
        # if type(self.Points[0]) == LatticePointD2Q5:
        #     for i in range(0, 4):
        #         wi.append(sum(self.Points[i].f_temp) * computation.Parameters['dth'] + self.Points[i].w)
        # # w0 = sum(self.Points[0].f_temp)*computation.Parameters['dth'] + self.Points[0].w
        # # w1 = sum(self.Points[1].f_temp)*computation.Parameters['dth'] + self.Points[1].w
        # # w2 = sum(self.Points[2].f_temp)*computation.Parameters['dth'] + self.Points[2].w
        # # w3 = sum(self.Points[3].f_temp)*computation.Parameters['dth'] + self.Points[3].w
        # # wi = list([w0, w1, w2, w3])
        # elif type(self.Points[0]) == LatticePointD2Q5Chopard:
        #     for i in range(0, 4):
        #         wi.append(sum(self.Points[0].f_temp))
        #     # w0 = sum(self.Points[0].f_temp)
        #     # w1 = sum(self.Points[1].f_temp)
        #     # w2 = sum(self.Points[2].f_temp)
        #     # w3 = sum(self.Points[3].f_temp)
        # else:
        #     print('something!')

        interpolation_coefficients = [
            1.0 - x_h - y_h + x_h * y_h,
            x_h - x_h * y_h,
            x_h * y_h,
            y_h - x_h * y_h,
        ]
        # w_out = 0.0
        # for i in range(0, 4):
        #     w_out = w_out + wi[i] * interpolation_coefficients[i]

        ids_boundary_points_in_cell = list()  # IDs of boundary points in cell in Lattice.BoundaryPointList
        coeffcients_boundary_points_in_cell = list()  # Interpolation coefficients of boundary points in cell
        ids_points_in_cell = list()  # IDs of points in cell in Lattice.Points which are not boundary points
        coeffcients_points_in_cell = (
            list()
        )  # Interpolation coefficients of  points in cell which are not boundary points
        n = 0
        for lattice_point in self.Points:
            if lattice_point.BoundaryPointListID != -1:
                ids_boundary_points_in_cell.append(self.Points[n].BoundaryPointListID)
                coeffcients_boundary_points_in_cell.append(interpolation_coefficients[n])
            else:
                ids_points_in_cell.append(self.Points[n].ID)
                coeffcients_points_in_cell.append(interpolation_coefficients[n])
            n = n + 1

        return list(
            [
                ids_boundary_points_in_cell,
                coeffcients_boundary_points_in_cell,
                ids_points_in_cell,
                coeffcients_points_in_cell,
            ]
        )

        # w_out = w0 * (1.0-x_h-y_h+x_h*y_h) + w1 * (x_h-x_h*y_h) + w2 * (x_h*y_h) + w3 * (y_h-x_h*y_h)

        # w_out = w0 + (w1-w0) * x_h \
        #        + (w3-w0) * y_h \
        #       + (w2-(w1 + w3 - w0)) * x_h * y_h

    # else:
    #    print(
    #    'Class {0} Method interpolate_displacements Error: Interpolation not implemented for Lattice point type!'.format(
    #        type(self).__name__))
    #    return None

    # def test_interpolate_displacements(self, search_point=None):
    #     for lattice_point in self.Points:
    #         if isinstance(lattice_point, LatticePointD2Q5) or isinstance(lattice_point, LatticePointD2Q5Chopard):
    #             try:
    #                 x_h = (search_point.x-self.Points[0].x)/(self.Points[1].x-self.Points[0].x)
    #                 y_h = (search_point.y-self.Points[0].y)/(self.Points[2].y-self.Points[0].y)
    #             except ZeroDivisionError:
    #                 x_h = 0.0
    #                 y_h = 0.0
    #                 print('Class {0} Method interpolate_displacements Error: Points of Cell must be different'.format(type(self).__name__))
    #             if type(self.Points[0]) == LatticePointD2Q5:
    #                 w0 = self.Points[0].w
    #                 w1 = self.Points[1].w
    #                 w2 = self.Points[2].w
    #                 w3 = self.Points[3].w
    #             else:
    #                 print('something!')
    #
    #             w_out = w0 + (w1-w0) * x_h \
    #                     + (w3-w0) * y_h \
    #                     + (w2-(w1 + w3 - w0)) * x_h * y_h
    #             return w_out
    #         else:
    #             print('Class {0} Method interpolate_displacements Error: Interpolation not implemented for Lattice point type!'.format(
    #                 type(self).__name__))
    #             return None

    def create_cell(self, start_point=None, direction=""):
        # works for D2Q5
        # if type(start_point) == LatticePointD2Q5:
        if direction == "northeast":  # start_point is p0
            all_points_exist = "x+" in start_point.Neighbors.keys() and "y+" in start_point.Neighbors.keys()
            if all_points_exist:
                all_points_exist = all_points_exist and "y+" in start_point.Neighbors["x+"].Neighbors.keys()
            if all_points_exist:
                p0 = start_point
                p1 = start_point.Neighbors["x+"]
                p2 = start_point.Neighbors["x+"].Neighbors["y+"]
                p3 = start_point.Neighbors["y+"]
                self.Points = list([p0, p1, p2, p3])
        elif direction == "northwest":  # start_point is p1
            all_points_exist = "x-" in start_point.Neighbors.keys() and "y+" in start_point.Neighbors.keys()
            if all_points_exist:
                all_points_exist = all_points_exist and "y+" in start_point.Neighbors["x-"].Neighbors.keys()
            if all_points_exist:
                p0 = start_point.Neighbors["x-"]
                p1 = start_point
                p2 = start_point.Neighbors["y+"]
                p3 = start_point.Neighbors["x-"].Neighbors["y+"]
                self.Points = list([p0, p1, p2, p3])
        elif direction == "southwest":  # start_point is p2
            all_points_exist = "x-" in start_point.Neighbors.keys() and "y-" in start_point.Neighbors.keys()
            if all_points_exist:
                all_points_exist = all_points_exist and "y-" in start_point.Neighbors["x-"].Neighbors.keys()
            if all_points_exist:
                p0 = start_point.Neighbors["x-"].Neighbors["y-"]
                p1 = start_point.Neighbors["y-"]
                p2 = start_point
                p3 = start_point.Neighbors["x-"]
                self.Points = list([p0, p1, p2, p3])
        elif direction == "southeast":  # start_point is p3
            all_points_exist = "x+" in start_point.Neighbors.keys() and "y-" in start_point.Neighbors.keys()
            if all_points_exist:
                all_points_exist = all_points_exist and "y-" in start_point.Neighbors["x+"].Neighbors.keys()
            if all_points_exist:
                p0 = start_point.Neighbors["y-"]
                p1 = start_point.Neighbors["x+"].Neighbors["y-"]
                p2 = start_point.Neighbors["x+"]
                p3 = start_point
                self.Points = list([p0, p1, p2, p3])

import numpy as np
import SolidLBM.solver.solution.SystemOfEquations as SystemOfEquations_Module
import SolidLBM.solver.solution.LatticePointD2Q5_abstract as LatticePointD2Q5_abstract_Module
import SolidLBM.solver.solution.LatticePointD2Q5_NavierEquation_Chopard_experimental as LatticePointD2Q5_Navier
import SolidLBM.solver.interpolation.Spline as Spline_Modul


class SplineInterpolation:
    @staticmethod
    def compute_dx_dy_at_every_point_of_lattice(bottom_left_corner_point):
        """
        computes derivative coefficient for rectangular grid for spline interpolation
        sets dictionaries in Boundary Data
        :param bottom_left_corner_point:
        :return:
        """
        bottom_edge_points = list()
        left_edge_points = list()
        if isinstance(
            bottom_left_corner_point,
            LatticePointD2Q5_abstract_Module.LatticePointD2Q5_abstract,
        ):
            tmp_point = bottom_left_corner_point
            bottom_edge_points.append(tmp_point)
            while "x+" in tmp_point.Neighbors.keys():
                tmp_point = tmp_point.Neighbors["x+"]
                bottom_edge_points.append(tmp_point)

            tmp_point = bottom_left_corner_point
            left_edge_points.append(tmp_point)
            while "y+" in tmp_point.Neighbors.keys():
                tmp_point = tmp_point.Neighbors["y+"]
                left_edge_points.append(tmp_point)

            # build mesh
            for starting_point in bottom_edge_points:
                # create splines in y direction
                points = list()
                y_of_points = list()
                is_boundary_point_of_points = list()
                id_of_points = list()

                tmp_point = starting_point
                points.append(tmp_point)
                y_of_points.append(tmp_point.y)
                id_of_points.append(tmp_point.ID)
                if tmp_point.BoundaryName:
                    is_boundary_point_of_points.append(1)
                else:
                    is_boundary_point_of_points.append(0)
                while "y+" in tmp_point.Neighbors.keys():
                    tmp_point = tmp_point.Neighbors["y+"]
                    points.append(tmp_point)
                    y_of_points.append(tmp_point.y)
                    id_of_points.append(tmp_point.ID)
                    if tmp_point.BoundaryName:
                        is_boundary_point_of_points.append(1)
                    else:
                        is_boundary_point_of_points.append(0)

                s = Spline_Modul.Spline(y_of_points)
                splineCoefficientOfFirstOrderDerivatives = s.get_coefficients_of_first_order_derivatives()
                splineCoefficientOfSecondOrderDerivatives = s.get_coefficients_of_second_order_derivatives()

                for i in range(
                    0, len(splineCoefficientOfFirstOrderDerivatives)
                ):  # derivative coefficient of point in row
                    dy = dict()
                    dy_boun = dict()
                    dy_nonboun = dict()
                    for j in range(0, len(splineCoefficientOfFirstOrderDerivatives[i])):
                        dy[id_of_points[j]] = splineCoefficientOfFirstOrderDerivatives[i][j]
                        if is_boundary_point_of_points[j]:
                            dy_boun[id_of_points[j]] = splineCoefficientOfFirstOrderDerivatives[i][j]
                        else:
                            dy_nonboun[id_of_points[j]] = splineCoefficientOfFirstOrderDerivatives[i][j]
                    points[i].BoundaryDataBalanceOfMomentum.set_dx_index(dy, 1)
                    points[i].BoundaryDataBalanceOfMomentum.set_dx_nonboun_index(dy_nonboun, 1)
                    points[i].BoundaryDataBalanceOfMomentum.set_dx_boun_index(dy_boun, 1)

                for i in range(
                    0, len(splineCoefficientOfSecondOrderDerivatives)
                ):  # derivative coefficient of point in row
                    dyy = dict()
                    dyy_boun = dict()
                    dyy_nonboun = dict()
                    for j in range(0, len(splineCoefficientOfSecondOrderDerivatives[i])):
                        dyy[id_of_points[j]] = splineCoefficientOfSecondOrderDerivatives[i][j]
                        if is_boundary_point_of_points[j]:
                            dyy_boun[id_of_points[j]] = splineCoefficientOfSecondOrderDerivatives[i][j]
                        else:
                            dyy_nonboun[id_of_points[j]] = splineCoefficientOfSecondOrderDerivatives[i][j]
                    points[i].BoundaryDataBalanceOfMomentum.set_dx_index(dyy, 3)
                    points[i].BoundaryDataBalanceOfMomentum.set_dx_nonboun_index(dyy_nonboun, 3)
                    points[i].BoundaryDataBalanceOfMomentum.set_dx_boun_index(dyy_boun, 3)

            # create splines in x direction
            # build mesh
            for starting_point in left_edge_points:
                points = list()
                x_of_points = list()
                is_boundary_point_of_points = list()
                id_of_points = list()

                tmp_point = starting_point
                points.append(tmp_point)
                x_of_points.append(tmp_point.x)
                id_of_points.append(tmp_point.ID)
                if tmp_point.BoundaryName:
                    is_boundary_point_of_points.append(1)
                else:
                    is_boundary_point_of_points.append(0)
                while "x+" in tmp_point.Neighbors.keys():
                    tmp_point = tmp_point.Neighbors["x+"]
                    points.append(tmp_point)
                    x_of_points.append(tmp_point.x)
                    id_of_points.append(tmp_point.ID)
                    if tmp_point.BoundaryName:
                        is_boundary_point_of_points.append(1)
                    else:
                        is_boundary_point_of_points.append(0)

                s = Spline_Modul.Spline(x_of_points)
                splineCoefficientOfFirstOrderDerivatives = s.get_coefficients_of_first_order_derivatives()
                splineCoefficientOfSecondOrderDerivatives = s.get_coefficients_of_second_order_derivatives()

                for i in range(
                    0, len(splineCoefficientOfFirstOrderDerivatives)
                ):  # derivative coefficient of point in row
                    dx = dict()
                    dx_boun = dict()
                    dx_nonboun = dict()
                    for j in range(0, len(splineCoefficientOfFirstOrderDerivatives[i])):
                        dx[id_of_points[j]] = splineCoefficientOfFirstOrderDerivatives[i][j]
                        if is_boundary_point_of_points[j]:
                            dx_boun[id_of_points[j]] = splineCoefficientOfFirstOrderDerivatives[i][j]
                        else:
                            dx_nonboun[id_of_points[j]] = splineCoefficientOfFirstOrderDerivatives[i][j]
                    points[i].BoundaryDataBalanceOfMomentum.set_dx_index(dx, 0)
                    points[i].BoundaryDataBalanceOfMomentum.set_dx_nonboun_index(dx_nonboun, 0)
                    points[i].BoundaryDataBalanceOfMomentum.set_dx_boun_index(dx_boun, 0)

                for i in range(
                    0, len(splineCoefficientOfSecondOrderDerivatives)
                ):  # derivative coefficient of point in row
                    dxx = dict()
                    dxx_boun = dict()
                    dxx_nonboun = dict()
                    for j in range(0, len(splineCoefficientOfSecondOrderDerivatives[i])):
                        dxx[id_of_points[j]] = splineCoefficientOfSecondOrderDerivatives[i][j]
                        if is_boundary_point_of_points[j]:
                            dxx_boun[id_of_points[j]] = splineCoefficientOfSecondOrderDerivatives[i][j]
                        else:
                            dxx_nonboun[id_of_points[j]] = splineCoefficientOfSecondOrderDerivatives[i][j]
                    points[i].BoundaryDataBalanceOfMomentum.set_dx_index(dxx, 2)
                    points[i].BoundaryDataBalanceOfMomentum.set_dx_nonboun_index(dxx_nonboun, 2)
                    points[i].BoundaryDataBalanceOfMomentum.set_dx_boun_index(dxx_boun, 2)

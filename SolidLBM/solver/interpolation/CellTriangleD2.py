import SolidLBM.util.geometry.Vector as Vector_Module
import SolidLBM.util.geometry.Triangle as Triangle_Module


class CellTriangleD2(Triangle_Module.Triangle):
    def __init__(self, points=[Vector_Module.Vector(x=None, y=None, z=None)] * 3):
        super(CellTriangleD2, self).__init__(points)

    def interpolate_displacements_alt(self, search_point=None):
        # works for D2Q5
        # like triangle linear fem elements
        for lattice_point in self.Points:
            # if isinstance(lattice_point, LatticePointD2Q5) or isinstance(lattice_point, LatticePointD2Q5Chopard):
            # if self.Points[0].BoundaryPointListID ==17:
            #   print('hi')

            x1 = self.Points[0].x
            y1 = self.Points[0].y
            x2 = self.Points[1].x
            y2 = self.Points[1].y
            x3 = self.Points[2].x
            y3 = self.Points[2].y

            x = search_point.x
            y = search_point.y

            b0 = list([x2 * y3 - x3 * y2, x3 * y1 - x1 * y3, x1 * y2 - x2 * y1])
            b1 = list([y2 - y3, y3 - y1, y1 - y2])
            b2 = list([x3 - x2, x1 - x3, x2 - x1])
            area = 0.5 * (x1 * (y2 - y3) + x2 * (y3 - y1) + x3 * (y1 - y2))

            if area != 0.0:
                n1 = 1.0 / 2.0 / area * (b0[0] + b1[0] * x + b2[0] * y)
                n2 = 1.0 / 2.0 / area * (b0[1] + b1[1] * x + b2[1] * y)
                n3 = 1.0 / 2.0 / area * (b0[2] + b1[2] * x + b2[2] * y)
                interpolation_coefficients = list([n1, n2, n3])
            else:
                print("Class {0} Method interpolate_displacements Error: Cell area is zero".format(type(self).__name__))

            ids_boundary_points_in_cell = list()  # IDs of boundary points in cell in Lattice.BoundaryPointList
            coeffcients_boundary_points_in_cell = list()  # Interpolation coefficients of boundary points in cell
            ids_points_in_cell = list()  # IDs of points in cell in Lattice.Points which are not boundary points
            coeffcients_points_in_cell = (
                list()
            )  # Interpolation coefficients of  points in cell which are not boundary points
            n = 0
            for lattice_point in self.Points:
                if lattice_point.BoundaryPointListID != -1:
                    ids_boundary_points_in_cell.append(self.Points[n].BoundaryPointListID)
                    coeffcients_boundary_points_in_cell.append(interpolation_coefficients[n])
                else:
                    ids_points_in_cell.append(self.Points[n].ID)
                    coeffcients_points_in_cell.append(interpolation_coefficients[n])
                n = n + 1

            return list(
                [
                    ids_boundary_points_in_cell,
                    coeffcients_boundary_points_in_cell,
                    ids_points_in_cell,
                    coeffcients_points_in_cell,
                ]
            )

    def create_cell(self, start_point=None, direction=""):
        # works for D2Q5
        # if type(start_point) == LatticePointD2Q5 or type(start_point) == LatticePointD2Q5Chopard:

        if direction == "northeast":
            all_points_exist = "x+" in start_point.Neighbors.keys()
            if all_points_exist:
                all_points_exist = "y+" in start_point.Neighbors.keys()
            if all_points_exist:
                p0 = start_point
                p1 = start_point.Neighbors["x+"]
                p2 = start_point.Neighbors["y+"]
                self.Points = list([p0, p1, p2])
        elif direction == "northeasteast":
            all_points_exist = "x+" in start_point.Neighbors.keys()
            if all_points_exist:
                all_points_exist = "y+" in start_point.Neighbors["x+"].Neighbors.keys()
            if all_points_exist:
                p0 = start_point
                p1 = start_point.Neighbors["x+"]
                p2 = start_point.Neighbors["x+"].Neighbors["y+"]
                self.Points = list([p0, p1, p2])
        elif direction == "northnortheast":
            all_points_exist = "y+" in start_point.Neighbors.keys()
            if all_points_exist:
                all_points_exist = "x+" in start_point.Neighbors["y+"].Neighbors.keys()
            if all_points_exist:
                p0 = start_point
                p1 = start_point.Neighbors["y+"].Neighbors["x+"]
                p2 = start_point.Neighbors["y+"]
                self.Points = list([p0, p1, p2])
        elif direction == "northwest":
            all_points_exist = "x-" in start_point.Neighbors.keys()
            if all_points_exist:
                all_points_exist = "y+" in start_point.Neighbors.keys()
            if all_points_exist:
                p0 = start_point
                p1 = start_point.Neighbors["y+"]
                p2 = start_point.Neighbors["x-"]
                self.Points = list([p0, p1, p2])
        elif direction == "northnorthwest":
            all_points_exist = "y+" in start_point.Neighbors.keys()
            if all_points_exist:
                all_points_exist = "x-" in start_point.Neighbors["y+"].Neighbors.keys()
            if all_points_exist:
                p0 = start_point
                p1 = start_point.Neighbors["y+"]
                p2 = start_point.Neighbors["y+"].Neighbors["x-"]
                self.Points = list([p0, p1, p2])
        elif direction == "northwestwest":
            all_points_exist = "x-" in start_point.Neighbors.keys()
            if all_points_exist:
                all_points_exist = "y+" in start_point.Neighbors["x-"].Neighbors.keys()
            if all_points_exist:
                p0 = start_point
                p1 = start_point.Neighbors["x-"].Neighbors["y+"]
                p2 = start_point.Neighbors["x-"]
                self.Points = list([p0, p1, p2])
        elif direction == "southwest":
            all_points_exist = "x-" in start_point.Neighbors.keys()
            if all_points_exist:
                all_points_exist = "y-" in start_point.Neighbors.keys()
            if all_points_exist:
                p0 = start_point
                p1 = start_point.Neighbors["x-"]
                p2 = start_point.Neighbors["y-"]
                self.Points = list([p0, p1, p2])
        elif direction == "southwestwest":
            all_points_exist = "x-" in start_point.Neighbors.keys()
            if all_points_exist:
                all_points_exist = "y-" in start_point.Neighbors["x-"].Neighbors.keys()
            if all_points_exist:
                p0 = start_point
                p1 = start_point.Neighbors["x-"]
                p2 = start_point.Neighbors["x-"].Neighbors["y-"]
                self.Points = list([p0, p1, p2])
        elif direction == "southsouthwest":
            all_points_exist = "y-" in start_point.Neighbors.keys()
            if all_points_exist:
                all_points_exist = "x-" in start_point.Neighbors["y-"].Neighbors.keys()
            if all_points_exist:
                p0 = start_point
                p1 = start_point.Neighbors["y-"].Neighbors["x-"]
                p2 = start_point.Neighbors["y-"]
                self.Points = list([p0, p1, p2])
        elif direction == "southeast":
            all_points_exist = "x+" in start_point.Neighbors.keys()
            if all_points_exist:
                all_points_exist = "y-" in start_point.Neighbors.keys()
            if all_points_exist:
                p0 = start_point
                p1 = start_point.Neighbors["y-"]
                p2 = start_point.Neighbors["x+"]
                self.Points = list([p0, p1, p2])
        elif direction == "southsoutheast":
            all_points_exist = "y-" in start_point.Neighbors.keys()
            if all_points_exist:
                all_points_exist = "x+" in start_point.Neighbors["y-"].Neighbors.keys()
            if all_points_exist:
                p0 = start_point
                p1 = start_point.Neighbors["y-"]
                p2 = start_point.Neighbors["y-"].Neighbors["x+"]
                self.Points = list([p0, p1, p2])
        elif direction == "southeasteast":
            all_points_exist = "x+" in start_point.Neighbors.keys()
            if all_points_exist:
                all_points_exist = "y-" in start_point.Neighbors["x+"].Neighbors.keys()
            if all_points_exist:
                p0 = start_point
                p1 = start_point.Neighbors["x+"].Neighbors["y-"]
                p2 = start_point.Neighbors["x+"]
                self.Points = list([p0, p1, p2])

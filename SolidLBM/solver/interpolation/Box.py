import SolidLBM.util.geometry.Vector as Vector_Module


class Box(object):
    def __init__(self, points=[Vector_Module.Vector(x=None, y=None, z=None)] * 4):
        data_types = [isinstance(point, Vector_Module.Vector) for point in points]
        if False in data_types:
            print("Class Box Error: Input Vector-objects!")
        else:
            self.Points = points

    def test_if_point_is_in_box(self, test_point=None, tol=0.0):
        if isinstance(test_point, Vector_Module.Vector):
            try:
                x_h = (test_point.x - self.Points[0].x) / (self.Points[1].x - self.Points[0].x)
                y_h = (test_point.y - self.Points[0].y) / (self.Points[2].y - self.Points[0].y)
            except ZeroDivisionError:
                x_h = -1.0
                y_h = -1.0
                print(
                    "Class {0} Method test_if_point_is_in_box Error: Points of Box must be different".format(
                        type(self).__name__
                    )
                )
            if x_h >= 0.0 and x_h <= 1.0 and y_h >= 0.0 and y_h <= 1.0:
                return True
            else:
                return False

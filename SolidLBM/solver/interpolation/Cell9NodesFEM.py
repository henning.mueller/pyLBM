import SolidLBM.util.geometry.Vector as Vector_Module


class Cell9NodesFEM:
    def __init__(self, points):
        """
        A nine node square FEM Lagrange element for interpolation and computation of derivatives,
        nodes ordered in correct order,
        NL FEM Wriggers
        :param leftBottomCornerPoint:
        """
        self.Points = points
        self.centerPoint = self.Points[8]
        self.h = self.Points[1].x - self.Points[0].x
        self.xi = list()
        for i in range(0, 8):
            self.xi[i] = (self.Points[i].x - self.centerPoint.x) / (self.h / 2)
        self.xi[8] = 0

        for i in range(0, 8):
            self.eta[i] = (self.Points[i].y - self.centerPoint.y) / (self.h / 2)
        self.eta[8] = 0

    def get_shape_functions(self, x, y):
        shape_functions = list()
        xi = (x - self.centerPoint.x) / (self.h / 2)
        eta = (y - self.centerPoint.y) / (self.h / 2)

        if self._test_if_point_is_in_box(x, y):
            for i in range(0, 4):
                shape_functions[i] = 0.25 * (xi**2 + self.xi[i] * xi) + (eta**2 + self.eta[i] * eta)
            for i in range(4, 8):
                shape_functions[i] = 0.5 * self.xi[i] ** 2 * (xi**2 - self.xi[i] * xi) * (
                    1.0 - eta**2
                ) + 0.5 * self.eta[i] ** 2 * (eta**2 - self.eta[i] * eta) * (1.0 - xi**2)
            shape_functions[8] = (1.0 - xi**2) * (1.0 - eta**2)

        else:
            raise Exception("Point is not in box")

    def _test_if_point_is_in_box(self, x, y, tol=1.0e-8):
        xi = (x - self.centerPoint.x) / (self.h / 2)
        eta = (y - self.centerPoint.y) / (self.h / 2)

        in_box = (xi < (1.0 + tol)) and (xi > (-1.0 - tol)) and (eta < (1.0 + tol)) and (eta > (-1.0 - tol))
        return in_box
